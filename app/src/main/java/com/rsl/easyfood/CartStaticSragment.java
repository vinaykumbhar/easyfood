package com.rsl.easyfood;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rsl.model.Cartpojo;
import com.rsl.utills.NonScrollListView;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
import static java.lang.Integer.parseInt;


/**
 * A simple {@link Fragment} subclass.
 */
public class CartStaticSragment extends Fragment {
    TextView tableno, tabletext;
    int pritype;
    int FOODID;
    float PRODUCTPRICE;
    //    MyDb myDb;
    SharedPreferences mSharedPreferences;
    ArrayList<Imagepojo> productarray;
    TextView nofgueststext;
    ImageView plus, minus;
    Button changetables, tables, billout;
    TextView noofguests;
    GroupFragment groupFragment;
    Group group;
    int noofchairs;
    String grand;
    Button send;
    String TYPENAME;
    String qty1 = "1";
    PopupTables popupTables;
    com.rsl.easyfood.BillFragment billfragment;
    int occupied;
    ArrayList<Cartpojo> cart;
    static CartStaticSragment CARTStaticSragment;
    SQLiteDatabase db;
    TextView item, qty, each, total;
    double totalvalue;
    TextView totalamount, totaltax, totaltaxvalue, totalamountvalue;
    TextView items, itemno;
    ExpandableListView listView;
    Button takeorder;
    FragmentManager fragmentManager;
    android.app.FragmentTransaction fragmentTransaction;
    //    CartAdapter cartAdapter;
    ExpandableCartAdapter cartAdapter;
    TablesFragment tablesFragment;
    TablesFragment tablesfragment1;
    Dialog tableChnage_popup;
    int occupiedchairs;
    String editTable;
    double TYPEPRICE;
    Boolean billoutFlag = false;

    public boolean checkInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public CartStaticSragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_cart_static_sragment, container, false);
        CARTStaticSragment = this;
        billoutFlag = false;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        tableno = (TextView) view.findViewById(R.id.tableno1);
        plus = (ImageView) view.findViewById(R.id.plus);
        minus = (ImageView) view.findViewById(R.id.minus);
        item = (TextView) view.findViewById(R.id.item);
        qty = (TextView) view.findViewById(R.id.qty);
        each = (TextView) view.findViewById(R.id.each);
        total = (TextView) view.findViewById(R.id.total);
        tabletext = (TextView) view.findViewById(R.id.table1);
        nofgueststext = (TextView) view.findViewById(R.id.noofguests);
        totalamount = (TextView) view.findViewById(R.id.totalamount);
        totaltax = (TextView) view.findViewById(R.id.totaltax);
        totaltaxvalue = (TextView) view.findViewById(R.id.taxvalue);
        totalamountvalue = (TextView) view.findViewById(R.id.totalamountvalue);
        items = (TextView) view.findViewById(R.id.items);
        itemno = (TextView) view.findViewById(R.id.itemno);
        listView = (ExpandableListView) view.findViewById(R.id.list1);
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView arg0, View itemView, int itemPosition, long itemId) {
                listView.expandGroup(itemPosition);
                return true;
            }
        });

        changetables = (Button) view.findViewById(R.id.changetables);
        tables = (Button) view.findViewById(R.id.tables);
        billout = (Button) view.findViewById(R.id.bilout);
        groupFragment = new GroupFragment();
        takeorder = (Button) view.findViewById(R.id.tables);
        send = (Button) view.findViewById(R.id.send);
        tablesFragment = new TablesFragment();
        tablesfragment1 = new TablesFragment();
        popupTables = new PopupTables();
        tableChnage_popup = new Dialog(getActivity());
        fragmentManager = getFragmentManager();
        billfragment = new BillFragment();
        plus.setEnabled(false);
        minus.setEnabled(false);
        noofguests = (TextView) view.findViewById(R.id.quantityamount);
        group = (Group) getActivity();
        cart = new ArrayList<Cartpojo>();
        productarray = new ArrayList<Imagepojo>();
//        myDb = new MyDb(getActivity());
//        myDb.open();
        db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CartStaticSragment.CARTStaticSragment.tableno.getText().toString().equals("")) {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            getActivity(), android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Select table");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                } else {
                    send();
                }
            }
        });
        takeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!groupFragment.isResumed()) {
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container2, groupFragment);
                    fragmentTransaction.commit();
                }
            }
        });
        changetables.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CartStaticSragment.CARTStaticSragment.tableno.getText().length() == 0) {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            getActivity(), android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Select Table");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                } else {
                    popupTables.show(getFragmentManager(), null);
                    listView.setAdapter((BaseExpandableListAdapter) null);
                    totalamountvalue.setText("");
                    totaltax.setText("");
                    itemno.setText("");
                }
            }
        });
        billout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ImageView searchView = (ImageView) getActivity().findViewById(R.id.titlebarSearchview);
                if (CARTStaticSragment.tableno.getText().toString().trim().equals("")) {
                    AlertDialog.Builder msg = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Select table");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                } else {
                    Group.GROUPID.table = CARTStaticSragment.tableno.getText().toString();
                    String noOfGuest = CartStaticSragment.CARTStaticSragment.noofguests.getText().toString();
                    db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                    Cursor cartData = db.rawQuery("SELECT cart_id from cart WHERE tableno=" + Group.GROUPID.table + " AND billflag = 0;", null);
                    int count = cartData.getCount();
                    Log.e("count", "==" + count);
                    if (count > 0) {
                        if (!billfragment.isResumed()) {
                            db.execSQL("UPDATE cart SET guestno = " + noOfGuest + " WHERE tableno = " + Group.GROUPID.table + " AND billflag = 0 ;");
                            db.execSQL("UPDATE tables SET occupiedchairs = " + noOfGuest + " WHERE tableno = " +
                                    Group.GROUPID.table + ";");
                            billoutFlag = true;
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container2, billfragment);
                            searchView.setVisibility(View.INVISIBLE);
                            fragmentTransaction.commit();
                        }
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                getActivity(), android.R.style.Theme_Dialog);
                        msg.setTitle("");
                        msg.setMessage("Cart Is Empty To Bill Out ");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                    db.close();
                }
            }//onClick
        });
        tables.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listView != null) {
                    listView.setAdapter((BaseExpandableListAdapter) null);
                    totalamountvalue.setText("");
                    totaltax.setText("");
                    itemno.setText("");
                    noofguests.setText("0");
                    tableno.setText("");
                    billoutFlag = false;
                    if (!tablesFragment.isResumed()) {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container2, tablesFragment);
                        fragmentTransaction.commit();
                    }
                } else {
                    tableno.setText("");
                    listView.setAdapter((BaseExpandableListAdapter) null);
                    totalamountvalue.setText("");
                    totaltax.setText("");
                    itemno.setText("");
                    noofguests.setText("0");
                    billoutFlag = false;
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container2, tablesFragment);
                    fragmentTransaction.commit();
                    group.alert();
                }
            }
        });

     /*   db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor chairs = db.rawQuery("SELECT tableno,noofchairs,occupiedchairs FROM tables WHERE locationid ="
                + UserLoginActivity.user_LoginID.LOCATIONNO + ";", null);
        int count = chairs.getCount();
        chairs.moveToFirst();
        for (Integer rowCount = 0; rowCount < count; rowCount++) {
            String chair = chairs.getString(chairs.getColumnIndex("noofchairs"));
            String occupiedchairs = chairs.getString(chairs.getColumnIndex("occupiedchairs"));
            noofchairs = Integer.parseInt(chair);
            occupied = Integer.parseInt(occupiedchairs);
            noofguests.setText(String.valueOf(occupied));
            chairs.moveToNext();
        }
        db.close();*/
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int val = parseInt((noofguests.getText().toString()));
                noofchairs = val;
                if (val <= noofchairs && !billoutFlag) {
                    noofchairs = noofchairs + 1;
                    noofguests.setText("" + noofchairs);
                    TablesFragment.COVERS = String.valueOf(noofchairs);
                }
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value = parseInt(noofguests.getText().toString());
                noofchairs = value;
                if (noofchairs != 0 && !billoutFlag) {
                    noofchairs = noofchairs - 1;
                    noofguests.setText("" + noofchairs);
                    TablesFragment.COVERS = String.valueOf(noofchairs);
                }
            }
        });
        return view;
    }

    @SuppressLint("DefaultLocale")
    public void alertadapter(Activity activity) {
        Double grandTotal = Double.valueOf(0);
        if (cart.size() != 0) {
            cart.clear();
        }
//        myDb = new MyDb(getActivity());
//        myDb.open();
        editTable = Group.GROUPID.table;
        db = activity.openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        CartStaticSragment.CARTStaticSragment.billoutFlag = false;
        Cursor cartData = db.rawQuery("SELECT cart_id,menu_item_name,menu_item_price,menu_item_qty,menu_id FROM cart WHERE tableno=" + editTable + " AND billflag=0;", null);
        int count = cartData.getCount();
        if (count > 0) {
            cartData.moveToLast();
            do {
                Cartpojo cartpojo = new Cartpojo();
                String cart_id = cartData.getString(cartData.getColumnIndex("cart_id"));
                String productname = cartData.getString(cartData.getColumnIndex("menu_item_name"));
                String menu_id = cartData.getString(cartData.getColumnIndex("menu_id"));
                Double productprice = cartData.getDouble(cartData.getColumnIndex("menu_item_price"));
                String productqty = cartData.getString(cartData.getColumnIndex("menu_item_qty"));
                Double totax = 0.00;//Double.parseDouble(cartData.getString(cartData.getColumnIndex("totaltax")));
                cartpojo.setSno(cart_id);
                cartpojo.setName(productname);
                cartpojo.setType("");
                cartpojo.setPprice(String.valueOf(productprice));
                cartpojo.setTprice("0");
                cartpojo.setPqty(productqty);
                cartpojo.setTax(String.valueOf(totax));

                Cursor cartMenuitem = db.rawQuery("SELECT id,ing_id,ing_qty,ingredient_name,ing_price FROM ingredients WHERE cart_id_fk=" + cart_id + ";", null);
                int itemcount = cartMenuitem.getCount();
                Double itemtotal = 0.00;

                ArrayList<Cartpojo> item_list = new ArrayList<>();
                if (itemcount > 0) {
                    while (cartMenuitem.moveToNext()) {
                        double ingredients_total = 0.00;
                        String id = cartMenuitem.getString(cartMenuitem.getColumnIndex("id"));
                        String ing_id = cartMenuitem.getString(cartMenuitem.getColumnIndex("ing_id"));
                        int ing_qty = cartMenuitem.getInt(cartMenuitem.getColumnIndex("ing_qty"));
                        String ing_name = cartMenuitem.getString(cartMenuitem.getColumnIndex("ingredient_name"));
                        Double ing_price = cartMenuitem.getDouble(cartMenuitem.getColumnIndex("ing_price"));

                        Cartpojo item = new Cartpojo();
                        item.setSno(ing_id);
                        item.setName(ing_name);

                        item.setPqty(String.valueOf(ing_qty));
                        Cursor subItmes = db.rawQuery("SELECT subitem_id,subitem_qty,subitem_name,subitem_price FROM subitems WHERE ing_id_fk=" + id + ";", null);

                        int subitemcount = subItmes.getCount();
                        Double subitemtotal = 0.00;
                        ArrayList<Cartpojo> sub_item_list = new ArrayList<>();
                        if (subitemcount > 0) {
                            while (subItmes.moveToNext()) {
                                String subitem_id = subItmes.getString(subItmes.getColumnIndex("subitem_id"));
                                int subitem_qty = subItmes.getInt(subItmes.getColumnIndex("subitem_qty"));
                                String subitem_name = subItmes.getString(subItmes.getColumnIndex("subitem_name"));
                                Double subitem_price = subItmes.getDouble(subItmes.getColumnIndex("subitem_price"));
                                Log.e("subitem_name_id", "" + ing_name + "_" + subitem_name + "_" + subitem_id + "_" + subitem_qty);
                                Cartpojo subItem = new Cartpojo();
                                subItem.setSno(subitem_id);
                                subItem.setName(subitem_name);
                                subItem.setPprice(String.valueOf(subitem_price));
                                subItem.setPqty(String.valueOf(subitem_qty));
                                subitemtotal = subitemtotal + (subitem_qty * subitem_price);
                                sub_item_list.add(subItem);
                            }
                        }
                        ingredients_total = (ing_qty * ing_price) + subitemtotal;

                        itemtotal = itemtotal + ingredients_total;
                        item.setItem_list(sub_item_list);
                        item.setTotal(String.valueOf(ingredients_total));
                        item_list.add(item);
                    }
                }
                cartpojo.setItem_list(item_list);
                totalvalue = (productprice * parseInt(productqty)) + itemtotal;
                grandTotal = grandTotal + totalvalue;
                cartpojo.setTotal(String.valueOf(totalvalue));
                cart.add(cartpojo);
            } while (cartData.moveToPrevious());
        }
        db.close();
        grand = String.format("%.2f", grandTotal);
        itemno.setText(String.valueOf(cart.size()));
//        db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
//        Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);
//        int taxDetailsCount = taxDetails.getCount();
        double tax1 = 0;
//        if (taxDetailsCount > 0) {
//            taxDetails.moveToFirst();
//            tax1 = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
//            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
//            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
//            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));
//
//        } else {
//        }
//        db.close();
        double taxAmount = (tax1 * grandTotal) / 100;
        String grand_tax = String.format("%.2f", taxAmount);
        totaltax.setText(getActivity().getString(R.string.pound_symbol) + " " + String.valueOf(grand_tax));
        grandTotal += (tax1 * grandTotal) / 100;
        String grand_Amount = String.format("%.2f", grandTotal);
        totalamountvalue.setText(getString(R.string.pound_symbol) + " " + String.valueOf(grand_Amount));
        Group.GROUPID.finaltotal = grand_Amount;
        if (cart.size() != 0) {
            cartAdapter = new ExpandableCartAdapter(activity);
            listView.setAdapter(cartAdapter);
            cartAdapter.notifyDataSetChanged();
        } else {
//            Toast t = Toast.makeText(getActivity(), "No Data found", Toast.LENGTH_SHORT);
//            t.setGravity(Gravity.CENTER, 0, 0);
//            t.show();
            Log.e("No_data","No Data found");
//            AlertDialog.Builder msg = new AlertDialog.Builder(
//                    getActivity(), android.R.style.Theme_Dialog);
//            msg.setTitle("");
//            msg.setMessage("Table Data has been cleared ");
//            msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
            // AlertDialog.Builder msg = new AlertDialog.Builder(
            // getActivity(), android.R.style.Theme_Dialog);
            //msg.setTitle("");
            //  msg.setMessage("Do you want to clear Cart ?");
//                    msg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            myDb = new MyDb(getActivity());
//                            myDb.open();
//                            myDb.Insert(String.valueOf(MainActivity.MAINID.outletid),CartStaticSragment.CARTStaticSragment.tableno.getText().toString(),"0","NULL","0");
//                            Intent intent = new Intent(getActivity(),ServiceOccupiedChairs.class);
//                            getActivity().startService(intent);
//                            if (!tablesFragment.isResumed()){
//                                fragmentTransaction = fragmentManager.beginTransaction();
//                                fragmentTransaction.replace(R.id.container2,tablesFragment);
//                                fragmentTransaction.commit();}else {
//                                fragmentTransaction = fragmentManager.beginTransaction();
//                                fragmentTransaction.replace(R.id.container2,tablesfragment1);
//                                fragmentTransaction.commit();
//                            }
//                            tableno.setText("");
//                            noofguests.setText("0");
//                        }
//                    });
//                    msg.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    });
            // msg.show();
//                }
//            });
//            msg.show();
        }
    }

    static class ViewHolder {
        TextView productname, producttype;
        TextView qtyvalue, totalvalue;
        ImageView delete;
        LinearLayout layout_subitems;
    }

    public class ExpandableCartAdapter extends BaseExpandableListAdapter {
        private Activity activity;

        public ExpandableCartAdapter(Activity a) {
            activity = a;
        }

        @Override
        public int getGroupCount() {
            return cart.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return cart.get(groupPosition).getItem_list().size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return null;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return cart.get(groupPosition).getItem_list().get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            TextView productname;
            TextView qtyvalue, totalvalue;
            ImageView delete;
            View divider;
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.cartadapter, null);
            }

            if (groupPosition == 0)
                convertView.findViewById(R.id.grpDivider).setVisibility(View.INVISIBLE);
            else
                convertView.findViewById(R.id.grpDivider).setVisibility(View.VISIBLE);

            productname = (TextView) convertView.findViewById(R.id.productname);
            qtyvalue = (TextView) convertView.findViewById(R.id.value);
            totalvalue = (TextView) convertView.findViewById(R.id.Totalvalue1);
            delete = (ImageView) convertView.findViewById(R.id.delete);

            qtyvalue.setText(cart.get(groupPosition).getPqty());
            productname.setText(cart.get(groupPosition).getName());
            String totalcost = String.format("%.2f", Double.parseDouble(cart.get(groupPosition).getTotal()));
            totalvalue.setText(getActivity().getString(R.string.pound_symbol) + " " + totalcost);
            delete.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (billoutFlag) {

                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                getActivity(), android.R.style.Theme_Dialog);
                        msg.setTitle("");
                        msg.setMessage("Are you sure you want to delete this menu?");
                        msg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //we are clearing the listview and refilling with and making occupied chairs 0
                                db = activity.openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                                Cursor subitemsData = db.rawQuery("SELECT id FROM ingredients WHERE cart_id_fk=" + cart.get(groupPosition).getSno() + ";", null);

                                if ((subitemsData.getCount() > 1)) {
                                    subitemsData.moveToFirst();
                                    db.execSQL("DELETE FROM subitems WHERE ing_id_fk = " + subitemsData.getColumnIndex("id") + ";");
                                }
                                db.execSQL("DELETE FROM ingredients WHERE cart_id_fk = " + cart.get(groupPosition).getSno() + ";");

                                //here we are commeting the delete database when home button is clicked
                                db.execSQL("DELETE FROM cart WHERE cart_id = " + cart.get(groupPosition).getSno() + ";");
                                // cart.remove(position);

                                Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
                                Cursor tableData = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno=" + Group.GROUPID.table + ";", null);
                                cart.clear();
                                Cursor cartData = db.rawQuery("SELECT * FROM cart WHERE tableno=" + Group.GROUPID.table + " AND billflag = 0;", null);
                                tableData.moveToFirst();
                                if ((cartData.getCount() < 1)) {
                                    if (tableData.getInt(tableData.getColumnIndex("occupiedchairs")) > 0) {
                                        TablesFragment.TABLESID.COVERS = "0";
                                        db.execSQL("UPDATE tables SET occupiedchairs = " + TablesFragment.TABLESID.COVERS + " WHERE  tableno =" + Group.GROUPID.table + ";");
                                        db.execSQL("DELETE FROM ordernos WHERE tableno = " + Group.GROUPID.table + " AND billflag = 0;");
                                        db.execSQL("DELETE FROM cart WHERE tableno = " + Group.GROUPID.table +
                                                " AND billflag = 0;");

                                        tableno.setText("");
                                        listView.setAdapter((BaseExpandableListAdapter) null);
                                        totalamountvalue.setText("");
                                        totaltax.setText("");
                                        itemno.setText("");
                                        noofguests.setText("0");
                                        if (!tablesFragment.isResumed()) {
                                            fragmentTransaction = fragmentManager.beginTransaction();
                                            fragmentTransaction.replace(R.id.container2, tablesFragment);
                                            fragmentTransaction.commit();
                                        } else {
                                            fragmentTransaction = fragmentManager.beginTransaction();
                                            fragmentTransaction.replace(R.id.container2, tablesfragment1);
                                            fragmentTransaction.commit();
                                        }
                                    }
                                } else {
                                    alertadapter(activity);
                                }

                                db.close();
                                notifyDataSetChanged();

                                dialog.dismiss();
                            }
                        });
                        msg.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        msg.show();
                    }
                }
            });
            listView.expandGroup(groupPosition);
            return convertView;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.row_cart_ingredients, null);
            }
            Cartpojo p = cart.get(groupPosition).getItem_list().get(childPosition);

            final TextView txt_item_name = (TextView) convertView.findViewById(R.id.txt_item_name);
            final TextView txt_item_cost = (TextView) convertView.findViewById(R.id.txt_item_cost);
            final NonScrollListView listsubitems = (NonScrollListView) convertView.findViewById(R.id.listsubitems);
            String name = p.getPqty() + "x " + p.getName();
//            Double tot = Double.parseDouble(p.getPprice()) * Integer.parseInt(p.getPqty());
            Double tot = Double.parseDouble(p.getTotal());
            txt_item_name.setText(name);
            txt_item_cost.setText(getActivity().getString(R.string.pound_symbol) + " " + String.format(Locale.US, "%.2f", tot));
            ArrayList<String> subdata = new ArrayList<>();
            if (p.getItem_list().size() > 0) {
                for (Cartpojo pojo : p.getItem_list()) {
                    String str = pojo.getPqty() + "x " + pojo.getName();
                    subdata.add(str);
                }
                ArrayAdapter<String> oppor_adapter = new ArrayAdapter<String>(getActivity(), R.layout.row_cart_ing_subitems, subdata);
                listsubitems.setAdapter(oppor_adapter);
                listsubitems.setDividerHeight(0);
            } else {
                ArrayAdapter<String> oppor_adapter = new ArrayAdapter<String>(getActivity(), R.layout.row_cart_ing_subitems, subdata);
                listsubitems.setAdapter(oppor_adapter);
                listsubitems.setDividerHeight(0);
            }
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }

    public void send() {
        db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor productDetails = db.rawQuery("SELECT * FROM cart WHERE tableno= " + CartStaticSragment.CARTStaticSragment.tableno.getText().toString() + " AND billflag = 0;", null);
        int count = productDetails.getCount();
        db.close();
        if (count > 0) {
            // Toast.makeText(getActivity(), "Order Saved Successfully", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getActivity(), Printer.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("TABLENO", parseInt(CartStaticSragment.CARTStaticSragment.tableno.getText().toString()));
//            intent.putExtra("LOCATIONNO", 0);
            intent.putExtra("FLAG", 3);//3
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "No New Order Data To Print",
                    Toast.LENGTH_LONG).show();
        }
    }

    public static CartStaticSragment getInstance() {
        return CARTStaticSragment;
    }
}

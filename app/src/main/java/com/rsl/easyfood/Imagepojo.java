package com.rsl.easyfood;

/**
 * Created by EmbDes on 10-07-2017.
 */

public class Imagepojo {
    private String paymnet;
    private String imageur;

    public int getInclude_price() {
        return include_price;
    }

    public void setInclude_price(int include_price) {
        this.include_price = include_price;
    }

    private int include_price;

    public int getProduct_type() {
        return product_type;
    }

    public void setProduct_type(int product_type) {
        this.product_type = product_type;
    }

    private int product_type=0;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    private Double price=00.00;

    public String getRestaurantid() {
        return restaurantid;
    }

    public void setRestaurantid(String restaurantid) {
        this.restaurantid = restaurantid;
    }

    private String restaurantid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    private String groupid;

    public String getPaymnet() {
        return paymnet;
    }

    public void setPaymnet(String paymnet) {
        this.paymnet = paymnet;
    }

    public String getImageur() {
        return imageur;
    }

    public void setImageur(String imageur) {
        this.imageur = imageur;
    }


}


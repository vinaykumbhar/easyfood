package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.model.OrderDisplay;
import com.rsl.utills.ConnectionDetector;
import com.rsl.utills.NonScrollListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static java.lang.Integer.parseInt;

@SuppressLint("ResourceAsColor")
public class NewOrderFragment extends Fragment implements View.OnClickListener {
    private ActionBarDrawerToggle mDrawerToggle;
    public static DrawerLayout mDrawerLayout;
    LayoutInflater mLayoutInflater;
    SharedPreferences mSharedPreferences;
    ViewPager mViewPager;
    Button btn_accept, btn_decline;
    CartOrderPagerAdapter mCustomPagerAdapter;
    //    private FragmentDrawerListener drawerListener;
    int count = 1;
    TextView text_no_of_orders, text_total_orders, text_previous, text_next;
    ArrayList<OrderDisplay> list_orderDisplays = new ArrayList<>();
    ArrayList<OrderDisplay> list_orders;
    double subtotal = 0;
    ProgressDialog dialog;
    ConnectionDetector internet;
    SQLiteDatabase db;
    ViewPager.OnPageChangeListener pageChangeListener = null;
//
//    public void setDrawerListener(FragmentDrawerListener listener) {
//        this.drawerListener = listener;
//    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.new_order_nav_drawer, container, false);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        text_no_of_orders = (TextView) layout.findViewById(R.id.text_no_of_orders);
        text_total_orders = (TextView) layout.findViewById(R.id.text_total_orders);
        text_previous = (TextView) layout.findViewById(R.id.text_previous);
        text_next = (TextView) layout.findViewById(R.id.text_next);
        mViewPager = (ViewPager) layout.findViewById(R.id.pager);
        btn_decline = (Button) layout.findViewById(R.id.btn_decline);
        btn_accept = (Button) layout.findViewById(R.id.btn_accept);
        list_orders = new ArrayList<>();
        mCustomPagerAdapter = new CartOrderPagerAdapter();
        mViewPager.setAdapter(mCustomPagerAdapter);
        internet = new ConnectionDetector(getActivity());

        db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);

        if (internet.isConnectingToInternet())
            getOrder(mSharedPreferences.getString("mid", ""), "", "");
        else
            internet.showAlertDialog(getActivity());

        pageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                Log.e("onPageScrolled", "i=" + i);
                Log.e("onPageScrolled", "v=" + v);
                Log.e("onPageScrolled", "i1=" + i1);
//                if (list_orders.size() > 2 && i1 == 0) {
//                    if (i == list_orders.size() - 2) {
//                        getOrder(mSharedPreferences.getString("mid", ""), "", mSharedPreferences.getString("start_id", ""));
//                    }
//                }
            }

            @Override
            public void onPageSelected(int i) {
                count = i;
                count++;
                text_no_of_orders.setText(String.valueOf(count));
                if (list_orders.size() > 0)
                    getOrderDetails(list_orders.get(i).getOrder_id());
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        };

        mViewPager.setOnPageChangeListener(pageChangeListener);

        //Add Listener to Widget
        btn_accept.setOnClickListener(this);
        btn_decline.setOnClickListener(this);
        text_next.setOnClickListener(this);
        text_previous.setOnClickListener(this);


        return layout;
    }


    public void setUp(DrawerLayout drawerLayout, final Toolbar toolbar) {
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout,
                toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.e("drawer", "opened");
                // getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.e("drawer", "closed");
                // getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
                Log.e("drawer", "onDrawerSlide");
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_accept:
                if (list_orders.size() > 0) {
                    if (isAdded())
                        if (internet.isConnectingToInternet())
                            updateOrderStatus("1", list_orders.get(mViewPager.getCurrentItem()).getOrder_id(), mViewPager.getCurrentItem());
                        else internet.showAlertDialog(getActivity());
                }
                break;
            case R.id.btn_decline:
//                String order_decline_id = mCustomPagerAdapter.getPositionData(mViewPager.getCurrentItem()).getOrder_id();
                if (list_orders.size() > 0) {
                    if (isAdded())
                        if (internet.isConnectingToInternet())
                            updateOrderStatus("2", list_orders.get(mViewPager.getCurrentItem()).getOrder_id(), mViewPager.getCurrentItem());
                        else internet.showAlertDialog(getActivity());
                }
                break;
            case R.id.text_next:
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
                text_no_of_orders.setText(String.valueOf(count));
                break;
            case R.id.text_previous:
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
                text_no_of_orders.setText(String.valueOf(count));
                break;
        }//switch
    }

//    public interface FragmentDrawerListener {
//        public void onDrawerItemSelected(View view, int position);
//    }

    class CartOrderPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return list_orders.size();

            // return 2;
        }


        private OrderDisplay getPositionData(int pos) {
            return list_orders.get(pos);
        }


        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            mLayoutInflater = LayoutInflater.from(getActivity());
            ViewGroup layout = (ViewGroup) mLayoutInflater.inflate(R.layout.row_new_order_pager, container, false);
            NonScrollListView cartOrder_listView = (NonScrollListView) layout.findViewById(R.id.cartOrder_listView);
            TextView cartOrderId = (TextView) layout.findViewById(R.id.cartOrderId);
            TextView cartGuests = (TextView) layout.findViewById(R.id.cartGuests);
            TextView cartTable = (TextView) layout.findViewById(R.id.cartTable);
            TextView cartSubtotal = (TextView) layout.findViewById(R.id.cartSubtotal);
            TextView cartTotal = (TextView) layout.findViewById(R.id.cartTotal);
            TextView cartTime = (TextView) layout.findViewById(R.id.cartTime);
            TextView cartDeliveryCharges = (TextView) layout.findViewById(R.id.cartDeliveryCharges);
            TextView resto_Address = (TextView) layout.findViewById(R.id.resto_Address);

            if (list_orderDisplays.size() > 0) {

                for (int l = 0; l < list_orderDisplays.size(); l++) {
                    OrderDisplay data = list_orderDisplays.get(l);
                    try {
                        if (!data.getDelivery_address().equals("[]")) {
                            JSONObject jsonObject = new JSONObject(data.getDelivery_address());

                            String address = jsonObject.getString("delivery_address1") + "," + jsonObject.getString("delivery_address2")
                                    + ",\n" + jsonObject.getString("delivery_city") + "," + jsonObject.getString("delivery_zipcode");
                            resto_Address.setText(address);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (data.getCart_items().size() > 0) {
                        subtotal = 0.00;
                        for (OrderDisplay d : data.getCart_items()) {
                            subtotal += Double.parseDouble(d.getPrice());
                        }
                    }
                    cartDeliveryCharges.setText(data.getShipping_cost());
                    cartOrderId.setText(data.getOrder_id());
                    cartTime.setText(data.getOrder_time());
                    cartSubtotal.setText(String.format(Locale.US, "%.2f", subtotal));
                    cartTotal.setText(String.format(Locale.US, "%.2f", Double.parseDouble(data.getOrder_total())));
                    RowListAdapter adapter = new RowListAdapter(data.getCart_items());
                    cartOrder_listView.setAdapter(adapter);
                    //cartOrder_listView.setDividerHeight(0);
                    adapter.notifyDataSetChanged();

                }
            }


            container.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }//CartOrderPagerAdapter


    private void getOrder(final String restaurant_id, final String page_no, final String start_id) {
        final RequestQueue queue = Volley.newRequestQueue(getActivity());
        String requestURL = getString(R.string.web_path) + "order_service/getOrder/?restaurant_id=" + restaurant_id + "&page_no=" + page_no + "&start_id=" + start_id;
        Log.d("requestURL", "getOrder:" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.d("getOrder", "res:" + result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj1 = new JSONObject(result);
                            ArrayList<String> ids = new ArrayList<>();
                            if (jObj1.length() > 0) {
                                if (!db.isOpen())
                                    db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
                                Object detailsObject = jObj1.get("order_list");
                                if (detailsObject instanceof JSONArray) {
                                    JSONArray detailsJsonArray = new JSONArray(String.valueOf(jObj1.getJSONArray("order_list")));
                                    if (detailsJsonArray.length() > 0) {
                                        for (int i = 0; i < detailsJsonArray.length(); i++) {
                                            JSONObject jObjDetails = detailsJsonArray.getJSONObject(i);
                                            String oId = jObjDetails.getString("order_id");
                                            String is_accepted = jObjDetails.getString("is_accepted");
                                            Cursor orderData = db.rawQuery("SELECT order_id,is_accepted FROM allOrders WHERE order_id=" + oId + ";", null);
                                            int orderCount = orderData.getCount();
                                            if (orderCount > 0) {
                                                while (orderData.moveToNext()) {
                                                    db.execSQL("" +
                                                            "UPDATE allOrders SET is_accepted = " + jObjDetails.getString("is_accepted") + " WHERE order_id = " +
                                                            oId + ";");
                                                }
                                            } else {
                                                db.execSQL("INSERT INTO allOrders VALUES(" + "'" + oId + "','" + jObjDetails.getString("order_num") +
                                                        "','" + jObjDetails.getString("order_zip") + "'," + jObjDetails.getString("order_total") + ",'" + jObjDetails.getString("order_date")
                                                        + "','" + jObjDetails.getString("order_time") + "'," + jObjDetails.getString("is_accepted") + ",'" + jObjDetails.getString("order_updated_on") +
                                                        "','" + jObjDetails.getString("customer_name") + "'" + ");");
                                            }

                                            if (is_accepted.equals("0")) {
                                                ids.add(oId);
                                            }
                                            mSharedPreferences.edit().putString("start_id", oId).apply();
                                        }
                                        if (ids.size() == 0) {
                                            getOrder(mSharedPreferences.getString("mid", ""), "", mSharedPreferences.getString("start_id", ""));
                                            return;
                                        }
                                    }
                                }//order_list instanceof

                                try {
                                    if (!db.isOpen())
                                        db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
                                    Cursor allOrderData = db.rawQuery("SELECT order_id,is_accepted from allOrders WHERE is_accepted=0", null);
                                    int count = allOrderData.getCount();
                                    if (count > 0) {
                                        Group.textNewordercount.setText(String.valueOf(count));
                                        while (allOrderData.moveToNext()) {
                                            OrderDisplay order_data = new OrderDisplay();
                                            order_data.setOrder_id(allOrderData.getString(allOrderData.getColumnIndex("order_id")));
                                            list_orders.add(order_data);
                                        }
                                    } else {
                                        Toast.makeText(getActivity(), "No new orders available ", Toast.LENGTH_SHORT).show();
                                    }
                                    allOrderData.close();
                                    if (list_orders.size() > 0 && mCustomPagerAdapter != null) {
                                        text_total_orders.setText("/" + String.valueOf(list_orders.size()));

                                        mViewPager.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                pageChangeListener.onPageSelected(mViewPager.getCurrentItem());
                                            }
                                        });
                                        mCustomPagerAdapter.notifyDataSetChanged();
                                        mViewPager.setCurrentItem(1);
                                        mViewPager.setCurrentItem(0);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    if (!db.isOpen())
                                        db.close();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("getOrder", "exception=" + e.toString());
                            if (isAdded())
                                Toast.makeText(getActivity(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("volleyError", "getOrder=" + volleyError.getMessage());
                Toast.makeText(getActivity(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOrder

    private void getOrderDetails(final String oId) {
        if (dialog == null)
            dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Order Details");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(getActivity());
        String requestURL = getString(R.string.web_path) + "order_service/details/?order_id=" + oId;
        Log.d("requestURL", "getOrderDetails:" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.d("getOrder", "getOrderDetails:" + result);
                if (dialog.isShowing())
                    dialog.dismiss();
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj1 = new JSONObject(result);

                            if (jObj1.length() > 0) {
                                if (!db.isOpen())
                                    db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
                                // list_orderDisplays.clear();
                                Object detailsObject = jObj1.get("order_detail");

                                if (detailsObject instanceof JSONObject) {
                                    JSONObject jObjDetails = jObj1.getJSONObject("order_detail");
                                    Cursor orderData = db.rawQuery("SELECT order_id FROM newOrder WHERE order_id=" + oId + ";", null);
                                    int orderCount = orderData.getCount();
                                    Log.e("Ordercount_", "local=" + orderCount);

                                    Cursor newOrderMenuitem = db.rawQuery("SELECT menu_item_name FROM newOrderMenuitem WHERE order_id=" + oId + ";", null);
                                    int orderMenuitemCount = newOrderMenuitem.getCount();
                                    Log.e("newOrderMenuitem_count", "" + newOrderMenuitem.getCount());
                                    while (newOrderMenuitem.moveToNext()) {
                                        String name = newOrderMenuitem.getString(newOrderMenuitem.getColumnIndex("menu_item_name"));
                                        Log.e("name", "" + name);
                                    }


                                    if (orderCount == 0) {
                                        db.execSQL("INSERT INTO newOrder (tableno,guestno,order_id,order_num,order_total,shipping_cost," +
                                                "is_accepted,product_type,payment_mode,delivery_address,billing_address,customer_name," +
                                                "is_paid,cartdatetime,billflag,orderflag,checkflag,ordertype) VALUES(" + 0 + "," + 0 +
                                                ",'" + jObjDetails.getString("order_id") +
                                                "','" + jObjDetails.getString("order_num") + "'," + jObjDetails.getString("order_total") +
                                                "," + jObjDetails.getString("shipping_cost") + "," + jObjDetails.getString("is_accepted") +
                                                "," + 0 + ",'" + jObjDetails.getString("payment_mode") +
                                                "','" + jObjDetails.getString("delivery_address") + "','" + jObjDetails.getString("billing_address") +
                                                "','" + jObjDetails.getString("customer_name") +
                                                "','" + jObjDetails.getString("is_paid") + "','" + jObjDetails.getString("order_time") + "'," + 0 + "," + 0 + "," + 0 + ",'Web');");


                                        JSONArray jsonArrayCart = jObjDetails.getJSONArray("cart_splitwithoutbill");
                                        Log.e("jsonArrayCart", "=" + jsonArrayCart.length());
                                        if (jsonArrayCart.length() > 0) {
                                            for (int j = 0; j < jsonArrayCart.length(); j++) {
                                                JSONObject jObj = jsonArrayCart.getJSONObject(j);
                                                String restro_id = jObj.has("restaurant_id") ? jObj.getString("restaurant_id") : mSharedPreferences.getString("mid", "");
                                                String is_meal = jObj.has("is_meal") ? jObj.getString("is_meal") : "false";
                                                String notes = jObj.has("notes") ? jObj.getString("notes") : "false";

                                                db.execSQL("INSERT INTO newOrderMenuitem (order_id,menu_item_id_fk,restaurant_id,menu_id,menu_item_name,menu_item_price,menu_item_qty,is_meal,notes,is_coupon,subtotal,options,billflag,orderflag,checkflag,ordertype) VALUES(" + "'" + jObjDetails.getString("order_id") + "'," + jObj.getString("id") +
                                                        "," + restro_id +
                                                        "," + jObj.getString("assign_menu_id") + ",'" + jObj.getString("name") + "'," + jObj.getString("price") + "," + jObj.getString("qty") +
                                                        ",'" + is_meal + "','" + notes +
                                                        "'," + jObj.getString("is_coupon") + "," + jObj.getString("subtotal") +
                                                        ",'" + jObj.getString("options") + "'," + 0 + "," + 0 + "," + 0 + ",'Web');");
                                            }
                                        }
                                        Object cart_splitData = jObjDetails.get("cart_split");
                                        if (cart_splitData instanceof JSONObject) {
                                            JSONObject jsonObj = jObjDetails.getJSONObject("cart_split");
                                            if (jsonObj.length() > 0) {
                                                Iterator<String> j = jsonObj.keys();
                                                for (int m = 0; m < jObjDetails.getJSONObject("cart_split").length(); m++) {
                                                    String keys = j.next();
                                                    Object cart_splitSubData = jObjDetails.getJSONObject("cart_split").get(keys);
                                                    if (cart_splitSubData instanceof JSONArray) {
                                                        JSONArray jsonArray = jObjDetails.getJSONObject("cart_split").getJSONArray((keys));
                                                        if (jsonArray.length() > 0) {
                                                            for (int f = 0; f < jsonArray.length(); f++) {
                                                                JSONObject jObj = jsonArray.getJSONObject(f);
                                                                String restro_id = jObj.has("restaurant_id") ? jObj.getString("restaurant_id") : mSharedPreferences.getString("mid", "");
                                                                String is_meal = jObj.has("is_meal") ? jObj.getString("is_meal") : "false";
                                                                String notes = jObj.has("notes") ? jObj.getString("notes") : "false";

                                                                db.execSQL("INSERT INTO newOrderMenuitem (order_id,menu_item_id_fk,restaurant_id,menu_id,menu_item_name,menu_item_price,menu_item_qty,is_meal,notes,is_coupon,subtotal,options,billflag,orderflag,checkflag,ordertype) VALUES('" + jObjDetails.getString("order_id") + "'," + jObj.getString("id") +
                                                                        "," + restro_id + "," + jObj.getString("assign_menu_id") + ",'" + jObj.getString("name") + "'," + jObj.getString("price") + "," + jObj.getString("qty") +
                                                                        ",'" + is_meal + "','" + notes +
                                                                        "'," + jObj.getString("is_coupon") + "," + jObj.getString("subtotal") +
                                                                        ",'" + jObj.getString("options") + "'," + 0 + "," + 0 + "," + 0 + ",'Web');");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else if (orderMenuitemCount == 0) {
                                        JSONArray jsonArrayCart = jObjDetails.getJSONArray("cart_splitwithoutbill");
                                        Log.e("jsonArrayCart", "=" + jsonArrayCart.length());
                                        if (jsonArrayCart.length() > 0) {
                                            for (int j = 0; j < jsonArrayCart.length(); j++) {
                                                JSONObject jObj = jsonArrayCart.getJSONObject(j);
                                                String restro_id = jObj.has("restaurant_id") ? jObj.getString("restaurant_id") : mSharedPreferences.getString("mid", "");
                                                String is_meal = jObj.has("is_meal") ? jObj.getString("is_meal") : "false";
                                                String notes = jObj.has("notes") ? jObj.getString("notes") : "false";

                                                db.execSQL("INSERT INTO newOrderMenuitem (order_id,menu_item_id_fk,restaurant_id,menu_id,menu_item_name,menu_item_price,menu_item_qty,is_meal,notes,is_coupon,subtotal,options,billflag,orderflag,checkflag,ordertype) VALUES(" + "'" + jObjDetails.getString("order_id") + "'," + jObj.getString("id") +
                                                        "," + restro_id +
                                                        "," + jObj.getString("assign_menu_id") + ",'" + jObj.getString("name") + "'," + jObj.getString("price") + "," + jObj.getString("qty") +
                                                        ",'" + is_meal + "','" + notes +
                                                        "'," + jObj.getString("is_coupon") + "," + jObj.getString("subtotal") +
                                                        ",'" + jObj.getString("options") + "'," + 0 + "," + 0 + "," + 0 + ",'Web');");
                                            }
                                        }
                                        Object cart_splitData = jObjDetails.get("cart_split");
                                        if (cart_splitData instanceof JSONObject) {
                                            JSONObject jsonObj = jObjDetails.getJSONObject("cart_split");
                                            if (jsonObj.length() > 0) {
                                                Iterator<String> j = jsonObj.keys();
                                                for (int m = 0; m < jObjDetails.getJSONObject("cart_split").length(); m++) {
                                                    String keys = j.next();
                                                    Object cart_splitSubData = jObjDetails.getJSONObject("cart_split").get(keys);
                                                    if (cart_splitSubData instanceof JSONArray) {
                                                        JSONArray jsonArray = jObjDetails.getJSONObject("cart_split").getJSONArray((keys));
                                                        if (jsonArray.length() > 0) {
                                                            for (int f = 0; f < jsonArray.length(); f++) {
                                                                JSONObject jObj = jsonArray.getJSONObject(f);
                                                                String restro_id = jObj.has("restaurant_id") ? jObj.getString("restaurant_id") : mSharedPreferences.getString("mid", "");
                                                                String is_meal = jObj.has("is_meal") ? jObj.getString("is_meal") : "false";
                                                                String notes = jObj.has("notes") ? jObj.getString("notes") : "false";

                                                                db.execSQL("INSERT INTO newOrderMenuitem (order_id,menu_item_id_fk,restaurant_id,menu_id,menu_item_name,menu_item_price,menu_item_qty,is_meal,notes,is_coupon,subtotal,options,billflag,orderflag,checkflag,ordertype) VALUES('" + jObjDetails.getString("order_id") + "'," + jObj.getString("id") +
                                                                        "," + restro_id + "," + jObj.getString("assign_menu_id") + ",'" + jObj.getString("name") + "'," + jObj.getString("price") + "," + jObj.getString("qty") +
                                                                        ",'" + is_meal + "','" + notes +
                                                                        "'," + jObj.getString("is_coupon") + "," + jObj.getString("subtotal") +
                                                                        ",'" + jObj.getString("options") + "'," + 0 + "," + 0 + "," + 0 + ",'Web');");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                list_orderDisplays = new ArrayList<>();

                                Cursor newOrderCount = db.rawQuery("SELECT * from newOrder WHERE order_id=" + oId, null);
                                int count = newOrderCount.getCount();
                                if (count > 0) {
                                    OrderDisplay order_data = new OrderDisplay();
                                    while (newOrderCount.moveToNext()) {
                                        Log.e("Inside_ID", "" + newOrderCount.getString(newOrderCount.getColumnIndex("order_id")));
                                        order_data.setOrder_id(newOrderCount.getString(newOrderCount.getColumnIndex("order_id")));
                                        order_data.setOrder_time(newOrderCount.getString(newOrderCount.getColumnIndex("cartdatetime")));
                                        order_data.setDelivery_address(newOrderCount.getString(newOrderCount.getColumnIndex("delivery_address")));
                                        order_data.setOrder_total(newOrderCount.getString(newOrderCount.getColumnIndex("order_total")));
                                        order_data.setShipping_cost(newOrderCount.getString(newOrderCount.getColumnIndex("shipping_cost")));
                                        String order_id = newOrderCount.getString(newOrderCount.getColumnIndex("order_id"));

                                        Cursor item_data = db.rawQuery("SELECT * from newOrderMenuitem WHERE order_id=" + order_id, null);
                                        int item_count = item_data.getCount();
                                        Log.e("item_count", "" + item_count);
                                        ArrayList<OrderDisplay> list_orders = new ArrayList<>();
                                        if (item_count > 0) {
                                            while (item_data.moveToNext()) {
                                                OrderDisplay order_data1 = new OrderDisplay();
                                                order_data1.setName(item_data.getString(item_data.getColumnIndex("menu_item_name")));
                                                order_data1.setQty(item_data.getString(item_data.getColumnIndex("menu_item_qty")));
                                                order_data1.setPrice(item_data.getString(item_data.getColumnIndex("menu_item_price")));
                                                list_orders.add(order_data1);

                                            }
                                        }
                                        order_data.setCart_items(list_orders);
                                    }
                                    list_orderDisplays.add(order_data);

                                }

//                                mViewPager.post(new Runnable()
//                                {
//                                    @Override
//                                    public void run()
//                                    {
//                                        pageChangeListener .onPageSelected(mViewPager.getCurrentItem());
//                                    }
//                                });
                                mCustomPagerAdapter.notifyDataSetChanged();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Log.e("getOrderDetails", "exception=" + e.toString());
                            Toast.makeText(getActivity(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("volleyError", "getOrderDetails=" + volleyError.getMessage());
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getActivity(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOrder

    private class updateSlider extends AsyncTask<String, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            list_orders.clear();
            list_orders = new ArrayList<>();
        }

        @Override
        protected Integer doInBackground(String... strings) {
            try {
                if (!db.isOpen())
                    db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);

                Cursor allOrderData = db.rawQuery("SELECT order_id,is_accepted from allOrders WHERE is_accepted=0", null);
                int count = allOrderData.getCount();
                if (count > 0) {
                    while (allOrderData.moveToNext()) {
                        OrderDisplay order_data = new OrderDisplay();
                        order_data.setOrder_id(allOrderData.getString(allOrderData.getColumnIndex("order_id")));
                        list_orders.add(order_data);
                    }
                }
                allOrderData.close();
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            } finally {
                if (!db.isOpen())
                    db.close();
            }
            return list_orders.size();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            Log.e("update", "=" + integer);

            /*if (integer < 5 && integer > 0) {
                if (internet.isConnectingToInternet())
                    getOrder(mSharedPreferences.getString("mid", ""), "", mSharedPreferences.getString("start_id", ""));
                else
                    internet.showAlertDialog(getActivity());
            } else*/
            if (integer > 0 && mCustomPagerAdapter != null) {
                text_total_orders.setText("/" + String.valueOf(integer));
                mCustomPagerAdapter.notifyDataSetChanged();
                mViewPager.setCurrentItem(1);
                mViewPager.setCurrentItem(0);
            }

        }
    }


    public class RowListAdapter extends BaseAdapter {

        ArrayList<OrderDisplay> list_data;

        RowListAdapter(ArrayList<OrderDisplay> list_data) {
            this.list_data = list_data;
        }

        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;

            TextView qty, price, name;
            if (vi == null) {
                // if it's not recycled, initialize some attributes
                final LayoutInflater inflater = LayoutInflater.from(getActivity());
                vi = inflater.inflate(R.layout.billoutadapter, parent, false);
            }
            name = (TextView) vi.findViewById(R.id.billoutitemname);
            price = (TextView) vi.findViewById(R.id.billoutamount);
            qty = (TextView) vi.findViewById(R.id.sno);
//            name.setText(list_data.get(position).getName());
            name.setText(Html.fromHtml(list_data.get(position).getName()));
            qty.setText(list_data.get(position).getQty());
            price.setText(list_data.get(position).getPrice());


            return vi;
        }
    }//RowListAdapter

    private void updateOrderStatus(final String status, final String order_id, final int pos) {
        final RequestQueue queue = Volley.newRequestQueue(getActivity());
        String requestURL = getString(R.string.web_path) + "order_service/orderUpdateStatus/?status=" + status + "&orderId=" + order_id;
        Log.d("requestURL", "orderUpdateStatus:" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.d("orderUpdateStatus", "res:" + result);
                if (result != null) {
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.length() > 0) {
                            if (jObj.getString("type_id").equals("1")) {
                                Toast.makeText(getActivity(), jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                                // getOrder(mSharedPreferences.getString("mid", ""), "", "");

                                list_orders.remove(pos);
                                text_total_orders.setText("/" + String.valueOf(list_orders.size()));
                                mViewPager.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        pageChangeListener.onPageSelected(mViewPager.getCurrentItem());
                                    }
                                });
                                if (list_orders.size() == 0) {
                                    Group.GROUPID.mDrawerLayout.closeDrawer(Gravity.END);
                                }
                                mCustomPagerAdapter.notifyDataSetChanged();

                            } else {
                                Toast.makeText(getActivity(), jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                            if (!db.isOpen())
                                db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
                            if (status.equals("1")) {
                                Cursor newOrderCount = db.rawQuery("SELECT * from newOrder WHERE order_id=" + order_id, null);
                                int count = newOrderCount.getCount();
                                if (count > 0) {
                                    while (newOrderCount.moveToNext()) {
                                        Log.e("ON_ACCEPT", "" + newOrderCount.getString(newOrderCount.getColumnIndex("order_id")));
                                        String order_id = newOrderCount.getString(newOrderCount.getColumnIndex("order_id"));
                                        Cursor item_data = db.rawQuery("SELECT * from newOrderMenuitem WHERE order_id=" + order_id, null);
                                        int item_count = item_data.getCount();
                                        Log.e("item_count", "accept" + item_count);

                                        if (item_count > 0) {
                                            while (item_data.moveToNext()) {
                                                ContentValues values = new ContentValues();
                                                values.put("tableno", newOrderCount.getString(newOrderCount.getColumnIndex("tableno")));
                                                values.put("guestno", newOrderCount.getString(newOrderCount.getColumnIndex("guestno")));
                                                values.put("menu_item_url", "");
                                                values.put("include_price", "1");
                                                values.put("product_type", newOrderCount.getString(newOrderCount.getColumnIndex("product_type")));
                                                values.put("cartdatetime", newOrderCount.getString(newOrderCount.getColumnIndex("tableno")));
                                                values.put("billflag", newOrderCount.getString(newOrderCount.getColumnIndex("billflag")));
                                                values.put("orderflag", newOrderCount.getString(newOrderCount.getColumnIndex("orderflag")));
                                                values.put("checkflag", newOrderCount.getString(newOrderCount.getColumnIndex("checkflag")));
                                                values.put("ordertype", newOrderCount.getString(newOrderCount.getColumnIndex("ordertype")));

                                                values.put("menu_item_id_fk", item_data.getString(item_data.getColumnIndex("menu_item_id_fk")));
                                                values.put("menu_item_name", item_data.getString(item_data.getColumnIndex("menu_item_name")));
                                                values.put("menu_item_price", item_data.getString(item_data.getColumnIndex("menu_item_price")));
                                                values.put("menu_item_qty", item_data.getString(item_data.getColumnIndex("menu_item_qty")));
                                                values.put("menu_id", item_data.getString(item_data.getColumnIndex("menu_id")));


                                                long cart_id = db.insert("cart", null, values);
                                                Log.e("cart_id", "NewOrder" + cart_id);
                                                db.execSQL("INSERT INTO ordernos (sno,tableno,orderno,billflag,orderflag) VALUES('" + cart_id + "'," + newOrderCount.getString(newOrderCount.getColumnIndex("tableno")) +
                                                        "," + order_id + "," + 0 + "," + 0 + ");");
                                                if (cart_id > 0) {
                                                    //   Toast.makeText(getActivity(), menuname + " added to cart", Toast.LENGTH_SHORT).show();
                                                    db.execSQL("UPDATE newOrder SET is_accepted = " + 1 + " WHERE order_id = " + order_id + ";");
                                                    db.execSQL("UPDATE allOrders SET is_accepted = " + 1 + " WHERE order_id = " + order_id + ";");
                                                }
                                            }
                                        }
                                    }
                                }
                                send(order_id);
                            } else if (status.equals("2")) {
                                Cursor newOrderCount = db.rawQuery("SELECT * from newOrder WHERE order_id=" + order_id, null);
                                int count = newOrderCount.getCount();
                                if (count > 0) {
                                    while (newOrderCount.moveToNext()) {
                                        db.execSQL("UPDATE newOrder SET is_accepted = 2 WHERE order_id = " + order_id + ";");
                                        db.execSQL("UPDATE allOrders SET is_accepted = 2 WHERE order_id = " + order_id + ";");
                                    }
                                }
                            }
                            db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                            Cursor allOrderData = db.rawQuery("SELECT order_id,is_accepted from allOrders WHERE is_accepted=0", null);
                            final int count = allOrderData.getCount();
                            Log.e("AfterAction", "=" + count);
                            Group.textNewordercount.setText(String.valueOf(count));
                            db.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getActivity(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOrder

    public void send(String order_id) {
        db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor productDetails = db.rawQuery("SELECT * FROM newOrder WHERE order_id = " + order_id + " AND billflag = 0;", null);
        int count = productDetails.getCount();
        db.close();
        if (count > 0) {
            // Toast.makeText(getActivity(), "Order Saved Successfully", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getActivity(), Printer.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("TABLENO", 0);
            intent.putExtra("ORDERID", order_id);
            intent.putExtra("FLAG", 4);//3
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "No New Order Data To Print",
                    Toast.LENGTH_LONG).show();
        }
    }


//    public class DeclineOrder extends AsyncTask<String, String, String> {
//
//        String decline_id;
//
//        DeclineOrder(String decline_id) {
//            this.decline_id = decline_id;
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//
//            if (!db.isOpen())
//                db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
//
//            Cursor newOrderCount = db.rawQuery("SELECT * from newOrder WHERE order_id=" + decline_id, null);
//            int count = newOrderCount.getCount();
//            if (count > 0) {
//                while (newOrderCount.moveToNext()) {
//                    db.execSQL("UPDATE newOrder SET is_accepted = " + 2 + " WHERE order_id = " + decline_id + ";");
//                    db.execSQL("UPDATE allOrders SET is_accepted = " + 2 + " WHERE order_id = " + decline_id + ";");
//                }
//            }
//            db.close();
//            return null;
//        }
//    }
}
/**
 * 
 */
package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author embdes
 *
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PRODUCTREPORTS extends Activity {
/* (non-Javadoc)
 * @see android.app.Activity#onCreate(android.os.Bundle)
 */
	
	DatePickerDialog toDate;
	SQLiteDatabase db;
	
	String toDateString;
	String fromDateString;
	String toReportsDate;
	String fromReportsDate;
	TextView titlebar,date;
	boolean isShow = false;
	static PRODUCTREPORTS pRODUCTREPORTS;
	static final int DATE_DIALOG_ID = 0;
	static final int FROM_DATE_DIALOG_ID = 2;

	// variables to save user selected date and time
	public  int monthSelected,daySelected,hourSelected,minuteSelected;

	// declare  the variables to show the date and time whenTime and Date Picker Dialog first appears
	private int mYear, mMonth, mDay,mHour,mMinute/*mMinute1*/; 

	//In the constructor you can initiallise the variable to current date and time.

@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
	setContentView(R.layout.reports);
	getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.titlemenu);
	ImageButton toSetButton = (ImageButton) findViewById(R.id.toButton);
	ImageButton fromSetButton = (ImageButton) findViewById(R.id.fromButton);
	Button menu = (Button) findViewById(R.id.Imageview);
	menu.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent  =new Intent(PRODUCTREPORTS.this,Home.class);
			startActivity(intent);
		}
	});
	date = (TextView) findViewById(R.id.date);
	titlebar = (TextView) findViewById(R.id.titelabar);
	final Calendar now = Calendar.getInstance();
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
	String nowDate = formatter.format(now.getTime());
	date.setText(nowDate);
	titlebar.setText("PRODUCT REPORTS");
    // Specify that the Home button should show an "Up" caret, indicating that touching the
    // button will take the user one step up in the application's hierarchy.
	ImageButton reportsOk = (ImageButton)findViewById(R.id.reportOk);
	ImageButton reportsCancel = (ImageButton)findViewById(R.id.reportsCancel);
	String timeStamp = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(Calendar.getInstance().getTime());
	String morningtimestamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
	morningtimestamp = morningtimestamp + " 04:00";
	toReportsDate = timeStamp;
	fromReportsDate = morningtimestamp;
	TextView toDate = (TextView) findViewById(R.id.toDateView);
	toDate.setText(" "+timeStamp);
	TextView fromDate = (TextView) findViewById(R.id.fromDateView);
	fromDate.setText(""+morningtimestamp);
	reportsCancel.setOnClickListener(cancelhandler);
	reportsOk.setOnClickListener(productreportshandler);
	fromSetButton.setOnClickListener(fromsethandler);
	toSetButton.setOnClickListener(tosethandler);
}
protected void onResume() {
	super.onResume();
	if(MainActivity.getInstance() ==null || UserLoginActivity.user_LoginID==null){
		Intent userLoginIntent = new Intent(); //Created new Intent to 
		userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
		userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(userLoginIntent);
		this.finish();
	}
	
};
/**
 * 
 */
View.OnClickListener  productreportshandler = new View.OnClickListener() {
	@SuppressLint("NewApi")
	public void onClick(View v) {  

		Intent intent=new Intent(PRODUCTREPORTS.this, Reports.class);
		intent.putExtra("FROMDATE", fromReportsDate);
		intent.putExtra("TODATE", toReportsDate);
		intent.putExtra("REPORTSFLAG", 5);
		startActivity(intent);
		finish();
	}
};
/**
 * This will call onBackPressed when user press cancel button in any screen 
 * which are called from home menu items  
 */
View.OnClickListener  cancelhandler = new View.OnClickListener() {
	@SuppressLint("NewApi")
	public void onClick(View v) {
		onBackPressed();

	}
};


View.OnClickListener  fromsethandler = new View.OnClickListener() {
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public void onClick(View v) {  
		final Calendar c = Calendar.getInstance();
		isShow = true;
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		mHour = c.get(Calendar.HOUR_OF_DAY);
		mMinute = c.get(Calendar.MINUTE);
		showDialog(FROM_DATE_DIALOG_ID);

	}
};	


View.OnClickListener  tosethandler = new View.OnClickListener() {
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public void onClick(View v) {
		final Calendar c = Calendar.getInstance();
		isShow = true;
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		mHour = c.get(Calendar.HOUR_OF_DAY);
		mMinute = c.get(Calendar.MINUTE);
		showDialog(DATE_DIALOG_ID);

	}
};			

/**
 * Dialog will come when user press date button in reports screen.
 * 
 */
protected Dialog onCreateDialog(int id) {
	switch (id) {

	case FROM_DATE_DIALOG_ID:
		final DatePickerDialog fromDate = new DatePickerDialog(this,
				fromDateSetListener,
				mYear, mMonth, mDay);
		fromDate.getDatePicker().setMaxDate(System.currentTimeMillis());
		fromDate.setCancelable(true);
		fromDate.setCanceledOnTouchOutside(true);
		fromDate.setButton(DialogInterface.BUTTON_NEGATIVE,
				"Cancel",
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				isShow = false; //Cancel flag, used in mTimeSetListener
			}
		});
		return fromDate;

	case DATE_DIALOG_ID:
		toDate = new DatePickerDialog(this,
				toDateSetListener,
				mYear, mMonth, mDay);
		toDate.getDatePicker().setMaxDate(System.currentTimeMillis());
		toDate.setButton(DialogInterface.BUTTON_NEGATIVE,
				"Cancel",
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				isShow = false; //Cancel flag, used in mTimeSetListener
			}
		});
		return toDate;
	}
	return null;
}


// Register  toDatePickerDialog listener
private DatePickerDialog.OnDateSetListener toDateSetListener =
		new DatePickerDialog.OnDateSetListener() {                 // the callback received when the user "sets" the Date in the DatePickerDialog
	public void onDateSet(DatePicker view, int yearSelected,
			int monthOfYear, int dayOfMonth) {
		if(isShow == true)
		{
			isShow = false;
			monthSelected = ++monthOfYear;
			daySelected = dayOfMonth;
			String newYear = ""+yearSelected;
			TextView toDate = (TextView) findViewById(R.id.toDateView);
			toDateString = String.format(""+String.format("%02d", daySelected)+"-"+
					String.format("%02d", monthSelected)+"-"+newYear);
			toDate.setText(""+toDateString+" "+String.format("%02d",hourSelected)+":"+
					String.format("%02d",minuteSelected));
			toTime();
		}
	}
};

// Register  fromDatePickerDialog listener
private DatePickerDialog.OnDateSetListener fromDateSetListener =
		new DatePickerDialog.OnDateSetListener() {                 // the callback received when the user "sets" the Date in the DatePickerDialog
	public void onDateSet(DatePicker view, int yearSelected,
			int monthOfYear, int dayOfMonth) {
		if(isShow == true)
		{
			isShow = false;
			monthSelected = ++monthOfYear;
			daySelected = dayOfMonth;
			TextView fromDate = (TextView) findViewById(R.id.fromDateView);
			String newYear = ""+yearSelected;
			fromDateString = String.format(""+String.format("%02d", daySelected)+"-"+
					String.format("%02d", monthSelected)+"-"+newYear);
			fromDate.setText(""+fromDateString+" "+String.format("%02d",hourSelected)+":"+
					String.format("%02d",minuteSelected));
			fromTime();
		}
	}
};

public void toTime(){
	TimePickerDialog toTime = new TimePickerDialog(this,
			toTimeSetListener, mHour, mMinute, false);
	toTime.show();
}

public void fromTime(){

	TimePickerDialog fromTime = new TimePickerDialog(this,
			fromTimeSetListener, mHour, mMinute, false);
	fromTime.show();
}

// Register  TimePickerDialog listener                 
private TimePickerDialog.OnTimeSetListener fromTimeSetListener =
		new TimePickerDialog.OnTimeSetListener() {
	// the callback received when the user "sets" the TimePickerDialog in the dialog
	public void onTimeSet(TimePicker view, int hourOfDay, int min) {
		hourSelected = hourOfDay;
		minuteSelected = min;
		TextView fromDate = (TextView) findViewById(R.id.fromDateView);
		fromReportsDate = ""+fromDateString+" "+String.format("%02d",hourSelected)+":"+
				String.format("%02d",minuteSelected);
		fromDate.setText(""+fromDateString+" "+String.format("%02d",hourSelected)+":"+
				String.format("%02d",minuteSelected));
	}
};

// Register  TimePickerDialog listener                 
private TimePickerDialog.OnTimeSetListener toTimeSetListener =
		new TimePickerDialog.OnTimeSetListener() {
	// the callback received when the user "sets" the TimePickerDialog in the dialog
	public void onTimeSet(TimePicker view, int hourOfDay, int min) {
		hourSelected = hourOfDay;
		minuteSelected = min;
		TextView toDateTime = (TextView) findViewById(R.id.toDateView);
		toReportsDate = ""+toDateString+" "+String.format("%02d", hourSelected)+":"+
				String.format("%02d", minuteSelected);
		toDateTime.setText(""+toDateString+" "+String.format("%02d", hourSelected)+":"+
				String.format("%02d", minuteSelected));

	}
};

public static PRODUCTREPORTS getInstance(){
	return pRODUCTREPORTS;
}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
		return super.dispatchTouchEvent(ev);
	}
	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}
}

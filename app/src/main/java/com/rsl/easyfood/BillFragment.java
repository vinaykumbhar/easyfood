package com.rsl.easyfood;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.model.Cartpojo;
import com.rsl.utills.ConnectionDetector;
import com.rsl.utills.NonScrollListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class BillFragment extends Fragment {
    ConnectionDetector internet;
    SQLiteDatabase db;
    Button print;
    Button pay, card;
    EditText cash_amount;
    TextView time, orderid, table, guests, change_amount, resto_Address;
    //    NonScrollListView listView;
    ExpandableBillAdapter billoutAdapater;
    ExpandableListView listView;
    TextView dine;
    ArrayList<Cartpojo> cart;
    String productname;
    String producttype;
    Double baseprice = 0.00;
    SharedPreferences mSharedPreferences;
    int qty;
    String sno;
    int i;
    double subtotal;
    TextView billouttax, billoutsubtotal, billouttotal;
    // BilloutAdapater billoutAdapater;
    DecimalFormat sdf = new DecimalFormat("00.00");
    ArrayList<Cartpojo> ingrediant_list;

    public BillFragment() {
        // Required empty public constructor
    }

    //    double total = 0;
    double tax = 00.00;
    CustomDialogClass myAlert;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bill, container, false);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        resto_Address = (TextView) view.findViewById(R.id.resto_Address);
        time = (TextView) view.findViewById(R.id.billouttime);
        orderid = (TextView) view.findViewById(R.id.billoutorderid);
        table = (TextView) view.findViewById(R.id.billouttable);
        guests = (TextView) view.findViewById(R.id.billoutguests);
        listView = (ExpandableListView) view.findViewById(R.id.billoutlistview);
        print = (Button) view.findViewById(R.id.print);
        Group.GROUPID.titlebar.setText("Bill Out");
        pay = (Button) view.findViewById(R.id.pay);
        card = (Button) view.findViewById(R.id.card);
        dine = (TextView) view.findViewById(R.id.dine);
        billouttax = (TextView) view.findViewById(R.id.billouttax);
        billoutsubtotal = (TextView) view.findViewById(R.id.billoutsubtotal);
        billouttotal = (TextView) view.findViewById(R.id.billouttotal);
        change_amount = (TextView) view.findViewById(R.id.txt_change_amount);
        cash_amount = (EditText) view.findViewById(R.id.edt_cash_amount);
        internet = new ConnectionDetector(getActivity());

        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View itemView, int groupPosition, long itemId) {
//                listView.expandGroup(groupPosition);
                setListViewHeight(parent, groupPosition);
                return false;
            }
        });
        cash_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (s.equals("") || s.toString().trim().length() <= 0) {
                        Group.GROUPID.cashpaid = "";
                        Group.GROUPID.changegiven = "";
                        change_amount.setText("0.00");

                    } else if (subtotal > 0 && Double.parseDouble(s.toString()) > subtotal) {
                        double change = Double.parseDouble(s.toString()) - subtotal;
                        Group.GROUPID.cashpaid = String.format("%.2f", Double.parseDouble(s.toString()));
                        Group.GROUPID.changegiven = String.format("%.2f", change);
                        String grand_Amount = String.format("%.2f", change);
                        change_amount.setText(String.valueOf(grand_Amount));
                    } else {
                        Group.GROUPID.cashpaid = "";
                        Group.GROUPID.changegiven = "";
                        change_amount.setText("0.00");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Group.GROUPID.cashpaid = "";
                    Group.GROUPID.changegiven = "";
                    cash_amount.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        billoutsubtotal.setText(CartStaticSragment.CARTStaticSragment.grand);
        cart = new ArrayList<Cartpojo>();
        if (cart.size() != 0) {
            cart.clear();
        }

        Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM:SS");
        String nowDate = formatter.format(now.getTime());
        time.setText(nowDate.toString());
        Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
        if (Integer.parseInt(Group.GROUPID.table) < 1500) {
            dine.setText("DINE IN");
        } else {
            dine.setText("T/A");
        }

        db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);

        Cursor masterData = db.rawQuery("SELECT * from masterterminal;", null);
        int masterDatacount = masterData.getCount();
        if (masterDatacount > 0) {
            masterData.moveToFirst();

            String abc = masterData.getString(masterData.getColumnIndex("uadd_address_01"));
            String webadd = masterData.getString(masterData.getColumnIndex("website"));
            String obj = abc.replace("\n\n", "\n");

            // resto_Address.setText(masterData.getString(masterData.getColumnIndex("uadd_address_01")));
            resto_Address.setText(obj + "\n" + webadd);

        }
        masterData.close();

        if (CartStaticSragment.CARTStaticSragment.tableno.length() != 0) {
            Cursor cartData = db.rawQuery("SELECT * from cart WHERE tableno=" + Group.GROUPID.table + " AND billflag=0;", null);
            int count = cartData.getCount();
            subtotal = 0;
            ingrediant_list = new ArrayList<>();
            if (count > 0) {
                while (cartData.moveToNext()) {
                    i = i + 1;
                    Cartpojo cartpojo = new Cartpojo();
                    sno = cartData.getString(cartData.getColumnIndex("cart_id"));
                    productname = cartData.getString(cartData.getColumnIndex("menu_item_name"));
                    //  producttype = cartData.getString(cartData.getColumnIndex("product_type"));
                    Double baseprice = cartData.getDouble(cartData.getColumnIndex("menu_item_price"));
                    qty = cartData.getInt(cartData.getColumnIndex("menu_item_qty"));
                    String noOfGuest = cartData.getString(cartData.getColumnIndex("guestno"));
                    guests.setText(noOfGuest);
                    cartpojo.setPqty(String.valueOf(qty));
                    cartpojo.setName(productname);

                    // cartpojo.setType(producttype);

                    ArrayList<Cartpojo> list_ingredients = new ArrayList<>();
                    Cursor cartIngredients = db.rawQuery("SELECT * from ingredients WHERE cart_id_fk=" + sno + ";", null);
                    int ingredientCount = cartIngredients.getCount();
                    Double itemtotal = 0.00;

                    if (ingredientCount > 0) {
                        while (cartIngredients.moveToNext()) {
                            double subitem_total = 0.00;
                            Cartpojo cartIngreData = new Cartpojo();
                            String id = cartIngredients.getString(cartIngredients.getColumnIndex("id"));
                            String ingredient_name = cartIngredients.getString(cartIngredients.getColumnIndex("ingredient_name"));
                            Double ingredient_price = cartIngredients.getDouble(cartIngredients.getColumnIndex("ing_price"));
                            int ing_qty = cartIngredients.getInt(cartIngredients.getColumnIndex("ing_qty"));

                            Cursor subItmes = db.rawQuery("SELECT subitem_id,subitem_qty,subitem_name,subitem_price FROM subitems WHERE ing_id_fk=" + id + ";", null);
                            int subitemcount = subItmes.getCount();
                            Double subitemtotal = 0.00;
                            ArrayList<Cartpojo> sub_item_list = new ArrayList<>();
                            if (subitemcount > 0) {
                                while (subItmes.moveToNext()) {
                                    String subitem_id = subItmes.getString(subItmes.getColumnIndex("subitem_id"));
                                    int subitem_qty = subItmes.getInt(subItmes.getColumnIndex("subitem_qty"));
                                    String subitem_name = subItmes.getString(subItmes.getColumnIndex("subitem_name"));
                                    Double subitem_price = subItmes.getDouble(subItmes.getColumnIndex("subitem_price"));
                                    Log.e("subitem_name_id", "" + ingredient_name + "_" + subitem_name + "_" + subitem_id + "_" + subitem_qty);
                                    Cartpojo subItem = new Cartpojo();
                                    subItem.setSno(subitem_id);
                                    subItem.setName(subitem_name);
                                    subItem.setPprice(String.valueOf(subitem_price));
                                    subItem.setPqty(String.valueOf(subitem_qty));
                                    subitemtotal = subitemtotal + (subitem_qty * subitem_price);
                                    sub_item_list.add(subItem);
                                }
                            }
                            subitem_total = (ing_qty * ingredient_price) + subitemtotal;
                            cartIngreData.setItem_list(sub_item_list);
                            cartIngreData.setName(ingredient_name);
                            cartIngreData.setPprice(String.valueOf(subitem_total));
                            cartIngreData.setPqty(String.valueOf(ing_qty));
                            itemtotal = itemtotal + subitem_total;
                            list_ingredients.add(cartIngreData);
                        }
                    }

                    cartpojo.setItem_list(list_ingredients);
                    ingrediant_list.add(cartpojo);
                    baseprice = (baseprice * qty) + itemtotal;
                    subtotal = subtotal + baseprice;
                    cartpojo.setTotal(String.valueOf(baseprice));
                    cart.add(cartpojo);
                }
            }
//            if (cart.size()>0)
            // setListViewHeightBasedOnChildren(listView);
            billoutsubtotal.setText(getActivity().getString(R.string.pound_symbol) + " " + sdf.format(subtotal));
            billouttotal.setText(getActivity().getString(R.string.pound_symbol) + " " + sdf.format(subtotal));
            billouttax.setText(getActivity().getString(R.string.pound_symbol) + " " + sdf.format(tax));
        } else

        {
            AlertDialog.Builder msg = new AlertDialog.Builder(
                    getActivity(), android.R.style.Theme_Dialog);
            msg.setTitle("");
            msg.setMessage("Table Is Empty To Print ");
            msg.setPositiveButton("Ok", null);
            msg.show();
            guests.setText("");
        }
        db.close();
        orderid.setText("");
        table.setText(CartStaticSragment.CARTStaticSragment.tableno.getText().toString());
        //   guests.setText(CartStaticSragment.CARTStaticSragment.noofguests.getText().toString());
        billoutAdapater = new ExpandableBillAdapter(getActivity());
        listView.setAdapter(billoutAdapater);
        billoutAdapater.notifyDataSetChanged();
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        getActivity(), android.R.style.Theme_Dialog);
                msg.setMessage("Yet to be implemented");
                msg.setPositiveButton("Ok", null);
                msg.show();
            }
        });
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    if (cash_amount.getText().toString().equals("")) {
//                        AlertDialog.Builder msg = new AlertDialog.Builder(
//                                getActivity(), android.R.style.Theme_Dialog);
//                        msg.setMessage("Enter Cash given");
//                        msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                        msg.show();

                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                getActivity(), android.R.style.Theme_Dialog);
                        msg.setTitle("Amount Payable " + sdf.format(subtotal));
                        msg.setMessage("Proceed to Pay The bill ?");
                        msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                generateJson jsoncall = new generateJson();
                                jsoncall.execute();
                            }
                        });
                        msg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        msg.show();

                    } else if (Double.parseDouble(cash_amount.getText().toString()) - subtotal < 0) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                getActivity(), android.R.style.Theme_Dialog);
                        msg.setMessage("Enter valid amount");
                        msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        msg.show();
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                getActivity(), android.R.style.Theme_Dialog);
                        msg.setTitle("Amount Payable " + sdf.format(subtotal));
                        msg.setMessage("Proceed to Pay The bill ?");
                        msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                generateJson jsoncall = new generateJson();
                                jsoncall.execute();
//                                addRowsToDisplay pay = new addRowsToDisplay();
//                                pay.execute(Group.GROUPID.table.toString());
                            }
                        });
                        msg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        msg.show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        print.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                setPrint();
            }
        });
        return view;
    }


    private String editTable;
    float grandTotal = 0;
    String grand;

    @SuppressLint("StaticFieldLeak")
    private class addRowsToDisplay extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            editTable = params[0];

//            db.execSQL("UPDATE cart SET  billqty = orderqty-billedqty, billtypeqty = ordertypeqty-billedtypeqty WHERE tableno = " + editTable + ";");

            Cursor cartData = db.rawQuery("SELECT * from cart WHERE tableno=" + editTable + " AND billflag = 0;", null);
            int count = cartData.getCount();
            grandTotal = 0;
            if (count > 0) {
                cartData.moveToLast();
                do {
                    int productqty = cartData.getInt(cartData.getColumnIndex("menu_item_qty"));
                    Double productprice = cartData.getDouble(cartData.getColumnIndex("menu_item_price"));
                    String cart_id = cartData.getString(cartData.getColumnIndex("cart_id"));
                    Cursor cartIngredients = db.rawQuery("SELECT id,ing_price,ing_qty from ingredients WHERE cart_id_fk=" + cart_id + ";", null);
                    int ingredientCount = cartIngredients.getCount();
                    Double itemtotal = 0.00;
                    if (ingredientCount > 0) {
                        while (cartIngredients.moveToNext()) {
                            double subitem_total = 0.00;
                            String id = cartIngredients.getString(cartIngredients.getColumnIndex("id"));
                            Double ingredient_price = cartIngredients.getDouble(cartIngredients.getColumnIndex("ing_price"));
                            int ing_qty = cartIngredients.getInt(cartIngredients.getColumnIndex("ing_qty"));
                            Cursor subItmes = db.rawQuery("SELECT subitem_qty,subitem_price FROM subitems WHERE ing_id_fk=" + id + ";", null);
                            int subitemcount = subItmes.getCount();
                            Double subitemtotal = 0.00;
                            if (subitemcount > 0) {
                                while (subItmes.moveToNext()) {
                                    int subitem_qty = subItmes.getInt(subItmes.getColumnIndex("subitem_qty"));
                                    Double subitem_price = subItmes.getDouble(subItmes.getColumnIndex("subitem_price"));
                                    subitemtotal = subitemtotal + (subitem_qty * subitem_price);
                                }
                            }
                            subitem_total = (ing_qty * ingredient_price) + subitemtotal;
                            itemtotal = itemtotal + subitem_total;
                            subItmes.close();
                        }
                    }
                    productprice = (productprice * productqty) + itemtotal;
                    grandTotal = (float) (grandTotal + productprice);
                    cartIngredients.close();
                } while (cartData.moveToPrevious());
            }
            Log.e("grandTotal", "=" + grandTotal);
//            for (Integer j = 0; j < count; j++) {
//                int billtypeqty = c.getInt(c.getColumnIndex("ordertypeqty")) - c.getInt(c.getColumnIndex("billedtypeqty"));
//                float total = (Float.parseFloat(c.getString(c.getColumnIndex("productprice"))) * Integer.parseInt(c.getString(c.getColumnIndex("billqty"))) +
//                        Float.parseFloat(c.getString(c.getColumnIndex("typeprice"))) * billtypeqty);
//                grandTotal = grandTotal + total;
//            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                String Query = "SELECT cart_id FROM cart WHERE tableno = " + editTable + " AND billflag= 0;";

                Cursor productDetails = db.rawQuery(Query, null);
                int count = productDetails.getCount();
                db.close();
                if (count > 0) {
                    Group.getInstance().totalAmount = grandTotal;
                    Group.getInstance().subTotalAmount = grandTotal;
                    cashpay();
                } else {
                    db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                    db.execSQL("UPDATE cart SET checkflag = 0 WHERE tableno=" +
                            editTable + " AND checkflag = 1 AND billflag = 1;");
                    db.close();
                    Intent intent = new Intent(getActivity(), Home.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        }
    }

    public void cashpay() {
        String USERNAME = "";
        float cash = 0;
        {
            if (Group.GROUPID.finaltotal == null) {
                payBill();
                cash = Float.parseFloat(Group.GROUPID.finaltotal);
            } else {
                payBill();
                cash = Float.parseFloat(Group.GROUPID.finaltotal);
            }
            if (cash > 0) {
                db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                ////
                Cursor shopDetails = db.rawQuery("SELECT res_name,uadd_address_01,uadd_address_02,uadd_city,phone,website,contact_name FROM masterterminal;", null);
                int shopCount = shopDetails.getCount();
                if (shopCount > 0) {                        //Here we are inserting the masterterminal into the structure
                    shopDetails.moveToFirst();
                    USERNAME = shopDetails.getString(shopDetails.getColumnIndex("contact_name"));
                }
                //////
                String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                String datetime = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
                Cursor recieptno = db.rawQuery("SELECT * FROM receiptnos WHERE receipthead = '" + datetime + "' ;", null);
                if (recieptno.getCount() == 0) {
                    if (cash > 0) {
                        db.execSQL("INSERT INTO receiptnos VALUES('" + datetime + "',1);");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('E" + datetime + "1','CASH', '0'," + cash + " ,'" + timeStamp + "','" + USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }
                } else {
                    if (cash > 0) {
                        recieptno.moveToLast();
                        String newReceiptNo = "E" + recieptno.getString(0) + (recieptno.getInt(1) + (int) 1);
                        db.execSQL("UPDATE receiptnos SET receiptnumber = receiptnumber+1 WHERE receipthead= '" + recieptno.getString(0) + "';");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('" + newReceiptNo + "','CASH', '0'," + cash + " ,'" + timeStamp + "','" + USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }
                }
                db.close();
                Intent intent = new Intent(getActivity(), Printer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("TABLENO", Integer.parseInt(editTable));
                intent.putExtra("FLAG", 1);
                startActivity(intent);
            }
        }
    }

    public void payBill() {
        double grandTotal = 0;
        db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor cartData = db.rawQuery("SELECT * from cart WHERE tableno=" + Group.GROUPID.table + " AND billflag = 0;", null);
        int count = cartData.getCount();
        if (count > 0) {
            cartData.moveToLast();
            do {
                int productqty = cartData.getInt(cartData.getColumnIndex("menu_item_qty"));
                Double productprice = cartData.getDouble(cartData.getColumnIndex("menu_item_price"));
                String cart_id = cartData.getString(cartData.getColumnIndex("cart_id"));
                Cursor cartIngredients = db.rawQuery("SELECT id,ing_price,ing_qty from ingredients WHERE cart_id_fk=" + cart_id + ";", null);
                int ingredientCount = cartIngredients.getCount();
                Double itemtotal = 0.00;
                if (ingredientCount > 0) {
                    while (cartIngredients.moveToNext()) {
                        double subitem_total = 0.00;
                        String id = cartIngredients.getString(cartIngredients.getColumnIndex("id"));
                        Double ingredient_price = cartIngredients.getDouble(cartIngredients.getColumnIndex("ing_price"));
                        int ing_qty = cartIngredients.getInt(cartIngredients.getColumnIndex("ing_qty"));
                        Cursor subItmes = db.rawQuery("SELECT subitem_qty,subitem_price FROM subitems WHERE ing_id_fk=" + id + ";", null);
                        int subitemcount = subItmes.getCount();
                        Double subitemtotal = 0.00;
                        if (subitemcount > 0) {
                            while (subItmes.moveToNext()) {
                                int subitem_qty = subItmes.getInt(subItmes.getColumnIndex("subitem_qty"));
                                Double subitem_price = subItmes.getDouble(subItmes.getColumnIndex("subitem_price"));
                                subitemtotal = subitemtotal + (subitem_qty * subitem_price);
                            }
                        }
                        subitem_total = (ing_qty * ingredient_price) + subitemtotal;
                        itemtotal = itemtotal + subitem_total;
                        subItmes.close();
                    }
                }
                productprice = (productprice * productqty) + itemtotal;
                grandTotal = grandTotal + productprice;
                cartIngredients.close();
            } while (cartData.moveToPrevious());
        }
        cartData.close();
        db.close();
//        db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
//        Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);
//        int taxDetailsCount = taxDetails.getCount();
        double tax1 = 0;
//        if (taxDetailsCount > 0) {
//            taxDetails.moveToFirst();            //Here we are inserting the tax information into the structure
//            tax1 = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
//            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
//            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
//            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));
//
//        }
//        db.close();
        grand = String.format("%.2f", grandTotal);
        double taxAmount = (tax1 * grandTotal) / 100;
        String grand_tax = String.format("%.2f", taxAmount);
        grandTotal += (tax1 * grandTotal) / 100;
        String grand_Amount = String.format("%.2f", grandTotal);
        Group.GROUPID.finaltotal = grand_Amount;
    }

    public void setPrint() {
        if (CartStaticSragment.CARTStaticSragment.tableno.getText().toString().length() > 0) {
            Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
            db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);

            //    Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid = " +
            //         UserLoginActivity.user_LoginID.LOCATIONNO + " AND tableno = " + Group.GROUPID.table +";", null);

            Cursor cartData = db.rawQuery("SELECT * from cart WHERE tableno = " + Group.GROUPID.table + ";", null);
            int count = cartData.getCount();
            db.close();
            if (count > 0) {
                Intent intent = new Intent(getActivity(), Printer.class);
                intent.putExtra("TABLENO", Integer.parseInt(CartStaticSragment.CARTStaticSragment.tableno.getText().toString()));
                // intent.putExtra("LOCATIONNO", Integer.parseInt(UserLoginActivity.user_LoginID.LOCATIONNO));
                intent.putExtra("FLAG", 0);
                startActivity(intent);

            } else {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        getActivity(), android.R.style.Theme_Dialog);
                msg.setTitle("");
                msg.setMessage("Table Is Empty To Print ");
                msg.setPositiveButton("Ok", null);
                msg.show();
            }
        }
    }

    static class ViewHolder {
        TextView billoutname, bilouttype, billoutamount;
        TextView sno;
    }

    public class ExpandableBillAdapter extends BaseExpandableListAdapter {
        private Activity activity;

        public ExpandableBillAdapter(Activity a) {
            activity = a;
        }

        @Override
        public int getGroupCount() {
            return cart.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return cart.get(groupPosition).getItem_list().size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return null;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return cart.get(groupPosition).getItem_list().get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            TextView productname;
            TextView qtyvalue, totalvalue;
            ImageView delete;
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.cartadapter, null);
            }

            if (groupPosition == 0)
                convertView.findViewById(R.id.grpDivider).setVisibility(View.INVISIBLE);
            else
                convertView.findViewById(R.id.grpDivider).setVisibility(View.VISIBLE);
            productname = (TextView) convertView.findViewById(R.id.productname);
            qtyvalue = (TextView) convertView.findViewById(R.id.value);
            totalvalue = (TextView) convertView.findViewById(R.id.Totalvalue1);
            convertView.findViewById(R.id.deleteview).setVisibility(View.GONE);

            qtyvalue.setText(cart.get(groupPosition).getPqty());
            productname.setText(cart.get(groupPosition).getName());
            String totalcost = String.format("%.2f", Double.parseDouble(cart.get(groupPosition).getTotal()));
            totalvalue.setText(getActivity().getString(R.string.pound_symbol) + " " + totalcost);

            listView.expandGroup(groupPosition);
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.row_cart_ingredients, null);
            }
            Cartpojo p = cart.get(groupPosition).getItem_list().get(childPosition);

            final TextView txt_item_name = (TextView) convertView.findViewById(R.id.txt_item_name);
            final TextView txt_item_cost = (TextView) convertView.findViewById(R.id.txt_item_cost);
            final NonScrollListView listsubitems = (NonScrollListView) convertView.findViewById(R.id.listsubitems);
            String name = p.getPqty() + "x " + p.getName();
//            Double tot = Double.parseDouble(p.getPprice()) * Integer.parseInt(p.getPqty());
            Double tot = Double.parseDouble(p.getPprice());
            txt_item_name.setText(name);
            txt_item_cost.setText(getActivity().getString(R.string.pound_symbol) + " " + String.format(Locale.US, "%.2f", tot));
            ArrayList<String> subdata = new ArrayList<>();
            if (p.getItem_list().size() > 0) {
                for (Cartpojo pojo : p.getItem_list()) {
                    String str = pojo.getPqty() + "x " + pojo.getName();
                    subdata.add(str);
                }
                ArrayAdapter<String> oppor_adapter = new ArrayAdapter<String>(getActivity(), R.layout.row_cart_ing_subitems, subdata);
                listsubitems.setAdapter(oppor_adapter);
                listsubitems.setDividerHeight(0);
            } else {
                ArrayAdapter<String> oppor_adapter = new ArrayAdapter<String>(getActivity(), R.layout.row_cart_ing_subitems, subdata);
                listsubitems.setAdapter(oppor_adapter);
                listsubitems.setDividerHeight(0);
            }
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }


    public class BilloutAdapater extends BaseAdapter {
        private Activity activity;
        private LayoutInflater inflater = null;

        public BilloutAdapater(Activity a) {
            activity = a;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return cart.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            ViewHolder holder = null;
            if (vi == null) {
                // if it's not recycled, initialize some attributes
                final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.billoutadapter, parent, false);
                holder = new ViewHolder();
                holder.billoutname = (TextView) vi.findViewById(R.id.billoutitemname);
                holder.billoutamount = (TextView) vi.findViewById(R.id.billoutamount);
                holder.bilouttype = (TextView) vi.findViewById(R.id.billouttype);
                holder.sno = (TextView) vi.findViewById(R.id.sno);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }
            holder.sno.setText(cart.get(position).getPqty());
            holder.billoutname.setText(cart.get(position).getName());
            holder.billoutamount.setText(sdf.format(Double.parseDouble(cart.get(position).getTotal())));

            return vi;
        }
    }

    JSONObject parent_jObject;

    private class generateJson extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            parent_jObject = new JSONObject();
            Log.e("onPreExecute", "PlaceData1");
        }

        @SuppressLint("DefaultLocale")
        @Override
        protected String doInBackground(String... strings) {
            try {
                db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                //get all menuitems from cart
                Cursor cartData = db.rawQuery("SELECT cart_id,menu_item_id_fk,menu_item_name,menu_item_price,menu_item_qty,menu_id,include_price,product_type FROM cart WHERE tableno=" + Group.GROUPID.table.toString() + " AND billflag=0;", null);
                int count = cartData.getCount();
                JSONArray mainItemDetail_JArray = new JSONArray();
                if (count > 0) {
                    JSONObject menu_item_object = new JSONObject();
                    //  cartData.moveToLast();
                    double order_total = 00.00;
                    double deliveryCharges = 00.00;
                    double cart_total = 00.00;
                    JSONArray menuItemDetail_JArray = new JSONArray();
                    int itemrowpos = 0;
                    JSONObject mItemRow = new JSONObject();
                    while (cartData.moveToNext()) {//looping for menuitems from cart
                        double subtotal = 00.00;
                        String cart_id = cartData.getString(cartData.getColumnIndex("cart_id"));
                        String productname = cartData.getString(cartData.getColumnIndex("menu_item_name"));
                        int menu_id = cartData.getInt(cartData.getColumnIndex("menu_id"));
                        int menu_item_id = cartData.getInt(cartData.getColumnIndex("menu_item_id_fk"));
                        Double productprice = cartData.getDouble(cartData.getColumnIndex("menu_item_price"));
                        int productqty = cartData.getInt(cartData.getColumnIndex("menu_item_qty"));
                        int include_price = cartData.getInt(cartData.getColumnIndex("include_price"));
                        int product_type = cartData.getInt(cartData.getColumnIndex("product_type"));
                        Double totax = 0.00;//Double.parseDouble(cartData.getString(cartData.getColumnIndex("totaltax")));

                        JSONObject menuItemDetail_JObj = new JSONObject();
                        menuItemDetail_JObj.put("rowid", itemrowpos);
                        menuItemDetail_JObj.put("assign_menu_id", menu_id);
                        menuItemDetail_JObj.put("restaurant_id", mSharedPreferences.getString("mid", ""));
                        menuItemDetail_JObj.put("id", menu_item_id);
                        menuItemDetail_JObj.put("name", productname);
                        menuItemDetail_JObj.put("qty", productqty);

                        //getting the Ingredients data from the ingredients table associated with the menuitem in cart
                        Cursor cartMenuitem = db.rawQuery("SELECT id,ing_id,ing_qty,ingredient_name,ing_price FROM ingredients WHERE cart_id_fk=" + cart_id + ";", null);
                        int itemcount = cartMenuitem.getCount();
                        Double itemtotal = 0.00;
                        Object optObject;
                        if (itemcount > 0) {
                            JSONObject mOptions = new JSONObject();
                            optObject = new JSONObject();
                            int optionsrowpos = 0;
                            //JSONObject moptionsRow = new JSONObject();
                            while (cartMenuitem.moveToNext()) {//looping for menuitem ingredients
                                JSONObject optionsData = new JSONObject();
                                String id = cartMenuitem.getString(cartMenuitem.getColumnIndex("id"));
                                String ing_id = cartMenuitem.getString(cartMenuitem.getColumnIndex("ing_id"));
                                int ing_qty = cartMenuitem.getInt(cartMenuitem.getColumnIndex("ing_qty"));
                                String ing_name = cartMenuitem.getString(cartMenuitem.getColumnIndex("ingredient_name"));
                                Double ing_price = cartMenuitem.getDouble(cartMenuitem.getColumnIndex("ing_price"));
                                Double subitemtotal = 0.00;
                                Cursor subItmes = db.rawQuery("SELECT subitem_id,subitem_name,subitem_qty,subitem_price FROM subitems WHERE ing_id_fk=" + id + ";", null);
                                int subitemcount = subItmes.getCount();
                                Object suboptObject;
                                if (subitemcount > 0) {
                                    suboptObject = new JSONObject();
                                    int subitemsrowpos = 0;
                                    while (subItmes.moveToNext()) {
                                        JSONObject subitemsData = new JSONObject();
                                        int subitem_id = subItmes.getInt(subItmes.getColumnIndex("subitem_id"));
                                        String subitem_name = subItmes.getString(subItmes.getColumnIndex("subitem_name"));
                                        int subitem_qty = subItmes.getInt(subItmes.getColumnIndex("subitem_qty"));
                                        Double subitem_price = subItmes.getDouble(subItmes.getColumnIndex("subitem_price"));
                                        double subitemTotal = (subitem_qty * subitem_price);

                                        subitemsData.put("rowid", subitemsrowpos);
                                        subitemsData.put("label", subitem_name);
                                        subitemsData.put("id", subitem_id);
                                        subitemsData.put("opt_qty", subitem_qty);
                                        subitemsData.put("price", String.format(Locale.US, "%.2f", subitemTotal));

                                        subitemtotal = subitemtotal + (subitem_qty * subitem_price);
                                        ((JSONObject) suboptObject).put(String.valueOf(subitemsrowpos), subitemsData);
                                        subitemsrowpos++;
                                    }
                                } else {
                                    suboptObject = new JSONArray();
                                }
                                subItmes.close();


                                double subitem_total = (ing_qty * ing_price) + subitemtotal;

                                itemtotal = itemtotal + subitem_total;
                                optionsData.put("rowid", optionsrowpos);
                                optionsData.put("label", ing_name);
                                optionsData.put("id", ing_id);
                                optionsData.put("opt_qty", ing_qty);
                                optionsData.put("price", String.format(Locale.US, "%.2f", subitem_total));
                                optionsData.put("selectedopt", suboptObject);

                                ((JSONObject) optObject).put(String.valueOf(optionsrowpos), optionsData);
                                optionsrowpos++;
                            }
                        } else {
                            optObject = new JSONArray();
                        }
                        if (include_price == 1)
                            subtotal = (productqty * productprice) + itemtotal;
                        else
                            subtotal = itemtotal;


                        menuItemDetail_JObj.put("customername", "");

                        if (product_type == 3) {
                            menuItemDetail_JObj.put("price", subtotal);
                            menuItemDetail_JObj.put("is_meal", true);
                        } else if (product_type == 1) {
                            menuItemDetail_JObj.put("price", productprice);
                            menuItemDetail_JObj.put("is_meal", false);
                        } else if (product_type == 2) {
                            menuItemDetail_JObj.put("price", subtotal);
                            menuItemDetail_JObj.put("is_meal", false);
                        }

                        menuItemDetail_JObj.put("notes", false);
                        menuItemDetail_JObj.put("options", optObject);
                        menuItemDetail_JObj.put("subtotal", String.format(Locale.US, "%.2f", subtotal));
                        menuItemDetail_JObj.put("is_coupon", 0);
                        cart_total = cart_total + subtotal;
                        mItemRow.put(String.valueOf(itemrowpos), menuItemDetail_JObj);
                        itemrowpos++;
                    }
                    //Log.e("mItemRow", "=" + mItemRow);
                    order_total = cart_total + deliveryCharges;
                    menuItemDetail_JArray.put(mItemRow);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
                    parent_jObject.put("requested_datetime", sdf.format(new Date()));
                    menu_item_object.put("delivery_charges", 0);
                    menu_item_object.put("order_total", String.format(Locale.US, "%.2f", order_total)); //order_total+delivery charges
                    menu_item_object.put("cart_total", String.format(Locale.US, "%.2f", cart_total));//menuitems + ingredients + subitems total
                    menu_item_object.put("tax_amount", 0);
                    menu_item_object.put("total_discount", 0);
                    menu_item_object.put("payment_mode", "COD");
                    menu_item_object.put("merchant_id", mSharedPreferences.getString("mid", ""));
                    menu_item_object.put("is_paid", 0);
                    menu_item_object.put("menu_item_data", menuItemDetail_JArray);
                    mainItemDetail_JArray.put(menu_item_object);
                }
                parent_jObject.put("order_data", mainItemDetail_JArray);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return parent_jObject.toString();
        }

        @Override
        protected void onPostExecute(final String orderjson) {
            super.onPostExecute(orderjson);
            if (orderjson != null) {
                Log.e("onPostExecute", "=" + orderjson);
                if (isAdded() && internet.isConnectingToInternet())
                    sendOrder(orderjson);
                else {
                    internet.showAlertDialog(getActivity());
                }
            }
        }
    }

    private void sendOrder(final String mRequestBody) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Sending Order...");
        dialog.setCancelable(false);
        dialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        String URL = getString(R.string.web_path) + "order_service/addOrder";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("normalResponse", "=" + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY_ERROR", error.toString());
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                            mRequestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                    dialog.dismiss();
                }
                return volleyError;
            }

            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response.statusCode);
                    Log.e("parseNetworkResponse", "=" + responseString);
                    // can get more details such as response.headers
                }
                Map<String, String> responseHeaders = response.headers;
                if (response.statusCode == 200) {
                    final Response<String> result = Response.success(responseHeaders.get("Content-Length"), HttpHeaderParser.parseCacheHeaders(response));
                    Log.e("sendOrder", "parseNetworkResponse" + parseToString(response));
                    final String mResponse = parseToString(response);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (mResponse != null) {
                                    JSONObject jsonObject = new JSONObject(mResponse);
                                    myAlert = new CustomDialogClass(getActivity(), jsonObject.getString("msg"));
                                    myAlert.show();

//                                    AlertDialog.Builder msg = new AlertDialog.Builder(
//                                            getActivity(), android.R.style.Theme_Dialog);
//                                    msg.setTitle("");
//                                    msg.setMessage(jsonObject.getString("msg"));
//                                    msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            addRowsToDisplay pay = new addRowsToDisplay();
//                                            pay.execute(Group.GROUPID.table.toString());
//                                        }
//                                    });
//                                    msg.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
//                    addRowsToDisplay pay = new addRowsToDisplay();
//                    pay.execute(Group.GROUPID.table.toString());
                }
                if (dialog.isShowing())
                    dialog.dismiss();
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                return params;
            }

            public String parseToString(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return parsed;
            }
        };
        requestQueue.add(stringRequest);
    }//SendOrder

    class CustomDialogClass extends Dialog {
        public Activity context;
        public Button ok, no;
        String msg;

        public CustomDialogClass(@NonNull Activity context, String msg) {
            super(context);
            this.context = context;
            this.msg = msg;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_alert_dialog);
            this.setCanceledOnTouchOutside(false);
            this.setCancelable(false);
            ok = (Button) findViewById(R.id.btn_yes);
            no = (Button) findViewById(R.id.btn_no);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addRowsToDisplay pay = new addRowsToDisplay();
                    pay.execute(Group.GROUPID.table.toString());
                    if (myAlert != null)
                        myAlert.dismiss();
                }
            });
        }
    }

    private void setListViewHeight(ExpandableListView listView,
                                   int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    public static void setListViewHeightBasedOnChildren(ExpandableListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, null);
            if (listItem != null) {
                int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                        View.MeasureSpec.EXACTLY);
                // listItem.setLayoutParams(new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                //listItem.measure(0,0);
                listItem.setLayoutParams(new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}



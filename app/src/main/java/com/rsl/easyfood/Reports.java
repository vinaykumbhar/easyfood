package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.lang.Integer.parseInt;

/**
 * @author EMBDES.
 * @version 1.0.
 *          This Activity doing all the reports printing operations.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Reports extends Activity {

    //// ���뷽ʽ
    SQLiteDatabase db;
    BluetoothDevice device;
    String btPrinterid;
    /**
     * printing text align left
     */
    public static final int AT_LEFT = 0;
    /**
     * printing text align center
     */
    public static final int AT_CENTER = 1;
    /**
     * printing text align right
     */
    public static final int AT_RIGHT = 2;
    boolean bConnect = true;
    String fdot1;

    class reportValueDetails {
        String header;
        String name;
        float value;

        public reportValueDetails(String newHeader, String newName, float newValue) {
            header = newHeader;
            name = newName;
            value = newValue;
        }
    }

    ;

    class itemDetails {
        String name;
        int qty;
        Double price;

        public itemDetails(String newName, int newQty, Double newPrice) {
            name = newName;
            price = newPrice;
            qty = newQty;
        }

    }

    ;

    class headerDetails {
        String title;
        String firstDate;
    }

    BluetoothAdapter mBluetoothAdapter;
    private static final int REQUEST_ENABLE_BT = 2;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    InputStream mmInStream;
    static OutputStream mmOutStream;
    BluetoothSocket mmSocket;
    public static Socket mysocket = null;
    private static int SERVERPORT = 9100;
    private static String SERVER_IP = "192.168.1.87";
    Dialog settingDialog;
    ListView lv;
    TextView titlebar, date;
    String printdata;
    SharedPreferences mSharedPreferences;
    ArrayAdapter<BluetoothDevice> adapter;
    ArrayList<BluetoothDevice> connections = new ArrayList<BluetoothDevice>();
    ArrayAdapter<String> adaptername;
    ArrayList<String> connectionsname = new ArrayList<String>();
    String globledevice;
    int mReportsFlag;
    String mstartDate/*, mendDate*/;
    boolean printerDeviceCheck = false;
    private static Reports REPORTSID;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.reports_display);
        REPORTSID = this;
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Reports.this, Home.class);
                startActivity(intent);
            }
        });
        mSharedPreferences = getSharedPreferences("printerpref", MODE_PRIVATE);
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);

        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String startDate = bundle.getString("FROMDATE");
        int ReportsFlag = bundle.getInt("REPORTSFLAG");

        Log.e("FROMDATE", "" + startDate);

        mstartDate = startDate;
        mReportsFlag = ReportsFlag;
        fdot1 = String.format("-------------------------------------------\n");
        if (mSharedPreferences.getBoolean("isWifiPrinterEnable", false)) {

            String strName = "192.168.1.92:5500";

            db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
            db.close();
            if (strName.length() == 0) {
                Toast.makeText(Reports.this, "Error:port name empty", Toast.LENGTH_SHORT).show();
                return;
            }

            Toast.makeText(Reports.this, strName, Toast.LENGTH_SHORT).show();

            if (bConnect) {
                try {
                    if (mysocket == null) {
                        new ClientThread().execute("ok");
                    }
                } finally {
                    if (ReportsFlag == 0) {
                        groupReports(startDate);
                        Pcut();
                    } else if (ReportsFlag == 1) {
                        setTitle("				PRODUCT REPORTS		");
                        productReports(startDate);
                    }
                    bConnect = false;
                }
            } else {
                bConnect = true;
            }
        } else {

            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                // Device does not support Bluetooth
            }
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }

            String restoredText = mSharedPreferences.getString("btPrinterid", null);
            if (restoredText != null) {
                btPrinterid = mSharedPreferences.getString("btPrinterid", null);
                device = mBluetoothAdapter
                        .getRemoteDevice(btPrinterid);
                if (ReportsFlag == 0) {
                    if (!mSharedPreferences.getBoolean("isWifiPrinterEnable", false)) {
                        deviceconnection();
                    } else {
                        groupReports(startDate);
                        Pcut();
                    }
                } else if (ReportsFlag == 1) {
                    setTitle("				PRODUCT REPORTS		");
                    productReports(startDate);
                }

            } else {
                ListPairedDevices();
            }
        }
        final ImageView home = (ImageView) findViewById(R.id.imageView11);
        final ImageView opentabs = (ImageView) findViewById(R.id.imageView3);
        final ImageView printmenu = (ImageView) findViewById(R.id.imageView2);
        printmenu.setVisibility(View.INVISIBLE);
        opentabs.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            public void onClick(View v) {
                onBackPressed();
                Intent intent = new Intent(Reports.this, BILLSPENDINGTABLES.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

        home.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("static-access")
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(Reports.this, Home.class);
                intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    protected void onResume() {
        super.onResume();
    }

    ;

    public class ClientThread extends AsyncTask<String, Void, Boolean> {

		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isconnected = false;
        ProgressDialog pDialog;

        public void onPreExecute() {
            isconnected = false;
            pDialog = new ProgressDialog(Reports.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            isconnected = false;
            pDialog.show();
        }

        public Boolean doInBackground(final String... args) {
            try {
                mysocket = new Socket();
                mysocket.connect(new InetSocketAddress(SERVER_IP, SERVERPORT), 10000);
                mmOutStream = mysocket.getOutputStream();
                mmInStream = mysocket.getInputStream();
                isconnected = true;
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
                isconnected = false;
            } catch (IOException e1) {
                e1.printStackTrace();
                isconnected = false;
            }

            return false;
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }
            if (isconnected == true) {

            } else {
                if (mysocket != null) {
                    try {
                        mysocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mysocket = null;
                }
                Toast.makeText(getApplicationContext(), "Selected WIFi Printer is not online", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (settingDialog != null) {
            if (settingDialog.isShowing()) {
                settingDialog.dismiss();
            }
            settingDialog = null;

        }
        if (mysocket != null) {
            try {
                mysocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mysocket = null;
        }
    }

    /**
     * @return void
     * @brief Reports:: grouptReports here we are passing staring data and ending date to
     * get the reports according to the group between that dates.
     */

    public void groupReports(String astartDate) {
        boolean flag = false;
        headerDetails header = new headerDetails();
        reportValueDetails[] valueDetails = new reportValueDetails[1000];
        int valueCount = 0;
        float totalValue = 0;
        String newHeader = "easyFood";
        String newName;
        Float newValue;
        SQLiteDatabase db;
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT res_name FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {
            shopDetails.moveToFirst();
            header.title = shopDetails.getString(shopDetails.getColumnIndex("res_name"));//copying shop name into the struct
            header.firstDate = astartDate;//copying Starting date into the strut
        } else {
            db.close();
            Toast.makeText(Reports.this, "No Data in master_terminal", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Reports.this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return;
        }

        Cursor cartMenuitem = db.rawQuery("select m.menu_item_id_fk,m.menu_item_name, sum(m.menu_item_qty), sum(menu_item_price),n.cartdatetime FROM allOrders a, newOrderMenuitem m,newOrder n where a.order_id = m.order_id AND a.order_id=n.order_id AND n.cartdatetime LIKE '" + astartDate + "%' GROUP BY menu_item_id_fk;", null);
        int cartCount = cartMenuitem.getCount();
        Double totalCost = 0.0;


        HashMap<String, Float> totalGrp = new HashMap<>();
        if (cartCount > 0) {
            while (cartMenuitem.moveToNext()) {
                String menu_item_id = cartMenuitem.getString(0);
                String menu_item_name = cartMenuitem.getString(1);
                int menu_item_qty = cartMenuitem.getInt(2);
                float menu_item_price = cartMenuitem.getFloat(3);
                String valMonth = cartMenuitem.getString(4);
                totalCost += menu_item_price;

                Cursor menuGroup = db.rawQuery("select g.menu_name FROM menugroup g, menuitem m WHERE g.menu_id = m.menu_id AND m.menu_item_id=" + menu_item_id + " GROUP BY g.menu_id;", null);
                Log.e("groupCount", "" + menuGroup.getCount());

                if (menuGroup.getCount() > 0) {
                    while (menuGroup.moveToNext()) {
                        String group_name = menuGroup.getString(0);
                        if (totalGrp.containsKey(group_name)) {
                            float d = totalGrp.get(group_name) + menu_item_price;
                            totalGrp.put(group_name, d);
                        } else {
                            totalGrp.put(group_name, menu_item_price);
                        }
                        Log.e("group_name", "" + group_name);
                    }
                }
                menuGroup.close();
            }
        }

        int i = 0;
        if (totalGrp.size() > 0) {
            for (Map.Entry<String, Float> d : totalGrp.entrySet()) {
                String key = d.getKey();
                Log.e("grpName", "" + key);
                if (d.getValue() > 0) {
                    valueDetails[i] = new reportValueDetails(newHeader, String.valueOf(Html.fromHtml(key)), d.getValue());
                    totalValue += d.getValue();
                    i++;
                }
            }
            groupPrintFormat(header, valueDetails, i, totalValue, 1);
        } else {
            db.close();
            Toast.makeText(Reports.this, "No Products available between that dates", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        cartMenuitem.close();
        db.close();

        return;
    }

    /**
     * @param startDate
     * @param endDate
     * @return void
     * @brief Reports::productReports here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */
    public void productReports(String sstartDate) {
        itemDetails productDetails[] = new itemDetails[1000];

        int productCount = 0;
        headerDetails header = new headerDetails();
//        String startDate = reverseDateFormat(sstartDate);

        Log.e("sstartDate", "" + sstartDate);
//        Log.e("startDate", "" + startDate);
        String newName;

        Float newPrice;

        SQLiteDatabase db;
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT res_name FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {
            shopDetails.moveToFirst();
            header.title = shopDetails.getString(0);//copying shop name into the struct
            header.firstDate = sstartDate.split(" ")[0];//copying Starting date into the struct

        } else {
            Toast.makeText(Reports.this, "No Data in master_terminal", Toast.LENGTH_SHORT).show();
            return;
        }
        shopDetails.close();
        Cursor cartMenuitem = db.rawQuery("select m.menu_item_id_fk,m.menu_item_name, sum(m.menu_item_qty), sum(menu_item_price),n.cartdatetime FROM allOrders a, newOrderMenuitem m,newOrder n where a.order_id = m.order_id AND a.order_id=n.order_id AND n.cartdatetime LIKE '" + sstartDate + "%' GROUP BY menu_item_id_fk;", null);
        int cartCount = cartMenuitem.getCount();

        Double totalCost = 0.0;
        if (cartCount > 0) {
            while (cartMenuitem.moveToNext()) {
                String menu_item_id = cartMenuitem.getString(0);
                String menu_item_name = cartMenuitem.getString(1);
                int menu_item_qty = cartMenuitem.getInt(2);
                Double menu_item_price = cartMenuitem.getDouble(3);
                String valMonth = cartMenuitem.getString(4);
                totalCost += menu_item_price;
                Log.e("valMonth", "" + valMonth);
                if (menu_item_price > 0) {
                    productDetails[productCount] = new itemDetails(menu_item_name, menu_item_qty, menu_item_price);
                    productCount++;
                }
                Log.e("menu_item_name", "" + menu_item_name + "_" + menu_item_qty + "_" + menu_item_price);


            }
        } else {
            db.close();
            Toast.makeText(Reports.this, "No Products available between that dates", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(Reports.this, Home.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);
            finish();
            return;
        }
        Log.e("totalCost", "=" + totalCost);
        cartMenuitem.close();
        db.close();
        productPrintFormat(header, productDetails, productCount, 5);
    }


    /**
     * @param header
     * @param totalSum
     * @param FormatFlag
     * @return void
     * @brief Reports::printFormat here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */

    public void groupPrintFormat(headerDetails header, reportValueDetails printableData[], int printableCount, float totalSum, int FormatFlag) {
        String tempBuffer = null;
        float sumOfValue = 0;
        float sumOfPercent = 0;
        String cut;
        for (int i = 1; i < printableCount; i++) {
            for (int j = 0; j < i; j++) {
                if (printableData[i].name != null && printableData[j].name != null) {

                    if (printableData[i].name.equals(printableData[j].name))    //Here we are adding the quantity of the same products
                    {
                        printableData[j].value = printableData[j].value + printableData[i].value;
                        printableData[i].value = 0;

                    }
                }
            }
        }
        tempBuffer = String.format("%s\n", header.title);

        tempBuffer += String.format("Group Report\n");

        tempBuffer += String.format("Date: %s\n", header.firstDate);
        //Copying Start & End dates into Buffer

        tempBuffer += String.format("Groups                      Value   Percent\n");


        int k = 0;
        for (k = 0; k < printableCount; k++) {
            if (printableData[k].value != 0) {
                if (printableData[k].name.length() > 18) {
                    cut = printableData[k].name.substring(0, 18);
                } else {
                    cut = printableData[k].name;
                }

                tempBuffer += String.format("%-18s%15.2f%10.2f\n", cut, printableData[k].value,
                        (printableData[k].value * 100) / totalSum);
                sumOfValue = sumOfValue + printableData[k].value;
                sumOfPercent = sumOfPercent + ((printableData[k].value * 100) / totalSum);

            }
        }
        if (k > 0) {
            tempBuffer += String.format("%-18s Total %26.2f%10.2f\n%s", fdot1, sumOfValue, sumOfPercent, fdot1);
            TextView groupReport = (TextView) findViewById(R.id.groupProductReports);
            TextView categoryReport = (TextView) findViewById(R.id.categoryReport);
            TextView userReport = (TextView) findViewById(R.id.userReport);

            groupReport.setText(tempBuffer);


            if (mSharedPreferences.getBoolean("isWifiPrinterEnable", false)) {
                Begin();
                write((tempBuffer).getBytes());
                write(("\r\n").getBytes());
            } else {
                printdata = tempBuffer;
                Begin();
                write((tempBuffer + "\n").getBytes());
            }
        }
        return;
    }


    /**
     * @param header
     * @param printableData
     * @param printableCount
     * @param FormatFlag
     * @return void
     * @brief Reports::productPrintFormat here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */

    public void productPrintFormat(headerDetails header, itemDetails printableData[], int printableCount, int FormatFlag) {
        String tempBuffer = null;
        int sumOfQty = 0;
        Double sumOfAmount = 0.0;
        String cut = null;

        for (int i = 1; i < printableCount; i++)                //Checking for Same Name Items
        {
            for (int j = 0; j < i; j++) {
                if (printableData[i].name != null && printableData[j].name != null)     //Checking for NULL values
                {
                    if (printableData[i].name.equals(printableData[j].name))//Checking for Items with same Name
                    {
                        printableData[j].qty = printableData[j].qty + printableData[i].qty;//Adding qty of same products
                        printableData[j].price = printableData[j].price + printableData[i].price;//Adding price of same products
                        printableData[i].qty = 0;        //making qty of new Item as 0 to avoid at printing time
                        printableData[i].price = 0.0;        //making qty of new Item as 0 to avoid at printing time

                    }
                }
            }
        }

        if (FormatFlag == 4) {
            tempBuffer = String.format("%-30s\n", header.title);        //Copying title to disply on top of the print into Buffer

            tempBuffer += String.format("Details       Receipts    Value \n"); //Copying column headings to Buffer

        } else if (FormatFlag == 5) {
            tempBuffer = String.format("%s\n", header.title);            //Copying shop name into Buffer


            tempBuffer += String.format("Product Report\n");            //Copying subtitle into Buffer


            tempBuffer += String.format("Report Date: %s\n", header.firstDate);//Report date

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            final Calendar now = Calendar.getInstance();
            String nowDate = formatter.format(now.getTime());
            date.setText(nowDate);
            tempBuffer += String.format("Print Date: %s\n", nowDate);//Print date
            //Copying Start & End dates into Buffer


            tempBuffer += String.format("Products Sold               Total     Value \n");    //Copying column headings into Buffer


        }
        int k;
        for (k = 0; k < printableCount; k++) {
            if (printableData[k].qty != 0)                    //Checing for Items with qty=0 to avoid in print
            {

                //Decreasing the size of the product name to 14 charecters
                if (printableData[k].name.length() > 26) {
                    cut = printableData[k].name.substring(0, 26);
                } else {
                    cut = printableData[k].name;
                }
                tempBuffer += String.format("%-26s%5d%12.2f\n", cut, printableData[k].qty, printableData[k].price);
                sumOfQty = sumOfQty + printableData[k].qty;        //Adding all Items qty to dispalay at the bottom of the Print
                sumOfAmount = sumOfAmount + printableData[k].price;    //Adding all Items price to dispalay at the bottom of the Print
            }
        }
        if (k > 0) {
            tempBuffer += String.format("%-18s Total %24d%12.2f\n%s", fdot1, sumOfQty, sumOfAmount, fdot1);
            //Copying footer information into Buffer

            if (4 == FormatFlag) {
                TextView receiptReport = (TextView) findViewById(R.id.receiptReport);
                receiptReport.setVisibility(View.VISIBLE);
                receiptReport.setText(tempBuffer);
            } else if (5 == FormatFlag) {
                TextView report = (TextView) findViewById(R.id.groupProductReports);
                report.setTypeface(Typeface.MONOSPACE);
                report.setText(tempBuffer);
            }


            if (mSharedPreferences.getBoolean("isWifiPrinterEnable", false)) {
                Begin();
                write((tempBuffer).getBytes());
                write(("\r\n").getBytes());
            } else if (5 == FormatFlag) {
                printdata = tempBuffer;
                deviceconnection();
            } else {
                printdata = tempBuffer;
                Begin();
                write((tempBuffer + "\n").getBytes());
                try {
                    mmSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Intent intent = new Intent(Reports.this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        return;
    }

    /**
     * @return void
     * @brief reverseDateFormat here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */

    public String reverseDateFormat(String date) {
        //return date;
        return date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2) + date.substring(10, 16);
    }


    /**
     * Here we are creating instance for class
     *
     * @return
     */

    public static Reports getInstance() {
        return REPORTSID;
    }

    @SuppressLint("NewApi")
    public synchronized void connect(BluetoothDevice device) throws IOException {

        BluetoothSocket tmp = null;
        // Get a BluetoothSocket to connect with the given BluetoothDevice
        try {
            Method m;
            m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
            tmp = (BluetoothSocket) m.invoke(device, 1);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(Reports.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(Reports.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(Reports.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(Reports.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        }
        mmSocket = tmp;
        mBluetoothAdapter.cancelDiscovery();
        new Connection().execute("ok");
    }

    public class Connection extends AsyncTask<String, Void, Boolean> {

		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isconnected = false;
        ProgressDialog pDialog;

        public void onPreExecute() {
            pDialog = new ProgressDialog(Reports.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            isconnected = false;
            pDialog.show();
        }

        public Boolean doInBackground(final String... args) {
            try {
                mmSocket.connect();
                mmInStream = mmSocket.getInputStream();
                mmOutStream = mmSocket.getOutputStream();
                isconnected = true;
            } catch (IOException e) {
                e.printStackTrace();
                isconnected = false;
            }

            return false;
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }
            if (mReportsFlag == 0) {
                groupReports(mstartDate);
                Pcut();
            }
            if (isconnected == true) {
                if (mReportsFlag == 0) {
                } else {
                    Begin();
                    write((printdata + "\n").getBytes());
                    Pcut();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Selected Printer is not Online", Toast.LENGTH_LONG).show();
            }
            try {
                mmSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void ListPairedDevices() {

        Set<BluetoothDevice> bondedDevices = BluetoothAdapter
                .getDefaultAdapter().getBondedDevices();
        for (int k = 0; k < bondedDevices.size(); k++) {
        }
        if (bondedDevices.size() > 0) {
            for (BluetoothDevice bondDeice : bondedDevices) {
                connections.add(bondDeice);
                connectionsname.add(bondDeice.getName());
                printerDeviceCheck = true;

            }
        }
        if (printerDeviceCheck == true) {
            printerDeviceCheck = false;
            Device_Select();
        } else {
            Toast.makeText(getApplicationContext(), "Printer not found", Toast.LENGTH_SHORT).show();
        }

    }

    private void Device_Select() {
        settingDialog = new Dialog(Reports.this);
        settingDialog.setContentView(R.layout.fragment_scanner_device_selection);
        settingDialog.setTitle("Paired Device");
        settingDialog.setCancelable(true);
        lv = (ListView) settingDialog.findViewById(R.id.list);
        settingDialog.show();
        adapter = new ArrayAdapter<BluetoothDevice>(this, android.R.layout.simple_list_item_1, connections);
        adaptername = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, connectionsname);
        lv.setAdapter(adaptername);


        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                try {
                    globledevice = adapter.getItem(arg2).getAddress();

                    mSharedPreferences.edit().putString("btPrinterid", globledevice).apply();
                    device = mBluetoothAdapter
                            .getRemoteDevice(globledevice);
                    if (mReportsFlag == 0) {
                        if (!mSharedPreferences.getBoolean("isWifiPrinterEnable", false)) {
                            deviceconnection();

                        } else {
                            groupReports(mstartDate);
                            Pcut();
                        }
                    } else if (mReportsFlag == 1) {
                        setTitle("				PRODUCT REPORTS		");
                        productReports(mstartDate);
                    }
//					
                } catch (RuntimeException e) {
                    Intent mIntent = new Intent();
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.setClass(getApplicationContext(), Home.class);
                    startActivity(mIntent);
                    Toast.makeText(Reports.this, " insert run time exception" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                settingDialog.dismiss();

            }
        });
    }

    private void deviceconnection() {

        try {
            connect(device);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void Begin() {
        WakeUpPritner();
        InitPrinter();
    }

    public static void InitPrinter() {
        byte[] combyte = new byte[]{(byte) 27, (byte) 64};

        // BT_Write(combyte);
        try {
            if (mmOutStream != null) {
                mmOutStream.write(combyte);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void WakeUpPritner() {
        byte[] b = new byte[3];

        try {
            if (mmOutStream != null) {
                mmOutStream.write(b);
            }
            Thread.sleep(100L);
        } catch (Exception var2) {
            var2.printStackTrace();
        }
    }

    public static void Pcut() {
        byte[] pcut = new byte[]{(byte) 0x1D, (byte) 0x56, (byte) 0x42, (byte) 0x00};

        // BT_Write(combyte);
        try {
            if (mmOutStream != null) {
                mmOutStream.write(pcut);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* Call this from the main activity to send data to the remote device */
    public void write(byte[] bytes) {
        try {
            byte[] cAlign = {(byte) 0x1b, (byte) 0x61, (byte) 0x01};
            if (mmOutStream != null) {
                mmOutStream.write(cAlign);
                mmOutStream.write(bytes);
            }
        } catch (IOException e) {
            Toast.makeText(this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }
}

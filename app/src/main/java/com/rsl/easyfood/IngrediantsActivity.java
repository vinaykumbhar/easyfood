package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.instabug.library.InstabugTrackingDelegate;
import com.rsl.model.OptionsData;
import com.rsl.utills.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.instabug.library.Instabug.getApplicationContext;

public class IngrediantsActivity extends Activity implements View.OnClickListener {
    SQLiteDatabase db;
    TextView restro_name, textName, textitemDesc;
    ImageView imgClose;
    Button btn_close, btn_add;
    ArrayList<OptionsData> mListAttributes = new ArrayList<>();
    ArrayList<OptionsData> optionlist = new ArrayList<>();
    ArrayList<OptionsData> list = new ArrayList<>();
    private LinearLayout mListViewOptions, mListViewOtherOptions;
    SharedPreferences mSharedPreferences;
    String res_name = "";
    String menuid = "";
    String menuitemid = "";
    String menuname = "";
    int include_price = 0;
    String menudescription = "";
    String menuitemurl = "";
    Double item_price = 00.0;
    String menuitemname = "";
    ConnectionDetector internet;
    private int product_type = 0;
    HashMap<String, HashMap<String, String>> ingr_data_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingrediants);
        imgClose = (ImageView) findViewById(R.id.imgClose);
        btn_close = (Button) findViewById(R.id.btn_close);
        btn_add = (Button) findViewById(R.id.btn_add);
        restro_name = (TextView) findViewById(R.id.restro_name);
        textName = (TextView) findViewById(R.id.textName);
        textitemDesc = (TextView) findViewById(R.id.textitemDesc);
        mListViewOptions = (LinearLayout) findViewById(R.id.linear_ListOptions);
        mListViewOtherOptions = (LinearLayout) findViewById(R.id.linear_ListView);
        internet = new ConnectionDetector(IngrediantsActivity.this);
        ingr_data_list = new HashMap<>();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        //add ClickListener to widget
        imgClose.setOnClickListener(this);
        btn_close.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        Bundle b = getIntent().getExtras();
        assert b != null;
//        String configitemid = b.getString("configitemid");
//        String residfk = b.getString("res_id_fk");
//        String itemidfk = b.getString("item_id_fk");
        menuid = b.getString("menuid");
        menuitemid = b.getString("menuitemid");

        // String baseprice = b.getString("item_base_price");
        db = openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
        Cursor masterData = db.rawQuery("SELECT * from masterterminal;", null);
        int masterDatacount = masterData.getCount();
        if (masterDatacount > 0) {
            masterData.moveToFirst();
            res_name = masterData.getString(masterData.getColumnIndex("res_name"));
            restro_name.setText(res_name + ":");
        }
        masterData.close();
        Cursor menuitemData = db.rawQuery("SELECT menu_item_name,price,product_type,imgurl,menu_item_desc,include_price FROM menuitem WHERE menu_item_id=" + menuitemid + " AND menu_id=" + menuid + ";", null);
        int menuitemcount = menuitemData.getCount();
        if (menuitemcount > 0) {
            menuitemData.moveToFirst();
            menuname = menuitemData.getString(menuitemData.getColumnIndex("menu_item_name"));
            item_price = menuitemData.getDouble(menuitemData.getColumnIndex("price"));
            menuitemurl = menuitemData.getString(menuitemData.getColumnIndex("imgurl"));
            product_type = menuitemData.getInt(menuitemData.getColumnIndex("product_type"));
            menudescription = menuitemData.getString(menuitemData.getColumnIndex("menu_item_desc"));
            include_price = menuitemData.getInt(menuitemData.getColumnIndex("include_price"));

            Cursor attributes = db.rawQuery("SELECT menu_attributes_id,attribute_type,attribute_name FROM attributes WHERE menu_item_id=" + menuitemid + " AND menu_id=" + menuid + ";", null);
            int attributescount = attributes.getCount();
            mListAttributes = new ArrayList<>();
            if (attributescount > 0) {
                attributes.moveToFirst();
                int attribute_id = attributes.getInt(attributes.getColumnIndex("menu_attributes_id"));
                String attrName = attributes.getString(attributes.getColumnIndex("attribute_name"));
                String attrType = attributes.getString(attributes.getColumnIndex("attribute_type"));

                OptionsData d = new OptionsData();
                d.setId(String.valueOf(attribute_id));
                d.setText(attrName);
                d.setType(attrType);

                ArrayList<OptionsData> mList = new ArrayList<>();
                Cursor options = db.rawQuery("SELECT configitemid,option_data,attribute_price FROM options WHERE main_attribute_id=" + attribute_id + " AND res_id_fk=" + mSharedPreferences.getString("mid", "") + " AND item_id_fk=" + menuitemid + ";", null);
                int optionscount = options.getCount();
                if (optionscount > 0) {
                    while (options.moveToNext()) {
                        int configid = options.getInt(options.getColumnIndex("configitemid"));
                        String optName = options.getString(options.getColumnIndex("option_data"));
                        Double optPrice = options.getDouble(options.getColumnIndex("attribute_price"));
                        OptionsData data = new OptionsData();
                        data.setId(String.valueOf(configid));
                        data.setText(optName);
                        data.setPrice(optPrice);
                        Log.e("name_price", "" + optName + " : " + optPrice + " : " + configid);
                        mList.add(data);
                    }
                }
                d.setListOptions(mList);
                mListAttributes.add(d);
            }
            setOptions();
        }


        menuitemData.close();
        textName.setText(menuname);
        textitemDesc.setText(menudescription);
        Log.e("item_price", "=" + item_price);


    }//onCreate

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgClose:
                finish();
                break;
            case R.id.btn_close:
                finish();
                break;
            case R.id.btn_add:
                insertTocart();
                break;

        }//switch
    }//onClick

    private void insertTocart() {
        if (rgOptions != null) {
            if (rgOptions.getCheckedRadioButtonId() == -1) {
                Toast t = Toast.makeText(IngrediantsActivity.this, "Please select options", Toast.LENGTH_SHORT);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                return;
            }
            if (include_price == 0)
                item_price = 0.00;
        }

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
        String nowDate = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());

        Cursor c1 = db.rawQuery("SELECT orderno FROM ordernos WHERE orderno LIKE '" + nowDate.toString() + "%';", null);
        int orderNOCount = c1.getCount();

        String oldOrderNO = "0";
        if (orderNOCount == 0) {
            oldOrderNO = nowDate.toString() + "1";
            db.execSQL("INSERT INTO ordernos values(1," + Group.GROUPID.table + ",'" + oldOrderNO + "', 0, 0);");
        } else {
            c1.moveToLast();
            String orderNumber = c1.getString(0);
            Cursor orderNoCheck = db.rawQuery("SELECT * FROM ordernos WHERE tableno =" + Group.GROUPID.table + " AND billflag = 0 ;", null);
            if (orderNoCheck.getCount() > 0) {
                orderNoCheck.moveToFirst();
                oldOrderNO = orderNoCheck.getString(orderNoCheck.getColumnIndex("orderno"));
            } else {
                Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);
                int occupiedCount = chairs.getCount();
                chairs.moveToFirst();
                if (occupiedCount > 0) {
                    int billNoIncrement = Integer.parseInt(orderNumber.substring(6, orderNumber.length())) + 1;
                    oldOrderNO = orderNumber.substring(0, 6) + billNoIncrement;
                    db.execSQL("INSERT INTO ordernos values(1," + Group.GROUPID.table + ",'" + oldOrderNO + "', 0, 0);");
                }
            }
        }
        c1.close();
        Log.e("item_price", "=" + item_price);

        ContentValues values = new ContentValues();
        values.put("menu_item_id_fk", menuitemid);
        values.put("tableno", Group.GROUPID.table);
        if (Integer.parseInt(Group.GROUPID.table) > 0 && Integer.parseInt(Group.GROUPID.table) < 1500) {
            values.put("ordertype", "Dine In");
        } else
            values.put("ordertype", "Counter Service");
        values.put("menu_item_name", menuname);
        values.put("menu_item_price", item_price);
        values.put("menu_item_qty", "1");
        values.put("guestno", CartStaticSragment.CARTStaticSragment.noofguests.getText().toString());
        values.put("menu_item_url", menuitemurl);
        values.put("menu_id", menuid);
        values.put("product_type", product_type);
        values.put("include_price", include_price);
        values.put("cartdatetime", timeStamp);
        values.put("billflag", 0);
        values.put("orderflag", 0);
        values.put("checkflag", 0);
        long cart_id = db.insert("cart", null, values);
        Log.e("cart_id", "=" + cart_id);
        if (ingr_data_list.size() > 0) {
            if (cart_id > 0) {
                for (Map.Entry<String, HashMap<String, String>> entry : ingr_data_list.entrySet()) {
                    HashMap<String, String> data = entry.getValue();
                    db.execSQL("INSERT INTO ingredients(ing_id,ing_qty,cart_id_fk,ingredient_name,ing_price,include_price)" +
                            " values(" + data.get("id") + "," + data.get("qty") + "," + cart_id + ",'" + data.get("name") + "'," + data.get("price") + ",1);");
                }
            }
        }

        Cursor tableData = db.rawQuery("SELECT occupiedchairs from tables WHERE tableno=" + Group.GROUPID.table + ";", null);
        tableData.moveToFirst();
        if (tableData.getInt(tableData.getColumnIndex("occupiedchairs")) == 0) {
            db.execSQL("UPDATE tables SET occupiedchairs = " + TablesFragment.COVERS + " WHERE  tableno =" + Group.GROUPID.table + ";");
        }

        db.close();
        finish();
        Group.GROUPID.alert();

    }

    private void getOtheroptions(final String configId, final String resid, final String product_id) {
        final ProgressDialog dialog = new ProgressDialog(IngrediantsActivity.this);
        dialog.setMessage(getString(R.string.str_loading_item_details));
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(IngrediantsActivity.this);
        String requestURL = getString(R.string.web_path) + "menu_item_service/getOtheroptions/?mainid=" + configId + "&resid=" + resid + "&product_id=" + product_id;
        Log.e("requestURL", " : " + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.e("getOtheroptions_response", result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONArray jsonArray = new JSONArray(result);
                            if (jsonArray.length() > 0) {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                                list = new ArrayList<>();
                                JSONObject jObjData = null;
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    OptionsData data = new OptionsData();
                                    jObjData = jsonArray.getJSONObject(i);

                                    data.setId(jObjData.getString("id"));
                                    data.setText(jObjData.getString("text"));
                                    data.setType(jObjData.getString("type"));

                                    JSONArray jsonArrayPrice = jObjData.getJSONArray("options_price");
                                    JSONArray jsonArrayids = jObjData.getJSONArray("saved_options_ids");

                                    ArrayList<OptionsData> list_option = new ArrayList<>();
                                    if (jObjData.getJSONObject("options").length() > 0) {
                                        Iterator<String> j = jObjData.getJSONObject("options").keys();
                                        for (int m = 0; m < jObjData.getJSONObject("options").length(); m++) {
                                            OptionsData data1 = new OptionsData();
                                            String k = j.next();
                                            //Log.e("Info", "Key: " + k + ", value: " + jObjData.getJSONObject("options").getString(k));
                                            data1.setId(jsonArrayids.get(m).toString());
                                            data1.setText(String.valueOf(Html.fromHtml(jObjData.getJSONObject("options").getString(k))));
                                            data1.setPrice(Double.parseDouble(jsonArrayPrice.get(m).toString()));
                                            list_option.add(data1);
                                        }
                                    }
                                    data.setListOptions(list_option);
                                    list.add(data);
                                }
                                setdata();

                            } else {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Log.e("getOtheroptions", "exception=" + e.toString());
                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOtheroptions

    RadioGroup rgOptions, rgOtherOptions;

    private void setOptions() {
        if (mListAttributes.size() > 0) {
            mListViewOptions.removeAllViews();
            for (int p = 0; p < mListAttributes.size(); p++) {//list all attributes
                LayoutInflater inflater2 = null;
                inflater2 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mLinearViewOptins = null;
                final OptionsData obj = mListAttributes.get(p);
                final String itemName = obj.getText();
                if (obj.getType().equalsIgnoreCase("radio")) {
                    mLinearViewOptins = inflater2.inflate(R.layout.row_radio_options, null);
                    final TextView ingr_name = (TextView) mLinearViewOptins.findViewById(R.id.ingr_name);
                    ingr_name.setText(itemName + ":");
                    final RadioButton[] rb = new RadioButton[obj.getListOptions().size()];
                    rgOptions = (RadioGroup) mLinearViewOptins.findViewById(R.id.radio);
                    for (final OptionsData obj1 : obj.getListOptions()) {//list all options
                        rb[p] = new RadioButton(this);
                        rgOptions.addView(rb[p]);
                        rb[p].setButtonDrawable(R.drawable.radio_selector);
                        rb[p].setText(obj1.getText() + "(" + obj1.getPrice() + ")");
                        rb[p].setTextSize(20);
                        rb[p].setPadding(0, 5, 0, 5);
                        rb[p].setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                        rb[p].setGravity(Gravity.START);
                        rb[p].setTextColor(getResources().getColor(R.color.black));
                        rb[p].setTag(p);

                        final int finalK = p;
                        rb[p].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (isChecked) {
                                    Log.e("config_id", obj1.getId());
                                    Log.e("opt_name", obj1.getText());
                                    Log.e("opt_price", "=" + obj1.getPrice());

                                    rgOptions.setTag(String.valueOf(obj1.getPrice()));

                                    for (OptionsData tempobj : obj.getListOptions()) {
                                        for (Map.Entry<String, HashMap<String, String>> entry : ingr_data_list.entrySet()) {
                                            if (entry.getKey().equals(tempobj.getId())) {
                                                ingr_data_list.remove(tempobj.getId());
                                            }
                                        }
                                    }
                                    HashMap<String, String> data = new HashMap<>();
                                    data.put("id", obj1.getId());
                                    data.put("name", obj1.getText());
                                    data.put("qty", String.valueOf("1"));
                                    data.put("price", String.valueOf(obj1.getPrice()));
                                    ingr_data_list.put(obj1.getId(), data);

                                    if (internet.isConnectingToInternet())
                                        getOtheroptions(obj1.getId(), mSharedPreferences.getString("mid", ""), menuitemid);//(config_id,res_id,menuitemid)
                                    else
                                        internet.showAlertDialog(IngrediantsActivity.this);
                                }
                            }
                        });

                    }


                } else if (obj.getType().equalsIgnoreCase("multiple")) {
                    if (mLinearViewOptins == null)
                        mLinearViewOptins = inflater2.inflate(R.layout.row_options, null);

                    final TextView ingr_name = (TextView) mLinearViewOptins.findViewById(R.id.ingr_name);
                    LinearLayout mlayoutoptions = (LinearLayout) mLinearViewOptins.findViewById(R.id.layoutoptions);

                    ingr_name.setText(itemName + ":");

                    for (int i = 0; i < obj.getListOptions().size(); i++) {
                        final OptionsData obj1 = obj.getListOptions().get(i);
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View mOptions = inflater.inflate(R.layout.row_multiselect_options, null);
                        mOptions.setTag(String.valueOf(i));

                        final TextView optionname = (TextView) mOptions.findViewById(R.id.optionname);
                        final TextView txtqty = (TextView) mOptions.findViewById(R.id.txtqty);
                        final TextView txttotalcost = (TextView) mOptions.findViewById(R.id.txttotalcost);

                        final ImageButton imgbtnincrement = (ImageButton) mOptions.findViewById(R.id.imgbtnincrement);
                        final ImageButton imgbtndecrement = (ImageButton) mOptions.findViewById(R.id.imgbtndecrement);
                        final LinearLayout layouttotal = (LinearLayout) mOptions.findViewById(R.id.layouttotal);
                        layouttotal.setTag(String.valueOf(i));
                        txtqty.setTag(String.valueOf(i));
                        txttotalcost.setTag(String.valueOf(i));

                        final TextView optionprice = (TextView) mOptions.findViewById(R.id.optionprice);
                        optionname.setText(obj1.getText());
                        optionprice.setText(String.format(Locale.US, "%.2f", obj1.getPrice()));
                        final int finalI = i;
                        imgbtnincrement.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int qty = Integer.parseInt(txtqty.getText().toString());
                                if (qty >= 0) {
                                    qty++;
                                }
                                Double total = qty * obj1.getPrice();
                                txtqty.setText(String.valueOf(qty));
                                txttotalcost.setText(String.format(Locale.US, "%.2f", total));
                                layouttotal.setVisibility(View.VISIBLE);

                                HashMap<String, String> data = new HashMap<>();
                                data.put("id", obj1.getId());
                                data.put("name", obj1.getText());
                                data.put("qty", String.valueOf(qty));
                                data.put("price", String.valueOf(obj1.getPrice()));
                                ingr_data_list.put(obj1.getId(), data);

                            }
                        });
                        imgbtndecrement.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int qty = Integer.parseInt(txtqty.getText().toString());
                                if (qty > 0) {
                                    qty--;
                                } else {
                                    qty = 0;
                                }
                                if (qty == 0) {
                                    layouttotal.setVisibility(View.GONE);
                                    txtqty.setText(String.valueOf(qty));
                                    ingr_data_list.remove(obj1.getId());
                                } else {
                                    Double total = qty * obj1.getPrice();
                                    txtqty.setText(String.valueOf(qty));
                                    txttotalcost.setText(String.format(Locale.US, "%.2f", total));
                                    HashMap<String, String> data = new HashMap<>();
                                    data.put("id", obj1.getId());
                                    data.put("name", obj1.getText());
                                    data.put("qty", String.valueOf(qty));
                                    data.put("price", String.valueOf(obj1.getPrice()));
                                    ingr_data_list.put(obj1.getId(), data);
                                }
                            }
                        });
                        mlayoutoptions.addView(mOptions);
                    }
                }
                mListViewOptions.addView(mLinearViewOptins);
            }
        } else {
            AlertDialog.Builder adb = new AlertDialog.Builder(IngrediantsActivity.this, android.R.style.Theme_Dialog);
            adb.setTitle("");
            adb.setMessage("Menu item not configured yet");
            adb.setPositiveButton("Ok", null);
            adb.show();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setdata() {
        mListViewOtherOptions.removeAllViews();
        for (int p = 0; p < list.size(); p++) {
            LayoutInflater inflater2 = null;
            inflater2 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mLinearViewOptins = null;
            final OptionsData obj = list.get(p);
            final String itemName = obj.getText();
            if (obj.getType().equalsIgnoreCase("radio")) {
                mLinearViewOptins = inflater2.inflate(R.layout.row_radio_options, null);
                final TextView ingr_name = (TextView) mLinearViewOptins.findViewById(R.id.ingr_name);
                ingr_name.setText(itemName + ":");
                final RadioButton[] rb = new RadioButton[obj.getListOptions().size()];
                rgOtherOptions = (RadioGroup) mLinearViewOptins.findViewById(R.id.radio);
                for (final OptionsData obj1 : obj.getListOptions()) {
                    rb[p] = new RadioButton(this);

                    rb[p].setButtonDrawable(R.drawable.radio_selector);
                    rb[p].setText(obj1.getText() + "(" + obj1.getPrice() + ")");
                    rb[p].setTextSize(20);
                    rb[p].setPadding(0, 5, 0, 5);
                    rb[p].setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                    rb[p].setGravity(Gravity.START);
                    rb[p].setTextColor(getResources().getColor(R.color.black));
                    rgOtherOptions.addView(rb[p]);
                    rb[p].setTag(p);
                    final int finalK = p;
                    rb[p].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                for (OptionsData tempobj : obj.getListOptions()) {
                                    for (Map.Entry<String, HashMap<String, String>> entry : ingr_data_list.entrySet()) {
                                        if (entry.getKey().equals(tempobj.getId())) {
                                            ingr_data_list.remove(tempobj.getId());
                                        }
                                    }
                                }

                                HashMap<String, String> data = new HashMap<>();
                                data.put("id", obj1.getId());
                                data.put("name", obj1.getText());
                                data.put("qty", String.valueOf("1"));
                                data.put("price", String.valueOf(obj1.getPrice()));
                                ingr_data_list.put(obj1.getId(), data);
                            }
                        }
                    });

                }
            } else if (obj.getType().equalsIgnoreCase("multiple")) {
                if (mLinearViewOptins == null)
                    mLinearViewOptins = inflater2.inflate(R.layout.row_options, null);

                final TextView ingr_name = (TextView) mLinearViewOptins.findViewById(R.id.ingr_name);
                LinearLayout mlayoutoptions = (LinearLayout) mLinearViewOptins.findViewById(R.id.layoutoptions);

                ingr_name.setText(itemName + ":");

                for (int i = 0; i < obj.getListOptions().size(); i++) {
                    final OptionsData obj1 = obj.getListOptions().get(i);
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View mOptions = inflater.inflate(R.layout.row_multiselect_options, null);
                    mOptions.setTag(String.valueOf(i));

                    final TextView optionname = (TextView) mOptions.findViewById(R.id.optionname);
                    final TextView txtqty = (TextView) mOptions.findViewById(R.id.txtqty);
                    final TextView txttotalcost = (TextView) mOptions.findViewById(R.id.txttotalcost);

                    final ImageButton imgbtnincrement = (ImageButton) mOptions.findViewById(R.id.imgbtnincrement);
                    final ImageButton imgbtndecrement = (ImageButton) mOptions.findViewById(R.id.imgbtndecrement);
                    final LinearLayout layouttotal = (LinearLayout) mOptions.findViewById(R.id.layouttotal);
                    layouttotal.setTag(String.valueOf(i));
                    txtqty.setTag(String.valueOf(i));
                    txttotalcost.setTag(String.valueOf(i));

                    final TextView optionprice = (TextView) mOptions.findViewById(R.id.optionprice);
                    optionname.setText(obj1.getText());
                    optionprice.setText(String.format(Locale.US, "%.2f", obj1.getPrice()));
                    final int finalI = i;
                    imgbtnincrement.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int qty = Integer.parseInt(txtqty.getText().toString());
                            if (qty >= 0) {
                                qty++;
                            }
                            Double total = qty * obj1.getPrice();
                            txtqty.setText(String.valueOf(qty));
                            txttotalcost.setText(String.format(Locale.US, "%.2f", total));
                            layouttotal.setVisibility(View.VISIBLE);

                            HashMap<String, String> data = new HashMap<>();
                            data.put("id", obj1.getId());
                            data.put("name", obj1.getText());
                            data.put("qty", String.valueOf(qty));
                            data.put("price", String.valueOf(obj1.getPrice()));
                            ingr_data_list.put(obj1.getId(), data);

                        }
                    });
                    imgbtndecrement.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int qty = Integer.parseInt(txtqty.getText().toString());
                            if (qty > 0) {
                                qty--;
                            } else {
                                qty = 0;
                            }
                            if (qty == 0) {
                                layouttotal.setVisibility(View.GONE);
                                txtqty.setText(String.valueOf(qty));
                                ingr_data_list.remove(obj1.getId());
                            } else {
                                Double total = qty * obj1.getPrice();
                                txtqty.setText(String.valueOf(qty));
                                txttotalcost.setText(String.format(Locale.US, "%.2f", total));
                                HashMap<String, String> data = new HashMap<>();
                                data.put("id", obj1.getId());
                                data.put("name", obj1.getText());
                                data.put("qty", String.valueOf(qty));
                                data.put("price", String.valueOf(obj1.getPrice()));
                                ingr_data_list.put(obj1.getId(), data);
                            }
                        }
                    });
                    mlayoutoptions.addView(mOptions);
                }
            }
            mListViewOtherOptions.addView(mLinearViewOptins);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

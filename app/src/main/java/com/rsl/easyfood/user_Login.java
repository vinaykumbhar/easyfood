package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.onesignal.OneSignal;
import com.rsl.utills.SessionManager;

import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

@SuppressLint("NewApi")
public class user_Login extends Activity {
    static String nameid;
    TextView date;
    String terminal;
    EditText email, passWord;
    View view;
    TextView titlebar;
    ImageView imageView;
    private BluetoothAdapter mBluetoothAdapter;
    public static user_Login user_LoginID;
    private static final int REQUEST_ENABLE_BT = 2;
    public String LOCATIONNO;
    private int userpermissionlocation = 0;
    int userpermissionmenu = 0;
    int userid = 0;
    private int locationPermission = 0;
    public int TOrder = 50;
    public int PBill = 70;
    public int Tchange = 60;
    public int MTUpdate = 80;
    public int BPrint = 60;
    //search/info
    TextView txt_info;
    SharedPreferences mSharedPreferences;
    public static SessionManager session;
    ProgressDialog dialog;

    /**
     * Called when the activity is first created.
     */

    public boolean checkInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || networkInfo.isConnected() == false) {
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        @SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) user_Login.this.getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        user_LoginID = this;
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_main);
        initview();
        check_login();
        txt_info.setText("Enter Email and Password to log in.");
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("Login");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {

        }
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent =
                    new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        userpermissionlocation = 0;
        userpermissionmenu = 0;
        userid = 0;
        locationPermission = 0;
        Button okButton = (Button) findViewById(R.id.ok);
        okButton.setOnClickListener(okhandler);
    }

    private void initview() {
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);
        txt_info = (TextView) findViewById(R.id.txt_info);
        findViewById(R.id.lay_info).setVisibility(View.INVISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        email = (EditText) findViewById(R.id.edt_email);
        passWord = (EditText) findViewById(R.id.edt_password);
        imageView = (ImageView) findViewById(R.id.logo);
//        passWord.setFocusable(true);
        dialog = new ProgressDialog(user_Login.this);
    }

    private void check_login() {
        if (session.isLoggedIn()) {
            Intent in = new Intent(user_Login.this, Group.class);
            in.putExtra("loggedin", false);
            startActivity(in);
            finishAffinity();
        }
    }//check is login

    private View.OnClickListener okhandler = new View.OnClickListener() {
        @SuppressLint("NewApi")
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            if (!checkInternet()) {
                Toast.makeText(user_Login.this, "NO INTERNET", Toast.LENGTH_SHORT).show();
            } else if (email.getText().toString().trim().length() <= 0 && passWord.getText().toString().trim().length() <= 0) {
                Toast.makeText(user_Login.this, "Please input all fields", Toast.LENGTH_SHORT).show();
            } else if (email.getText().toString().trim().length() <= 0) {
                Toast.makeText(user_Login.this, "Please input Email", Toast.LENGTH_SHORT).show();
            } else if (passWord.getText().toString().trim().length() <= 0) {
                Toast.makeText(user_Login.this, "Please input password", Toast.LENGTH_SHORT).show();
            } else if (validateEmail((email.getText().toString().trim()))) {
                Login(email.getText().toString().trim(), passWord.getText().toString().trim());
            } else {
                Toast.makeText(user_Login.this, getString(R.string.str_valid_emailid), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private boolean validateEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void Login(final String email, final String password) {
        if (dialog != null) {
            dialog = new ProgressDialog(user_Login.this);
        }
        final String[] devicetoken = {""};
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String playerId, String registrationId) {
                Log.d("debug", "playerId:" + playerId);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);
                if (playerId != null)
                    devicetoken[0] = playerId;
            }
        });
        dialog.setMessage(getString(R.string.str_requesting));
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(user_Login.this);
        String requestURL = getString(R.string.web_path) + "login_service/login/?email=" + email + "&password=" + password + "&devicetoken=" + devicetoken[0];
        Log.e("requestURL", ": " + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.e("response", result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.length() > 0) {
                                if (jObj.getString("type_id").equals("1")) {
                                    mSharedPreferences.edit().putString("mid", jObj.getString("id")).apply();
                                    mSharedPreferences.edit().putString("email", jObj.getString("email")).apply();
                                    Toast.makeText(getApplicationContext(), jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                                    // Creating Session
                                    session.createLoginSession("", jObj.getString("id"), jObj.getString("email"), "email", "");
                                    Intent login = new Intent(user_Login.this, Group.class);
                                    login.putExtra("loggedin", true);
                                    startActivity(login);
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                                }
                                dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }

        }) {
           /* @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();

                String fcm_id = FirebaseInstanceId.getInstance().getToken();
                params.put("email_id", email);
                params.put("password", password);
                params.put("devicetoken", fcm_id);
                params.put("device_type", "android");
                params.put("language", selected_lang[0]);
                Log.e("params", "" + params);
                return params;
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }//Login


    /**
     * @return
     */
    public static Object getInstance() {
        return user_LoginID;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

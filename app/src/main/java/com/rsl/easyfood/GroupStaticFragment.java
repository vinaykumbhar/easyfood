package com.rsl.easyfood;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.lucasr.twowayview.TwoWayView;

import java.io.File;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 * From This fragment it will go to Groupactivity and catgory fragment is displayed
 */
public class GroupStaticFragment extends Fragment {
    private int selectedPosition = -1;
    SQLiteDatabase db;
    TextView group;
    ArrayList<Imagepojo> imageview;
    String groupId;
    String groupName;
    Bitmap mBitmapgroup;
    static boolean group_clicked = false;
    Group group1;
    static GroupStaticFragment GROUPID;
    //    File file;
//    File[] allFiles;
    TwoWayView adapter;
    Myadapter myadapter;

    private static class ViewHolder {
        TextView itemName;
        ImageView itemImage;
    }

    public class Myadapter extends BaseAdapter {

        // Declare variables
        private Activity activity;
        private LayoutInflater layoutInflater = null;

        public Myadapter(Activity a) {
            this.activity = a;
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return imageview.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public void setSelectedPosition(int position) {
            selectedPosition = position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            ViewHolder holder = null;
            if (vi == null) {
                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.grid_viewitem1, parent, false);
                holder = new ViewHolder();
                holder.itemName = (TextView) vi.findViewById(R.id.text);
                holder.itemImage = (ImageView) vi.findViewById(R.id.image);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

            holder.itemName.setText(Html.fromHtml(imageview.get(position).getPaymnet()));
            // File imgFile = new File(Environment.getExternalStorageDirectory().getPath() + "/pos/img/" + imageview.get(position).getImageur());
            String imgUrl = imageview.get(position).getImageur();

            if (imgUrl != null || !imgUrl.equals("")) {
                Glide.with(activity).load(imgUrl)
                        .thumbnail(0.5f)
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.itemImage);

            } else {
                holder.itemImage.setImageResource(R.drawable.you_cloud);
            }
            if (position == selectedPosition) {
                vi.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorPrimary));
                holder.itemImage.setColorFilter(Color.argb(255, 255, 255, 255));
                holder.itemName.setTextColor(ContextCompat.getColor(activity,R.color.white));
            } else {
                vi.setBackgroundColor(ContextCompat.getColor(activity,R.color.appBackground));
                holder.itemName.setTextColor(ContextCompat.getColor(activity,R.color.darkgray));
                holder.itemImage.clearColorFilter();

            }

            return vi;
        }
    }

    public GroupStaticFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group_static, container, false);
        group = (TextView) view.findViewById(R.id.group);
        imageview = new ArrayList<Imagepojo>();
        adapter = (TwoWayView) view.findViewById(R.id.gridview3);
        adapter.setSelector(getResources().getDrawable(R.drawable.item_selector));
        adapter.setItemMargin(6);
        GROUPID = this;
        group_clicked = false;
        group1 = (Group) getActivity();
        groupName = "group";
        Group.GROUPID.titlebar.setText("ORDERS");
//        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
//            Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_LONG).show();
//        } else {
//            file = new File(Environment.getExternalStorageDirectory() + File.separator + "/pos/img");
//            allFiles = file.listFiles();
//            if (allFiles.length == 0) {
//                Toast.makeText(getActivity(), "FOLDER IS EMPTY", Toast.LENGTH_LONG).show();
//            }
//        }
        db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * from menugroup ;", null);
        int count = c.getCount();
        System.out.println("Count :- " + count);
        c.moveToFirst();
        imageview.clear();
        for (Integer rowCount = 0; rowCount < count; rowCount++) {
            Imagepojo imagepojo = new Imagepojo();
            String product = "";
            String image = "";
            String groupid = "";
            String res_id = "";

            product = c.getString(c.getColumnIndex("menu_name"));
            image = c.getString(c.getColumnIndex("imageurl"));
            groupid = c.getString(c.getColumnIndex("menu_id"));
            res_id = c.getString(c.getColumnIndex("merchant_id"));
            System.out.println("anything");
//            String[] path = image.split("\\/");
//            String val = path[path.length - 1];
            imagepojo.setPaymnet(product);
            imagepojo.setImageur(image);
            imagepojo.setId(groupid);
            imagepojo.setRestaurantid(res_id);
            imageview.add(imagepojo);
            c.moveToNext();
        }

        db.close();
        myadapter = new Myadapter(getActivity());
        adapter.setAdapter(myadapter);
        myadapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                group_clicked = true;
                myadapter.setSelectedPosition(position);
                myadapter.notifyDataSetChanged();
                db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                groupName = String.valueOf(imageview.get(position).getPaymnet())/*Menu[arg2]*/;//groupList.getAdapter().getItem(arg2).toString() ;
                Cursor groupData = db.rawQuery("SELECT menu_id from menugroup " + "WHERE menu_id =" +
                        imageview.get(position).getId() + " AND merchant_id=" + imageview.get(position).getRestaurantid() + ";", null);
                groupData.moveToFirst();
                if (groupData.getCount() > 0) {
                    groupId = groupData.getString(groupData.getColumnIndex("menu_id"));
                    Log.e("groupId", "=" + groupId);
                } else {
                }
                if (position > -1) {
                    group1.catgoryfragment(groupId);
                } else {
                }
                db.close();
            }
        });

        return view;
    }

    public static GroupStaticFragment getInstance() {
        return GROUPID;
    }
}




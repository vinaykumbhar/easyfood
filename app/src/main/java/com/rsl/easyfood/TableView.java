package com.rsl.easyfood;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TableView extends Activity {
    int i = 1;
    static  TableView TABLEVIEW;
GridView gridView;
    MYAdapter myAdapter;
    static class ViewHolder{
         TextView tableEdit;
          TextView coversEdit;
         Button plus,minus;
    }
    public class MYAdapter extends BaseAdapter{
        private Activity activity;
        private LayoutInflater inflater = null;
        public MYAdapter(Activity a){
            this.activity = a;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getViewTypeCount() {
            return getCount();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getCount() {
            return 10;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, final View convertView, ViewGroup parent) {
            View vi = convertView;
            ViewHolder holder = null;
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            if (vi == null){
                final LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE );
                vi = inflater.inflate(R.layout.tableadapter, parent, false);
                holder = new ViewHolder();
                vi.setTag(holder);
            }else {
                holder = (ViewHolder) vi.getTag();
            }
            holder.tableEdit.setLayoutParams(lp);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.tableEdit.getLayoutParams();
            params.width = getpixelsfromDps(TableView.this,168);
            for (int i = 0;i<10;i++){
                holder.tableEdit.setText(String.valueOf(position));
            }
            holder.tableEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            final ViewHolder finalHolder = holder;
            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String val = String.valueOf(finalHolder.coversEdit.getText());
                    i= Integer.parseInt(val);
                    if (i <=10){
                        finalHolder.coversEdit.setText(""+i++);
                    }
                }
                });
            holder.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String value = String.valueOf(finalHolder.coversEdit.getText());
                    i= Integer.parseInt(value);
                    if (i !=0){
                        i--;
                        finalHolder.coversEdit.setText(""+i);}
                }
            });
            return vi;
        }

        private int getpixelsfromDps(TableView tableView, int dps) {
            Resources r = tableView.getResources();
            int  px = (int) (TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
            return px;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_view);
        TABLEVIEW = this;
        gridView = (GridView) findViewById(R.id.grid);
        myAdapter = new MYAdapter(TABLEVIEW);
        gridView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
    }
}

package com.rsl.easyfood;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by mom on 02-06-2017.
 * here we are getting all the type values
 */

public class TypeDialog extends DialogFragment {
    AutoCompleteTextView CustomEditPrice, CustomEdittype;
    Button ok;
    GridView gridView;
    typeAdadpter typeAdadpter;
    static TypeDialog TYPEDIALOG;
    boolean[] thumb;
    MyDb myDb;
    ArrayList<String> typearray;
    ArrayList<String> paymentTitle = new ArrayList<String>();
    int count1 = 1;
    SQLiteDatabase db;
    String item;
    Group group;
    String TYPENAME;
    double TYPEPRICE;
    static String type;
    Dialog dialog = null;
    private java.lang.String qty = "1";

    public TypeDialog() {

    }

    public boolean checkInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || networkInfo.isConnected() == false) {
            return false;
        }
        return true;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        TYPEDIALOG = this;
        group = (Group) getActivity();
        myDb = new MyDb(getActivity());
        myDb.open();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //builder.setIcon(R.drawable.logo);
        builder.setTitle(" Custom Type");
        View vi = getActivity().getLayoutInflater().inflate(R.layout.typedialog, null);
        Button positivebtn = (Button) vi.findViewById(R.id.positivebutton);
        Button negativebtn = (Button) vi.findViewById(R.id.negativebutton);
        gridView = (GridView) vi.findViewById(R.id.gridview3);
        CustomEdittype = (AutoCompleteTextView) vi.findViewById(R.id.customedit);
        CustomEditPrice = (AutoCompleteTextView) vi.findViewById(R.id.custompriceedit);
        InputFilter[] coversFilterArray = new InputFilter[1];
        coversFilterArray[0] = new InputFilter.LengthFilter(15);
        InputFilter[] priceFilterArray = new InputFilter[1];
        priceFilterArray[0] = new InputFilter.LengthFilter(6);
        CustomEditPrice.setFilters(priceFilterArray);
        CustomEdittype.setFilters(coversFilterArray);
        Bundle b = getArguments();
        final String sno = b.getString("sno");
        CustomEdittype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        CustomEditPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        typeAdadpter = new typeAdadpter(getActivity());
        gridView.setAdapter(typeAdadpter);
        typeAdadpter.notifyDataSetChanged();
        refresh(type);
        this.thumb = new boolean[this.count1];

        typearray = new ArrayList<String>();
        positivebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CustomEdittype.getText().toString().length() > 0 && CustomEditPrice.getText().toString().length() > 0) {
                    typearray.add(CustomEdittype.getText().toString());
                    TYPEPRICE = Double.parseDouble(CustomEditPrice.getText().toString());
                } else {
                    CustomEditPrice.setText("");
                    CustomEdittype.setText("");
                }
                for (int i1 = 0; i1 < typearray.size(); i1++) {
                    db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
                    Cursor c = db.rawQuery("SELECT price from type WHERE typename ='" + typearray.get(i1) +
                            "';", null);

                    int count = c.getCount();
                    c.moveToFirst();
                    if (Integer.parseInt(qty) >= 0 && Integer.parseInt(Group.GROUPID.table) >= 0) {
                        db.beginTransaction();
                        Calendar now = Calendar.getInstance();
                        SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
                        String nowDate = formatter.format(now.getTime());
                        Cursor c12 = db.rawQuery("SELECT orderno FROM ordernos WHERE orderno LIKE '" + nowDate.toString() + "%';", null);
                        int orderNOCount = c12.getCount();
                        String oldOrderNO = "0";

                        if (orderNOCount == 0) {
                            oldOrderNO = nowDate.toString() + "1";
                            db.execSQL("INSERT INTO ordernos values(1," + UserLoginActivity.user_LoginID.LOCATIONNO + "," +
                                    Group.GROUPID.table + ",'" + oldOrderNO + "', 0, 0);");
                            myDb.Insert(String.valueOf(MainActivity.MAINID.outletid), CartStaticSragment.CARTStaticSragment.tableno.getText().toString(), CartStaticSragment.CARTStaticSragment.noofguests.getText().toString(), UserLoginActivity.user_LoginID.nameid, oldOrderNO);
                            if (checkInternet()) {
                                Intent intent = new Intent(getActivity(), ServiceOccupiedChairs.class);
                                getActivity().startService(intent);
                            }

                        } else {
                            c12.moveToLast();
                            String orderNumber = c12.getString(0);
                            Cursor orderNoCheck = db.rawQuery("SELECT * FROM ordernos WHERE tableno =" + Group.GROUPID.table +
                                    " AND locationid =" + UserLoginActivity.user_LoginID.LOCATIONNO + " AND billflag = 0 ;", null);
                            if (orderNoCheck.getCount() > 0) {
                                orderNoCheck.moveToFirst();
                                oldOrderNO = orderNoCheck.getString(orderNoCheck.getColumnIndex("orderno"));

                            } else {
                                Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" + Group.GROUPID.table + " AND locationid =" + UserLoginActivity.user_LoginID.LOCATIONNO + ";", null);
                                int occupiedCount = chairs.getCount();
                                chairs.moveToFirst();
                                if (occupiedCount > 0 && chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                                    int billNoIncrement = Integer.parseInt(orderNumber.substring(6, orderNumber.length())) + 1;
                                    oldOrderNO = orderNumber.substring(0, 6) + billNoIncrement;
                                    db.execSQL("INSERT INTO ordernos values(1," + UserLoginActivity.user_LoginID.LOCATIONNO + "," + Group.GROUPID.table + ",'"
                                            + oldOrderNO + "', 0, 0);");
                                    myDb.Insert(String.valueOf(MainActivity.MAINID.outletid), CartStaticSragment.CARTStaticSragment.tableno.getText().toString(), CartStaticSragment.CARTStaticSragment.noofguests.getText().toString(), UserLoginActivity.user_LoginID.nameid, oldOrderNO);
                                    if (checkInternet()) {
                                        Intent intent = new Intent(getActivity(), ServiceOccupiedChairs.class);
                                        getActivity().startService(intent);
                                    }
                                }
                            }
                        }
                        try {
                            for (int i = 0; i < typearray.size(); i++) {
                                Cursor c11 = db.rawQuery("SELECT price from type WHERE typename ='" + typearray.get(i) +
                                        "';", null);
                                if (TYPENAME == null) {
                                    TYPENAME = typearray.get(i);
                                } else {
                                    TYPENAME += "+" + typearray.get(i);
                                }
                                int count12 = c11.getCount();
                                c11.moveToFirst();
                                if (TYPEPRICE == 0) {
                                    TYPEPRICE = c11.getFloat(0);
                                }
                            }
                            db.execSQL("UPDATE cart SET typeprice = " + TYPEPRICE + " , typename = '" + TYPENAME + "' WHERE sno = " + sno + ";");
                            CartStaticSragment.CARTStaticSragment.plus.setEnabled(false);
                            CartStaticSragment.CARTStaticSragment.minus.setEnabled(false);
                            if (Group.GROUPID.productflag) {
                                if (!Group.GROUPID.groupfragment.isResumed()) {
                                    Group.GROUPID.search.setVisibility(View.INVISIBLE);
                                    View view = Group.GROUPID.getCurrentFocus();
                                    InputMethodManager inputMethodManager = (InputMethodManager) Group.GROUPID.getSystemService(Context.INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                    Group.GROUPID.fragmentTransaction = Group.GROUPID.fragmentManager.beginTransaction();
                                    Group.GROUPID.fragmentTransaction.replace(R.id.container2, Group.GROUPID.groupfragment);
                                    Group.GROUPID.fragmentTransaction.commit();
                                }
                            }
                        } catch (NullPointerException e) {
                        }
                        db.setTransactionSuccessful();
                        db.endTransaction();
                        TYPENAME = "";
                        TYPEPRICE = 0;
                        if (CartStaticSragment.CARTStaticSragment.cart.size() != 0) {
                            CartStaticSragment.CARTStaticSragment.cart.clear();
                        }
                        if (typearray.size() != 0) {
                            typearray.clear();
                        }
                        Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
                        group.alert();
                    }


                }
                if (paymentTitle.size() != 0) {
                    paymentTitle.clear();
                }
                dialog.dismiss();
            }
        });
        negativebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typearray.clear();
                paymentTitle.clear();
                dialog.cancel();
            }
        });
        builder.setView(vi);
        dialog = builder.create();
        return dialog;
    }

    public static void typeid(String typeId) {
        type = typeId;
    }

    static class ViewHolder {
        Button type;
        CheckBox checkbox;
        int id;
        TextView itemtype;
    }

    public class typeAdadpter extends BaseAdapter {
        private Activity activity;
        int id;
        String type;
        @SuppressWarnings("unused")
        private LayoutInflater inflater = null;

        public typeAdadpter(Activity a) {
            activity = a;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return paymentTitle.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            ViewHolder holder = null;
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.typeadadpter, parent, false);
                holder = new ViewHolder();
                holder.itemtype = (TextView) vi.findViewById(R.id.itemtype);
                holder.checkbox = (CheckBox) vi.findViewById(R.id.checkbox);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }
            holder.itemtype.setId(position);
            holder.checkbox.setId(position);
            final ViewHolder finalHolder = holder;
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox checkBox = (CheckBox) v;
                    id = checkBox.getId();
                    thumb = new boolean[15];
                    if (!checkBox.isChecked()) {
                        thumb[id] = false;
                        item = finalHolder.itemtype.getText().toString();
                        typearray.remove(item);

                    } else {
                        thumb[id] = true;
                        item = finalHolder.itemtype.getText().toString();
                        typearray.add(item);
                        db.close();
                    }
                }
            });
            //Issue
            //here we are getting the values position  from main adapter so its reating for 63 times
            //we need to get  15 times
            //check box is one time only  clickable

            holder.itemtype.setText(paymentTitle.get(position));
            holder.checkbox.setChecked(thumb[id]);

            int value = position;
            position++;
            holder.id = position;
            return vi;
        }
    }

    //getting type values from database
    public void refresh(String type) {
        String[] splitTypeId = type.split("\\&");
        db = getActivity().openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        if (splitTypeId.length > 0) {
            for (int typeCount = 0; typeCount < splitTypeId.length; typeCount++) {
                Cursor c = db.rawQuery("SELECT typename from type WHERE typeid =" +
                        splitTypeId[typeCount] + ";", null);
                int count = c.getCount();
                c.moveToFirst();
                for (Integer rowCount = 0; rowCount < count; rowCount++) {
                    String product = "";
                    product = c.getString(c.getColumnIndex("typename"));
                    paymentTitle.add(product);
                    c.moveToNext();
                }


            }
        }
        db.close();
    }

    public static TypeDialog getInstance() {
        return TYPEDIALOG;
    }
}

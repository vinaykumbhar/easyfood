/**
 *
 */
package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author embdes
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Settingoption extends Activity {
    SharedPreferences mprinterPreferences;
    SharedPreferences mSharedPreferences;
    SQLiteDatabase db;
    public float discountAmount;
    public float voucherAmount = 0;
    Dialog dialogMarketList;
    View viewList;
    int occupiedchairs;
    Dialog settingDialog, PaymentLoginpopup;
    ListView lv;
    TextView date, titlebar;
    ArrayAdapter<BluetoothDevice> adapter;
    ArrayList<BluetoothDevice> connections = new ArrayList<BluetoothDevice>();
    ArrayAdapter<String> adaptername;
    ArrayList<String> connectionsname = new ArrayList<String>();
    String globledevice;
    boolean printerDeviceCheck = false;
    boolean kotcheck = false;
    //search/info
    TextView txt_info;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.settings);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settingoption.this, Home.class);
                startActivity(intent);
            }
        });
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("Settings");
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mprinterPreferences = getSharedPreferences("printerpref", MODE_PRIVATE);
        txt_info = (TextView) findViewById(R.id.txt_info);
        txt_info.setText("Customize printer settings");
        findViewById(R.id.lay_info).setVisibility(View.INVISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);

        Button serverSettings = (Button) findViewById(R.id.severIpButton);
        Button kotsetting = (Button) findViewById(R.id.Button01);
        Button paymentloginButton = (Button) findViewById(R.id.Button02);
        kotsetting.setOnClickListener(kotbtPrinterHandler);
        serverSettings.setOnClickListener(serverIpHandler);
        Button btPrinterSettings = (Button) findViewById(R.id.printerBluetoothButton);
        Button wifiPrinterSettings = (Button) findViewById(R.id.PrinterWifiButton1);
        btPrinterSettings.setOnClickListener(btPrinterHandler);
        wifiPrinterSettings.setOnClickListener(wifiPrinterHandler);
        final CheckBox btPrinterName = (CheckBox) findViewById(R.id.RadioBtPrinter);
        final CheckBox wifiPrinterName = (CheckBox) findViewById(R.id.RadioWifiPrinter);
        final CheckBox kotbtPrinterName = (CheckBox) findViewById(R.id.CheckBox01);


        kotbtPrinterName.setChecked(mprinterPreferences.getBoolean("kotPrinterEnable", false));
        btPrinterName.setChecked(!mprinterPreferences.getBoolean("isWifiPrinterEnable", false));
        wifiPrinterName.setChecked(mprinterPreferences.getBoolean("isWifiPrinterEnable", false));


        btPrinterName.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (true == btPrinterName.isChecked()) {
                    wifiPrinterName.setChecked(false);
                    mprinterPreferences.edit().putBoolean("isWifiPrinterEnable", false).apply();
                } else {
                    wifiPrinterName.setChecked(true);
                    mprinterPreferences.edit().putBoolean("isWifiPrinterEnable", true).apply();
                }
            }
        });
        kotbtPrinterName.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (true == kotbtPrinterName.isChecked()) {
                    mprinterPreferences.edit().putBoolean("kotPrinterEnable", true).apply();
                    kotcheck = true;
                    ListPairedDevices();

                } else {
                    mprinterPreferences.edit().putBoolean("kotPrinterEnable", false).apply();
                }
            }
        });

        wifiPrinterName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (true == wifiPrinterName.isChecked()) {
                    btPrinterName.setChecked(false);
                    mprinterPreferences.edit().putBoolean("isWifiPrinterEnable", true).apply();
                } else {
                    btPrinterName.setChecked(true);
                    mprinterPreferences.edit().putBoolean("isWifiPrinterEnable", false).apply();

                }
            }
        });

        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor serverIpDetails = db.rawQuery("SELECT ipaddress, portno " +
                " FROM serverprinter WHERE res_id =" +
                mSharedPreferences.getString("mid", "") + ";", null);
        if (serverIpDetails.getCount() > 0) {
            serverIpDetails.moveToFirst();
            TextView serverDetails = (TextView) findViewById(R.id.severIpView);
            serverDetails.setText("Server IpAddress\nIpAddress:" + serverIpDetails.getString(0) +
                    "\nPortNo:" + serverIpDetails.getString(1));
        }
        serverIpDetails.close();
        db.close();
    }


    ;
    /**
     * Creating pop-up window for web server configuration.
     * Here user can enter IP address or Host name and Port number of Server.
     */
    View.OnClickListener serverIpHandler = new View.OnClickListener() {

        public void onClick(View v) {
//            if (UserLoginActivity.user_LoginID.userpermissionmenu >= UserLoginActivity.user_LoginID.MTUpdate) {
            viewList = Settingoption.this.getLayoutInflater().inflate(R.layout.settings_popup,
                    null);
            dialogMarketList = new Dialog(Settingoption.this);

            dialogMarketList.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            dialogMarketList.setTitle("Server Configuration");
            dialogMarketList.setContentView(viewList);
            dialogMarketList.setCancelable(false);
            dialogMarketList.show();
            ImageButton popUpOk = (ImageButton) viewList.findViewById(R.id.settingsPopUpOk);
            ImageButton PopUpCancel = (ImageButton) viewList.findViewById(R.id.settingsPopUpCancel);
            final CheckBox ipAddressCheck = (CheckBox) viewList.findViewById(R.id.checkBoxIpAddress);
            final CheckBox hostNameCheck = (CheckBox) viewList.findViewById(R.id.checkBoxHostName);
            ipAddressCheck.setChecked(true);

            PopUpCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    dialogMarketList.dismiss();
                }
            });

            final EditText editIpAddress = (EditText) viewList.findViewById(R.id.editPopUpIpNumber);
            final EditText editPortNumber = (EditText) viewList.findViewById(R.id.editPopUpPortNumber);

            if (true == ipAddressCheck.isChecked()) {
                editIpAddress.setText("");
                editPortNumber.setText("");
                TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
                hostNameView.setText("Ip Address");
                editIpAddress.setHint("192.168.001.102");
                InputFilter[] filters = new InputFilter[1];
                filters[0] = new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence source, int start, int end,
                                               android.text.Spanned dest, int dstart, int dend) {
                        if (end > start) {
                            String destTxt = dest.toString();
                            String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
                            if (!resultingTxt.matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                                return "";
                            } else {
                                String[] splits = resultingTxt.split("\\.");
                                for (int i = 0; i < splits.length; i++) {
                                    if (Integer.valueOf(splits[i]) > 255) {
                                        return "";
                                    }
                                }
                            }
                        }
                        return null;
                    }

                };
                editIpAddress.setFilters(filters);
            } else {
                editIpAddress.setText("");
                editPortNumber.setText("");
                TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
                hostNameView.setText("Host Name");
                editIpAddress.setHint("www.google.com");
                editIpAddress.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                InputFilter[] filters = new InputFilter[1];
                filters[0] = new InputFilter.LengthFilter(50);
                editIpAddress.setFilters(filters);
            }

            ipAddressCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if (true == ipAddressCheck.isChecked()) {
                        editIpAddress.setText("");
                        editPortNumber.setText("");
                        editIpAddress.setInputType(InputType.TYPE_CLASS_PHONE);
                        hostNameCheck.setChecked(false);
                        TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
                        hostNameView.setText("Ip Address");
                        editIpAddress.setHint("192.168.001.102");
                        InputFilter[] filters = new InputFilter[1];
                        filters[0] = new InputFilter() {
                            @Override
                            public CharSequence filter(CharSequence source, int start, int end,
                                                       android.text.Spanned dest, int dstart, int dend) {
                                if (end > start) {
                                    String destTxt = dest.toString();
                                    String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
                                    if (!resultingTxt.matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                                        return "";
                                    } else {
                                        String[] splits = resultingTxt.split("\\.");
                                        for (int i = 0; i < splits.length; i++) {
                                            if (Integer.valueOf(splits[i]) > 255) {
                                                return "";
                                            }
                                        }
                                    }
                                }
                                return null;
                            }
                        };
                        editIpAddress.setFilters(filters);
                    } else {
                        hostNameCheck.setChecked(true);
                        TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
                        hostNameView.setText("Host Name");
                        editIpAddress.setHint("www.google.com");
                        editIpAddress.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        editIpAddress.setText("");
                        editPortNumber.setText("");
                        InputFilter[] filters = new InputFilter[1];
                        filters[0] = new InputFilter.LengthFilter(50);
                        editIpAddress.setFilters(filters);

                    }

                }
            });

            hostNameCheck.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (false == hostNameCheck.isChecked()) {
                        editIpAddress.setText("");
                        editPortNumber.setText("");
                        editIpAddress.setInputType(InputType.TYPE_CLASS_PHONE);
                        ipAddressCheck.setChecked(true);
                        TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
                        hostNameView.setText("Ip Address");
                        editIpAddress.setHint("192.168.001.102");
                        InputFilter[] filters = new InputFilter[1];
                        filters[0] = new InputFilter() {
                            @Override
                            public CharSequence filter(CharSequence source, int start, int end,
                                                       android.text.Spanned dest, int dstart, int dend) {
                                if (end > start) {
                                    String destTxt = dest.toString();
                                    String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
                                    if (!resultingTxt.matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                                        return "";
                                    } else {
                                        String[] splits = resultingTxt.split("\\.");
                                        for (int i = 0; i < splits.length; i++) {
                                            if (Integer.valueOf(splits[i]) > 255) {
                                                return "";
                                            }
                                        }
                                    }
                                }
                                return null;
                            }

                        };
                        editIpAddress.setFilters(filters);
                    } else {
                        TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
                        hostNameView.setText("Host Name");
                        editIpAddress.setHint("www.google.com");
                        editIpAddress.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        ipAddressCheck.setChecked(false);
                        editIpAddress.setText("");
                        editPortNumber.setText("");
                        InputFilter[] filters = new InputFilter[1];
                        filters[0] = new InputFilter.LengthFilter(50);
                        editIpAddress.setFilters(filters);


                    }
                }
            });

            popUpOk.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (editIpAddress.getText().length() > 0 &&
                            editPortNumber.getText().length() > 0) {
                        TextView serverDetails = (TextView) findViewById(R.id.severIpView);


                        if (true == ipAddressCheck.isChecked()) {
                            serverDetails.setText("Server Ip Address\nIP Address: " +
                                    editIpAddress.getText() + "\nPort No: " + editPortNumber.getText());

                            db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                            db.execSQL("UPDATE serverprinter SET ipAddress ='" +
                                    editIpAddress.getText() + "', portno =" + editPortNumber.getText() +
                                    " WHERE res_id = " + mSharedPreferences.getString("mid", "") + ";");
                            db.close();
                            dialogMarketList.dismiss();
                        } else {

                            try {
                                InetAddress address = InetAddress.getByName("" + editIpAddress.getText());
                                serverDetails.setText("Server Ip Address\nIP Address: " +
                                        address.getHostAddress() + "\nPort No: " + editPortNumber.getText());
                                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                                db.execSQL("UPDATE serverprinter SET ipAddress ='" +
                                        address.getHostAddress() + "', portno =" + editPortNumber.getText() +
                                        " WHERE res_id = " + mSharedPreferences.getString("mid", "") + ";");
                                db.close();
                                dialogMarketList.dismiss();
                            } catch (UnknownHostException e) {
                                e.printStackTrace();
                                Toast.makeText(Settingoption.this, "" + e.getMessage(),
                                        Toast.LENGTH_LONG).show();
                                dialogMarketList.dismiss();
                                return;
                            }
                        }
                    }
                }
            });
        }

    };

    View.OnClickListener kotbtPrinterHandler = new View.OnClickListener() {
        public void onClick(View v) {
            kotcheck = true;
            //TODO check bluetooth permission
            ListPairedDevices();
        }
    };
    /**
     * Creating pop-up window for WiFi printer configuration.
     * Here user can select printer from the listview items.
     */

    View.OnClickListener btPrinterHandler = new View.OnClickListener() {
        public void onClick(View v) {
            //TODO check bluetooth permission
            ListPairedDevices();
        }
    };

    private void ListPairedDevices() {
        connections.clear();
        connectionsname.clear();
        Set<BluetoothDevice> bondedDevices = BluetoothAdapter
                .getDefaultAdapter().getBondedDevices();
//        for (int k = 0; k < bondedDevices.size(); k++) {
//        }
        if (bondedDevices.size() > 0) {
            for (BluetoothDevice bondDeice : bondedDevices) {
                connections.add(bondDeice);
                connectionsname.add(bondDeice.getName());
                printerDeviceCheck = true;
            }
        }
        if (printerDeviceCheck == true) {
            printerDeviceCheck = false;
            Device_Select();
        } else {
            Toast.makeText(getApplicationContext(), "Printer not found", Toast.LENGTH_SHORT).show();
        }

    }

    private void Device_Select() {
        settingDialog = new Dialog(Settingoption.this);
        settingDialog.setContentView(R.layout.fragment_scanner_device_selection);
        settingDialog.setTitle("Paired Device");
        settingDialog.setCancelable(true);
        lv = (ListView) settingDialog.findViewById(R.id.list);
        settingDialog.show();
        adapter = new ArrayAdapter<BluetoothDevice>(this, android.R.layout.simple_list_item_1, connections);
        adaptername = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, connectionsname);
        lv.setAdapter(adaptername);


        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                globledevice = adapter.getItem(arg2).getAddress();
                if (kotcheck == true) {

                    mprinterPreferences.edit().putString("btkPrinterid", globledevice).apply();

                    kotcheck = false;
                } else {

                    mprinterPreferences.edit().putString("btPrinterid", globledevice).apply();

                }
                settingDialog.dismiss();
            }
        });
    }
    /**
     * @param data
     * @param printformate
     */

    /**
     * Creating pop-up window for blue-tooth printer configuration.
     * Here user can enter IP address and Port number of WiFi printer.
     */
    View.OnClickListener wifiPrinterHandler = new View.OnClickListener() {
        public void onClick(View v) {
//            if (UserLoginActivity.user_LoginID.userpermissionmenu >= UserLoginActivity.user_LoginID.BPrint) {
            @SuppressWarnings("unused")
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            viewList = Settingoption.this.getLayoutInflater().inflate(R.layout.settings_popup,
                    null);
            dialogMarketList = new Dialog(Settingoption.this);

            dialogMarketList.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            dialogMarketList.setTitle("WIFI Printer Configuration");
            dialogMarketList.setContentView(viewList);
            dialogMarketList.setCancelable(false);
            dialogMarketList.show();

            final CheckBox ipAddressCheck = (CheckBox) viewList.findViewById(R.id.checkBoxIpAddress);
            ipAddressCheck.setVisibility(View.INVISIBLE);
            final CheckBox hostNameCheck = (CheckBox) viewList.findViewById(R.id.checkBoxHostName);
            hostNameCheck.setVisibility(View.INVISIBLE);

            ImageButton popUpOk = (ImageButton) viewList.findViewById(R.id.settingsPopUpOk);
            ImageButton PopUpCancel = (ImageButton) viewList.findViewById(R.id.settingsPopUpCancel);
            PopUpCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    dialogMarketList.dismiss();
                }
            });

            final EditText editIpAddress = (EditText) viewList.findViewById(R.id.editPopUpIpNumber);
            final EditText editPortNumber = (EditText) viewList.findViewById(R.id.editPopUpPortNumber);

            InputFilter[] filters = new InputFilter[1];
            filters[0] = new InputFilter() {
                @Override
                public CharSequence filter(CharSequence source, int start, int end,
                                           android.text.Spanned dest, int dstart, int dend) {
                    if (end > start) {
                        String destTxt = dest.toString();
                        String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
                        if (!resultingTxt.matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                            return "";
                        } else {
                            String[] splits = resultingTxt.split("\\.");
                            for (int i = 0; i < splits.length; i++) {
                                if (Integer.valueOf(splits[i]) > 255) {
                                    return "";
                                }
                            }
                        }
                    }
                    return null;
                }

            };
            editIpAddress.setFilters(filters);


            popUpOk.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (editIpAddress.getText().length() > 0 &&
                            editPortNumber.getText().length() > 0) {
                        CheckBox printerName = (CheckBox) findViewById(R.id.RadioWifiPrinter);
                        printerName.setText("WiFi Printer\nIP Address: " + editIpAddress.getText() + "\nPort No: " + editPortNumber.getText());
                        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                        db.execSQL("UPDATE locationprinter SET ipAddress ='" +
                                editIpAddress.getText() + "', portno =" + editPortNumber.getText() + ";");
                        db.close();
                        dialogMarketList.dismiss();
                    }
                }
            });
//            } else {
//                AlertDialog.Builder msg = new AlertDialog.Builder(
//                        Settingoption.this, android.R.style.Theme_Dialog);
//                msg.setTitle("Opration Failed");
//                msg.setMessage("User Doesn't have the permission for this opration");
//                msg.setPositiveButton("Ok", null);
//                msg.show();
//            }
        }
    };

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}

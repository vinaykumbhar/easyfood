package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.rsl.utills.SessionManager;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author EMBDES.
 * @version 1.0.
 *          This Activity display all menus available in home screen.
 */

@SuppressLint("NewApi")
public class Home extends Activity {

    SQLiteDatabase db;
    GridView listView;
    static Home HOMEID;
    TextView date, titlebar;
    public static boolean variable;
    Socket_Connection socket;
    int COUNTER_SERVICE = 0;
    int PHONEORDERS = 1;
    int DINEIN = 2;
    int BILLSPENDINGTABLES = 3;
    int MANAGER = 4;
    int SETTINGS = 5;
    int VERSION = 6;
    int UPDATE = 7;
    int LOGOUT = 8;
    NetworkChangeReceiver mConnReceiver;
    SessionManager manager;
    //search/info
    TextView txt_info;

    static int flag = 0;
    GridViewAdapter adapter;
    int quickTable;

    String[] Menu = {"COUNTER SERVICE",
            "PHONE ORDERS", "DINE IN", "PENDING ORDERS",
            "MANAGER",
          /*  "QUEUE SEND",*/
            "SETTINGS", "VERSION", "UPDATE", "LOGOUT"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HOMEID = this;
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.home_screen);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        titlebar = (TextView) findViewById(R.id.titelabar);
        date = (TextView) findViewById(R.id.date);
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("MENU");
        listView = (GridView) findViewById(R.id.gridview);
        //  listView.setDividerHeight(0);
        adapter = new GridViewAdapter(this);
        listView.setAdapter(adapter);
        manager = new SessionManager(Home.this);
        txt_info = (TextView) findViewById(R.id.txt_info);
        txt_info.setText("Select desired options");
        findViewById(R.id.lay_info).setVisibility(View.INVISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                if (arg2 == COUNTER_SERVICE) {

//                    Intent intent = new Intent(Home.this, Group.class);
//                    startActivity(intent);
//                    finish();

                    final SharedPreferences mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    int restoredText = mSharedPreference.getInt("tableno", 1500);
                    if (restoredText != 0) {
                        quickTable = mSharedPreference.getInt("tableno", 1500);
                        System.out.println("quick table : " + quickTable);
                        if (quickTable < 1500) {
                            quickTable = 1500;
                        } else {
                        }
                        if (quickTable > 1599) {
                            quickTable = 1500;
                        } else {

                        }
                    }
                    if (!db.isOpen())
                        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                    Cursor pendindDetails = db.rawQuery("SELECT  tableno, orderno FROM ordernos WHERE billflag = 0;", null);
                    int pendindCount = pendindDetails.getCount();
                    if (pendindCount > 0) {
                        while (pendindDetails.moveToNext()) {
                            int tableno = Integer.parseInt(pendindDetails.getString(0));
                            if (tableno > 1500 && tableno < 1599) {
                                quickTable = tableno;
                            }
                        }
                    }
                    db.close();
                    if (quickTable != 0) {
                        if (quickTable < 1500) {
                            quickTable = 1500;

                        } else {
                        }
                        if (quickTable >= 1599) {
                            quickTable = 1500;
                        } else {

                        }

                        Group.GROUPID.table = String.valueOf(quickTable + 1);
                        quickTable = Integer.parseInt(Group.GROUPID.table);
                        TablesFragment.COVERS = "1";
                        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                        Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);

                        int chairsCount = chairs.getCount();
                        chairs.moveToFirst();
                        if (chairsCount > 0) {
                            if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                                Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);
                                int count = c.getCount();
                                c.moveToFirst();
                                if (count > 0) {
                                    int covers = c.getInt(c.getColumnIndex("noofchairs"));
                                    if (covers >= Integer.parseInt(TablesFragment.COVERS)) {
                                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                        SharedPreferences.Editor editor = prefs.edit();
                                        if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) > 1) {
                                            editor.putInt("tableno", quickTable).apply();
                                        }
                                        variable = true;
                                        CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                                        CartStaticSragment.CARTStaticSragment.tableno.setText("#####");
                                        CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                                        CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                                        CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                                        CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                                        Intent intent = new Intent(Home.this, Group.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    } else {
                                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                                Home.this, android.R.style.Theme_Dialog);
                                        msg.setTitle("");
                                        msg.setMessage("Covers Should Not Exceed " + covers);
                                        msg.setPositiveButton("Ok", null);
                                        msg.show();
                                    }
                                } else {
                                    AlertDialog.Builder msg = new AlertDialog.Builder(
                                            Home.this, android.R.style.Theme_Dialog);
                                    msg.setTitle("");
                                    msg.setMessage("Table is Not Available");
                                    msg.setPositiveButton("Ok", null);
                                    msg.show();
                                }

                            } else {
                                variable = true;
                                CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                                CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                                Intent intent = new Intent(Home.this, Group.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);

                            }
                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    Home.this, android.R.style.Theme_Dialog);
                            msg.setTitle("");
                            msg.setMessage("Table Is Not Available");
                            msg.setPositiveButton("Ok", null);
                            msg.show();

                        }
                        db.close();
                    } else {
                        Group.GROUPID.table = "1501";
                        quickTable = Integer.parseInt(Group.GROUPID.table);
                        TablesFragment.COVERS = "1";
                        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                        Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);

                        int chairsCount = chairs.getCount();
                        chairs.moveToFirst();
                        if (chairsCount > 0) {
                            if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                                Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);
                                int count = c.getCount();
                                c.moveToFirst();
                                if (count > 0) {
                                    int covers = c.getInt(c.getColumnIndex("noofchairs"));
                                    if (covers >= Integer.parseInt(TablesFragment.COVERS)) {
                                        variable = true;
                                        Intent intent = new Intent(Home.this, Group.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);

                                    } else {
                                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                                Home.this, android.R.style.Theme_Dialog);
                                        msg.setTitle("");
                                        msg.setMessage("Covers Should Not Exceed " + covers);
                                        msg.setPositiveButton("Ok", null);
                                        msg.show();
                                    }

                                } else {

                                    AlertDialog.Builder msg = new AlertDialog.Builder(
                                            Home.this, android.R.style.Theme_Dialog);
                                    msg.setTitle("");
                                    msg.setMessage("Table is Not Available");
                                    msg.setPositiveButton("Ok", null);
                                    msg.show();
                                }

                            } else {
                                variable = true;
                                CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                                CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                                CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                                Intent intent = new Intent(Home.this, Group.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    Home.this, android.R.style.Theme_Dialog);
                            msg.setTitle("");
                            msg.setMessage("Table Is Not Available");
                            msg.setPositiveButton("Ok", null);
                            msg.show();
                        }
                        db.close();
                    }
                } else if (arg2 == PHONEORDERS) {
                    //Intent i = new Intent(Home.this, CheckUserRecord.class);
                    // startActivity(i);
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            Home.this, android.R.style.Theme_Dialog);
                    msg.setMessage("Yet to be implemented");
                    msg.setPositiveButton("Ok", null);
                    msg.show();

                } else if (arg2 == DINEIN) {
                    Intent intent = new Intent(Home.this, Group.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();

                } else if (arg2 == BILLSPENDINGTABLES) {
                    Intent pendingbill = new Intent(Home.this, BILLSPENDINGTABLES.class);
                    startActivity(pendingbill);
                } else if (arg2 == MANAGER) {
                    Intent i = new Intent(Home.this, ManagerReports.class);
                    startActivity(i);
                } else if (arg2 == SETTINGS) {
                    Intent i = new Intent(Home.this, Settingoption.class);
                    startActivity(i);
                } else if (arg2 == VERSION) {
                    Intent ver = new Intent(Home.this, Version.class);
                    startActivity(ver);

                } else if (arg2 == UPDATE) {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            Home.this, android.R.style.Theme_Dialog);
                    msg.setMessage("Yet to be implemented");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                } else if (arg2 == LOGOUT) {
                    AlertDialog.Builder mesg = new AlertDialog.Builder(Home.this,
                            android.R.style.Theme_Dialog);
                    mesg.setMessage("Do You Want To Logout ?");
                    mesg.setNegativeButton("No", null);
                    mesg.setPositiveButton("Yes", new OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
//                            Group.GROUPID.logout();
                            dropTables();
                        }
                    });
                    mesg.show();
                }
            }
        });
    }

    private void dropTables() {
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        // query to obtain the names of all tables in your database
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        List<String> tables = new ArrayList<>();

// iterate over the result set, adding every table name to a list
        while (c.moveToNext()) {
            tables.add(c.getString(0));
        }

// call DROP TABLE on every table name
        for (String table : tables) {
            if (table.startsWith("sqlite_")) {
                continue;
            }
            String dropQuery = "DROP TABLE IF EXISTS " + table;
            Log.e("dropQuery", ": " + dropQuery);
            db.execSQL(dropQuery);
        }
        manager.logoutUser();
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        MainActivity.networkcheck = false;
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onPause()
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (!MainActivity.networkcheck && mConnReceiver != null) {
            unregisterReceiver(mConnReceiver);
        }
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            if (checkInternet(context)) {
                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                Cursor queueData = db.rawQuery("SELECT * FROM queuetable;", null);
                int recCount = queueData.getCount();
                db.close();
                if (recCount > 0) {
                    System.out.println("In Quesend ");
                    MainActivity.getInstance().queueMessage = "1";
                    Intent userLoginIntent = new Intent(); //Created new Intent to
                    userLoginIntent.setClass(getApplicationContext(), Socket_Connection.class);
                    userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(userLoginIntent);
                } else {
                }
            } else {
            }

        }


        boolean checkInternet(Context context) {
            ServiceManager serviceManager = new ServiceManager(context);
            if (serviceManager.isNetworkAvailable()) {
                return true;
            } else {
                return false;
            }
        }

    }


    @SuppressLint("NewApi")
    public void onBackPressed() {
        AlertDialog.Builder mesg = new AlertDialog.Builder(Home.this, android.R.style.Theme_Dialog);
        mesg.setMessage("Do You Want To Exit ?\n");
        mesg.setNegativeButton("No", null);
        mesg.setPositiveButton("Yes", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
            }
        });
        mesg.show();
    }

    public class GridViewAdapter extends BaseAdapter {

        // Declare variables
        private Activity activity;

        private LayoutInflater inflater = null;

        public GridViewAdapter(Activity a) {
            activity = a;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            System.out.println("getCount " + Menu.length);
            return Menu.length;
        }

        public Object getItem(int position) {
            System.out.println("Object getItem " + position);
            return position;
        }

        public long getItemId(int position) {
            System.out.println("getItemId " + position);
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            System.out.println("View " + position);
            View vi = convertView;
            if (convertView == null)
                System.out.println("IF " + position);
            vi = inflater.inflate(R.layout.gridview_item, null);
            TextView text = (TextView) vi.findViewById(R.id.text);
            text.setText(Menu[position]);

            return vi;
        }

    }

    public void stop() {
        TablesFragment.TABLESID.handler.removeMessages(0);
    }

    /**
     * Here we are creating instance for class
     *
     * @param
     * @return Instance
     */
    public static Home getInstance() {
        return HOMEID;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

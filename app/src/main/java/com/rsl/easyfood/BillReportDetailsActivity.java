package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.model.OrderDisplay;
import com.rsl.utills.ConnectionDetector;
import com.rsl.utills.NonScrollListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BillReportDetailsActivity extends Activity {

    TextView date, titlebar;
    ProgressDialog dialog;
    ConnectionDetector internet;
    ExpandableListView expand_list_details;
    ArrayList<OrderDisplay> list_data = new ArrayList<>();
    ExpandableListCustomAdapter adapter;
    OrderDisplay order_data = null;
    TextView text_consumer_name, text_order_no, text_payment_mode, text_order_id, text_order_total, text_order_time, text_shipping_cost, text_delivery_Address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_bill_report_details);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        internet = new ConnectionDetector(BillReportDetailsActivity.this);
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BillReportDetailsActivity.this, Home.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.GONE);

        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("BILL REPORT");
        // order_list = new ArrayList<>();
        adapter = new ExpandableListCustomAdapter();
        expand_list_details = (ExpandableListView) findViewById(R.id.list_details);
        text_consumer_name = (TextView) findViewById(R.id.text_consumer_name);
        text_payment_mode = (TextView) findViewById(R.id.text_payment_mode);
        text_order_id = (TextView) findViewById(R.id.text_order_id);
        text_order_no = (TextView) findViewById(R.id.text_order_no);
        text_order_total = (TextView) findViewById(R.id.text_order_total);
        text_order_time = (TextView) findViewById(R.id.text_order_time);
        text_shipping_cost = (TextView) findViewById(R.id.text_shipping_cost);
        text_delivery_Address = (TextView) findViewById(R.id.text_delivery_Address);
        expand_list_details.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView arg0, View itemView, int itemPosition, long itemId) {
                expand_list_details.expandGroup(itemPosition);
                return true;

            }
        });
        expand_list_details.setAdapter(adapter);

        if (internet.isConnectingToInternet())
            getOrderDetails(getIntent().getStringExtra("order_id"));
        else
            internet.showAlertDialog(BillReportDetailsActivity.this);

    }//onCreate

    private void getOrderDetails(final String id) {
        if (dialog == null)
            dialog = new ProgressDialog(BillReportDetailsActivity.this);
        dialog.setMessage("Order Details");
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(BillReportDetailsActivity.this);
        String requestURL = getString(R.string.web_path) + "order_service/details/?order_id=" + id;
        Log.d("requestURL", "getOrderDetails:" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.d("getOrder", "getOrderDetails:" + result);
                if (dialog.isShowing())
                    dialog.dismiss();

                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj1 = new JSONObject(result);
                            if (jObj1.length() > 0) {
                                Object detailsObject = jObj1.get("order_detail");
                                if (detailsObject instanceof JSONObject) {
                                    JSONObject jObjDetails = jObj1.getJSONObject("order_detail");
                                    //OrderDisplay main=new OrderDisplay();
                                    text_order_no.setText("Order No : " + jObjDetails.getString("order_num"));
                                    text_consumer_name.setText("Consumer Name : " + jObjDetails.getString("customer_name"));
                                    text_payment_mode.setText("Payment Mode : " + jObjDetails.getString("payment_mode"));
                                    text_order_id.setText("Order Id : " + jObjDetails.getString("order_id"));
                                    text_order_total.setText("Order Total : " + getString(R.string.pound_symbol) + " " + jObjDetails.getString("order_total"));
                                    text_order_time.setText("Order Time : " + jObjDetails.getString("order_time"));
                                    text_shipping_cost.setText("Shipping Cost : " + jObjDetails.getString("shipping_cost"));


                                    String address = "";
                                    Object addressObject = jObjDetails.get("delivery_address");
                                    if (addressObject instanceof JSONObject) {
                                        JSONObject delivery_address = new JSONObject(jObjDetails.getString("delivery_address"));
                                        address = delivery_address.getString("first_name") + " " + delivery_address.getString("last_name")
                                                + "," + delivery_address.getString("delivery_address1") + " ," + delivery_address.getString("delivery_address2")
                                                + ", " + delivery_address.getString("delivery_city") + "," + delivery_address.getString("delivery_zipcode");

                                    }
                                    text_delivery_Address.setText("Delivery Address : " + address);


                                    JSONArray jsonArrayCart = jObjDetails.getJSONArray("cart_splitwithoutbill");

                                    OrderDisplay sub_order_data = null;
                                    if (jsonArrayCart.length() > 0) {
                                        for (int j = 0; j < jsonArrayCart.length(); j++) {
                                            order_data = new OrderDisplay();
                                            JSONObject jObj = jsonArrayCart.getJSONObject(j);
                                            order_data.setName(String.valueOf(Html.fromHtml(jObj.getString("name"))));
                                            order_data.setCustomername(jObj.getString("customername"));
                                            order_data.setPrice(jObj.getString("price"));
                                            order_data.setQty(jObj.getString("qty"));
                                            order_data.setIs_coupon(jObj.getString("is_coupon"));
                                            order_data.setOptions(jObj.getString("options"));
                                            order_data.setSubtotal(jObj.getString("subtotal"));
                                            order_data.setIs_subvalue("false");
                                            list_data.add(order_data);
                                        }

                                    }
                                    Object cart_splitData = jObjDetails.get("cart_split");
                                    if (cart_splitData instanceof JSONObject) {
                                        JSONObject jsonObj = jObjDetails.getJSONObject("cart_split");

                                        if (jsonObj.length() > 0) {
                                            Iterator<String> j = jsonObj.keys();
                                            for (int m = 0; m < jObjDetails.getJSONObject("cart_split").length(); m++) {
                                                String keys = j.next();
                                                Object cart_splitSubData = jObjDetails.getJSONObject("cart_split").get(keys);
                                                if (cart_splitSubData instanceof JSONArray) {
                                                    JSONArray jsonArray = jObjDetails.getJSONObject("cart_split").getJSONArray((keys));
                                                    if (jsonArray.length() > 0) {
                                                        String nm = "";
                                                        for (int f = 0; f < jsonArray.length(); f++) {

                                                            order_data = new OrderDisplay();
                                                            JSONObject jObj = jsonArray.getJSONObject(f);
                                                            order_data.setName(String.valueOf(Html.fromHtml(jObj.getString("name"))));
                                                            order_data.setCustomername(jObj.getString("customername"));
                                                            order_data.setPrice(jObj.getString("price"));
                                                            order_data.setQty(jObj.getString("qty"));
                                                            order_data.setIs_coupon(jObj.getString("is_coupon"));
                                                            order_data.setOptions(jObj.getString("options"));
                                                            order_data.setSubtotal(jObj.getString("subtotal"));
                                                            if (nm.equals(jObj.getString("customername")))
                                                                order_data.setIs_subvalue("false");
                                                            else
                                                                order_data.setIs_subvalue("true");
                                                            list_data.add(order_data);
                                                            nm = jObj.getString("customername");

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    adapter.notifyDataSetChanged();
                                }


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Log.e("getOrderDetails", "exception=" + e.toString());
                            Toast.makeText(BillReportDetailsActivity.this, getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("volleyError", "getOrderDetails=" + volleyError.getMessage());
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(BillReportDetailsActivity.this, getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOrder

    public class ExpandableListCustomAdapter extends BaseExpandableListAdapter {

        @Override
        public int getGroupCount() {
            return list_data.size();


        }

        @Override
        public int getChildrenCount(int groupPosition) {
            JSONArray j = null;
            try {
                j = new JSONArray(list_data.get(groupPosition).getOptions());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return j.length();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return null;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return null;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            TextView productname, text_name;
            TextView qtyvalue, totalvalue;
            LinearLayout layout_name;

            ImageView delete;
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.cartadapter, null);
            }

            productname = (TextView) convertView.findViewById(R.id.productname);
            qtyvalue = (TextView) convertView.findViewById(R.id.value);
            totalvalue = (TextView) convertView.findViewById(R.id.Totalvalue1);
            delete = (ImageView) convertView.findViewById(R.id.delete);
            text_name = (TextView) convertView.findViewById(R.id.text_name);
            layout_name = (LinearLayout) convertView.findViewById(R.id.layout_name);

            if (list_data.get(groupPosition).getIs_subvalue().equals("true")) {
                convertView.findViewById(R.id.grpDivider).setVisibility(View.INVISIBLE);
                layout_name.setVisibility(View.VISIBLE);
                text_name.setText(list_data.get(groupPosition).getCustomername());
            } else {
                convertView.findViewById(R.id.grpDivider).setVisibility(View.VISIBLE);
                layout_name.setVisibility(View.GONE);
            }
            String nm = "";
            if (nm.equals(list_data.get(groupPosition).getCustomername()))
                convertView.findViewById(R.id.grpDivider).setVisibility(View.VISIBLE);
            else {
                convertView.findViewById(R.id.grpDivider).setVisibility(View.INVISIBLE);
            }
            nm = list_data.get(groupPosition).getCustomername();
            productname.setText(list_data.get(groupPosition).getName());
            qtyvalue.setText(list_data.get(groupPosition).getQty());
            totalvalue.setText(getString(R.string.pound_symbol) + " " + list_data.get(groupPosition).getPrice());
            delete.setVisibility(View.GONE);
            expand_list_details.expandGroup(groupPosition);
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.row_cart_ingredients, null);
            }
            final TextView txt_item_name = (TextView) convertView.findViewById(R.id.txt_item_name);
            final TextView txt_item_cost = (TextView) convertView.findViewById(R.id.txt_item_cost);
            final NonScrollListView listsubitems = (NonScrollListView) convertView.findViewById(R.id.listsubitems);
            JSONObject job = null;
            try {
                JSONArray j = new JSONArray(list_data.get(groupPosition).getOptions());
                job = j.getJSONObject(childPosition);
                txt_item_name.setText(job.getString("label"));
                txt_item_cost.setText(getString(R.string.pound_symbol) + " " + job.getString("price"));
                if (job.has("selOpt") && job.get("selOpt") instanceof JSONArray) {
                    JSONArray jArray = job.getJSONArray("selOpt");
                    ArrayList<String> subdata = new ArrayList<>();
                    if (jArray.length() > 0) {
                        for (int p = 0; p < jArray.length(); p++) {
                            JSONObject subObj = jArray.getJSONObject(p);
                            String str = subObj.getString("opt_qty") + "x " + subObj.getString("label");
                            subdata.add(str);
                        }
                        ArrayAdapter<String> oppor_adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.row_cart_ing_subitems, subdata);
                        listsubitems.setAdapter(oppor_adapter);
                        listsubitems.setDividerHeight(0);
                    } else {
                        ArrayAdapter<String> oppor_adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.row_cart_ing_subitems, subdata);
                        listsubitems.setAdapter(oppor_adapter);
                        listsubitems.setDividerHeight(0);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }
}

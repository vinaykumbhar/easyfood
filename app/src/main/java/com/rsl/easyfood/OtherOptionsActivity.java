package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.instabug.library.InstabugTrackingDelegate;
import com.rsl.model.MealOptions;
import com.rsl.model.OptionsData;
import com.rsl.utills.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.widget.ExpandableListView.*;

public class OtherOptionsActivity extends Activity implements View.OnClickListener {
    SQLiteDatabase db;
    TextView textName, textitemDesc, txtheadertotal;
    ImageView imgClose;
    Button btn_close, btn_add, btn_save;
    MealOptions finalMealOptions;
    ArrayList<MealOptions> optionlist = new ArrayList<>();
    ArrayList<MealOptions> permoptionlist = new ArrayList<>();
    ArrayList<OptionsData> list = new ArrayList<>();
    HashMap<Integer, MealOptions> mealFinalList;
    private LinearLayout mListViewOtherOptions;
    SharedPreferences mSharedPreferences;
    Boolean subitemFlag = false;
    int mPosition = -1;
    String res_name = "";
    String menuid = "";
    String menuitemid = "";
    String menuname = "";
    String menudescription = "";
    String menuitemurl = "";
    Double item_price = 00.0;
    int include_price = 0;
    String menuitemname = "";
    ConnectionDetector internet;
    private int product_type = 0;
    HashMap<String, HashMap<String, String>> ingr_data_list;
    HashMap<String, HashMap<String, String>> otherOptions_list;
    ExpandableCartAdapter cartAdapter;
    ExpandableListView expandableoptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_options);
        imgClose = (ImageView) findViewById(R.id.imgClose);
        btn_close = (Button) findViewById(R.id.btn_close);
        btn_add = (Button) findViewById(R.id.btn_add);
        btn_save = (Button) findViewById(R.id.btn_save);

        textName = (TextView) findViewById(R.id.textName);
        textitemDesc = (TextView) findViewById(R.id.textitemDesc);
        txtheadertotal = (TextView) findViewById(R.id.txtheadertotal);
        mListViewOtherOptions = (LinearLayout) findViewById(R.id.linear_ListView);
        expandableoptions = (ExpandableListView) findViewById(R.id.expandableoptions);
        expandableoptions.setChildDivider(getResources().getDrawable(R.drawable.blackimage1));
        // Clicking on Group
        expandableoptions.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                setListViewHeight(parent, groupPosition);
                int index = parent.getFlatListPosition(getPackedPositionForGroup(groupPosition));
                parent.setItemChecked(index, true);
                return false;
            }
        });

        // Clicking on Child
        expandableoptions.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                int index = parent.getFlatListPosition(getPackedPositionForChild(groupPosition, childPosition));
                parent.setItemChecked(index, true);
                return false;
            }
        });

//        expandableoptions.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//
//            @Override
//            public boolean onGroupClick(ExpandableListView parent, View v,
//                                        int groupPosition, long id) {
//                setListViewHeight(parent, groupPosition);
//                return false;
//            }
//        });

        internet = new ConnectionDetector(OtherOptionsActivity.this);
        cartAdapter = new ExpandableCartAdapter(OtherOptionsActivity.this);
        ingr_data_list = new HashMap<>();
        otherOptions_list = new HashMap<>();
        optionlist = new ArrayList<MealOptions>();
//        mealFinalList = new ArrayList<MealOptions>();
        mealFinalList = new HashMap<>();
        permoptionlist = new ArrayList<MealOptions>();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        //add ClickListener to widget
        imgClose.setOnClickListener(this);
        btn_close.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        Bundle b = getIntent().getExtras();
        assert b != null;

        menuid = b.getString("menuid");
        menuitemid = b.getString("menuitemid");
        // String baseprice = b.getString("item_base_price");
        db = openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
        Cursor masterData = db.rawQuery("SELECT * from masterterminal;", null);
        int masterDatacount = masterData.getCount();
        if (masterDatacount > 0) {
            masterData.moveToFirst();
            res_name = masterData.getString(masterData.getColumnIndex("res_name"));
        }
        masterData.close();
        Cursor menuitemData = db.rawQuery("SELECT menu_item_name,price,product_type,imgurl,menu_item_desc,include_price FROM menuitem WHERE menu_item_id=" + menuitemid + " AND menu_id=" + menuid + ";", null);
        int menuitemcount = menuitemData.getCount();
        if (menuitemcount > 0) {
            menuitemData.moveToFirst();
            menuname = menuitemData.getString(menuitemData.getColumnIndex("menu_item_name"));
            item_price = menuitemData.getDouble(menuitemData.getColumnIndex("price"));
            menuitemurl = menuitemData.getString(menuitemData.getColumnIndex("imgurl"));
            product_type = menuitemData.getInt(menuitemData.getColumnIndex("product_type"));
            menudescription = menuitemData.getString(menuitemData.getColumnIndex("menu_item_desc"));
            include_price = menuitemData.getInt(menuitemData.getColumnIndex("include_price"));

            if (internet.isConnectingToInternet())
                getMealOptions(menuitemid);//get meal deal
            else
                internet.showAlertDialog(OtherOptionsActivity.this);
        } else return;

        menuitemData.close();
        textName.setText(menuname);
        textitemDesc.setText(menudescription);
        txtheadertotal.setText(getString(R.string.pound_symbol) + String.valueOf(item_price));
        Log.e("item_price", "=" + item_price);

    }//onCreate

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgClose:
                finish();
                break;
            case R.id.btn_close:
                finish();
                break;

            case R.id.btn_save://save options
                saveOtherOptions();
                break;

            case R.id.btn_add:
                insertTocart();
                break;

        }//switch
    }//onClick

    private void saveOtherOptions() {
        try {

            if (ingr_data_list.size() > 0) {
                Log.e("sizeeee", "=" + mealFinalList.size());
                Log.e("mPosition", "=" + mPosition);
                finalMealOptions.setMealfinallist(ingr_data_list);
                mealFinalList.put(mPosition, finalMealOptions);
            }

            mListViewOtherOptions.removeAllViews();
            findViewById(R.id.layoutotheroptions).setVisibility(View.GONE);
            subitemFlag = true;
        } catch (Exception ignore) {
            subitemFlag = false;
        }
    }

    private void insertTocart() {
        try {
            if (mealFinalList.size() != permoptionlist.size()) {
                Toast t = Toast.makeText(OtherOptionsActivity.this, "Please select options", Toast.LENGTH_SHORT);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                return;
            }
            if (rgOptions != null) {
                if (rgOptions.getCheckedRadioButtonId() == -1) {
                    Toast t = Toast.makeText(OtherOptionsActivity.this, "Please select options", Toast.LENGTH_SHORT);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                    return;
                }
                if (include_price == 0)
                    item_price = 0.00;
            }

            String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
            String nowDate = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());

            Cursor c1 = db.rawQuery("SELECT orderno FROM ordernos WHERE orderno LIKE '" + nowDate.toString() + "%';", null);
            int orderNOCount = c1.getCount();

            String oldOrderNO = "0";
            if (orderNOCount == 0) {
                oldOrderNO = nowDate.toString() + "1";
                db.execSQL("INSERT INTO ordernos values(1," + Group.GROUPID.table + ",'" + oldOrderNO + "', 0, 0);");
            } else {
                c1.moveToLast();
                String orderNumber = c1.getString(0);
                Cursor orderNoCheck = db.rawQuery("SELECT * FROM ordernos WHERE tableno =" + Group.GROUPID.table + " AND billflag = 0 ;", null);
                if (orderNoCheck.getCount() > 0) {
                    orderNoCheck.moveToFirst();
                    oldOrderNO = orderNoCheck.getString(orderNoCheck.getColumnIndex("orderno"));
                } else {
                    Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);
                    int occupiedCount = chairs.getCount();
                    chairs.moveToFirst();
                    if (occupiedCount > 0) {
                        int billNoIncrement = Integer.parseInt(orderNumber.substring(6, orderNumber.length())) + 1;
                        oldOrderNO = orderNumber.substring(0, 6) + billNoIncrement;
                        db.execSQL("INSERT INTO ordernos values(1," + Group.GROUPID.table + ",'" + oldOrderNO + "', 0, 0);");
                    }
                }
            }
            c1.close();

            ContentValues values = new ContentValues();
            values.put("menu_item_id_fk", menuitemid);
            values.put("tableno", Group.GROUPID.table);
            if (Integer.parseInt(Group.GROUPID.table) > 0 && Integer.parseInt(Group.GROUPID.table) < 1500) {
                values.put("ordertype", "Dine In");
            } else
                values.put("ordertype", "Counter Service");
            values.put("menu_item_name", String.valueOf(Html.fromHtml(menuname)));
            values.put("menu_item_price", item_price);
            values.put("menu_item_qty", "1");
            values.put("guestno", CartStaticSragment.CARTStaticSragment.noofguests.getText().toString());
            values.put("menu_item_url", menuitemurl);
            values.put("menu_id", menuid);
            values.put("product_type", product_type);
            values.put("include_price", include_price);
            values.put("cartdatetime", timeStamp);
            values.put("billflag", 0);
            values.put("orderflag", 0);
            values.put("checkflag", 0);
            long cart_id = db.insert("cart", null, values);
            if (mealFinalList.size() > 0) {
                if (cart_id > 0) {
                    for (Map.Entry<Integer, MealOptions> menuData : mealFinalList.entrySet()) {
                        Log.e("ingr_data_list", "Key = " + menuData.getKey() + ", Value_ing_id = " + menuData.getValue().getId());
                        MealOptions data = menuData.getValue();
                        ContentValues ingValues = new ContentValues();
                        ingValues.put("ing_id", data.getId());
                        ingValues.put("ing_qty", data.getQuantity());
                        ingValues.put("cart_id_fk", cart_id);
                        ingValues.put("ingredient_name", data.getText());
                        ingValues.put("include_price", data.getInclude_price());

                        if (data.getInclude_price() == 0)
                            ingValues.put("ing_price", 0.00);
                        else
                            ingValues.put("ing_price", data.getPrice());

                        long ingredient_id = db.insert("ingredients", null, ingValues);
                        // Log.e("ingredient_id", " : " + ingredient_id);
                        if (data.getMealfinallist() != null && subitemFlag) {
                            if (data.getMealfinallist().size() > 0) {
                                for (Map.Entry<String, HashMap<String, String>> subItem : data.getMealfinallist().entrySet()) {
                                    Log.e("ingr_data_list", "Key = " + subItem.getKey() + ", Value = " + subItem.getValue());
                                    HashMap<String, String> d = subItem.getValue();
                                    if (Integer.parseInt(d.get("include_price")) == 0)
                                        d.put("price", "0");

                                    String strOptions = data.getText() + "\n" + d.get("qty") + "X" + d.get("name");
                                    db.execSQL("INSERT INTO subitems(subitem_id,subitem_qty,ing_id_fk,subitem_name,subitem_price,include_price)" +
                                            " values(" + d.get("id") + "," + d.get("qty") + "," + ingredient_id + ",'" + d.get("name") + "'," + d.get("price") + "," + d.get("include_price") + ");");
                                }
                            }
                        }
                    }
                }
            }
            Cursor tableData = db.rawQuery("SELECT occupiedchairs from tables WHERE tableno=" + Group.GROUPID.table + ";", null);
            tableData.moveToFirst();
            if (tableData.getInt(tableData.getColumnIndex("occupiedchairs")) == 0) {
                db.execSQL("UPDATE tables SET occupiedchairs = " + TablesFragment.COVERS + " WHERE  tableno =" + Group.GROUPID.table + ";");
            }
            tableData.close();
            db.close();
            finish();
            Group.GROUPID.alert();
        } catch (Exception e) {
            Toast.makeText(OtherOptionsActivity.this, getResources().getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void getMealOptions(final String product_id) {
        final ProgressDialog dialog = new ProgressDialog(OtherOptionsActivity.this);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(OtherOptionsActivity.this);
        String requestURL = getString(R.string.web_path) + "menu_item_service/getmealoptions/?productid=" + product_id;
        Log.d("requestURL", " : " + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.e("getMealOptions", result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            optionlist.clear();
                            permoptionlist.clear();
                            JSONObject jObjResult = new JSONObject(result);
                            if (jObjResult.has("productMealOptions")) {
                                JSONArray jArrayMealoptions = jObjResult.getJSONArray("productMealOptions");
                                for (int i = 0; i < jArrayMealoptions.length(); i++) {
                                    JSONObject objMeal = jArrayMealoptions.getJSONObject(i);
                                    if (!objMeal.getString("qty").equals("")) {
                                        int qty = 0;
                                        try {
                                            qty = Integer.parseInt(objMeal.getString("qty"));
                                        } catch (Exception ignore) {
                                            qty = 0;
                                        }
                                        if (qty > 0) {
                                            for (int q = 0; q < qty; q++) {
                                                MealOptions mealOptions = new MealOptions();
                                                mealOptions.setId(objMeal.getString("meal_config_id"));
                                                mealOptions.setText("Select " + String.valueOf(Html.fromHtml(objMeal.getString("category_name"))));
                                                mealOptions.setIsSimple(objMeal.getString("is_simple"));

                                                if (objMeal.getString("is_simple").equals("0")) {
                                                    OptionsData optionsData = new OptionsData();
                                                    optionsData.setText(objMeal.getJSONObject("option_data").getString("option_data"));
                                                    optionsData.setId(objMeal.getJSONObject("option_data").getString("option_id"));
                                                    mealOptions.setOptionsData(optionsData);
                                                }

                                                ArrayList<MealOptions> subitems = new ArrayList<>();
                                                Iterator<String> j = objMeal.getJSONObject("sub_items").keys();
                                                for (int m = 0; m < objMeal.getJSONObject("sub_items").length(); m++) {
                                                    String itemId = j.next();
                                                    JSONObject jObjSubitems = objMeal.getJSONObject("sub_items").getJSONObject(itemId);

                                                    MealOptions data = new MealOptions();
                                                    data.setSubItemId(itemId);
                                                    data.setId(jObjSubitems.getString("id"));
                                                    data.setMenuid(jObjSubitems.getString("menu_id"));
                                                    data.setProductType(jObjSubitems.getString("product_type"));
                                                    data.setText(String.valueOf(Html.fromHtml(jObjSubitems.getString("name"))));
                                                    data.setDescription(String.valueOf(Html.fromHtml(jObjSubitems.getString("desc"))));
                                                    data.setCode(jObjSubitems.getString("code"));
                                                    data.setInclude_price(jObjSubitems.getInt("include_price"));
                                                    subitems.add(data);
                                                }
                                                mealOptions.setListOptions(subitems);
                                                optionlist.add(mealOptions);
                                                permoptionlist.add(mealOptions);
                                            }
                                        }
                                    }
                                }
                            }

                            if (optionlist.size() != 0) {
                                cartAdapter = new ExpandableCartAdapter(OtherOptionsActivity.this);
                                expandableoptions.setAdapter(cartAdapter);
                                cartAdapter.notifyDataSetChanged();
                                setListViewHeightBasedOnChildren(expandableoptions);
                            }
                            if (dialog.isShowing())
                                dialog.dismiss();

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Log.e("jsonException", "=" + e.toString());
                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Log.e("volleyException", "=" + volleyError.getMessage());
                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getMealOptions

    private void getOtheroptionsIsSimple(final String product_id) {
        final ProgressDialog dialog = new ProgressDialog(OtherOptionsActivity.this);
        dialog.setMessage(getString(R.string.str_loading_item_details));
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(OtherOptionsActivity.this);
        String requestURL = getString(R.string.web_path) + "menu_item_service/getitemoptionsMeal/?productid=" + product_id;
        Log.e("requestURL", " : " + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.d("get_is_simple2", result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject mainJsonObj = new JSONObject(result);
                            if (mainJsonObj.length() > 0) {
                                if (dialog.isShowing())
                                    dialog.dismiss();

                                list = new ArrayList<>();
                                Object detailsObject = mainJsonObj.get("productdetails");
                                if (detailsObject instanceof JSONObject) {
                                    JSONObject jObjDetails = mainJsonObj.getJSONObject("productdetails");

                                }

                                //JSONObject attributesObject = mainJsonObj.getJSONObject("main_attribute");
                                Iterator<String> iteratorAttrbs = mainJsonObj.getJSONObject("main_attribute").keys();
                                for (int m = 0; m < mainJsonObj.getJSONObject("main_attribute").length(); m++) {
                                    String itemId = iteratorAttrbs.next();
                                    JSONObject jObjAttr = mainJsonObj.getJSONObject("main_attribute").getJSONObject(itemId);
                                    Log.e("menu_attributes_id", "=" + jObjAttr.getString("menu_attributes_id"));
                                    OptionsData attrData = new OptionsData();

                                    attrData.setId(jObjAttr.getString("menu_attributes_id"));
                                    attrData.setText(jObjAttr.getString("attribute_name"));
                                    attrData.setType(jObjAttr.getString("attribute_type"));
                                    ArrayList<OptionsData> mList = new ArrayList<>();
                                    Object optionsObject = jObjAttr.get("options");
                                    if (optionsObject instanceof JSONArray) {
                                        JSONArray optionsJsonArray = jObjAttr.getJSONArray("options");
                                        for (int j = 0; j < optionsJsonArray.length(); j++) {
                                            JSONObject opt = optionsJsonArray.getJSONObject(j);
                                            int configid = opt.getInt("configitemid");
                                            String optName = opt.getString("option_data");
                                            Double optPrice = opt.getDouble("attribute_price");
                                            OptionsData data = new OptionsData();
                                            data.setId(String.valueOf(configid));
                                            data.setText(optName);
                                            data.setPrice(optPrice);
                                            data.setInclude_price(1);
                                            mList.add(data);
                                        }
                                    }
                                    attrData.setListOptions(mList);
                                    list.add(attrData);
                                }
                                if (list.size() > 0)
                                    setdata();

                            } else {
                                mListViewOtherOptions.removeAllViews();
                                findViewById(R.id.layoutotheroptions).setVisibility(View.GONE);
                                if (dialog.isShowing())
                                    dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Log.e("getOtheroptions", "exception=" + e.toString());
                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOtheroptions

    private void getOtheroptions(final int grppos, final String configId, final String resid, final String product_id) {
        final ProgressDialog dialog = new ProgressDialog(OtherOptionsActivity.this);
        dialog.setMessage(getString(R.string.str_loading_item_details));
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(OtherOptionsActivity.this);
        String requestURL = getString(R.string.web_path) + "menu_item_service/getOtheroptions/?mainid=" + configId + "&resid=" + resid + "&product_id=" + product_id;
        Log.e("requestURL", " : " + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.e("getOtheroptions_response", result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            //  String res = "[{\"id\":\"176\",\"text\":\"Vegetable Topping\",\"type\":\"multiple\",\"options\":{\"881\":\"Cheese\",\"887\":\"Mixed Pepper\",\"886\":\"red onions\",\"885\":\"Pineapple\",\"884\":\"Mushroom\",\"883\":\"jalapenos\",\"882\":\"cherry tomatoes\"},\"saved_options_ids\":[\"12494\",\"12500\",\"12499\",\"12498\",\"12497\",\"12496\",\"12495\"],\"options_price\":[\"0.55\",\"0.55\",\"0.55\",\"0.55\",\"0.55\",\"0.55\",\"0.55\"]},{\"id\":\"175\",\"text\":\"Meat Topping\",\"type\":\"multiple\",\"options\":{\"872\":\"Pepperoni\",\"873\":\"Spicy Mince\",\"874\":\"Chicken\",\"875\":\"Chicken Tikka\",\"876\":\"Spicy Chicken\",\"877\":\"Tuna\"},\"saved_options_ids\":[\"12501\",\"12502\",\"12503\",\"12504\",\"12505\",\"12506\"],\"options_price\":[\"0.95\",\"0.95\",\"0.95\",\"0.95\",\"0.95\",\"0.95\"]}]";
                            JSONArray jsonArray = new JSONArray(result);
                            if (jsonArray.length() > 0) {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                                list = new ArrayList<>();
                                JSONObject jObjData = null;
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    OptionsData data = new OptionsData();
                                    jObjData = jsonArray.getJSONObject(i);

                                    data.setSubitemid(configId);
                                    data.setGroupPos(grppos);
                                    data.setMenuidid(product_id);
                                    data.setId(jObjData.getString("id"));
                                    data.setText(jObjData.getString("text"));
                                    data.setType(jObjData.getString("type"));
                                    JSONArray jsonArrayPrice = jObjData.getJSONArray("options_price");
                                    JSONArray jsonArrayids = jObjData.getJSONArray("saved_options_ids");

                                    ArrayList<OptionsData> list_option = new ArrayList<>();
                                    if (jObjData.getJSONObject("options").length() > 0) {
                                        Iterator<String> j = jObjData.getJSONObject("options").keys();
                                        for (int m = 0; m < jObjData.getJSONObject("options").length(); m++) {
                                            OptionsData data1 = new OptionsData();
                                            String k = j.next();
                                            //Log.e("Info", "Key: " + k + ", value: " + jObjData.getJSONObject("options").getString(k));
                                            data1.setId(jsonArrayids.get(m).toString());
                                            data1.setText(jObjData.getJSONObject("options").getString(k));
                                            data1.setInclude_price(1);
                                            data1.setPrice(Double.parseDouble(jsonArrayPrice.get(m).toString()));
                                            list_option.add(data1);
                                        }
                                    }
                                    data.setListOptions(list_option);
                                    list.add(data);
                                }
                                setdata();
                            } else {
                                mListViewOtherOptions.removeAllViews();
                                findViewById(R.id.layoutotheroptions).setVisibility(View.GONE);
                                if (dialog.isShowing())
                                    dialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Log.e("getOtheroptions", "exception=" + e.toString());
                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOtheroptions

    RadioGroup rgOptions, rgOtherOptions;


    public class ExpandableCartAdapter extends BaseExpandableListAdapter {
        private Activity activity;

        public ExpandableCartAdapter(Activity a) {
            activity = a;
        }

        @Override
        public int getGroupCount() {
            return optionlist.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return optionlist.get(groupPosition).getListOptions().size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return null;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return optionlist.get(groupPosition).getListOptions().get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            TextView productname;
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.rowgroupotheroptions, null);
            }
            productname = (TextView) convertView.findViewById(R.id.productname);
            productname.setText(optionlist.get(groupPosition).getText());
            return convertView;
        }

        @Override
        public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.rowchildotheroptions, null);
            }
            final MealOptions p = optionlist.get(groupPosition).getListOptions().get(childPosition);

            String strOptiondata = "";
            if (optionlist.get(groupPosition).getIsSimple().equals("0")) {
                strOptiondata = " - " + optionlist.get(groupPosition).getOptionsData().getText();
            }
            final TextView txt_item_name = (TextView) convertView.findViewById(R.id.txt_item_name);
            final String subitem = p.getText() + strOptiondata;
            txt_item_name.setText(subitem);
            txt_item_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ingr_data_list = new HashMap<>();
                    subitemFlag = false;
                    if (p.getProductType().equals("1")) {
                        finalMealOptions = new MealOptions();
                        finalMealOptions = p;
                        finalMealOptions.setQuantity(1);
                        finalMealOptions.setMealfinallist(ingr_data_list);
                        mealFinalList.put(groupPosition, p);
                        toastMessage("Added " + p.getText());
                        findViewById(R.id.layoutotheroptions).setVisibility(View.GONE);
                    } else if (p.getProductType().equals("2") && optionlist.get(groupPosition).getIsSimple().equals("2")) {
                        mPosition = groupPosition;
                        finalMealOptions = new MealOptions();
                        finalMealOptions = p;
                        finalMealOptions.setQuantity(1);
                        finalMealOptions.setMealfinallist(ingr_data_list);
                        mealFinalList.put(groupPosition, p);
                        if (internet.isConnectingToInternet())
                            getOtheroptionsIsSimple(p.getId());
                        else
                            internet.showAlertDialog(OtherOptionsActivity.this);

                    } else if (p.getProductType().equals("2")) {

                        mPosition = groupPosition;
                        finalMealOptions = new MealOptions();
                        finalMealOptions = p;
                        finalMealOptions.setQuantity(1);
                        finalMealOptions.setMealfinallist(ingr_data_list);
                        mealFinalList.put(groupPosition, p);
                        //toastMessage("Added " + p.getText());
                        if (internet.isConnectingToInternet())
                            getOtheroptions(groupPosition, p.getSubItemId(), mSharedPreferences.getString("mid", ""), p.getId());
                        else
                            internet.showAlertDialog(OtherOptionsActivity.this);
                    }
                    optionlist.get(groupPosition).setText(subitem);
                    expandableoptions.collapseGroup(groupPosition);

                }
            });
            return convertView;
        }


        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }

    private void toastMessage(String msg) {
        Toast t = Toast.makeText(OtherOptionsActivity.this, "" + msg, Toast.LENGTH_SHORT);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setdata() {
        mListViewOtherOptions.removeAllViews();
        for (int p = 0; p < list.size(); p++) {
            LayoutInflater inflater2 = null;
            inflater2 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mLinearViewOptins = null;
            final OptionsData obj = list.get(p);
            final String itemName = obj.getText();
            if (obj.getType().equalsIgnoreCase("radio")) {
                mLinearViewOptins = inflater2.inflate(R.layout.row_radio_options, null);
                final TextView ingr_name = (TextView) mLinearViewOptins.findViewById(R.id.ingr_name);
                ingr_name.setText(itemName + ":");
                final RadioButton[] rb = new RadioButton[obj.getListOptions().size()];
                rgOtherOptions = (RadioGroup) mLinearViewOptins.findViewById(R.id.radio);
                for (final OptionsData obj1 : obj.getListOptions()) {
                    rb[p] = new RadioButton(this);

                    rb[p].setButtonDrawable(R.drawable.radio_selector);
                    rb[p].setText(obj1.getText() + "(" + obj1.getPrice() + ")");
                    rb[p].setTextSize(18);
                    rb[p].setPadding(0, 5, 0, 5);
                    rb[p].setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                    rb[p].setGravity(Gravity.START);
                    rb[p].setTextColor(getResources().getColor(R.color.black));
                    rgOtherOptions.addView(rb[p]);
                    rb[p].setTag(p);
                    final int finalK = p;
                    rb[p].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                for (OptionsData tempobj : obj.getListOptions()) {
                                    for (Map.Entry<String, HashMap<String, String>> entry : ingr_data_list.entrySet()) {
                                        if (entry.getKey().equals(tempobj.getId())) {
                                            ingr_data_list.remove(tempobj.getId());
                                        }
                                    }
                                }
                                HashMap<String, String> data = new HashMap<>();
                                data.put("id", obj1.getId());
                                data.put("name", obj1.getText());
                                data.put("qty", String.valueOf("1"));
                                data.put("price", String.valueOf(obj1.getPrice()));
                                data.put("include_price", String.valueOf(obj1.getInclude_price()));

                                ingr_data_list.put(obj1.getId(), data);
                            }
                        }
                    });

                }
            } else if (obj.getType().equalsIgnoreCase("multiple")) {
                if (mLinearViewOptins == null)
                    mLinearViewOptins = inflater2.inflate(R.layout.row_options, null);

                final TextView ingr_name = (TextView) mLinearViewOptins.findViewById(R.id.ingr_name);
                LinearLayout mlayoutoptions = (LinearLayout) mLinearViewOptins.findViewById(R.id.layoutoptions);

                ingr_name.setText(itemName + ":");

                for (int i = 0; i < obj.getListOptions().size(); i++) {
                    final OptionsData obj1 = obj.getListOptions().get(i);
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View mOptions = inflater.inflate(R.layout.row_multiselect_options, null);
                    mOptions.setTag(String.valueOf(i));

                    final TextView optionname = (TextView) mOptions.findViewById(R.id.optionname);
                    final TextView txtqty = (TextView) mOptions.findViewById(R.id.txtqty);
                    final TextView txttotalcost = (TextView) mOptions.findViewById(R.id.txttotalcost);

                    final ImageButton imgbtnincrement = (ImageButton) mOptions.findViewById(R.id.imgbtnincrement);
                    final ImageButton imgbtndecrement = (ImageButton) mOptions.findViewById(R.id.imgbtndecrement);
                    final LinearLayout layouttotal = (LinearLayout) mOptions.findViewById(R.id.layouttotal);
                    layouttotal.setTag(String.valueOf(i));
                    txtqty.setTag(String.valueOf(i));
                    txttotalcost.setTag(String.valueOf(i));
                    txttotalcost.setVisibility(View.GONE);
                    final TextView optionprice = (TextView) mOptions.findViewById(R.id.optionprice);
                    optionname.setText(obj1.getText());
                    optionprice.setText(String.format(Locale.US, "%.2f", obj1.getPrice()));
                    optionprice.setVisibility(View.GONE);
                    final int finalI = i;
                    imgbtnincrement.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int qty = Integer.parseInt(txtqty.getText().toString());
                            if (qty >= 0) {
                                qty++;
                            }
                            Double total = qty * obj1.getPrice();
                            txtqty.setText(String.valueOf(qty));
                            txttotalcost.setText(String.format(Locale.US, "%.2f", total));
                            layouttotal.setVisibility(View.VISIBLE);

                            HashMap<String, String> data = new HashMap<>();
                            data.put("id", obj1.getId());
                            data.put("name", obj1.getText());
                            data.put("qty", String.valueOf(qty));
                            data.put("price", String.valueOf(obj1.getPrice()));
                            data.put("include_price", String.valueOf(obj1.getInclude_price()));
                            ingr_data_list.put(obj1.getId(), data);

                        }
                    });
                    imgbtndecrement.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int qty = Integer.parseInt(txtqty.getText().toString());
                            if (qty > 0) {
                                qty--;
                            } else {
                                qty = 0;
                            }
                            if (qty == 0) {
                                layouttotal.setVisibility(View.GONE);
                                txtqty.setText(String.valueOf(qty));
                                ingr_data_list.remove(obj1.getId());
                            } else {
                                Double total = qty * obj1.getPrice();
                                txtqty.setText(String.valueOf(qty));
                                txttotalcost.setText(String.format(Locale.US, "%.2f", total));
                                HashMap<String, String> data = new HashMap<>();
                                data.put("id", obj1.getId());
                                data.put("name", obj1.getText());
                                data.put("qty", String.valueOf(qty));
                                data.put("price", String.valueOf(obj1.getPrice()));
                                data.put("include_price", String.valueOf(obj1.getInclude_price()));
                                ingr_data_list.put(obj1.getId(), data);
                            }
                        }
                    });
                    mlayoutoptions.addView(mOptions);
                }
            }
            mListViewOtherOptions.addView(mLinearViewOptins);
        }
        findViewById(R.id.layoutotheroptions).setVisibility(View.VISIBLE);
    }

    private void setListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.setLayoutParams(new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += groupItem.getMeasuredHeight();
            if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null, listView);
                    listItem.setLayoutParams(new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += listItem.getMeasuredHeight();
                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void setListViewHeightBasedOnChildren(ExpandableListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, null);
            if (listItem != null) {
                int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                        View.MeasureSpec.EXACTLY);
                // listItem.setLayoutParams(new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                //listItem.measure(0,0);
                listItem.setLayoutParams(new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

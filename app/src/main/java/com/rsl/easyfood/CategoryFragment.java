package com.rsl.easyfood;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import static com.instabug.library.Instabug.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 * From this fragment it will go to group ACTIVITY it will display product fragment
 */
public class CategoryFragment extends Fragment {

    Bitmap mBitmapcat;
    String groupId;
    ArrayList<Imagepojo> cartarray;
    static CategoryFragment CATEGORYID;
    MyAdapter adapter;
    android.app.FragmentManager fragmentManager;
    android.app.FragmentTransaction fragmentTransaction;
    SharedPreferences mSharedPreferences;
    File file;
    File[] allFiles;
    Random r;
    Group group;
    String itemId;
    String searchFlag;
    String groupSearch = "0";
    String notSearchMode = "1";
    SQLiteDatabase db;
//    ArrayList<Imagepojo> productarray;

    static class ViewHolder {
        TextView itemName;
        ImageView itemImage;
    }

    GridView categoryList;

    public class MyAdapter extends BaseAdapter {
        // Declare variables
        private Activity activity;

        @SuppressWarnings("unused")
        private LayoutInflater inflater = null;
        ArrayList<Imagepojo> list;

        public MyAdapter(Activity a, ArrayList<Imagepojo> list) {
            activity = a;
            this.list = list;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            return list.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            ViewHolder holder = null;

            if (vi == null) {
                vi = inflater.inflate(R.layout.grid_viewitem_category, parent, false);
                holder = new ViewHolder();
                holder.itemName = (TextView) vi.findViewById(R.id.text);
                holder.itemImage = (ImageView) vi.findViewById(R.id.image);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }
            holder.itemName.setText(Html.fromHtml(list.get(position).getPaymnet()));
//            File imgFile = new File(Environment.getExternalStorageDirectory().getPath() + "/pos/img/" + cartarray.get(position).getImageur());
            String imgUrl = list.get(position).getImageur();
            if (imgUrl != null) {
                Glide.with(activity).load(imgUrl)
                        .thumbnail(0.5f)
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.itemImage);

//                mBitmapcat = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//                holder.itemImage.setImageBitmap(mBitmapcat);
            } else {
                holder.itemImage.setImageResource(R.drawable.you_cloud);
            }
//            if (position == 0) {
//                String categoryId = "-1";
//                Log.e("CategoryFragment", "=" + group_clicked);
//                if (!group_clicked) {
//                    db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
//                    Cursor c = db.rawQuery("SELECT categoryid from category WHERE categoryname ='"
//                            + cartarray.get(position).getPaymnet() + "';", null);
//                    c.moveToFirst();
//                    categoryId = c.getString(c.getColumnIndex("categoryid"));
//                    db.close();
//                }
//                if (!productFragment.isResumed()) {
//                    fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.container3, productFragment);
//                    fragmentTransaction.commit();
//                    Bundle bundle1 = new Bundle();
//                    bundle1.putString("searchFlag", "1");
//                    bundle1.putString("SearchText", categoryId);
//                    productFragment.setArguments(bundle1);
//                } else {
//                    fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.container3, cat1Frag);
//                    fragmentTransaction.commit();
//                    Bundle bundle1 = new Bundle();
//                    bundle1.putString("searchFlag", "1");
//                    bundle1.putString("SearchText", categoryId);
//                    cat1Frag.setArguments(bundle1);
//                }
//            }

            return vi;
        }
    }

    public CategoryFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        CATEGORYID = this;
        group = (Group) getActivity();
        cartarray = new ArrayList<Imagepojo>();
        //    productarray = new ArrayList<>();
        fragmentManager = getFragmentManager();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());



        /*if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_LONG)
                    .show();
        } else {
            file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "/pos/img");
            allFiles = file.listFiles();
            if (allFiles.length == 0) {
                Toast.makeText(getActivity(), "FOLDER IS EMPTY", Toast.LENGTH_LONG).show();
            }
        }*/

        categoryList = (GridView) view.findViewById(R.id.gridview3);
        Bundle bundle = getArguments();
        groupId = bundle.getString("SearchText");
        searchFlag = bundle.getString("searchFlag");
        if (searchFlag.equals(groupSearch)) {
            db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
//            getActivity().getWindow().setSoftInputMode(
//                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

            InputMethodManager inputMethodManager = (InputMethodManager) Group.GROUPID.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

            Cursor productData = db.rawQuery("SELECT menu_id,menu_item_name,imgurl,menu_item_id,include_price,price,product_type from menuitem ;", null);
            int count1 = productData.getCount();
            productData.moveToFirst();
            for (Integer rowCount = 0; rowCount < count1; rowCount++) {
                Imagepojo imagepojo = new Imagepojo();
                String product = "";
                String image = "";
                String itemid = "";
                int include_price = 0;
                product = productData.getString(productData.getColumnIndex("menu_item_name"));
                image = productData.getString(productData.getColumnIndex("imgurl"));
                itemid = productData.getString(productData.getColumnIndex("menu_item_id"));
                include_price = productData.getInt(productData.getColumnIndex("include_price"));

                imagepojo.setPaymnet(product);
                imagepojo.setGroupid(productData.getString(productData.getColumnIndex("menu_id")));
                imagepojo.setImageur(image);
                imagepojo.setId(itemid);
                imagepojo.setInclude_price(include_price);
                imagepojo.setPrice(productData.getDouble(productData.getColumnIndex("price")));
                imagepojo.setProduct_type(productData.getInt(productData.getColumnIndex("product_type")));
                cartarray.add(imagepojo);
                productData.moveToNext();
            }
            search();
        } else if (searchFlag.equals(notSearchMode)) {
            db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
            Cursor c = db.rawQuery("SELECT * from menuitem WHERE menu_id =" + groupId + ";", null);
            int count = c.getCount();
            if (count > 0) {
                while (c.moveToNext()) {
                    Imagepojo imagepojo = new Imagepojo();
                    String product = "";
                    String image = "";
                    String itemid = "";
                    int include_price = 0;
                    imagepojo.setGroupid(groupId);
                    product = c.getString(c.getColumnIndex("menu_item_name"));
                    image = c.getString(c.getColumnIndex("imgurl"));
                    itemid = c.getString(c.getColumnIndex("menu_item_id"));
                    include_price = c.getInt(c.getColumnIndex("include_price"));
                    imagepojo.setPaymnet(product);
                    imagepojo.setImageur(image);
                    imagepojo.setId(itemid);
                    imagepojo.setInclude_price(include_price);
                    imagepojo.setPrice(c.getDouble(c.getColumnIndex("price")));
                    imagepojo.setProduct_type(c.getInt(c.getColumnIndex("product_type")));
                    cartarray.add(imagepojo);
                }
            }
            db.close();
            adapter = new MyAdapter(getActivity(), cartarray);
            categoryList.setAdapter(adapter);
            categoryList.setSelector(getResources().getDrawable(R.drawable.item_selector));
            adapter.notifyDataSetChanged();


        }
        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                try {

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(arg1.getWindowToken(), 0);

                    if (!db.isOpen())
                        db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
                    if (CartStaticSragment.CARTStaticSragment.tableno.getText().toString().trim().equals("")) {
                        Toast.makeText(getActivity(), "Select table", Toast.LENGTH_SHORT).show();
                    } else if (Integer.parseInt(CartStaticSragment.CARTStaticSragment.tableno.getText().toString()) < 1500) {
                        Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();

                        if (!CartStaticSragment.CARTStaticSragment.noofguests.getText().toString().matches("0")) {
                            selectData(arg2);
                        } else {
                            AlertDialog.Builder adb = new AlertDialog.Builder(
                                    getActivity(), android.R.style.Theme_Dialog);
                            adb.setTitle("");
                            adb.setMessage("Select number of guests before adding data");
                            adb.setPositiveButton("Ok", null);
                            adb.show();
                        }
                    } else {
                        TablesFragment.COVERS = "1";
                        selectData(arg2);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }

    public void test() {
        adapter = new MyAdapter(getActivity(), cartarray);
        categoryList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    TextWatcher searchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if (s.length() == 0) {
                search.setVisibility(View.INVISIBLE);
                //  Group.GROUPID.delete.setVisibility(View.INVISIBLE);
                Group.GROUPID.lay_search.setVisibility(View.GONE);
                Group.GROUPID.lay_info.setVisibility(View.VISIBLE);
                searchView.setVisibility(View.VISIBLE);

                if (!Group.GROUPID.groupfragment.isResumed()) {
                    Group.GROUPID.fragmentTransaction = Group.GROUPID.fragmentManager.beginTransaction();
                    Group.GROUPID.fragmentTransaction.replace(R.id.container2, Group.GROUPID.groupfragment);
                    Group.GROUPID.fragmentTransaction.commit();
//                        View view = Group.GROUPID.getCurrentFocus();
//                        InputMethodManager inputMethodManager = (InputMethodManager) Group.GROUPID.getSystemService(Context.INPUT_METHOD_SERVICE);
//                        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
            //  if (searchFlag.equals(groupSearch)) {
            if (db != null && !db.isOpen())
                db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);

            Cursor c = db.rawQuery("SELECT menu_id,menu_item_id,menu_item_name,menu_item_desc,imgurl,price,product_type,include_price FROM menuitem WHERE menu_item_name LIKE '" +
                    s.toString() + "%';", null);
            int count = c.getCount();
            if (count > 0) {
                c.moveToFirst();
                cartarray.clear();
                for (Integer j = 0; j < count; j++) {
                    Imagepojo imagepojo = new Imagepojo();
                    String product = "";
                    String image = "";
                    imagepojo.setGroupid(c.getString(c.getColumnIndex("menu_id")));
                    String itemid = c.getString(c.getColumnIndex("menu_item_id"));
                    product = c.getString(c.getColumnIndex("menu_item_name"));
                    image = c.getString(c.getColumnIndex("imgurl"));
                    imagepojo.setInclude_price(c.getColumnIndex("include_price"));
                    imagepojo.setPrice(c.getDouble(c.getColumnIndex("price")));
                    imagepojo.setProduct_type(c.getInt(c.getColumnIndex("product_type")));
                    imagepojo.setPaymnet(product);
                    imagepojo.setId(itemid);
                    imagepojo.setImageur(image);
                    cartarray.add(imagepojo);
                    c.moveToNext();
                }
                test();
            }
            c.close();
            db.close();
        }
    };
    EditText search;
    ImageView searchView;

    public void search() {
//        SQLiteDatabase   db;
        search = (EditText) getActivity().findViewById(R.id.titleditext);
        search.requestFocus();
        searchView = (ImageView) getActivity().findViewById(R.id.titlebarSearchview);
        search.addTextChangedListener(searchWatcher);

    }

    private void selectData(int arg2) {
        String menuitemid = cartarray.get(arg2).getId();
        String groupId = cartarray.get(arg2).getGroupid();
        String menuname = cartarray.get(arg2).getPaymnet();
        String imageurl = cartarray.get(arg2).getImageur();
        Double PRODUCTPRICE = cartarray.get(arg2).getPrice();
        int prod_type = cartarray.get(arg2).getProduct_type();
        int include_price = cartarray.get(arg2).getInclude_price();
        String noOfGuest = CartStaticSragment.CARTStaticSragment.noofguests.getText().toString();
        if (prod_type == 1) {

//            db.beginTransaction();
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
            String nowDate = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());

//                                Cursor ordercheck = db.rawQuery("SELECT orderid FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);
//                                int count = ordercheck.getCount();
//                                String checkorder = "0";
//                                if (count > 0) {
//                                    while (ordercheck.moveToNext()) {
//                                        checkorder = ordercheck.getString(ordercheck.getColumnIndex("orderid"));
//                                    }
//                                }

            Cursor c1 = db.rawQuery("SELECT orderno FROM ordernos WHERE orderno LIKE '" + nowDate.toString() + "%';", null);
            int orderNOCount = c1.getCount();

            String oldOrderNO = "0";
            if (orderNOCount == 0) {
                oldOrderNO = nowDate.toString() + "1";
                db.execSQL("INSERT INTO ordernos values(1," + Group.GROUPID.table + ",'" + oldOrderNO + "', 0, 0);");
//                                    if (checkInternet()) {
//                                        Intent intent = new Intent(getActivity(), ServiceOccupiedChairs.class);
//                                        getActivity().startService(intent);
//                                    }
            } else {
                c1.moveToLast();
                String orderNumber = c1.getString(0);
                Cursor orderNoCheck = db.rawQuery("SELECT * FROM ordernos WHERE tableno =" + Group.GROUPID.table + " AND billflag = 0 ;", null);
                if (orderNoCheck.getCount() > 0) {
                    orderNoCheck.moveToFirst();
                    oldOrderNO = orderNoCheck.getString(orderNoCheck.getColumnIndex("orderno"));
                } else {
                    Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);
                    int occupiedCount = chairs.getCount();
                    chairs.moveToFirst();
                    if (occupiedCount > 0) {
                        int billNoIncrement = Integer.parseInt(orderNumber.substring(6, orderNumber.length())) + 1;
                        oldOrderNO = orderNumber.substring(0, 6) + billNoIncrement;
                        db.execSQL("INSERT INTO ordernos values(1," + Group.GROUPID.table + ",'" + oldOrderNO + "', 0, 0);");
//                                        my.Insert(String.valueOf(MainActivity.MAINID.outletid), CartStaticSragment.CARTStaticSragment.tableno.getText().toString(), CartStaticSragment.CARTStaticSragment.noofguests.getText().toString(), UserLoginActivity.user_LoginID.nameid, oldOrderNO);
//                                        if (checkInternet()) {
//                                            Intent intent = new Intent(getActivity(), ServiceOccupiedChairs.class);
//                                            getActivity().startService(intent);
//                                        }
                    }
                }
            }
            //is item exist
            Cursor cartData = db.rawQuery("SELECT cart_id,menu_item_id_fk,menu_item_qty FROM cart WHERE tableno=" + Group.GROUPID.table + " AND menu_item_id_fk=" + menuitemid + " AND menu_id=" + groupId + " AND billflag=0;", null);
            int cartDatacount = cartData.getCount();
            if (cartDatacount > 0) {
                cartData.moveToFirst();
                int oldQty = cartData.getInt(cartData.getColumnIndex("menu_item_qty"));
                int cartid = cartData.getInt(cartData.getColumnIndex("cart_id"));
                if (oldQty > 0) {
                    oldQty += 1;
                    db.execSQL("UPDATE cart SET menu_item_qty =" + oldQty + " WHERE  tableno=" + Group.GROUPID.table + " AND menu_item_id_fk =" + menuitemid
                            + " AND menu_id=" + groupId + " AND guestno=" + noOfGuest
                            + " AND cart_id=" + cartid + ";");
                    db.execSQL("UPDATE tables SET occupiedchairs = " + noOfGuest + " WHERE tableno = " +
                            Group.GROUPID.table + ";");
                    Toast.makeText(getActivity(), menuname + " added to cart", Toast.LENGTH_SHORT).show();
                }
            } else {

                ContentValues values = new ContentValues();
                values.put("menu_item_id_fk", menuitemid);
                values.put("tableno", Group.GROUPID.table);
                if (Integer.parseInt(Group.GROUPID.table) > 0 && Integer.parseInt(Group.GROUPID.table) < 1500) {
                    values.put("ordertype", "Dine In");
                } else
                    values.put("ordertype", "Counter Service");

                values.put("menu_item_name", menuname);
                values.put("menu_item_price", PRODUCTPRICE);
                values.put("menu_item_qty", "1");
                values.put("guestno", noOfGuest);
                values.put("menu_item_url", imageurl);
                values.put("menu_id", groupId);
                values.put("product_type", prod_type);
                values.put("include_price", include_price);
                values.put("cartdatetime", timeStamp);
                values.put("billflag", 0);
                values.put("orderflag", 0);
                values.put("checkflag", 0);
                long cart_id = db.insert("cart", null, values);
                Log.e("cart_id","type1"+cart_id);
                if (cart_id > 0) {
                    Toast.makeText(getActivity(), menuname + " added to cart", Toast.LENGTH_SHORT).show();
                }
            }
            CartStaticSragment.CARTStaticSragment.plus.setEnabled(false);
            CartStaticSragment.CARTStaticSragment.minus.setEnabled(false);

            if (!Group.GROUPID.groupfragment.isResumed()) {
                Group.GROUPID.search.setVisibility(View.INVISIBLE);
                Group.GROUPID.search.setText("");
                Group.GROUPID.lay_search.setVisibility(View.GONE);
                Group.GROUPID.lay_info.setVisibility(View.VISIBLE);
                View view = Group.GROUPID.getCurrentFocus();
                InputMethodManager inputMethodManager = (InputMethodManager) Group.GROUPID.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                Group.GROUPID.fragmentTransaction = Group.GROUPID.fragmentManager.beginTransaction();
                Group.GROUPID.fragmentTransaction.replace(R.id.container2, Group.GROUPID.groupfragment);
                Group.GROUPID.fragmentTransaction.commit();
            }
//            db.setTransactionSuccessful();
//            db.endTransaction();

            if (!db.isOpen())
                db = getActivity().openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
            Cursor tableData = db.rawQuery("SELECT occupiedchairs from tables WHERE tableno=" + Group.GROUPID.table + ";", null);
            tableData.moveToFirst();
            if (tableData.getInt(tableData.getColumnIndex("occupiedchairs")) == 0) {
                db.execSQL("UPDATE tables SET occupiedchairs = " + TablesFragment.COVERS + " WHERE  tableno =" + Group.GROUPID.table + ";");
            }

            db.close();
            if(search!=null){
                search.setText("");
            }
            group.alert();
        } else if (prod_type == 2) {
            Cursor attributes = db.rawQuery("SELECT menu_attributes_id FROM attributes WHERE menu_item_id=" + menuitemid + " AND menu_id=" + groupId + ";", null);
            int attributescount = attributes.getCount();
            if (attributescount > 0) {
                attributes.moveToFirst();
                int attribute_id = attributes.getInt(attributes.getColumnIndex("menu_attributes_id"));
                Cursor options = db.rawQuery("SELECT configitemid FROM options WHERE main_attribute_id=" + attribute_id + " AND res_id_fk=" + mSharedPreferences.getString("mid", "") + " AND item_id_fk=" + menuitemid + ";", null);
                int optionscount = options.getCount();
                if (optionscount > 0) {
                    options.moveToFirst();
                    int configitemid = options.getInt(options.getColumnIndex("configitemid"));
                    Intent i = new Intent(getActivity(), IngrediantsActivity.class);
                    i.putExtra("menuid", groupId);
                    i.putExtra("menuitemid", menuitemid);
                    i.putExtra("configitemid", String.valueOf(configitemid));
                    i.putExtra("res_id_fk", mSharedPreferences.getString("mid", ""));
                    i.putExtra("item_id_fk", String.valueOf(menuitemid));
                    startActivity(i);
                } else {
                    AlertDialog.Builder adb = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Dialog);
                    adb.setTitle("");
                    adb.setMessage("Menu item not configured yet");
                    adb.setPositiveButton("Ok", null);
                    adb.show();
                }
            } else {
                AlertDialog.Builder adb = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Dialog);
                adb.setTitle("");
                adb.setMessage("Menu item not configured yet");
                adb.setPositiveButton("Ok", null);
                adb.show();
            }
        } else if (prod_type == 3) {
            Intent i = new Intent(getActivity(), OtherOptionsActivity.class);
            i.putExtra("menuid", groupId);
            i.putExtra("menuitemid", menuitemid);
            i.putExtra("res_id_fk", mSharedPreferences.getString("mid", ""));
            i.putExtra("item_id_fk", String.valueOf(menuitemid));
            startActivity(i);
        } else {
            AlertDialog.Builder adb = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Dialog);
            adb.setTitle("");
            adb.setMessage("Product type not available");
            adb.setPositiveButton("Ok", null);
            adb.show();
        }
    }

    public boolean checkInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || networkInfo.isConnected() == false) {
            return false;
        }
        return true;
    }


}

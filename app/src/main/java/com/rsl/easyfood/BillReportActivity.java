package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.model.OrderDisplay;
import com.rsl.utills.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class BillReportActivity extends Activity {


    ListView list_billRecords;
    LayoutInflater inflater;
    SharedPreferences mSharedPreferences;
    ProgressDialog dialog;
    ConnectionDetector internet;
    ArrayList<HashMap<String, String>> list_data;
    CustomAdapter adapter;
    TextView date, titlebar;
    ImageView print_bill;
    String order_id;
    TextView text_order_total;
    Double order_total = 0.00;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_bill_report);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        db = openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        list_billRecords = (ListView) findViewById(R.id.list_billRecords);
        print_bill = (ImageView) findViewById(R.id.print_bill);
        internet = new ConnectionDetector(BillReportActivity.this);
        list_data = new ArrayList<>();
        text_order_total = (TextView) findViewById(R.id.text_order_total);
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BillReportActivity.this, Home.class);
                startActivity(intent);
            }
        });
        String morningtimestamp = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
//        morningtimestamp = morningtimestamp + " 00:00:00";
        fromReportsDate = morningtimestamp;
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.GONE);

        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");

        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("YOUR ORDER");
        dropAndCreateTables();


        adapter = new CustomAdapter();
        list_billRecords.setAdapter(adapter);

        print_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showList();
            }
        });
    }//onCreate

    private void showList() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(BillReportActivity.this, android.R.style.Theme_Dialog);
        builderSingle.setTitle("Select Report");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(BillReportActivity.this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("GROUP Report");
        arrayAdapter.add("PRODUCT Report");
        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DialogFragment newFragment = new DatePickerFragment(which);
                newFragment.show(getFragmentManager(), "Date Picker");
                dialog.dismiss();
            }
        });
        builderSingle.show();
    }

    private void dropAndCreateTables() {
        db.execSQL("DROP TABLE IF EXISTS newOrder;");
        db.execSQL("DROP TABLE IF EXISTS newOrderMenuitem;");
        db.execSQL("DROP TABLE IF EXISTS allOrders;");

        db.execSQL(String.format("CREATE TABLE if not exists newOrder(id INTEGER PRIMARY KEY AUTOINCREMENT,tableno INTEGER NOT NULL,guestno INTEGER NOT NULL, order_id VARCHAR[50] NOT NULL,order_num TEXT,order_total DECIMAL(9.2) NOT NULL,shipping_cost  DECIMAL(9.2) NOT NULL,is_accepted INTEGER, product_type INTEGER NOT NULL,payment_mode TEXT,delivery_address TEXT,billing_address TEXT,customer_name TEXT,is_paid TEXT,cartdatetime VARCHAR[25] NOT NULL,billflag INTEGER NOT NULL,orderflag INTEGER NOT NULL,checkflag INTEGER NOT NULL,ordertype VARCHAR[25]);"));

        db.execSQL(String.format("CREATE TABLE if not exists newOrderMenuitem(cart_id INTEGER PRIMARY KEY AUTOINCREMENT,order_id VARCHAR[50] NOT NULL,menu_item_id_fk INTEGER NOT NULL,restaurant_id INTEGER NOT NULL,menu_id INTEGER NOT NULL,menu_item_name VARCHAR[50] NOT NULL,menu_item_price DECIMAL(9.2) NOT NULL,menu_item_qty INTEGER NOT NULL,is_meal TEXT NOT NULL,notes TEXT,is_coupon INTEGER NOT NULL,subtotal DECIMAL(9.2) NOT NULL,options TEXT NOT NULL,billflag INTEGER NOT NULL,orderflag INTEGER NOT NULL,checkflag INTEGER NOT NULL,ordertype VARCHAR[25]);"));

        db.execSQL(String.format("CREATE TABLE if not exists allOrders(order_id VARCHAR[50] NOT NULL,order_num VARCHAR[50] NOT NULL,order_zip VARCHAR[50] NOT NULL,order_total DECIMAL(9.2) NOT NULL,order_date TEXT NOT NULL,order_time TEXT NOT NULL,is_accepted INTEGER NOT NULL,order_updated_on TEXT NOT NULL,customer_name TEXT NOT NULL);"));
        if (internet.isConnectingToInternet()) {
            shoprogress();
            getOrder(mSharedPreferences.getString("mid", ""), "", "");
        } else
            internet.showAlertDialog(BillReportActivity.this);
    }

    static final int FROM_DATE_DIALOG_ID = 2;
    boolean isShow = false;
    private int mYear, mMonth, mDay;
    String fromReportsDate;
    public int yearSelected, monthSelected, daySelected, hourSelected, minuteSelected;

    @SuppressLint("ValidFragment")
    class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        int prodType;

        private DatePickerFragment(int prodType) {
            this.prodType = prodType;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            //Use the current date as the default date in the date picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            //Create a new DatePickerDialog instance and return it
        /*
            DatePickerDialog Public Constructors - Here we uses first one
            public DatePickerDialog (Context context, DatePickerDialog.OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth)
            public DatePickerDialog (Context context, int theme, DatePickerDialog.OnDateSetListener listener, int year, int monthOfYear, int dayOfMonth)
         */
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            //Do something with the date chosen by the user
            month = month + 1;

            fromReportsDate = String.format("" + String.format("%02d", day) + "/" +
                    String.format("%02d", month) + "/" + year);
            Log.e("OnDateSetListener", "" + fromReportsDate);

            Intent intent = new Intent(BillReportActivity.this, Reports.class);
            intent.putExtra("FROMDATE", fromReportsDate);
            intent.putExtra("REPORTSFLAG", prodType);
            startActivity(intent);
        }
    }

    // Register  fromDatePickerDialog listener
    private DatePickerDialog.OnDateSetListener fromDateSetListener =
            new DatePickerDialog.OnDateSetListener() {                 // the callback received when the user "sets" the Date in the DatePickerDialog
                public void onDateSet(DatePicker view, int yearSelected,
                                      int monthOfYear, int dayOfMonth) {
                    if (isShow == true) {
                        isShow = false;
                        monthSelected = ++monthOfYear;
                        daySelected = dayOfMonth;
                        //  TextView fromDate = (TextView) findViewById(R.id.fromDateView);
                        String newYear = "" + yearSelected;
                        fromReportsDate = String.format("" + String.format("%02d", daySelected) + "-" +
                                String.format("%02d", monthSelected) + "-" + newYear);
                        Log.e("OnDateSetListener", "" + fromReportsDate);
//                        fromDate.setText("" + fromDateString + " " + String.format("%02d", hourSelected) + ":" +
//                                String.format("%02d", minuteSelected));
                    }
                    Log.e("else", "" + fromReportsDate);

                }
            };

    public class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return list_data.size();
        }

        public void add(HashMap<String, String> data) {
            list_data.add(data);
            notifyDataSetChanged();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (vi == null) {
                inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.row_bill_records, parent, false);
            }
            TextView order_no, order_type, order_total, order_date, payment_type;
            ImageView imageView_delete = (ImageView) vi.findViewById(R.id.imageView_delete);
            order_no = (TextView) vi.findViewById(R.id.text_no);
            order_type = (TextView) vi.findViewById(R.id.text_type);
            order_total = (TextView) vi.findViewById(R.id.text_total);
            order_date = (TextView) vi.findViewById(R.id.text_date);
            payment_type = (TextView) vi.findViewById(R.id.text_paymentType);

            String dates = list_data.get(position).get("order_date") + "\n" + list_data.get(position).get("order_time");
            order_date.setText(dates);
            order_total.setText(list_data.get(position).get("order_total"));
            order_no.setText(list_data.get(position).get("order_id"));
            order_type.setText(list_data.get(position).get("order_type"));
            payment_type.setText(list_data.get(position).get("payment_mode"));
            imageView_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder msg = new AlertDialog.Builder(
                            BillReportActivity.this, android.R.style.Theme_Dialog);
                    msg.setMessage("Are you sure you want to delete order?");
                    msg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
//                            for (int p = 0; i < list_data.size(); p++) {
//                                deleteOrder(list_data.get(position).get("order_id"), position);
//                            }
                            deleteOrder(list_data.get(position).get("order_id"), position);
                        }
                    });
                    msg.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    msg.show();

                }
            });
            vi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(BillReportActivity.this, BillReportDetailsActivity.class);
                    i.putExtra("order_id", list_data.get(position).get("order_id"));
                    startActivity(i);
                }
            });
            return vi;
        }
    }

    private void shoprogress() {
        dialog = new ProgressDialog(BillReportActivity.this);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.setCancelable(false);
        dialog.show();
    }

    private void getOrder(final String restaurant_id, final String page_no, final String start_id) {
        if (dialog != null) {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
        final RequestQueue queue = Volley.newRequestQueue(BillReportActivity.this);
        String requestURL = getString(R.string.web_path) + "order_service/getOrder/?restaurant_id=" + restaurant_id + "&page_no=" + page_no + "&start_id=" + start_id;
        Log.d("requestURL", "getOrder:" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.e("getOrder", ": " + result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        // list_data.clear();
                        try {
                            JSONObject jObj1 = new JSONObject(result);
                            if (jObj1.length() > 0) {
                                if (!db.isOpen())
                                    db = openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
                                Object detailsObject = jObj1.get("order_list");
                                if (detailsObject instanceof JSONArray) {
                                    JSONArray detailsJsonArray = new JSONArray(String.valueOf(jObj1.getJSONArray("order_list")));
                                    if (detailsJsonArray.length() > 0) {
                                        for (int i = 0; i < detailsJsonArray.length(); i++) {
                                            JSONObject jObjDetails = detailsJsonArray.getJSONObject(i);
                                            HashMap<String, String> data = new HashMap<>();
                                            data.put("order_id", jObjDetails.getString("order_id"));
                                            data.put("order_total", jObjDetails.getString("order_total"));
                                            data.put("order_date", jObjDetails.getString("order_date"));
                                            data.put("order_time", jObjDetails.getString("order_time"));
                                            order_total += Double.parseDouble(jObjDetails.getString("order_total"));
                                            if (jObjDetails.has("payment_mode")) {
                                                data.put("payment_mode", jObjDetails.getString("payment_mode"));
                                            } else {
                                                data.put("payment_mode", "COD");
                                            }
                                            if (jObjDetails.has("order_type")) {
                                                data.put("order_type", jObjDetails.getString("order_type"));
                                            } else {
                                                data.put("order_type", "APP Order");
                                            }
                                            list_data.add(data);

                                            order_id = jObjDetails.getString("order_id");
                                            String is_accepted = jObjDetails.getString("is_accepted");
                                            Cursor orderData = db.rawQuery("SELECT order_id,is_accepted FROM allOrders WHERE order_id=" + order_id + ";", null);
                                            int orderCount = orderData.getCount();
                                            if (orderCount > 0) {
                                                while (orderData.moveToNext()) {
                                                    db.execSQL("" +
                                                            "UPDATE allOrders SET is_accepted = " + jObjDetails.getString("is_accepted") + " WHERE order_id = " +
                                                            order_id + ";");
                                                }
                                            } else {
                                                db.execSQL("INSERT INTO allOrders VALUES(" + "'" + order_id + "','" + jObjDetails.getString("order_num") +
                                                        "','" + jObjDetails.getString("order_zip") + "'," + jObjDetails.getString("order_total") + ",'" + jObjDetails.getString("order_date")
                                                        + "','" + jObjDetails.getString("order_time") + "'," + jObjDetails.getString("is_accepted") + ",'" + jObjDetails.getString("order_updated_on") +
                                                        "','" + jObjDetails.getString("customer_name") + "'" + ");");
                                            }
                                            getOrderDetails(order_id);
                                        }
                                        if (internet.isConnectingToInternet()) {
                                            getOrder(mSharedPreferences.getString("mid", ""), "", order_id);
                                            return;
                                        } else
                                            internet.showAlertDialog(BillReportActivity.this);
                                    }
                                }//order_list instanceof
                            }

                            if (dialog != null)
                                dialog.dismiss();
                            text_order_total.setText(String.format("%.2f", order_total));

                        } catch (Exception e) {
                            if (dialog != null)
                                dialog.dismiss();
                            e.printStackTrace();
                            Log.e("getOrder", "exception=" + e.toString());
                            Toast.makeText(BillReportActivity.this, getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog != null)
                    dialog.dismiss();
                Log.e("volleyError", "getOrder=" + volleyError.getMessage());
                Toast.makeText(BillReportActivity.this, getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOrder

    Double menuitemtotal = 0.00;

    private void getOrderDetails(final String oId) {
      /*  if (dialog == null)
            dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Order Details");
        dialog.setCancelable(false);
        dialog.show();*/
        final RequestQueue queue = Volley.newRequestQueue(BillReportActivity.this);
        String requestURL = getString(R.string.web_path) + "order_service/details/?order_id=" + oId;
        Log.d("requestURL", "getOrderDetails:" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.d("getOrderDetails", "Details:" + result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj1 = new JSONObject(result);

                            if (jObj1.length() > 0) {
                                if (!db.isOpen())
                                    db = openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
                                // list_orderDisplays.clear();
                                Object detailsObject = jObj1.get("order_detail");
                                if (detailsObject instanceof JSONObject) {
                                    JSONObject jObjDetails = jObj1.getJSONObject("order_detail");
                                    Cursor orderData = db.rawQuery("SELECT order_id FROM newOrder WHERE order_id=" + oId + ";", null);
                                    int orderCount = orderData.getCount();
                                    Cursor newOrderMenuitem = db.rawQuery("SELECT menu_item_name FROM newOrderMenuitem WHERE order_id=" + oId + ";", null);
                                    int orderMenuitemCount = newOrderMenuitem.getCount();
                                    while (newOrderMenuitem.moveToNext()) {
                                        String name = newOrderMenuitem.getString(newOrderMenuitem.getColumnIndex("menu_item_name"));

                                    }
                                    if (orderCount == 0) {
                                        db.execSQL("INSERT INTO newOrder (tableno,guestno,order_id,order_num,order_total,shipping_cost," +
                                                "is_accepted,product_type,payment_mode,delivery_address,billing_address,customer_name," +
                                                "is_paid,cartdatetime,billflag,orderflag,checkflag,ordertype) VALUES(" + 0 + "," + 0 +
                                                ",'" + jObjDetails.getString("order_id") +
                                                "','" + jObjDetails.getString("order_num") + "'," + jObjDetails.getString("order_total") +
                                                "," + jObjDetails.getString("shipping_cost") + "," + jObjDetails.getString("is_accepted") +
                                                "," + 0 + ",'" + jObjDetails.getString("payment_mode") +
                                                "','" + jObjDetails.getString("delivery_address") + "','" + jObjDetails.getString("billing_address") +
                                                "','" + jObjDetails.getString("customer_name") +
                                                "','" + jObjDetails.getString("is_paid") + "','" + jObjDetails.getString("order_time") + "'," + 0 + "," + 0 + "," + 0 + ",'Web');");


                                        JSONArray jsonArrayCart = jObjDetails.getJSONArray("cart_splitwithoutbill");
                                        if (jsonArrayCart.length() > 0) {
                                            for (int j = 0; j < jsonArrayCart.length(); j++) {
                                                JSONObject jObj = jsonArrayCart.getJSONObject(j);
                                                String util_html = DatabaseUtils.sqlEscapeString(String.valueOf(Html.fromHtml(jObj.getString("name"))));
                                                String name = String.valueOf(Html.fromHtml(jObj.getString("name"))).replace("'", "\'");
                                                String util = DatabaseUtils.sqlEscapeString(jObj.getString("name"));
                                                String optionsData = String.valueOf(Html.fromHtml(jObj.getString("options"))).replace("'", "\'");

                                                String restro_id = jObj.has("restaurant_id") ? jObj.getString("restaurant_id") : mSharedPreferences.getString("mid", "");
                                                String is_meal = jObj.has("is_meal") ? jObj.getString("is_meal") : "false";
                                                String notes = jObj.has("notes") ? String.valueOf(Html.fromHtml(jObj.getString("notes"))).replace("'", "\'") : "false";

                                                db.execSQL("INSERT INTO newOrderMenuitem (order_id,menu_item_id_fk,restaurant_id,menu_id,menu_item_name,menu_item_price,menu_item_qty,is_meal,notes,is_coupon,subtotal,options,billflag,orderflag,checkflag,ordertype) VALUES(" + "'" + jObjDetails.getString("order_id") + "'," + jObj.getString("id") +
                                                        "," + restro_id +
                                                        "," + jObj.getString("assign_menu_id") + "," + DatabaseUtils.sqlEscapeString(name) + "," + jObj.getString("price") + "," + jObj.getString("qty") +
                                                        ",'" + is_meal + "'," + DatabaseUtils.sqlEscapeString(notes) +
                                                        "," + jObj.getString("is_coupon") + "," + jObj.getString("subtotal") +
                                                        "," + DatabaseUtils.sqlEscapeString(optionsData) + "," + 0 + "," + 0 + "," + 0 + ",'Web');");

                                                Log.e("price", "" + jObj.getString("price"));
                                                menuitemtotal += Double.parseDouble(jObj.getString("price"));

                                            }
                                        }
                                        Object cart_splitData = jObjDetails.get("cart_split");
                                        if (cart_splitData instanceof JSONObject) {
                                            JSONObject jsonObj = jObjDetails.getJSONObject("cart_split");
                                            if (jsonObj.length() > 0) {
                                                Iterator<String> j = jsonObj.keys();
                                                for (int m = 0; m < jObjDetails.getJSONObject("cart_split").length(); m++) {
                                                    String keys = j.next();
                                                    Object cart_splitSubData = jObjDetails.getJSONObject("cart_split").get(keys);
                                                    if (cart_splitSubData instanceof JSONArray) {
                                                        JSONArray jsonArray = jObjDetails.getJSONObject("cart_split").getJSONArray((keys));
                                                        if (jsonArray.length() > 0) {
                                                            for (int f = 0; f < jsonArray.length(); f++) {
                                                                JSONObject jObj = jsonArray.getJSONObject(f);
                                                                String name = String.valueOf(Html.fromHtml(jObj.getString("name"))).replace("'", "\'");
                                                                String restro_id = jObj.has("restaurant_id") ? jObj.getString("restaurant_id") : mSharedPreferences.getString("mid", "");
                                                                String is_meal = jObj.has("is_meal") ? jObj.getString("is_meal") : "false";
                                                                String notes = jObj.has("notes") ? String.valueOf(Html.fromHtml(jObj.getString("notes"))).replace("'", "\'") : "false";

                                                                db.execSQL("INSERT INTO newOrderMenuitem (order_id,menu_item_id_fk,restaurant_id,menu_id,menu_item_name,menu_item_price,menu_item_qty,is_meal,notes,is_coupon,subtotal,options,billflag,orderflag,checkflag,ordertype) VALUES('" + jObjDetails.getString("order_id") + "'," + jObj.getString("id") +
                                                                        "," + restro_id + "," + jObj.getString("assign_menu_id") + "," + DatabaseUtils.sqlEscapeString(name) + "," + jObj.getString("price") + "," + jObj.getString("qty") +
                                                                        ",'" + is_meal + "'," + DatabaseUtils.sqlEscapeString(notes) +
                                                                        "," + jObj.getString("is_coupon") + "," + jObj.getString("subtotal") +
                                                                        ",'" + String.valueOf(Html.fromHtml(jObj.getString("options"))) + "'," + 0 + "," + 0 + "," + 0 + ",'Web');");
                                                                menuitemtotal += Double.parseDouble(jObj.getString("price"));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else if (orderMenuitemCount == 0) {
                                        JSONArray jsonArrayCart = jObjDetails.getJSONArray("cart_splitwithoutbill");
                                        if (jsonArrayCart.length() > 0) {
                                            for (int j = 0; j < jsonArrayCart.length(); j++) {
                                                JSONObject jObj = jsonArrayCart.getJSONObject(j);
                                                String name = String.valueOf(Html.fromHtml(jObj.getString("name"))).replace("'", "\'");
                                                String restro_id = jObj.has("restaurant_id") ? jObj.getString("restaurant_id") : mSharedPreferences.getString("mid", "");
                                                String is_meal = jObj.has("is_meal") ? jObj.getString("is_meal") : "false";
                                                String notes = jObj.has("notes") ? String.valueOf(Html.fromHtml(jObj.getString("notes"))).replace("'", "\'") : "false";

                                                db.execSQL("INSERT INTO newOrderMenuitem (order_id,menu_item_id_fk,restaurant_id,menu_id,menu_item_name,menu_item_price,menu_item_qty,is_meal,notes,is_coupon,subtotal,options,billflag,orderflag,checkflag,ordertype) VALUES(" + "'" + jObjDetails.getString("order_id") + "'," + jObj.getString("id") +
                                                        "," + restro_id +
                                                        "," + jObj.getString("assign_menu_id") + ",'" + name + "'," + jObj.getString("price") + "," + jObj.getString("qty") +
                                                        ",'" + is_meal + "'," + DatabaseUtils.sqlEscapeString(notes) +
                                                        "," + jObj.getString("is_coupon") + "," + jObj.getString("subtotal") +
                                                        ",'" + String.valueOf(Html.fromHtml(jObj.getString("options"))) + "'," + 0 + "," + 0 + "," + 0 + ",'Web');");
                                                menuitemtotal += Double.parseDouble(jObj.getString("price"));
                                            }
                                        }
                                        Object cart_splitData = jObjDetails.get("cart_split");
                                        if (cart_splitData instanceof JSONObject) {
                                            JSONObject jsonObj = jObjDetails.getJSONObject("cart_split");
                                            if (jsonObj.length() > 0) {
                                                Iterator<String> j = jsonObj.keys();
                                                for (int m = 0; m < jObjDetails.getJSONObject("cart_split").length(); m++) {
                                                    String keys = j.next();
                                                    Object cart_splitSubData = jObjDetails.getJSONObject("cart_split").get(keys);
                                                    if (cart_splitSubData instanceof JSONArray) {
                                                        JSONArray jsonArray = jObjDetails.getJSONObject("cart_split").getJSONArray((keys));
                                                        if (jsonArray.length() > 0) {
                                                            for (int f = 0; f < jsonArray.length(); f++) {
                                                                JSONObject jObj = jsonArray.getJSONObject(f);
                                                                String name = String.valueOf(Html.fromHtml(jObj.getString("name"))).replace("'", "\'");

                                                                String restro_id = jObj.has("restaurant_id") ? jObj.getString("restaurant_id") : mSharedPreferences.getString("mid", "");
                                                                String is_meal = jObj.has("is_meal") ? jObj.getString("is_meal") : "false";
                                                                String notes = jObj.has("notes") ? String.valueOf(Html.fromHtml(jObj.getString("notes"))).replace("'", "\'") : "false";

                                                                db.execSQL("INSERT INTO newOrderMenuitem (order_id,menu_item_id_fk,restaurant_id,menu_id,menu_item_name,menu_item_price,menu_item_qty,is_meal,notes,is_coupon,subtotal,options,billflag,orderflag,checkflag,ordertype) VALUES('" + jObjDetails.getString("order_id") + "'," + jObj.getString("id") +
                                                                        "," + restro_id + "," + jObj.getString("assign_menu_id") + ",'" + name + "'," + jObj.getString("price") + "," + jObj.getString("qty") +
                                                                        ",'" + is_meal + "'," + DatabaseUtils.sqlEscapeString(notes) +
                                                                        "," + jObj.getString("is_coupon") + "," + jObj.getString("subtotal") +
                                                                        ",'" + String.valueOf(Html.fromHtml(jObj.getString("options"))) + "'," + 0 + "," + 0 + "," + 0 + ",'Web');");
                                                                menuitemtotal += Double.parseDouble(jObj.getString("price"));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            Log.e("Details_menuitemtotal", ":" + menuitemtotal);
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Log.e("getOrderDetails", "exception=" + e.toString());
                            Toast.makeText(BillReportActivity.this, getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("volleyError", "getOrderDetails=" + volleyError.getMessage());
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(BillReportActivity.this, getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getOrder


    private void deleteOrder(final String order_id, final int pos) {
        if (dialog != null) {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        }
        final RequestQueue queue = Volley.newRequestQueue(BillReportActivity.this);
        String requestURL = getString(R.string.web_path) + "order_service/deleteOrder/?order_id=" + order_id;

        Log.d("requestURL", "deleteOrder:" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.e("deleteOrder", "res:" + result);
                if (dialog != null)
                    dialog.dismiss();
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.length() > 0) {
                                if (jObj.getString("type").equals("1")) {
                                    if (!db.isOpen())
                                        db = openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
                                    Cursor orderData = db.rawQuery("DELETE FROM allOrders WHERE order_id=" + order_id + ";", null);
                                    int orderCount = orderData.getCount();

                                    double total = order_total - Double.parseDouble(list_data.get(pos).get("order_total"));
                                    text_order_total.setText(String.format("%.2f", total));
                                    list_data.remove(pos);
                                    adapter.notifyDataSetChanged();
                                    Toast.makeText(BillReportActivity.this, jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (Exception e) {
                            if (dialog != null)
                                dialog.dismiss();
                            e.printStackTrace();
                            Log.e("deleteOrder", "exception=" + e.toString());
                            Toast.makeText(BillReportActivity.this, getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        } finally {
                            if (!db.isOpen())
                                db.close();
                        }

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog != null)
                    dialog.dismiss();
                Log.e("volleyError", "deleteOrder=" + volleyError.getMessage());
                Toast.makeText(BillReportActivity.this, getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//deleteOrder
}

package com.rsl.easyfood;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;
import com.rsl.model.Cartpojo;
import com.rsl.utills.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author EMBDES.
 * @version 1.0.
 *          This Activity doing kitchen, bill and order printing.
 */

@SuppressLint({"NewApi", "DefaultLocale"})
public class Printer extends Activity {
    SharedPreferences mSharedPreferences;
    BluetoothAdapter mBluetoothAdapter;
    MyDb myDb;
    private static final int REQUEST_ENABLE_BT = 2;
    public static final int MESSAGE_READ1 = 2;
    public static final int MESSAGE_WRITE1 = 3;
    boolean printerclassCheck = true;
    InputStream mmInStream;
    static OutputStream mmOutStream;
    public static Socket mysocket = null;
    ProgressDialog pDialog;
    Handler handler;
    int tableNo;
    String ORDERID;
    String appName = "";
    int locationNo;
    String strName;
    int PRINTBILL = 0;
    int PAYBILLPRINT = 1;
    int ORDERNOPRINT = 2;
    int KITCHENPRINT = 4;
    int printFlag;
    String dataToPrint;
    Dialog settingDialog;
    ListView lv;
    BluetoothDevice mdevice, kdevice;
    String printdata;
    String btPrinterid, btkitchenPrinterid;
    private static int SERVERPORT = 9100;
    private static String SERVER_IP = "192.168.1.87";
    ArrayAdapter<BluetoothDevice> adapter;
    ArrayList<BluetoothDevice> connections = new ArrayList<BluetoothDevice>();
    ArrayAdapter<String> adaptername;
    ArrayList<String> connectionsname = new ArrayList<String>();
    String globledevice, globalsbuff;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_TOAST = 4;
    private static final boolean D = true;
    private static final String TAG = "WIFIPrinterActivity";
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    public static int revBytes = 0;
    BluetoothSocket mmSocket;
    boolean printerDeviceCheck = false;
    int pFormate = 10;
    static int kitchenid;
    static Printer PrinterId;

    class details {
        String name;
        String add1;
        String add2;
        String city;
        String pin;
        String ph;
        String fid;
        String email;
        String tid;
        String webadd;
        String mess1;
        String mess2;
        int table;
        int dummytable = 0;
        float st;
        float vt;
        float dis;
        float tax;
        float tax1;
        float tax2;
        float tax3;
        int location;
        float cash;
        String lastloginusername;
    }

    ;

    class voucher {
        String voucherno;
        float voucheramount;
        int vouchercount;

        public voucher(String newVoucherno, float newVoucheramount) {
            voucherno = newVoucherno;
            voucheramount = newVoucheramount;
        }

        public voucher(int newVoucherCount) {
            vouchercount = newVoucherCount;
        }

    }

    class products {
        String productName;
        int qty;
        float price;
        float total;
        float subtotal;
        String id;
        float typeprice;
        int typeqty;
        float discount;
        String orderno;
        products[] subproducts = new products[100];

        public products(String productName, int qty, float price,
                        float total, float subtotal, String id) {
            this.productName = productName;
            this.qty = qty;
            this.price = price;
            this.total = total;
            this.subtotal = subtotal;
            this.id = id;

        }
    }

    ;

    //btOperation bo = new btOperation();
    //// ���뷽ʽ
    SQLiteDatabase db;
    /**
     * printing text align left
     */
    public static final int AT_LEFT = 0;
    /**
     * printing text align center
     */
    public static final int AT_CENTER = 1;
    /**
     * printing text align right
     */
    public static final int AT_RIGHT = 2;
    //	private regoPrinter mobileprint = null;
    boolean bConnect = true;
    //private int iObjectCode;

    //IntentFilter filter1,filter2,filter3;
    boolean isRegister = false;

    /**
     * Called when the activity is first created.
     */
    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        tableNo = bundle.getInt("TABLENO");
        if (tableNo == 0) {
            ORDERID = bundle.getString("ORDERID", String.valueOf(0));
        }
        locationNo = 0;
        printFlag = bundle.getInt("FLAG");
        setContentView(R.layout.progress);
        mSharedPreferences = getSharedPreferences("printerpref", MODE_PRIVATE);
        appName = getString(R.string.app_name);
        if (mSharedPreferences.getBoolean("isWifiPrinterEnable", false)) {
            PrinterId = this;
            strName = "192.168.1.87:9100";
            db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
            Cursor printerDetails = db.rawQuery("SELECT ipaddress, portno FROM locationprinter;", null);
            int printerDetailsCount = printerDetails.getCount();
            if (printerDetailsCount > 0) {
                printerDetails.moveToFirst();
                strName = printerDetails.getString(0) + ":" + printerDetails.getString(1);
                SERVER_IP = printerDetails.getString(0);
                SERVERPORT = Integer.parseInt(printerDetails.getString(1));
            }
            db.close();
            if (strName.length() == 0) {
                Toast.makeText(Printer.this, "Error:port name empty", Toast.LENGTH_SHORT).show();
                return;
            }
            if (bConnect) {
                try {
                    if (mysocket == null) {
                        new ClientThread().execute("ok");
                    }
                } finally {
                    if (printFlag == PRINTBILL) {//0
                        billClick(tableNo, locationNo, 0);
                        Intent mIntent = new Intent();
                        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.setClass(getApplicationContext(), Home.class);
                        startActivity(mIntent);
                    } else if (printFlag == PAYBILLPRINT) {//1
                        billClick(tableNo, locationNo, 1);
                        updatePayInfo();
                        Intent mIntent = new Intent();
                        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.setClass(getApplicationContext(), Home.class);
                        startActivity(mIntent);
                    } else if (printFlag == KITCHENPRINT) {//4
//							orderNoBillPrint(""+tableNo);
                        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                        Cursor FoodId = db.rawQuery("SELECT cart_id,menu_id from cart WHERE tableno=" + tableNo + " AND billflag=0;", null);
                        if (FoodId.getCount() > 0) {
                            click(tableNo, 0, 4);
                        }
                        FoodId.close();
                    } else {

                        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                        Cursor FoodId = db.rawQuery("SELECT cart_id,menu_id from cart WHERE tableno=" + tableNo + " AND billflag=0;", null);
                        if (FoodId.getCount() > 0) {
                            click(tableNo, 0, 0);
                        }
                        FoodId.close();

                    }
                    bConnect = false;
                }
            } else {
                bConnect = true;
            }
        } else {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                // Device does not support Bluetooth
            }
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent =
                        new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
            if (printFlag == PRINTBILL || printFlag == PAYBILLPRINT || printFlag == ORDERNOPRINT) {
                String restoredText = mSharedPreferences.getString("btPrinterid", null);
                if (restoredText != null) {
                    btPrinterid = mSharedPreferences.getString("btPrinterid", null);
                    mdevice = mBluetoothAdapter
                            .getRemoteDevice(btPrinterid);
                    if (printFlag == PRINTBILL) {
                        billClick(tableNo, locationNo, 0);
                    } else if (printFlag == PAYBILLPRINT) {
                        billClick(tableNo, locationNo, 1);
                    } else if (printFlag == ORDERNOPRINT) {
                        // orderNoBillPrint("" + tableNo);
                    }
                } else {
                    ListPairedDevices();
                }
            } else {
                if (mSharedPreferences.getBoolean("kotPrinterEnable", false)) {
                    String restoredText = mSharedPreferences.getString("btkPrinterid", null);
                    if (restoredText != null) {
                        btPrinterid = mSharedPreferences.getString("btkPrinterid", null);
                        kdevice = mBluetoothAdapter
                                .getRemoteDevice(btPrinterid);
                    } else {
                        ListPairedDevices();
                    }
                }
                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                Cursor FoodId = db.rawQuery("SELECT cart_id,menu_id from cart WHERE tableno=" + tableNo + " AND billflag=0;", null);
                if (FoodId.getCount() > 0) {
                    click(tableNo, 0, 0);
                }
                FoodId.close();
            }
        }
    }

    public class ClientThread extends AsyncTask<String, Void, Boolean> {

		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isconnected = false;

        public void onPreExecute() {
            isconnected = false;


        }

        public Boolean doInBackground(final String... args) {
            try {
                mysocket = new Socket();
                mysocket.connect(new InetSocketAddress(SERVER_IP, SERVERPORT), 10000);
                mmOutStream = mysocket.getOutputStream();
                mmInStream = mysocket.getInputStream();
                isconnected = true;
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
                isconnected = false;
            } catch (IOException e1) {
                e1.printStackTrace();
                isconnected = false;
            }
            return false;
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            if (isconnected == true) {

            } else {
                Toast.makeText(getApplicationContext(), "Selected WIFi KOT Printer is not Online trying to connect to bluetooth", Toast.LENGTH_LONG).show();
                Intent mIntent = new Intent();
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.setClass(getApplicationContext(), Home.class);
                startActivity(mIntent);
            }
        }
    }

    /**
     * This click function get called when user click on the kitchen print
     *
     * @param tbl
     * @param loc
     */
    @SuppressWarnings("null")
    public void click(int tbl, int loc, int foodid) {//kitchen print
        String productName1 = "product";
        int qty1 = 0;
        float price1 = 0;
        float total1 = 0;
        float subtotal1 = 0;
        String id = "01";
        float typeprice1 = 0;
        int typeqty1 = 0;
        float discount1 = 0;
        String orderno1 = "10001";
        String voucherno1 = "1001";
        details k_detail = new details();
        k_detail.table = tbl;
        k_detail.location = loc;
        k_detail.lastloginusername = "EmbDEs";
//        kitchenid = foodid;
        products[] cart_items = new products[100];
        int item = 0;
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT res_name,contact_name FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {
            shopDetails.moveToFirst();
            k_detail.name = shopDetails.getString(shopDetails.getColumnIndex("res_name"));
            k_detail.lastloginusername = shopDetails.getString(shopDetails.getColumnIndex("contact_name"));
        }
        Cursor productDetails;
        if (tableNo == 0) {
            productDetails = db.rawQuery("SELECT * FROM newOrderMenuitem WHERE order_id = " + ORDERID + " AND billflag=0;", null);
        } else
            productDetails = db.rawQuery("SELECT * FROM cart WHERE tableno = " + tableNo + " AND billflag=0;", null);
        int count = productDetails.getCount();
        int updateQtyFlag = 1;
        ArrayList<Cartpojo> printData = new ArrayList<>();
        if (count > 0) {
            productDetails.moveToFirst();
            for (Integer rowCount = 0; rowCount < count; rowCount++) {
                qty1 = 0;
                price1 = 0;
                int cart_id = productDetails.getInt(productDetails.getColumnIndex("cart_id"));
                productName1 = productDetails.getString(productDetails.getColumnIndex("menu_item_name"));
                id = productDetails.getString(productDetails.getColumnIndex("menu_item_id_fk"));
                qty1 = productDetails.getInt(productDetails.getColumnIndex("menu_item_qty"));

                Cartpojo groupdata = new Cartpojo();//Object for Menu
                groupdata.setSno(id);
                groupdata.setName(productName1);
                groupdata.setPqty(String.valueOf(qty1));
                ArrayList<Cartpojo> ingredientsList = new ArrayList<>();
                Cursor cartMenuitem = db.rawQuery("SELECT id,ing_id,ing_qty,ingredient_name,ing_price FROM ingredients WHERE cart_id_fk=" + cart_id + ";", null);
                int itemcount = cartMenuitem.getCount();
                Double itemtotal = 0.00;
                if (itemcount > 0) {
                    cartMenuitem.moveToFirst();
                    for (int ingCount = 0; ingCount < itemcount; ingCount++) {

                        Cartpojo ingredientsdata = new Cartpojo();//Object for ingredients
                        String cid = cartMenuitem.getString(cartMenuitem.getColumnIndex("id"));
                        int ing_qty = cartMenuitem.getInt(cartMenuitem.getColumnIndex("ing_qty"));
                        Double ing_price = cartMenuitem.getDouble(cartMenuitem.getColumnIndex("ing_price"));
                        String ing_name = cartMenuitem.getString(cartMenuitem.getColumnIndex("ingredient_name"));

                        Cursor subItmes = db.rawQuery("SELECT subitem_id,subitem_qty,subitem_name,subitem_price FROM subitems WHERE ing_id_fk=" + cid + ";", null);

                        int subitemcount = subItmes.getCount();
                        Double subitemtotal = 0.00;
                        ArrayList<Cartpojo> subitemsList = new ArrayList<>();
                        if (subitemcount > 0) {
                            while (subItmes.moveToNext()) {
                                Cartpojo subitemsdata = new Cartpojo();//Object for ingredients
                                String subitem_id = subItmes.getString(subItmes.getColumnIndex("subitem_id"));
                                int subitem_qty = subItmes.getInt(subItmes.getColumnIndex("subitem_qty"));
                                String subitem_name = subItmes.getString(subItmes.getColumnIndex("subitem_name"));
                                Double subitem_price = subItmes.getDouble(subItmes.getColumnIndex("subitem_price"));
                                // Log.e("subitem_name_id", "" + ing_name + "_" + subitem_name + "_" + subitem_id + "_" + subitem_qty);
                                subitemtotal = subitemtotal + (subitem_qty * subitem_price);
                                subitemsdata.setSno(subitem_id);
                                subitemsdata.setName(subitem_name);
                                subitemsdata.setPqty(String.valueOf(subitem_qty));
                                subitemsdata.setPprice(String.format("%.2f", subitem_price));
                                subitemsList.add(subitemsdata);
                            }
                        }

                        ingredientsdata.setItem_list(subitemsList);
                        ingredientsdata.setSno(cartMenuitem.getString(cartMenuitem.getColumnIndex("ing_id")));
                        ingredientsdata.setName(ing_name);
                        ingredientsdata.setPqty(String.valueOf(ing_qty));
                        ingredientsList.add(ingredientsdata);
                        itemtotal = itemtotal + (ing_qty * ing_price) + subitemtotal;
                        cartMenuitem.moveToNext();
                    }
                    price1 = (float) (price1 + itemtotal);
                }
                total1 += price1;
                groupdata.setItem_list(ingredientsList);//added ingredeints to group
                printData.add(groupdata);//main group arraylist
                groupdata.setTotal(String.format("%.2f", price1));
                cart_items[item] = new products(productName1, qty1, price1, total1, subtotal1, id);
                item++;
                productDetails.moveToNext();

            }
            Log.e("total1", "==" + total1);
            db.close();

            if (printFlag == PRINTBILL) {//0
                billClick(tableNo, locationNo, 0);
            } else if (printFlag == PAYBILLPRINT) {//1
                updatePayInfo();
            } else if (printFlag == KITCHENPRINT) {//4
                //orderNoBillPrint(""+tableNo);
            } else {//3
                Group.GROUPID.cartUpdate();
            }
            insert(k_detail, printData, item);//send to print
        } else {
            db.close();
            AlertDialog.Builder adb = new AlertDialog.Builder(
                    Printer.this, android.R.style.Theme_Dialog);
            adb.setMessage("Cart Table Empty For Current TableNo");
            adb.setPositiveButton("Ok", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent mIntent = new Intent();
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mIntent.setClass(getApplicationContext(), Home.class);
                            startActivity(mIntent);
                        }
                    });
            return;
        }
    }

    /**
     * This method inputs details and products from the click method
     *
     * @param k_details
     * @param k_item
     * @param pcount
     */

    public void insert(details k_details, ArrayList<Cartpojo> groupList, int pcount) {
        String[] buff = null;

        StringBuilder dummy = new StringBuilder("dummy");
        String bill1;
        String product1;
        String fdot1;
        String user;
        String orderno;
        String table;
        int i = 0, j = 0, x = 0;
        StringBuilder groupString;
        String kitchenMessage = "";
        //orderno = String.format("Order:%-9s", k_item[0].orderno);
        String timeStamp = new SimpleDateFormat("dd/MM/yy HH:mm").format(Calendar.getInstance().getTime());
        user = String.format("User:%-30s Table:%-9d\n", k_details.lastloginusername, k_details.table);
        bill1 = String.format("                             %-2s \n", timeStamp);
        product1 = "PRODUCT                                 QTY\n\n";

        for (int g = 0; g < groupList.size(); g++) {
            Cartpojo grp = groupList.get(g);//get group list item
            String prodname = grp.getName();
            String prodQty = grp.getPqty();
            groupString = new StringBuilder(String.format("%-41s%s \n", prodname, prodQty));//groupname        qty
            Log.e("product1", "" + product1);
            Log.e("groupString", "" + groupString);

            StringBuilder ingString = new StringBuilder("dummy");
            for (i = 0; i < grp.getItem_list().size(); i++) {
                Cartpojo ingre = grp.getItem_list().get(i);//get ingredients list item

                ingString = new StringBuilder(String.format("  %-40s \n", ingre.getPqty() + " x " + ingre.getName()));//ingredient        qty
                String subitemString = "dummy";
                for (int s = 0; s < ingre.getItem_list().size(); s++) {
                    Cartpojo subitem = ingre.getItem_list().get(s);//get ingredients list item
                    subitemString = String.format("     %-35s \n", subitem.getPqty() + " x " + subitem.getName());//ingredient        qty
                    ingString.append(subitemString);
                }
                groupString.append(ingString);
            }
            if (dummy.toString().equals("dummy")) {
                dummy = new StringBuilder(groupString.toString());
            } else {
                dummy.append(groupString);
            }

        }
        fdot1 = "-------------------------------------------\n";
        kitchenMessage = String.format(/*%s\n*/"%s%s%s%s%22s   \n",/*shopName,*/user, bill1, product1, dummy.toString(), fdot1);
        Log.e("kitchenMessage", "\n" + kitchenMessage);

        //kitchenMessage = String.format(/*%s\n*/"%s%s%s%s%22s   \n",/*shopName,*/user, bill1, product1, dummy, fdot1); //Here we are inserting buffer data into the print buffer
        deviceconnection(kitchenMessage, 4);
    }

    /**
     * This method inputs table, location number from the user.
     *
     * @param tableNo
     * @param locationNo
     * @param printFormat
     */
    public void billClick(int tableNo, int locationNo, int printFormat) {//customer print
        Log.w("Printer", "billClick");
        details detail = new details();
        detail.table = tableNo;
        int item = 0;
        String productName1 = "product";
        int qty1 = 0;
        float price1 = 0;
        float total1 = 0;
        float subtotal1 = 0;
        String typeName1 = "some";
        String id = "0";
        float typeprice1 = 0;
        int typeqty1 = 0;
        float discount1 = 0;
        String orderno1 = "10001";
        String voucherno1 = "1001";
        voucher[] voucherDetails = new voucher[100];
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        StringBuilder consumerInfo = new StringBuilder();
        if (tableNo == 0) {
            Cursor consumerDetails = db.rawQuery("SELECT customer_name,delivery_address FROM newOrder WHERE order_id=" + ORDERID + ";", null);
            int consumerCount = consumerDetails.getCount();
            if (consumerCount > 0) {
                try {
                    consumerDetails.moveToFirst();

                    String ordernumber = consumerDetails.getString(consumerDetails.getColumnIndex("customer_name"));
                    String name = consumerDetails.getString(consumerDetails.getColumnIndex("customer_name"));
                    String address = consumerDetails.getString(consumerDetails.getColumnIndex("delivery_address"));
                    JSONObject delivery_address = new JSONObject(address);
                    String addressline = delivery_address.getString("delivery_address1") + "\nCity:"
                            + delivery_address.getString("delivery_city") + "\nPostCode:"
                            + delivery_address.getString("delivery_zipcode");
                    String consumerData = String.format("Consumer Name:%s\nPhone No.\nDelivery Address:%s\n", name, "", addressline);
                    String fdot1 = String.format("-------------------------------------------\n");
                    consumerInfo.append(consumerData);
                    consumerInfo.append(fdot1);
                } catch (JSONException e) {
                    consumerInfo = new StringBuilder();
                }
            }
        }
        Cursor shopDetails = db.rawQuery("SELECT res_name,uadd_address_01,uadd_address_02,uadd_city,phone,website,contact_name FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {                        //Here we are inserting the masterterminal into the structure
            shopDetails.moveToFirst();
            detail.name = shopDetails.getString(shopDetails.getColumnIndex("res_name"));

            String abc = shopDetails.getString(shopDetails.getColumnIndex("uadd_address_01"));
            String obj = abc.replace("\n\n", "\n");
            detail.add1 = obj;
            detail.add2 = shopDetails.getString(shopDetails.getColumnIndex("uadd_address_02"));
            detail.city = shopDetails.getString(shopDetails.getColumnIndex("uadd_city"));
            detail.ph = shopDetails.getString(shopDetails.getColumnIndex("phone"));
            detail.webadd = shopDetails.getString(shopDetails.getColumnIndex("website"));
            detail.lastloginusername = shopDetails.getString(shopDetails.getColumnIndex("contact_name"));

        } else {
            db.close();
            AlertDialog.Builder adb = new AlertDialog.Builder(
                    Printer.this, android.R.style.Theme_Dialog);
            adb.setMessage("MasterTerminal Table is Empty");
            adb.setPositiveButton("Ok", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent mIntent = new Intent();
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mIntent.setClass(getApplicationContext(), Home.class);
                            startActivity(mIntent);
                        }
                    });
            adb.show();
        }
        ArrayList<Cartpojo> printDataList = new ArrayList<>();
        Cursor productDetails;
        if (tableNo == 0) {
            productDetails = db.rawQuery("SELECT * FROM newOrderMenuitem WHERE order_id = " + ORDERID + " AND billflag=0;", null);
        } else
            productDetails = db.rawQuery("SELECT * FROM cart WHERE tableno = " + tableNo + " AND billflag=0;", null);

        int count = productDetails.getCount();
        if (count > 0) {
            productDetails.moveToFirst();
            for (Integer rowCount = 0; rowCount < count; rowCount++) {

                productName1 = productDetails.getString(productDetails.getColumnIndex("menu_item_name"));
                price1 = productDetails.getFloat(productDetails.getColumnIndex("menu_item_price"));
                int cart_id = productDetails.getInt(productDetails.getColumnIndex("cart_id"));
                qty1 = productDetails.getInt(productDetails.getColumnIndex("menu_item_qty"));

                Cartpojo groupData = new Cartpojo();

                Cursor cartMenuitem = db.rawQuery("SELECT id,ing_id,ing_qty,ingredient_name,ing_price FROM ingredients WHERE cart_id_fk=" + cart_id + ";", null);
                int itemcount = cartMenuitem.getCount();
                Double itemtotal = 0.00;
                float prodTotal = 0f;
                products[] cart_sub_items = new products[100];
                int subcount = 0;
                ArrayList<Cartpojo> ingredientsList = new ArrayList<>();
                if (itemcount > 0) {
                    while (cartMenuitem.moveToNext()) {
                        float subitem_total = 0.00f;
                        Cartpojo ingData = new Cartpojo();
                        int ing_qty = cartMenuitem.getInt(cartMenuitem.getColumnIndex("ing_qty"));
                        float ing_price = (float) cartMenuitem.getDouble(cartMenuitem.getColumnIndex("ing_price"));
                        String cid = cartMenuitem.getString(cartMenuitem.getColumnIndex("id"));
                        String ing_name = cartMenuitem.getString(cartMenuitem.getColumnIndex("ingredient_name"));

                        ingData.setName(ing_name);
                        ingData.setPprice(String.format("%.2f", ing_price));
                        ingData.setPqty(String.valueOf(ing_qty));

                        Cursor subItmes = db.rawQuery("SELECT subitem_id,subitem_qty,subitem_name,subitem_price FROM subitems WHERE ing_id_fk=" + cid + ";", null);

                        int subitemcount = subItmes.getCount();
                        Double subitemtotal = 0.00;
                        ArrayList<Cartpojo> subitemsList = new ArrayList<>();
                        if (subitemcount > 0) {
                            while (subItmes.moveToNext()) {
                                Cartpojo subitemsData = new Cartpojo();

                                String subitem_id = subItmes.getString(subItmes.getColumnIndex("subitem_id"));
                                int subitem_qty = subItmes.getInt(subItmes.getColumnIndex("subitem_qty"));
                                String subitem_name = subItmes.getString(subItmes.getColumnIndex("subitem_name"));
                                Double subitem_price = subItmes.getDouble(subItmes.getColumnIndex("subitem_price"));

                                subitemtotal = subitemtotal + (subitem_qty * subitem_price);
                                subitemsData.setName(subitem_name);
                                subitemsData.setPprice(String.format("%.2f", subitem_price));
                                subitemsData.setPqty(String.valueOf(subitem_qty));

                                subitemsList.add(subitemsData);
                            }
                        }
                        subItmes.close();
                        subitem_total = (float) ((ing_qty * ing_price) + subitemtotal);
                        itemtotal = itemtotal + subitem_total;
                        cart_sub_items[subcount] = new products(ing_name, ing_qty, subitem_total, subitem_total, subitem_total, "");
//                        cart_sub_items[subcount].subproducts=
                        subcount++;
                        ingData.setTotal(String.format("%.2f", subitem_total));
                        ingData.setItem_list(subitemsList);
                        ingredientsList.add(ingData);
                    }
                    prodTotal = price1 = (float) (price1 + itemtotal);
                } else {
                    prodTotal = qty1 * price1;
                }
                subtotal1 = subtotal1 + prodTotal;

                groupData.setName(productName1);
                groupData.setPprice(String.format("%.2f", price1));
                groupData.setPqty(String.valueOf(qty1));
                cartMenuitem.close();

                productDetails.moveToNext();
                groupData.setTotal(String.format("%.2f", prodTotal));
                groupData.setItem_list(ingredientsList);
                printDataList.add(groupData);
                item++;
            }
        } else {
            db.close();
            AlertDialog.Builder adb = new AlertDialog.Builder(
                    Printer.this, android.R.style.Theme_Dialog);
            adb.setMessage("Cart Table Empty For Current TableNo");
            adb.setPositiveButton("Ok", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent mIntent = new Intent();
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mIntent.setClass(getApplicationContext(), Home.class);
                            startActivity(mIntent);
                        }
                    });
            adb.show();
            return;
        }
        productDetails.close();

        detail.tax = 0;//taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
        detail.tax1 = 0;//taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
        detail.tax2 = 0;//taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
        detail.tax3 = 0;//taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));
        db.close();
        billInsert(detail, printDataList, subtotal1, item, printFormat, consumerInfo);
    }


    public static Printer getInstance() {
        return PrinterId;
    }

    /**
     * This method inputs @param from the billClick method.
     *
     * @param details_items
     * @param pro_items
     * @param voucherDetails
     * @param pro_count
     * @param printFormat
     */
    public void bill(details details_items, products pro_items[], voucher voucherDetails[], int pro_count, int printFormat) {
        String buff[];
        String cut = null, dummy = "dummy", ph1, bill1, product1, sub1, sdot1, fdot1, ddot1;
        int i = 0, j = 0, count = 0, x = 0, vcount = 0;
        float total = 0, subtotal = 0, service_charge = 0, dis_amount = 0, gtotal = 0, typetotal = 0, service_charge1 = 0,
                service_charge2 = 0, service_charge3 = 0, realsubtotal = 0;
        String sbuff = null;
        String total1, date, time, tax = ".", user;

        ph1 = String.format("Tel:%s\n", details_items.ph);
        bill1 = String.format("Bill: %-7s    Table: %-7d\n", pro_items[0].orderno, details_items.table);
        product1 = String.format("Product            Qty      Price     Total\n");

        for (j = 1; j < pro_count; j++) {
            for (x = 0; x < j; x++) {
                if (pro_items[j].id.equals(pro_items[x].id))    //Here we are adding the quantity of the same products
                {
                    pro_items[x].qty = pro_items[x].qty + pro_items[j].qty;
                    if (printFormat != 0)
                        pro_items[x].price = pro_items[x].price + pro_items[j].price;
                    pro_items[j].qty = 0;
                }
            }
        }

        for (j = 0; j < pro_count; j++) {
            if (pro_items[j].qty > 0 && pro_items[j].price > 0)    //Here we are inserting the product details into the buffer
            {
                total = pro_items[j].qty * pro_items[j].price;
                if (printFormat != 0) {
                    subtotal = subtotal + pro_items[j].price;
                } else {
                    subtotal = subtotal + total;
                }
                if (printFormat != 0) {
                    if (pro_items[j].productName.length() > 18) {
                        cut = pro_items[j].productName.substring(0, 18);
                        //reducing product name to 9 character
                    } else {
                        cut = pro_items[j].productName.substring(0, pro_items[j].productName.length());
                    }
                    sbuff = String.format("%-18s %4d %12.2f\n", cut, pro_items[j].qty, pro_items[j].price);
                } else {
                    if (pro_items[j].productName.length() > 18) {
                        cut = pro_items[j].productName.substring(0, 18);
                        //Here reducing product name to 9 character
                    } else {
                        cut = pro_items[j].productName.substring(0, pro_items[j].productName.length());
                    }
                    sbuff = String.format("%-9s%3d%8.2f%11.2f\n", cut, pro_items[j].qty, pro_items[j].price, total);
                }
                if (dummy.equals("dummy")) {
                    dummy = sbuff;
                } else {
                    dummy = dummy + sbuff;
                }
            }
        }
        sdot1 = String.format("                                   --------\n");
        realsubtotal = subtotal;
        sub1 = String.format("Sub Total:      %27.2f\n", subtotal);
        if (printFormat != 0) {
            if (pro_items[0].discount > 0) {
                // If discount is more than 0 it will print the Bill
                dis_amount = (pro_items[0].discount * 100) / (subtotal);
                subtotal = subtotal - pro_items[0].discount;
                if (printFormat == 2) {
                    sub1 = sub1 + String.format("Discount:      %27.2f\n", pro_items[0].discount);
                } else {
                    sub1 = sub1 + String.format("Discount@%5.2f%%  %14.2f\n", dis_amount, pro_items[0].discount);
                }
            }

            for (vcount = 0; vcount < voucherDetails[0].vouchercount; vcount++) {
                if (voucherDetails[vcount].voucheramount > 0) {
                    subtotal = subtotal - voucherDetails[vcount].voucheramount;
                    sbuff = String.format("Voucher: %-13s%9.2f\n", voucherDetails[vcount].voucherno, voucherDetails[vcount].voucheramount);
                    sub1 = sub1 + sbuff;
                }

            }

            if (pro_items[0].discount > 0 || vcount > 0) {
                sbuff = String.format("                                   --------\nSub Total:      %27.2f\n", subtotal);
                sub1 = sub1 + sbuff;
            }

        }

        fdot1 = String.format("-------------------------------------------\n");

        if (details_items.tax > 0)                    // If Service tax is more than 0 it will print the Bill
        {
            service_charge = (realsubtotal * details_items.tax) / 100;
            subtotal = subtotal + service_charge;
            tax = String.format("VAT        @%5.2f%% %12.2f\n", details_items.tax, service_charge);
        }

        if (details_items.tax1 > 0)                    // If Service tax1 is more than 0 it will print the Bill
        {
            service_charge1 = (realsubtotal * details_items.tax1) / 100;
            subtotal = subtotal + service_charge1;
            tax = tax + String.format("Service tax @%5.2f%%%12.2f\n", details_items.tax1, service_charge1);
        }
        if (details_items.tax2 > 0)                    // If Service tax2 is more than 0 it will print the Bill
        {
            service_charge2 = (realsubtotal * details_items.tax2) / 100;
            subtotal = subtotal + service_charge2;
            tax = tax + String.format("Service tax2@%5.2f%% %11.2f\n", details_items.tax2, service_charge2);
        }

        if (details_items.tax3 > 0)                    // If Service tax3 is more than 0 it will print the Bill
        {
            service_charge3 = (realsubtotal * details_items.tax3) / 100;
            subtotal = subtotal + service_charge3;
            tax = tax + String.format("Service tax3@%5.2f%%%12.2f\n", details_items.tax3, service_charge3);

        }
        if (tax.equals(".")) {
            tax = String.format("                                   --------\n");
        } else {
            tax = tax + String.format("                                   --------\n");
        }
        gtotal = subtotal;

        if (printFormat != 0) {
            details_items.cash = Float.parseFloat(Group.GROUPID.finaltotal);//cashdiscount.cash;
            //balance
            total1 = String.format("Total Amount:%18.2f\nCash Paid:%21.2f", gtotal, details_items.cash);

        } else {
            total1 = String.format("Total Amount To Pay%12.2f\n", gtotal);
        }
        ddot1 = String.format("                       ========\n");
        user = String.format("User: %s", details_items.lastloginusername);
        String timeStamp = new SimpleDateFormat("dd/MM/yy HH:mm").format(Calendar.getInstance().getTime());
//        sbuff = String.format("%s\n%s\n%s\n%s\n%s%s\n%s%s%s%s%s%s%s%s%s%s\n%s\n%s%s %15s  ", details_items.name, details_items.add1,
//                details_items.add2, details_items.city, ph1, details_items.webadd, bill1, product1, dummy, sdot1,
//                sub1, tax, total1, ddot1, fdot1, details_items.mess1, details_items.mess2, fdot1, user, timeStamp);
        sbuff = String.format("%s\n%s\n%s\n%s\n%s\n%s%s\n%s%s%s%s%s%s%s%s%s%s\n%s\n%s%s %15s  ", appName, details_items.name, details_items.add1,
                details_items.webadd, bill1, product1, dummy, sdot1,
                sub1, tax, total1, ddot1, fdot1, details_items.mess1, details_items.mess2, fdot1, user, timeStamp);
        // Here we are storing all the data into one String sbuff.
        if (mSharedPreferences.getBoolean("isWifiPrinterEnable", false)) {
            Begin();
            write((sbuff).getBytes());
            write(("\r\n").getBytes());
        } else {
            globalsbuff = sbuff;
            deviceconnection(sbuff, printFormat);
        }
    }

    public void billInsert(details details_items, ArrayList<Cartpojo> groupList, float subtotal, int pro_count, int printFormat, StringBuilder conInfo) {
        //customer print
        Log.w("Printer", "billInsert");
        String buff[];
        StringBuilder dummy = new StringBuilder("dummy");
        String cut = null, ph1, bill1, product1, sub1, sdot1, fdot1, ddot1;
        int i = 0, j = 0, count = 0, x = 0, vcount = 0;
        float subtotal1 = subtotal;
        float total = 0, service_charge = 0, dis_amount = 0, gtotal = 0, typetotal = 0, service_charge1 = 0,
                service_charge2 = 0, service_charge3 = 0, realsubtotal = 0;
        String sbuff = null;
        String total1, date, time, tax = ".", user;
        ph1 = String.format("Tel:%s\n", details_items.ph);
        String dine = "DINE IN";
        String take = "T/A";
        String web = "WEB";
        String visitagain = "Thank You! Visit Again";

        if (Integer.parseInt(Group.GROUPID.table) == 0) {
            bill1 = String.format("Order Id: %-1s                           %-2s\n", ORDERID, web);
            // bill1 = String.format("   %40s\n", /*pro_items[0].orderno,*/ web);
        } else if (Integer.parseInt(Group.GROUPID.table) < 1500) {
            bill1 = String.format("   %40s\n", /*pro_items[0].orderno,*/ dine);
        } else {
            bill1 = String.format("   %40s\n", /*pro_items[0].orderno,*/ take);
        }

        product1 = String.format("Product            Qty      Price     Total\n");
        StringBuilder groupString;
        for (int g = 0; g < groupList.size(); g++) {
            Cartpojo grp = groupList.get(g);//get group list item
            String prodname = grp.getName();
            String prodQty = grp.getPqty();
            if (grp.getName().length() > 18) {
                cut = grp.getName().substring(0, 18); //Here reducing product name to 9 character
            } else {
                cut = grp.getName().substring(0, grp.getName().length());
            }
            groupString = new StringBuilder(String.format("%-18s%3d%12.2f%10.2f\n", cut, Integer.parseInt(prodQty), Float.parseFloat(grp.getPprice()), Float.parseFloat(grp.getTotal())));
            StringBuilder ingString = new StringBuilder("dummy");
            for (i = 0; i < grp.getItem_list().size(); i++) {
                Cartpojo ingre = grp.getItem_list().get(i);//get ingredients list item

                if (ingre.getName().length() > 16) {
                    cut = ingre.getName().substring(0, 16); //Here reducing product name to 9 character
                } else {
                    cut = ingre.getName().substring(0, ingre.getName().length());
                }
                ingString = new StringBuilder(String.format("  %-16s%3d%12s%10s\n", cut, Integer.parseInt(ingre.getPqty()), "", ""));//ingredient        qty

                String subitemString = "dummy";
                for (int s = 0; s < ingre.getItem_list().size(); s++) {
                    Cartpojo subitem = ingre.getItem_list().get(s);//get ingredients list item
                    if (subitem.getName().length() > 13) {
                        cut = subitem.getName().substring(0, 13); //Here reducing product name to 9 character
                    } else {
                        cut = subitem.getName().substring(0, subitem.getName().length());
                    }
                    subitemString = String.format("     %-13s%3d%12s%10s\n", cut, Integer.parseInt(subitem.getPqty()), "", "");
                    ingString.append(subitemString);
                }
                groupString.append(ingString);
            }
            if (dummy.toString().equals("dummy")) {
                dummy = new StringBuilder(groupString.toString());
            } else {
                dummy.append(groupString);
            }

        }

        sdot1 = String.format("                                   --------\n");
        realsubtotal = subtotal1;
        sub1 = String.format("Sub Total:      %27.2f\n", subtotal1);
        fdot1 = String.format("-------------------------------------------\n");
        String vfdot = String.format("-------------------------------------------");
        if (details_items.tax > 0)                    // If Service tax is more than 0 it will print the Bill
        {
            service_charge = (realsubtotal * details_items.tax) / 100;
            subtotal1 = subtotal1 + service_charge;
            tax = String.format("VAT        @%5.2f%% %12.2f\n", details_items.tax, service_charge);
        }

        if (details_items.tax1 > 0)                    // If Service tax1 is more than 0 it will print the Bill
        {
            service_charge1 = (realsubtotal * details_items.tax1) / 100;
            subtotal1 = subtotal1 + service_charge1;
            tax = tax + String.format("Service tax @%5.2f%%%12.2f\n", details_items.tax1, service_charge1);
        }
        if (details_items.tax2 > 0)                    // If Service tax2 is more than 0 it will print the Bill
        {
            service_charge2 = (realsubtotal * details_items.tax2) / 100;
            subtotal1 = subtotal1 + service_charge2;
            tax = tax + String.format("Service tax2@%5.2f%% %11.2f\n", details_items.tax2, service_charge2);
        }

        if (details_items.tax3 > 0)                    // If Service tax3 is more than 0 it will print the Bill
        {
            service_charge3 = (realsubtotal * details_items.tax3) / 100;
            subtotal1 = subtotal1 + service_charge3;
            tax = tax + String.format("Service tax3@%5.2f%%%12.2f\n", details_items.tax3, service_charge3);

        }
        if (tax.equals(".")) {
            tax = String.format("                                   --------\n");
        } else {
            tax = tax + String.format("                                   --------\n");
        }
        gtotal = subtotal1;
        String changeGiven = "";
        String cashGiven = "";
//        if (printFormat != 0) {
        try {
            if (Group.GROUPID.finaltotal != null && !Group.GROUPID.finaltotal.equals("")) {
                float cashpaid = Float.parseFloat(Group.GROUPID.cashpaid);
                float change = Float.parseFloat(Group.GROUPID.changegiven);
                details_items.cash = Float.parseFloat(Group.GROUPID.finaltotal);//cashdiscount.cash;
                total1 = String.format("Total Amount:   %27.2f\n", gtotal);
                cashGiven = String.format("Cash Given:     %27.2f\n", cashpaid);
                changeGiven = String.format("Change:         %27.2f\n", change);
            } else {
                details_items.cash = gtotal;
                total1 = String.format("Total Amount:   %27.2f\n", gtotal);
            }
        } catch (Exception ignore) {
            details_items.cash = gtotal;
            total1 = String.format("Total Amount:   %27.2f\n", gtotal);
        }
//        } else {
//            total1 = String.format("Total Amount:   %27.2f\n", gtotal);
//        }
        ddot1 = String.format("                                   ========\n");
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor chairs = db.rawQuery("SELECT guestno FROM cart WHERE tableno =" + Group.GROUPID.table + ";", null);
        int cou = chairs.getCount();
        chairs.moveToFirst();
        String guests = "";
        if (cou > 0) {
            guests = chairs.getString(chairs.getColumnIndex("guestno"));
        }
        db.close();
        String timeStamp = new SimpleDateFormat("dd/MM/yy HH:mm").format(Calendar.getInstance().getTime());
//        byte[]  app_logo;
//        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.billout);
//        if (bmp != null) {
//            app_logo = Utils.decodeBitmap(bmp);
//        } else {
//            Log.e("Print Photo error", "the file isn't exists");
//        }

        user = String.format("User: %-7s\n", details_items.lastloginusername);
        String ticket = String.format("%43s\n", /*pro_items[0].orderno,*/ timeStamp);
        String table;
        if (Integer.parseInt(Group.GROUPID.table) < 1500) {
            table = String.format("Table: %-1s                         Guests:%-2s", details_items.table, guests);
        } else
            table = String.format("Table: %-1s", "Take away");

        sbuff = String.format("%s\n%s\n%s\n%s\n\n%s%s%s%s%s%s\n%s%s%s%s%s%s%s%s%s%s%s\n%s\n%s  \n", appName, details_items.name, details_items.add1,
                details_items.webadd, user, fdot1, conInfo, ticket, bill1, table, fdot1, product1, dummy, sdot1,
                sub1, tax, total1, cashGiven, changeGiven, ddot1, vfdot, visitagain, vfdot);
        // Here we are storing all the data into one String sbuff.
        Log.e("sbuff", "\n" + sbuff);
        if (mSharedPreferences.getBoolean("isWifiPrinterEnable", false)) {
            Begin();
            write((sbuff).getBytes());
            write(("\r\n").getBytes());
        } else {
            globalsbuff = sbuff;
            deviceconnection(sbuff, printFormat);
        }
        if (printFormat == 1) {
            UpdateCart();
        }
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param
     */
    @SuppressLint("NewApi")
    public synchronized void connect(BluetoothDevice device) throws IOException {
        BluetoothSocket tmp = null;
        try {
            Method m;
            m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
            tmp = (BluetoothSocket) m.invoke(device, 1);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(Printer.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            if (printFlag == KITCHENPRINT) {
                finish();
            } else {
                Intent mIntent = new Intent();
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.setClass(getApplicationContext(), Home.class);
                startActivity(mIntent);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            Toast.makeText(Printer.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            if (printFlag == KITCHENPRINT) {
                finish();
            } else {
                Intent mIntent = new Intent();
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.setClass(getApplicationContext(), Home.class);
                startActivity(mIntent);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            Toast.makeText(Printer.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            if (printFlag == KITCHENPRINT) {
                finish();
            } else {
                Intent mIntent = new Intent();
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.setClass(getApplicationContext(), Home.class);
                startActivity(mIntent);
            }
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            Toast.makeText(Printer.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            if (printFlag == KITCHENPRINT) {
                finish();
            } else {
                Intent mIntent = new Intent();
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.setClass(getApplicationContext(), Home.class);
                startActivity(mIntent);
            }
        }
        mmSocket = tmp;
        mBluetoothAdapter.cancelDiscovery();
        new Connection().execute("ok");
    }

    public class Connection extends AsyncTask<String, Void, Boolean> {

		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isconnected = false;

        public void onPreExecute() {
            isconnected = false;
        }

        public Boolean doInBackground(final String... args) {
            try {
                mmSocket.connect();
                mmInStream = mmSocket.getInputStream();
                mmOutStream = mmSocket.getOutputStream();
                isconnected = true;
            } catch (IOException e) {
                e.printStackTrace();
                isconnected = false;
            }

            return false;
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            try {
                Begin();
                write((printdata + "\n").getBytes());
                Pcut();
                mmSocket.close();
                if (pFormate == PAYBILLPRINT) {
                    updatePayInfo();
                } else if (printFlag == KITCHENPRINT) {
                    finish();
                } else {
                    Intent mIntent = new Intent();
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.setClass(getApplicationContext(), Home.class);
                    startActivity(mIntent);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void ListPairedDevices() {

        Set<BluetoothDevice> bondedDevices = BluetoothAdapter
                .getDefaultAdapter().getBondedDevices();
        for (int k = 0; k < bondedDevices.size(); k++) {
        }
        if (bondedDevices.size() > 0) {
            for (BluetoothDevice bondDeice : bondedDevices) {
                connections.add(bondDeice);
                connectionsname.add(bondDeice.getName());
                printerDeviceCheck = true;
            }
        }
        if (printerDeviceCheck == true) {
            printerDeviceCheck = false;
            Device_Select();
        } else {
            Toast.makeText(getApplicationContext(), "Printer not found", Toast.LENGTH_SHORT).show();
            if (printFlag == KITCHENPRINT) {
                finish();
            } else {
                Intent mIntent = new Intent();
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.setClass(getApplicationContext(), Home.class);
                startActivity(mIntent);
            }
        }

    }

    private void Device_Select() {
        settingDialog = new Dialog(Printer.this);
        settingDialog.setContentView(R.layout.fragment_scanner_device_selection);
        settingDialog.setTitle("Paired Device");
        settingDialog.setCanceledOnTouchOutside(false);
        settingDialog.setCancelable(false);
        lv = (ListView) settingDialog.findViewById(R.id.list);
        settingDialog.show();
        adapter = new ArrayAdapter<BluetoothDevice>(this, android.R.layout.simple_list_item_1, connections);
        adaptername = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, connectionsname);
        lv.setAdapter(adaptername);
        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                try {
                    globledevice = adapter.getItem(arg2).getAddress();
                    if (printFlag == PRINTBILL || printFlag == PAYBILLPRINT || printFlag == ORDERNOPRINT) {
                        mSharedPreferences.edit().putString("btPrinterid", globledevice).apply();
                        mdevice = mBluetoothAdapter
                                .getRemoteDevice(globledevice);
                        if (printFlag == PRINTBILL) {
                            billClick(tableNo, locationNo, 0);
                        } else if (printFlag == PAYBILLPRINT) {
                            billClick(tableNo, locationNo, 1);
                        } else if (printFlag == ORDERNOPRINT) {
                            //orderNoBillPrint(""+tableNo);
                        } else {
                            db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                            Cursor FoodId = db.rawQuery("SELECT cart_id,menu_id from cart WHERE tableno=" + tableNo + " AND billflag=0;", null);
                            if (FoodId.getCount() > 0) {
                                click(tableNo, 0, 0);
                            }
                            FoodId.close();

                        }
                    } else {
                        mSharedPreferences.edit().putString("btkPrinterid", globledevice).apply();
                        kdevice = mBluetoothAdapter
                                .getRemoteDevice(globledevice);
                        if (printFlag == PRINTBILL) {
                            billClick(tableNo, locationNo, 0);
                        } else if (printFlag == PAYBILLPRINT) {
                            billClick(tableNo, locationNo, 1);
                        } else if (printFlag == ORDERNOPRINT) {
                            //orderNoBillPrint(""+tableNo);
                        } else {
                            db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                            Cursor FoodId = db.rawQuery("SELECT cart_id,menu_id from cart WHERE tableno=" + tableNo + " AND billflag=0;", null);
                            if (FoodId.getCount() > 0) {
                                click(tableNo, 0, 0);
                            }
                            FoodId.close();

                        }

                    }
                } catch (RuntimeException e) {
                    Toast.makeText(Printer.this, " insert run time exception" + e.getMessage(), Toast.LENGTH_LONG).show();
                    if (printFlag == KITCHENPRINT) {
                        finish();
                    } else {
                        Intent mIntent = new Intent();
                        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.setClass(getApplicationContext(), Home.class);
                        startActivity(mIntent);
                    }
                }
                settingDialog.dismiss();

            }
        });
    }

    /**
     * @param data
     * @param printformate
     */
    private void deviceconnection(String data, int printformate) {
        // TODO Auto-generated method stub
        printdata = data;
        pFormate = printformate;
        if (pFormate == PRINTBILL || pFormate == PAYBILLPRINT) {
            try {
                connect(mdevice);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            WiFi();
        }

    }

    public void WiFi() {
        boolean isconnected = false;
        strName = "192.168.1.87:9100";
//        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
//        Cursor printerDetails = db.rawQuery("SELECT ipaddress, portno FROM locationprinter where foodid =" + kitchenid + ";", null);
//        int printerDetailsCount = printerDetails.getCount();
//        if (printerDetailsCount > 0) {
//            printerDetails.moveToFirst();
//            strName = printerDetails.getString(0) + ":" + printerDetails.getString(1);
//            SERVER_IP = printerDetails.getString(0);
//            SERVERPORT = Integer.parseInt(printerDetails.getString(1));
//        }
//        db.close();
//        if (strName.length() == 0) {
//            Toast.makeText(Printer.this, "Error:port name empty", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        isconnected = false;
//        try {
//            if (mysocket != null) {
//                if (mmOutStream != null) {
//                    mmOutStream.close();
//                }
//                if (mmInStream != null) {
//                    mmInStream.close();
//                }
//                mmOutStream = null;
//                mmInStream = null;
//                mysocket.close();
//                mysocket = null;
//            }
//            mysocket = new Socket();
//            mysocket.connect(new InetSocketAddress(SERVER_IP, SERVERPORT), 5000);
//            mmOutStream = mysocket.getOutputStream();
//            mmInStream = mysocket.getInputStream();
//            isconnected = true;
//        } catch (UnknownHostException e1) {
//            e1.printStackTrace();
//            isconnected = false;
//        } catch (IOException e1) {
//            e1.printStackTrace();
//            isconnected = false;
//        }
        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (isconnected == true) {
            Begin();
            write((printdata + "\n").getBytes());
            Pcut();
            if (mSharedPreferences.getBoolean("isWifiPrinterEnable", false)) {
                optionalwifi();
            }
            if (mSharedPreferences.getBoolean("kotPrinterEnable", false)) {
                if (kdevice != null) {
                    try {
                        connect(kdevice);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (printFlag == KITCHENPRINT) {
                finish();
            } else {
                Intent mIntent = new Intent();
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.setClass(getApplicationContext(), Home.class);
                startActivity(mIntent);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selected WIFi KOT Printer is not Online trying to connect to bluetooth", Toast.LENGTH_LONG).show();
            if (mysocket != null) {
                try {
                    if (mmOutStream != null) {
                        mmOutStream.close();
                    }
                    if (mmInStream != null) {
                        mmInStream.close();
                    }
                    mmInStream = null;
                    mmOutStream = null;
                    mysocket.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                mysocket = null;
            }
            try {
                if (kdevice != null) {
                    connect(kdevice);
                } else if (printFlag == KITCHENPRINT) {
                    finish();
                } else {
                    Intent mIntent = new Intent();
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.setClass(getApplicationContext(), Home.class);
                    startActivity(mIntent);
                    // UpdateCart();
                    Toast.makeText(getApplicationContext(), "Bluetooth KOT Printer is not available ", Toast.LENGTH_LONG).show();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void UpdateCart() {
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor cartData = db.rawQuery("SELECT cart_id from cart WHERE tableno=" + tableNo + ";", null);
        int count = cartData.getCount();
        if (count > 0) {
            while (cartData.moveToNext()) {
                String sno = cartData.getString(cartData.getColumnIndex("cart_id"));
                //here we are commeting the delete database when home button is clicked

//                db.execSQL("DELETE FROM ingredients WHERE cart_id_fk = " + sno + ";");
//                db.execSQL("DELETE FROM cart WHERE tableNo = " + tableNo + ";");
//                billflag=0 AND orderflag=0
                db.execSQL("UPDATE cart SET billflag=1,orderflag=1 WHERE tableNo = " + tableNo + ";");

//                if (!guests.matches("")) {
//                    String tab_num_query = "UPDATE tables SET occupiedchairs = " + guests + ",orderid = NULL" + " WHERE tableno =" + tableNo + ";";
//                    db.execSQL(tab_num_query);
//                }
            }
        }
        if (printFlag == KITCHENPRINT) {
            finish();
        } else {
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        }
        Toast.makeText(getApplicationContext(), "Order Sent Successfully", Toast.LENGTH_LONG).show();
        // Toast.makeText(getApplicationContext(), "Bluetooth KOT Printer is not available ", Toast.LENGTH_LONG).show();

    }

    public void optionalwifi() {
        boolean isconnected = false;
        strName = "192.168.1.87:9100";
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor printerDetails = db.rawQuery("SELECT ipaddress, portno FROM locationprinter where foodid =0;", null);
        int printerDetailsCount = printerDetails.getCount();
        if (printerDetailsCount > 0) {
            printerDetails.moveToFirst();
            strName = printerDetails.getString(0) + ":" + printerDetails.getString(1);
            SERVER_IP = printerDetails.getString(0);
            SERVERPORT = Integer.parseInt(printerDetails.getString(1));
        }
        db.close();
        if (strName.length() == 0) {
            Toast.makeText(Printer.this, "Error:port name empty", Toast.LENGTH_SHORT).show();
            return;
        }
        isconnected = false;
        try {
            if (mysocket != null) {
                if (mmOutStream != null) {
                    if (mmInStream != null) {
                        mmOutStream.close();
                    }
                    if (mmInStream != null) {
                        mmInStream.close();
                    }
                    mmOutStream = null;
                    mmInStream = null;

                }
                mysocket.close();
                mysocket = null;
            }
            mysocket = new Socket();
            mysocket.connect(new InetSocketAddress(SERVER_IP, SERVERPORT), 5000);
            mmOutStream = mysocket.getOutputStream();
            mmInStream = mysocket.getInputStream();
            isconnected = true;
        } catch (UnknownHostException e1) {
            e1.printStackTrace();
            isconnected = false;
        } catch (IOException e1) {
            e1.printStackTrace();
            isconnected = false;
        }
        for (int i = 0; i < 150; i++) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (isconnected == true) {
            Begin();
            write((printdata + "\n").getBytes());
        }

    }

    public static void Begin() {
        WakeUpPritner();
        InitPrinter();
    }

    public static void InitPrinter() {
        byte[] combyte = new byte[]{(byte) 27, (byte) 64};

        try {
            if (mmOutStream != null) {
                mmOutStream.write(combyte);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void WakeUpPritner() {
        byte[] b = new byte[3];

        try {
            if (mmOutStream != null) {
                mmOutStream.write(b);
            }

            Thread.sleep(100L);
        } catch (Exception var2) {
            var2.printStackTrace();
        }
    }

    public static void Pcut() {
        byte[] pcut = new byte[]{(byte) 0x1D, (byte) 0x56, (byte) 0x42, (byte) 0x00};
        try {
            if (mmOutStream != null) {
                mmOutStream.write(pcut);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* Call this from the main activity to send data to the remote device */
    public void write(byte[] bytes) {
        try {
            byte[] cAlign = {(byte) 0x1b, (byte) 0x61, (byte) 0x01};
            byte[] letterSize = {(byte) 0x1D, (byte) 0x21, (byte) 0x01};
            if (mmOutStream != null) {
                mmOutStream.write(cAlign);
                if (pFormate != PRINTBILL && pFormate != PAYBILLPRINT) {
                    mmOutStream.write(letterSize);
                }
                mmOutStream.write(bytes);
            }
        } catch (IOException e) {
            if (printFlag == KITCHENPRINT) {
                finish();
            } else {
                Intent mIntent = new Intent();
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent.setClass(getApplicationContext(), Home.class);
                startActivity(mIntent);
            }
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                pDialog = new ProgressDialog(
                        Printer.this, android.R.style.Theme_Dialog);
                pDialog.setMessage("Connecting To Printer...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pDialog.show();
                return pDialog;

        }
        return null;
    }


    /* (non-Javadoc)
     * @see android.app.Activity#onDestroy()
	 */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (settingDialog != null) {
            if (settingDialog.isShowing()) {
                settingDialog.dismiss();
            }
            settingDialog = null;
        }
        if (mysocket != null) {
            try {
                if (mmOutStream != null) {
                    mmOutStream.close();
                    if (mmInStream != null) {
                        mmInStream.close();
                    }

                    mmOutStream = null;
                    mmInStream = null;
                }
                mysocket.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            mysocket = null;
        }
    }

    public void updatePayInfo() {
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
//        db.execSQL("UPDATE cart SET billedqty = (billedqty + billqty), billedtypeqty = (billedtypeqty + billtypeqty) WHERE tableno = " + tableNo +
//                " AND locationid = " + UserLoginActivity.user_LoginID.LOCATIONNO + " AND billflag = 0 AND checkflag = 0;");
//        db.execSQL("UPDATE cart SET  billqty = orderqty-billedqty, billtypeqty = ordertypeqty-billedtypeqty WHERE tableno = " +
//                tableNo + " AND locationid = " + UserLoginActivity.user_LoginID.LOCATIONNO + ";");
        Cursor updateCount = db.rawQuery("SELECT * from cart  WHERE tableno = " + tableNo + " AND billflag = 0;", null);
        int count = updateCount.getCount();
        updateCount.moveToPrevious();
        String newMessage = null;
        String message = null;
        String orderNo = null;

//        myDb.Insert(String.valueOf(MainActivity.MAINID.outletid), String.valueOf(tableNo), "0", "NULL", "NULL");
//        Intent intent = new Intent(Printer.this, ServiceOccupiedChairs.class);
//        startService(intent);


        db.execSQL("UPDATE cart SET  billflag= 1 WHERE tableno = " + tableNo + ";");

        db.execSQL("UPDATE receipt SET checkflag = 1 where orderid ='" + orderNo + "' AND checkflag = 0;");

        Cursor cartData = db.rawQuery("SELECT * from cart WHERE tableno = " + tableNo + " AND orderflag = 1 AND billflag = 0 AND checkflag = 1;", null);
        int cartCount = cartData.getCount();
        if (cartCount < 1) {
            Cursor qtyData = db.rawQuery("SELECT * from cart WHERE tableno = " + tableNo + " AND orderflag = 1 AND billflag = 0;", null);

            if (qtyData.getCount() == 0) {
                Toast.makeText(Printer.this, "Order Successfully Closed ", Toast.LENGTH_SHORT)
                        .show();
                db.execSQL("UPDATE tables SET occupiedchairs = 0 WHERE tableno = " + tableNo + ";");
                db.execSQL("UPDATE ordernos SET  billflag= 1 WHERE tableno = " + tableNo + ";");
//                db.execSQL("UPDATE cart SET  tbillflag= 1 WHERE tableno = " + tableNo + " AND locationid = " +
//                        UserLoginActivity.user_LoginID.LOCATIONNO + " AND billflag =1;");
                CartStaticSragment.CARTStaticSragment.noofguests.setText("0");

            } else {
                qtyData.moveToFirst();

            }
        } else {
            db.execSQL("UPDATE cart SET checkflag = 0 WHERE tableno = " + tableNo + " AND billflag = 0 AND checkflag = 1;");
        }
        db.close();
//        if (count > 0) {
//            Socket_Connection socket = new Socket_Connection();
//            int status = socket.connectionToServer();
//            if (status != 0) {
//                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
//                db.execSQL("INSERT INTO queuetable(message) VALUES('" + MainActivity.getInstance().finnalMesgToSend + "');");
//                MainActivity.getInstance().queueMessage = "0";
//                db.close();
//            }
//        }
        return;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
package com.rsl.easyfood;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.instabug.library.InstabugTrackingDelegate;
import com.rsl.model.OrderDisplay;
import com.rsl.utills.SessionManager;
import com.rsl.utills.VerticalButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author EMBDES.
 * @version 1.0.
 *          This Activity display all available groups.
 *          This is the main Activity(Contains All Fragments )
 *          (Group Fragmnet,Cart-Static Fragment)
 */
@SuppressLint("NewApi")
public class Group extends Activity /*implements NewOrderFragment.FragmentDrawerListener*/ {
    final String TAG = "Group";
    private NewOrderFragment drawerFragment;
    public float discountAmount;
    float totalAmount;
    float subTotalAmount;
    public float voucherAmount = 0;
    float taxAmount;
    SQLiteDatabase db;
    String finaltotal;
    String cashpaid;
    String changegiven;
    static String table;
    String groupId;
    VerticalButton verticalButtonNewOrder;
    TextView group, product;
    public static boolean IsActivityGroupOpen = false;
    Button menu;
    //search/info
    TextView txt_info;
    ImageView searchView;
    LinearLayout lay_info;
    RelativeLayout lay_search;
    SharedPreferences mSharedPreferences;
    BillFragment billfragment;
    EditText search;
    ImageView imgclosesearch;
    public static TextView textNewordercount;
    String groupName;
    String categoryName;
    public String voucherNumber = "0";
    public String ORDERNO = "0";
    public static String CONSUMERID = "0";
    TextView date, titlebar;
    static Group GROUPID;
    Bitmap bmThumbnail;
    int i = 1;
    TablesFragment tablesFragment;
    TextView tableno;
    TextView username;
    CartStaticSragment cartstaticfragment;
    GroupFragment groupfragment;
    CategoryFragment categoryFragment, cat1frag;
    android.app.FragmentManager fragmentManager;
    android.app.FragmentTransaction fragmentTransaction;
    Boolean productflag = false;
    ProgressDialog dialog;
    DrawerLayout mDrawerLayout;
    Boolean newOrderFlag = true;
    ObjectAnimator objectAnimator;

    //    DrawerLayout mDrawerLayout
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.group);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

//        drawerFragment = new NewOrderFragment();
//        drawerFragment.setUp(R.id.fragment_navigation_drawer,(DrawerLayout)
//                findViewById(R.id.drawer_layout), mToolbar);
//        drawerFragment.setDrawerListener(this);
//       Fragment mDrawerLayout = (Fragment) findViewById(R.id.drawer_layout);
//        int width = getResources().getDisplayMetrics().widthPixels/2;
//        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) mDrawerList.getLayoutParams();
//        params.width = width;
//        mDrawerList.setLayoutParams(params);
//        ft.commit();
        GROUPID = this;
        manager = new SessionManager(Group.this);
        dialog = new ProgressDialog(Group.this);
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        createTables();
        //  new updateSlider().execute();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        menu = (Button) findViewById(R.id.Imageview);
        date = (TextView) findViewById(R.id.date);
        searchView = (ImageView) findViewById(R.id.titlebarSearchview);
        search = (EditText) findViewById(R.id.titleditext);
        imgclosesearch = (ImageView) findViewById(R.id.imgclosesearch);
        textNewordercount = (TextView) findViewById(R.id.txtnewordercount);
        txt_info = (TextView) findViewById(R.id.txt_info);
        titlebar = (TextView) findViewById(R.id.titelabar);
        lay_info = (LinearLayout) findViewById(R.id.lay_info);
        lay_search = (RelativeLayout) findViewById(R.id.lay_search);
        verticalButtonNewOrder = (VerticalButton) findViewById(R.id.verticalButtonNewOrder);

        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Group.this, Home.class);
                startActivity(intent);
            }
        });
        imgclosesearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setText("");
                lay_info.setVisibility(View.VISIBLE);
                lay_search.setVisibility(View.GONE);
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                assert inputMethodManager != null;
                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        verticalButtonNewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelAnimator();
                verticalButtonNewOrder.clearAnimation();
                verticalButtonNewOrder.clearFocus();
                Log.e("accepted_count", "method" + setCount());
                if (setCount() > 0) {
                    mDrawerLayout.openDrawer(Gravity.END);
                } else {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            Group.this, android.R.style.Theme_Dialog);
                    msg.setMessage("No New Orders");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                }

            }
        });

        groupName = "group";
        categoryName = "Category";
        discountAmount = 0;
        totalAmount = 0;
        voucherAmount = 0;
        taxAmount = 0;
        subTotalAmount = 0;

        cat1frag = new CategoryFragment();
        categoryFragment = new CategoryFragment();
        groupfragment = new GroupFragment();
        billfragment = new BillFragment();
        fragmentManager = getFragmentManager();
        tablesFragment = new TablesFragment();
        cartstaticfragment = (CartStaticSragment) fragmentManager.findFragmentById(R.id.staticfragmet2);
        Cursor tablesData = db.rawQuery("SELECT id FROM tables;", null);

        Cursor terminalData = db.rawQuery("SELECT id FROM masterterminal;", null);
        if (terminalData.getCount() == 0) {
            getMerchantInfo(mSharedPreferences.getString("mid", ""));
        } else if (tablesData.getCount() == 0) {
            dropTables();
            createTables();
            getMerchantInfo(mSharedPreferences.getString("mid", ""));
        }


        if (Home.HOMEID.variable /*|| GetUserInfo.User.deliveryvariable*/) {
            Home.HOMEID.variable = false;
            GetUserInfo.User.deliveryvariable = false;
            CartStaticSragment.CARTStaticSragment.tableno.setText(Group.GROUPID.table);
            if (getIntent().getBooleanExtra("print_bill", false)) {
                Log.e("print_bill", "" + getIntent().getBooleanExtra("print_bill", false));
                getIntent().putExtra("print_bill", false);
                if (cartstaticfragment.tableno.getText().toString().trim().equals("")) {
                    AlertDialog.Builder msg = new AlertDialog.Builder(Group.this, android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Select table");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                } else {
                    if (Integer.parseInt(cartstaticfragment.tableno.getText().toString()) > 1500 && Integer.parseInt(cartstaticfragment.tableno.getText().toString()) < 1600) {
                        CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away:-");
                        CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                        CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                        CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                        CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                        CartStaticSragment.CARTStaticSragment.changetables.setClickable(false);
                        CartStaticSragment.CARTStaticSragment.tables.setClickable(false);
                    } else {
                        Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" + cartstaticfragment.tableno.getText().toString() + ";", null);
                        int cou = chairs.getCount();
                        chairs.moveToFirst();
                        if (cou > 0) {
                            String guests = chairs.getString(chairs.getColumnIndex("occupiedchairs"));
                            CartStaticSragment.CARTStaticSragment.noofguests.setText(guests);
                        }
                    }
//                    Group.GROUPID.table = cartstaticfragment.tableno.getText().toString();
                    String noOfGuest = CartStaticSragment.CARTStaticSragment.noofguests.getText().toString();
                    db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                    Cursor cartData = db.rawQuery("SELECT cart_id from cart WHERE tableno=" + Group.GROUPID.table + " AND billflag=0;", null);
                    int count = cartData.getCount();
                    Log.e("count", "==" + count);
                    if (count > 0) {
                        if (!billfragment.isResumed()) {
//                            db.execSQL("UPDATE cart SET guestno = " + noOfGuest + " WHERE tableno = " + Group.GROUPID.table + " AND billflag = 0 ;");
//                            db.execSQL("UPDATE tables SET occupiedchairs = " + noOfGuest + " WHERE tableno = " + Group.GROUPID.table + ";");

                            cartstaticfragment.billoutFlag = true;
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container2, billfragment);
                            fragmentTransaction.commit();
                        }
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(Group.this, android.R.style.Theme_Dialog);
                        msg.setTitle("");
                        msg.setMessage("Cart Is Empty To Bill Out ");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                    db.close();
                }
            } else {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container2, groupfragment);
                fragmentTransaction.commit();


                CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away:-");
                CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                CartStaticSragment.CARTStaticSragment.changetables.setClickable(false);
                CartStaticSragment.CARTStaticSragment.tables.setClickable(false);

                CartStaticSragment.CARTStaticSragment.changetables.setBackgroundColor(ContextCompat.getColor(Group.this, R.color.gray));
                CartStaticSragment.CARTStaticSragment.tables.setBackgroundColor(ContextCompat.getColor(Group.this, R.color.gray));
            }
            alert();

        } else if (!tablesFragment.isResumed()) {
            CONSUMERID = "0";
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.container2, tablesFragment);
            fragmentTransaction.commit();
        }
        if (getIntent() != null) {
            Log.e("OnNotification", "=" + setCount());
            if (setCount() > 0) {
                fadeIn();
            }

        }
        if (setCount() > 0) {
            fadeIn();
        }

        terminalData.close();
        if (setCount() > 0) {
            fadeIn();
        }

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productflag = true;

                Group.GROUPID.lay_info.setVisibility(View.GONE);
                Group.GROUPID.lay_search.setVisibility(View.VISIBLE);
                Group.GROUPID.imgclosesearch.setVisibility(View.VISIBLE);

                search.setVisibility(View.VISIBLE);
                //     delete.setVisibility(View.VISIBLE);
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                searchView.setVisibility(View.INVISIBLE);
                if (!categoryFragment.isResumed()) {
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container2, categoryFragment);
                    fragmentTransaction.commit();
                    Bundle bundle = new Bundle();
                    bundle.putString("searchFlag", "0");
                    bundle.putString("SearchText", "");
                    categoryFragment.setArguments(bundle);
                } else if (groupfragment.isResumed()) {
//                    fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.remove(groupfragment);
//                    fragmentTransaction.remove(categoryFragment);
//                    Fragment newInstance = recreateFragment(categoryFragment);
//                    fragmentTransaction.add(R.id.container2, newInstance);
//                    fragmentTransaction.commit();
//                    Bundle bundle = new Bundle();
//                    bundle.putString("searchFlag", "0");
//                    bundle.putString("SearchText", "");
//                    newInstance.setArguments(bundle);
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container2, cat1frag);
                    fragmentTransaction.commit();
                    Bundle bundle = new Bundle();
                    bundle.putString("searchFlag", "0");
                    bundle.putString("SearchText", "");
                    cat1frag.setArguments(bundle);
                }
            }
        });
    }

    private Fragment recreateFragment(Fragment f) {
        try {
            Fragment.SavedState savedState = fragmentManager.saveFragmentInstanceState(f);

            Fragment newInstance = f.getClass().newInstance();
            newInstance.setInitialSavedState(savedState);

            return newInstance;
        } catch (Exception e) // InstantiationException, IllegalAccessException
        {
            throw new RuntimeException("Cannot reinstantiate fragment " + f.getClass().getName(), e);
        }
    }

//    @Override
//    public void onDrawerItemSelected(View view, int position) {
//
//    }

    private void dropTables() {
        // query to obtain the names of all tables in your database
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        List<String> tables = new ArrayList<>();

// iterate over the result set, adding every table name to a list
        while (c.moveToNext()) {
            tables.add(c.getString(0));
        }

// call DROP TABLE on every table name
        for (String table : tables) {
            if (table.startsWith("sqlite_")) {
                continue;
            }
            String dropQuery = "DROP TABLE IF EXISTS " + table;
            Log.e("dropQuery", ": " + dropQuery);
            db.execSQL(dropQuery);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("Group", "onPause");
        IsActivityGroupOpen = false;
    }


    private void createTables() {
        if (!db.isOpen())
            db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        //Merchant information
        db.execSQL(String.format("CREATE TABLE if not exists masterterminal(id INTEGER PRIMARY KEY AUTOINCREMENT,restaurant_id INTEGER NOT NULL,res_name VARCHAR[50] NOT NULL,phone VARCHAR[25],postcode VARCHAR[10] NOT NULL,website VARCHAR[100] NOT NULL,contact_name VARCHAR[25] NOT NULL,logo VARCHAR[100] NOT NULL,listing_type VARCHAR[20] NOT NULL,uadd_address_01 VARCHAR[100] NOT NULL, uadd_address_02 VARCHAR[100],uadd_city VARCHAR[35],uadd_county VARCHAR[20] NOT NULL,uadd_country VARCHAR[10] NOT NULL,sun_open VARCHAR[15],sun_close  VARCHAR[15],mon_open  VARCHAR[15],mon_close  VARCHAR[15],tues_open  VARCHAR[15], tues_close  VARCHAR[15], wed_open  VARCHAR[15],wed_close  VARCHAR[15], thurs_open  VARCHAR[15], thurs_close VARCHAR[15],fri_open  VARCHAR[15], fri_close VARCHAR[15],sat_open  VARCHAR[15], sat_close VARCHAR[15]);"));
        //Number of tables
        db.execSQL(String.format("CREATE TABLE if not exists tables(id INTEGER NOT NULL,tableno INTEGER NOT NULL,noofchairs INTEGER NOT NULL,occupiedchairs INTEGER NOT NULL,freechairs INTEGER NOT NULL,orderid VARCHAR[15]);"));
        // menu data
        db.execSQL("CREATE TABLE if not exists menugroup(menu_name VARCHAR[50] NOT NULL,menu_id INTEGER NOT NULL,imageurl VARCHAR[25] NOT NULL,merchant_id INTEGER NOT NULL);");
        //menu item data
        db.execSQL("CREATE TABLE if not exists menuitem(menu_item_id INTEGER NOT NULL,restaurant_id INTEGER NOT NULL," +
                "menu_id INTEGER NOT NULL,product_type INTEGER NOT NULL,menu_item_name VARCHAR[50] NOT NULL," +
                "menu_item_desc VARCHAR[1000] NOT NULL,price DECIMAL(9.2) NOT NULL,special_price DECIMAL(9.2) NOT NULL,start_date VARCHAR[25]," +
                "end_date  VARCHAR[25],code INTEGER NOT NULL,is_vegetarian VARCHAR[5] NOT NULL,is_avail_delivery VARCHAR[5] NOT NULL,nuts VARCHAR[5] NOT NULL," +
                "max_toppings VARCHAR[10] NOT NULL,is_meal INTEGER NOT NULL,is_active INTEGER NOT NULL,is_base INTEGER NOT NULL," +
                "show_indiviual INTEGER NOT NULL,include_price INTEGER NOT NULL," +
                "datetime VARCHAR[25] NOT NULL,imgurl VARCHAR[100] NOT NULL);");

        db.execSQL("CREATE TABLE if not exists cart(cart_id INTEGER PRIMARY KEY AUTOINCREMENT,tableno INTEGER NOT NULL,guestno INTEGER NOT NULL,menu_item_id_fk INTEGER NOT NULL," +
                "menu_item_name VARCHAR[50] NOT NULL,menu_item_price DECIMAL(9.2) NOT NULL," +
                "menu_item_qty INTEGER NOT NULL,menu_item_url VARCHAR[100],menu_id INTEGER NOT NULL,product_type INTEGER NOT NULL,include_price INTEGER NOT NULL," +
                "cartdatetime VARCHAR[25] NOT NULL,billflag INTEGER NOT NULL,orderflag INTEGER NOT NULL,checkflag INTEGER NOT NULL,ordertype VARCHAR[25]);");
        //ingredients data of cart
        db.execSQL("CREATE TABLE if not exists itemoptions(option_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "cart_id_fk INTEGER NOT NULL,options_name VARCHAR[50] NOT NULL);");

        //menu item ingredients
        db.execSQL("CREATE TABLE if not exists ingredients(id INTEGER PRIMARY KEY AUTOINCREMENT,ing_id INTEGER NOT NULL,ing_qty INTEGER NOT NULL," +
                "cart_id_fk INTEGER NOT NULL,ingredient_name VARCHAR[50] NOT NULL,ing_price DECIMAL(9.2),include_price INTEGER NOT NULL);");

        //menu item subitems
        db.execSQL("CREATE TABLE if not exists subitems(id INTEGER PRIMARY KEY AUTOINCREMENT,subitem_id INTEGER NOT NULL,subitem_qty INTEGER NOT NULL," +
                "ing_id_fk INTEGER NOT NULL,subitem_name VARCHAR[50] NOT NULL,subitem_price DECIMAL(9.2),include_price INTEGER NOT NULL);");

        //create table for menu item details
        db.execSQL("CREATE TABLE if not exists menuItemDetails(menu_item_id INTEGER NOT NULL,restaurant_id INTEGER NOT NULL," +
                "menu_id INTEGER NOT NULL,menu_item_name VARCHAR[50] NOT NULL," +
                "price DECIMAL(9.2) NOT NULL,special_price DECIMAL(9.2) NOT NULL);");
        //menu attributes
        db.execSQL("CREATE TABLE if not exists attributes(menu_item_id INTEGER NOT NULL,menu_id INTEGER NOT NULL,menu_attributes_id INTEGER NOT NULL," +
                "category_id_fk INTEGER NOT NULL, attribute_set_id_fk INTEGER NOT NULL,attribute_id_fk INTEGER NOT NULL,restaurant_id_fk INTEGER NOT NULL,attribute_id INTEGER NOT NULL, " +
                "attribute_set_id INTEGER NOT NULL,attribute_name VARCHAR[50] NOT NULL,attribute_type VARCHAR[50] NOT NULL, attribute_order INTEGER,is_main INTEGER,max_allowed INTEGER,datetime TEXT);");
        //insert menu item other options
        db.execSQL("CREATE TABLE if not exists options(id INTEGER NOT NULL,item_id_fk INTEGER NOT NULL,res_id_fk INTEGER NOT NULL,main_attribute_id INTEGER NOT NULL," +
                "attribute_option_id INTEGER NOT NULL, attribute_price INTEGER NOT NULL,is_main INTEGER,parent_id INTEGER,configitemid INTEGER NOT NULL, " +
                "category_id_fk INTEGER NOT NULL,attribute_set_id_fk INTEGER NOT NULL,menu_attributes_id_fk INTEGER NOT NULL, option_id_fk INTEGER," +
                "option_id INTEGER NOT NULL,attribute_id_fk INTEGER,option_data TEXT,datetime TEXT);");

        db.execSQL("CREATE TABLE if not exists locationprinter(foodid INTEGER NOT NULL,locationorderid INTEGER NOT NULL," +
                "ipaddress VARCHAR[35] NOT NULL,portno INTEGER NOT NULL, btprintername VARCHAR[150] );");

        db.execSQL("CREATE TABLE if not exists serverprinter(res_id INTEGER NOT NULL,ipaddress VARCHAR[35] NOT NULL,portno INTEGER NOT NULL, btprintername VARCHAR[150] );");
        //priner settings
        db.execSQL("CREATE TABLE if not exists location(locationid INTEGER NOT NULL,locationname VARCHAR[35] NOT NULL," +
                "locationprinterid VARCHAR[35] NOT NULL,permissionslocation INTEGER NOT NULL);");

        //consumer information for delivery
        db.execSQL("CREATE TABLE if not exists consumerinfo(cid Integer PRIMARY KEY AUTOINCREMENT,consumerid VARCHAR[10]," +
                "cname VARCHAR[20] NOT NULL,cmobileno VARCHAR[10] NOT NULL,caddress VARCHAR[35] NOT NULL);");

        db.execSQL(String.format("CREATE TABLE if not exists ordernos(sno INTEGER NOT NULL,tableno INTEGER NOT NULL, orderno VARCHAR[15], billflag INTEGER, orderflag INTEGER);"));

        db.execSQL("CREATE TABLE if not exists receiptnos(receipthead VARCHAR[20] NOT NULL, receiptnumber INTEGER NOT NULL);");
        db.execSQL("CREATE TABLE if not exists receipt(receiptno VARCHAR[20] NOT NULL,docname VARCHAR[35] NOT NULL" +
                ",docid VARCHAR[35] NOT NULL,amount DECIMAL(9.2) NOT NULL,datetime VARCHAR[35] NOT NULL,username VARCHAR[35] NOT NULL," +
                "orderid VARCHAR[35] NOT NULL,checkflag INTEGER NOT NULL);");


        db.execSQL("CREATE TABLE if not exists newOrder(id INTEGER PRIMARY KEY AUTOINCREMENT,tableno INTEGER NOT NULL,guestno INTEGER NOT NULL, order_id VARCHAR[50] NOT NULL," +
                "order_num TEXT,order_total DECIMAL(9.2) NOT NULL,shipping_cost  DECIMAL(9.2) NOT NULL,is_accepted INTEGER, product_type INTEGER NOT NULL," +
                "payment_mode TEXT,delivery_address TEXT,billing_address TEXT,customer_name TEXT,is_paid TEXT," +
                "cartdatetime VARCHAR[25] NOT NULL,billflag INTEGER NOT NULL,orderflag INTEGER NOT NULL,checkflag INTEGER NOT NULL,ordertype VARCHAR[25]);");

        db.execSQL("CREATE TABLE if not exists newOrderMenuitem(cart_id INTEGER PRIMARY KEY AUTOINCREMENT,order_id VARCHAR[50] NOT NULL,menu_item_id_fk INTEGER NOT NULL,restaurant_id INTEGER NOT NULL," +
                "menu_id INTEGER NOT NULL,menu_item_name VARCHAR[50] NOT NULL," +
                "menu_item_price DECIMAL(9.2) NOT NULL,menu_item_qty INTEGER NOT NULL,is_meal TEXT NOT NULL,notes TEXT,is_coupon INTEGER NOT NULL," +
                "subtotal DECIMAL(9.2) NOT NULL,options TEXT NOT NULL,billflag INTEGER NOT NULL,orderflag INTEGER NOT NULL,checkflag INTEGER NOT NULL,ordertype VARCHAR[25]);");

        db.execSQL("CREATE TABLE if not exists allOrders(order_id VARCHAR[50] NOT NULL,order_num VARCHAR[50] NOT NULL,order_zip VARCHAR[50] NOT NULL," +
                "order_total DECIMAL(9.2) NOT NULL,order_date TEXT NOT NULL,order_time TEXT NOT NULL,is_accepted INTEGER NOT NULL," +
                "order_updated_on TEXT NOT NULL,customer_name TEXT NOT NULL);");

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onCreate(savedInstanceState);
    }

    private boolean doublebackpressed = false;
    SessionManager manager;

    @Override
    public void onBackPressed() {
        if (doublebackpressed) {
            super.onBackPressed();
            finish();
        }
        Toast t = Toast.makeText(Group.this, getString(R.string.str_double_backpress_message), Toast.LENGTH_SHORT);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
        doublebackpressed = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doublebackpressed = false;
            }
        }, 2000);
    }

    public void groupfragment() {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container2, groupfragment);
        fragmentTransaction.commit();
    }

    public void alert() {
        cartstaticfragment.alertadapter(GROUPID);
    }

    public void catgoryfragment(String groupId) {
        if (!categoryFragment.isResumed()) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container1, categoryFragment);
            fragmentTransaction.commit();
            Bundle bundle = new Bundle();
            bundle.putString("SearchText", groupId);
            bundle.putString("searchFlag", "1");
            categoryFragment.setArguments(bundle);
        } else {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container1, cat1frag);
            fragmentTransaction.commit();
            Bundle bundle = new Bundle();
            bundle.putString("SearchText", groupId);
            bundle.putString("searchFlag", "1");
            cat1frag.setArguments(bundle);
        }
    }


    public void cartUpdate() {
        String consumer_name = "0";
        String consumer_mobile = "0";
        String consumer_address = "0";

        Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
        String editTable = Group.GROUPID.table;
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);

//        Cursor CustomerQuery = db.rawQuery("SELECT cname,cmobileno,caddress from consumerinfo WHERE cid=" + CONSUMERID, null);
//        int consumerCount = CustomerQuery.getCount();
//        if (consumerCount > 0) {
//            CustomerQuery.moveToFirst();
//
//            consumer_name = CustomerQuery.getString(0);
//            consumer_mobile = CustomerQuery.getString(1);
//            consumer_address = CustomerQuery.getString(2);
//        }
//        CustomerQuery.close();
        Cursor cartCount = db.rawQuery("SELECT * from cart WHERE tableno=" + editTable /*+ " AND menu_item_id_fk=" + Printer.kitchenid*/ + " AND billflag=0;", null);
        int count = cartCount.getCount();
        if (count > 0) {
            db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
            db.execSQL("UPDATE cart SET orderflag = 1  WHERE tableno=" + editTable + "  AND billflag = 0;");

            db.execSQL("UPDATE ordernos SET orderflag = 1 WHERE tableno=" + editTable + "  AND billflag = 0;");
            db.close();

//            Socket_Connection socket = new Socket_Connection();
//            int status = socket.connectionToServer();
//            if (status != 0) {
//                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
//                db.execSQL("INSERT INTO queuetable(message) VALUES('" + MainActivity.getInstance().finnalMesgToSend + "');");
//                MainActivity.getInstance().queueMessage = "0";
//                db.close();
//            }
        }
    }

    public int setCount() {
        if (!db.isOpen())
            db = openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
        Cursor allOrderData = db.rawQuery("SELECT order_id,is_accepted from allOrders WHERE is_accepted=0", null);
        final int count = allOrderData.getCount();
        Log.e("accepted_count", "" + count);

        runOnUiThread(new Runnable() {
            public void run() {
                Log.e("Inside", "=" + count);
                textNewordercount.setText(String.valueOf(count));
            }
        });

        allOrderData.close();
        return count;
    }

    private void fadeOut() {
        objectAnimator = ObjectAnimator.ofFloat(verticalButtonNewOrder, "alpha", 1f, 0f);
        objectAnimator.setDuration(300L);
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                fadeIn();
            }
        });
        objectAnimator.start();
    }

    private void cancelAnimator() {
        if (objectAnimator != null) {
            objectAnimator.cancel();
            newOrderFlag = false;
            objectAnimator = null;
        }

    }

    private void fadeIn() {
        objectAnimator = ObjectAnimator.ofFloat(verticalButtonNewOrder, "alpha", 0f, 1f);
        objectAnimator.setDuration(300L);
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (newOrderFlag)
                    fadeOut();
            }
        });
        objectAnimator.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("Group", "onResume");
        IsActivityGroupOpen = true;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        newOrderFlag = true;
        Log.e("onNewIntent", "==" + intent);
        Fragment fragment = new NewOrderFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_navigation_drawer, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

        if (setCount() > 0) {
            fadeIn();
        }
    }

    @Override
    protected void onDestroy() {
        if (bmThumbnail != null) {
            bmThumbnail.recycle();
        }
        IsActivityGroupOpen = false;
        super.onDestroy();
    }

    public static Group getInstance() {
        return GROUPID;
    }

    private void getMerchantInfo(final String id) {
        if (dialog == null)
            dialog = new ProgressDialog(Group.this);
        dialog.setMessage(getString(R.string.str_loading_restaurant_info));
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(Group.this);
        String requestURL = getString(R.string.web_path) + "restaurants_detail_service/getdetails/?res_id=" + id;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.d("restro_response", result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj1 = new JSONObject(result);
                            if (jObj1.length() > 0) {
                                if (!db.isOpen())
                                    db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                                JSONObject jObj = jObj1.getJSONArray("details").getJSONObject(0);
                                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                                db.execSQL("INSERT INTO masterterminal (restaurant_id,res_name,phone,postcode,website,contact_name,logo," +
                                        "listing_type,uadd_address_01, uadd_address_02,uadd_city,uadd_county,uadd_country,sun_open," +
                                        "sun_close ,mon_open,mon_close,tues_open, tues_close, wed_open,wed_close," +
                                        "thurs_open, thurs_close,fri_open, fri_close,sat_open, sat_close) VALUES(" +
                                        jObj.getString("restaurant_id") + "," + DatabaseUtils.sqlEscapeString(jObj.getString("res_name")) +
                                        ",'" + jObj.getString("phone") +
                                        "','" + jObj.getString("postcode") +
                                        "','" + jObj.getString("website") +
                                        "'," + DatabaseUtils.sqlEscapeString(jObj.getString("contact_name")) +
                                        ",'" + jObj.getString("logo") +
                                        "','" + jObj.getString("listing_type") +
                                        "'," + DatabaseUtils.sqlEscapeString(jObj.getString("uadd_address_01")) +
                                        "," + DatabaseUtils.sqlEscapeString(jObj.getString("uadd_address_02")) +
                                        ",'" + jObj.getString("uadd_city") +
                                        "','" + jObj.getString("uadd_county") +
                                        "','" + jObj.getString("uadd_country") +
                                        "','" + jObj.getString("sun_open") +
                                        "','" + jObj.getString("sun_close") +
                                        "','" + jObj.getString("mon_open") +
                                        "','" + jObj.getString("mon_close") +
                                        "','" + jObj.getString("tues_open") +
                                        "','" + jObj.getString("tues_close") +
                                        "','" + jObj.getString("wed_open") +
                                        "','" + jObj.getString("wed_close") +
                                        "','" + jObj.getString("thurs_open") +
                                        "','" + jObj.getString("thurs_close") +
                                        "','" + jObj.getString("fri_open") +
                                        "','" + jObj.getString("fri_close") +
                                        "','" + jObj.getString("sat_open") +
                                        "','" + jObj.getString("sat_close") +
                                        "');");
                                db.execSQL("INSERT INTO serverprinter (res_id,ipAddress,portno) VALUES('" + jObj.getString("restaurant_id") + "','192.168.1.102',3344);");

                                getProducts(mSharedPreferences.getString("mid", ""));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        } finally {
                            if (db.isOpen())
                                db.close();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }//Login

    private void getProducts(final String id) {
        if (dialog == null)
            dialog = new ProgressDialog(Group.this);
        dialog.setMessage(getString(R.string.str_loading_products));
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(Group.this);
        String requestURL = getString(R.string.web_path) + "menu_service/getdetails/?res_id=" + id;
        Log.d("requestURL", ":" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.d("result", ":" + result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj1 = new JSONObject(result);
                            if (jObj1.length() > 0) {
                                if (!db.isOpen())
                                    db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                                Object menuobject = jObj1.get("menus");
                                if (menuobject instanceof JSONArray) {
                                    JSONArray menujJsonArray = jObj1.getJSONArray("menus");
                                    for (int i = 0; i < menujJsonArray.length(); i++) {
                                        JSONObject jObjMenu = menujJsonArray.getJSONObject(i);
                                        db.execSQL("INSERT INTO menugroup VALUES(" + DatabaseUtils.sqlEscapeString(jObjMenu.getString("name")) +
                                                ",'" + jObjMenu.getString("id") +
                                                "','" + jObjMenu.getString("imgurl") +
                                                "','" + jObjMenu.getString("merchantid") +
                                                "');");
                                        Object menuitemobject = jObjMenu.get("menuitems");
                                        if (menuitemobject instanceof JSONArray) {
                                            JSONArray menuItemJsonArray = jObjMenu.getJSONArray("menuitems");
                                            for (int j = 0; j < menuItemJsonArray.length(); j++) {
                                                JSONObject jObjMenuItem = menuItemJsonArray.getJSONObject(j);
                                                getItemDetails(jObjMenuItem.getString("menu_id"), jObjMenuItem.getString("id"));
                                                db.execSQL("INSERT INTO menuitem VALUES(" +
                                                        jObjMenuItem.getString("id") + "," + jObjMenuItem.getString("restaurant_id") +
                                                        "," + jObjMenuItem.getString("menu_id") +
                                                        "," + jObjMenuItem.getString("product_type") + "," + DatabaseUtils.sqlEscapeString(jObjMenuItem.getString("name")) +
                                                        "," + DatabaseUtils.sqlEscapeString(jObjMenuItem.getString("desc")) + "," + jObjMenuItem.getString("price") +
                                                        "," + jObjMenuItem.getString("special_price") + ",'" + jObjMenuItem.getString("start_date") +
                                                        "','" + jObjMenuItem.getString("end_date") + "'," + jObjMenuItem.getString("code") +
                                                        ",'" + jObjMenuItem.getString("is_vegetarian") + "','" + jObjMenuItem.getString("is_avail_delivery") +
                                                        "','" + jObjMenuItem.getString("nuts") + "','" + jObjMenuItem.getString("max_toppings") +
                                                        "','" + jObjMenuItem.getString("is_meal") + "','" + jObjMenuItem.getString("is_active") +
                                                        "'," + jObjMenuItem.getString("is_base") + "," + jObjMenuItem.getString("show_indiviual") +
                                                        "," + jObjMenuItem.getString("include_price") + ",'" + jObjMenuItem.getString("datetime") +
                                                        "','" + jObjMenuItem.getString("imgurl") +
                                                        "');");
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("getProducts", "exception=" + e.toString());
                            if (dialog.isShowing())
                                dialog.dismiss();

                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        } finally {
                            if (db.isOpen())
                                db.close();
                            getTables(mSharedPreferences.getString("mid", ""));
                        }

                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Log.e("ProductsvolleyError", "=" + volleyError.getMessage());
                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        queue.add(stringRequest);
    }//getProducts

    private void getTables(final String id) {
        if (dialog == null)
            dialog = new ProgressDialog(Group.this);
        dialog.setMessage(getString(R.string.str_loading_tables));
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(Group.this);
        String requestURL = getString(R.string.web_path) + "restaurants_tables_service/tablelist/?id=" + id;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            if (!db.isOpen())
                                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);

                            JSONObject j = new JSONObject(result);
                            JSONArray k = j.getJSONArray("tables");
                            for (int i = 0; i < k.length(); i++) {

                                JSONObject m = k.getJSONObject(i);
                                String mid = m.getString("id");
                                String tableno = m.getString("table_no");
                                String total_seats = m.getString("total_seats");

                                db.execSQL("INSERT INTO tables VALUES(" + mid +
                                        "," + tableno +
                                        "," + total_seats +
                                        "," + 0 +
                                        "," + total_seats +
                                        ",'" + 0 +
                                        "');");
                            }
                            for (int temp = 1500; temp < 1600; temp++) {
                                db.execSQL("INSERT INTO tables VALUES(" + temp +
                                        "," + temp +
                                        "," + 4 +
                                        "," + 1 +
                                        "," + 4 +
                                        ",'" + 0 +
                                        "');");
                            }
                            mSharedPreferences.edit().putInt("tableno", 1500).apply();
                            if (!tablesFragment.isResumed()) {
                                fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.add(R.id.container2, tablesFragment);
                                fragmentTransaction.commit();
                            }
                            if (dialog.isShowing())
                                dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        } finally {
                            if (db.isOpen())
                                db.close();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }//getTables

    private void getItemDetails(final String menuId, final String menuItemsId) {
        final RequestQueue queue = Volley.newRequestQueue(Group.this);
        String requestURL = getString(R.string.web_path) + "menu_item_service/itemDetails/?id=" + menuItemsId;
        Log.d("requestURL", ":" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.d("ItemDetails", ":" + result);
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            JSONObject jObj1 = new JSONObject(result);
                            if (jObj1.length() > 0) {
                                if (!db.isOpen())
                                    db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);

                                Object detailsObject = jObj1.get("details");
                                if (detailsObject instanceof JSONArray) {
//                                    JSONArray detailsJsonArray = jObj1.getJSONArray("details");
                                    JSONArray detailsJsonArray = new JSONArray(String.valueOf(jObj1.getJSONArray("details")));
                                    if (detailsJsonArray.length() > 0) {
                                        for (int i = 0; i < detailsJsonArray.length(); i++) {
                                            JSONObject jObjDetails = detailsJsonArray.getJSONObject(i);
                                            db.execSQL("INSERT INTO menuItemDetails VALUES(" + jObjDetails.getString("id") +
                                                    "," + jObjDetails.getString("restaurant_id") +
                                                    "," + jObjDetails.getString("menu_id") +
                                                    "," + DatabaseUtils.sqlEscapeString(jObjDetails.getString("name")) +
                                                    ",'" + jObjDetails.getString("price") +
                                                    "','" + jObjDetails.getString("special_price") + "');");
                                        }
                                    }
                                }//detailsObject instanceof
                                Object attributesObject = jObj1.get("attributes");
                                if (attributesObject instanceof JSONArray) {
                                    JSONArray attributesJsonArray = jObj1.getJSONArray("attributes");
                                    if (attributesJsonArray.length() > 0) {
                                        for (int i = 0; i < attributesJsonArray.length(); i++) {
                                            JSONObject jObjAttributes = attributesJsonArray.getJSONObject(i);
                                            db.execSQL("INSERT INTO attributes VALUES(" + menuItemsId + "," + menuId +
                                                    "," + jObjAttributes.getString("menu_attributes_id") +
                                                    "," + jObjAttributes.getString("category_id_fk") +
                                                    "," + jObjAttributes.getString("attribute_set_id_fk") +
                                                    "," + jObjAttributes.getString("attribute_id_fk") +
                                                    "," + jObjAttributes.getString("restaurant_id_fk") +
                                                    "," + jObjAttributes.getString("attribute_id") +
                                                    "," + jObjAttributes.getString("attribute_set_id") +
                                                    "," + String.valueOf(DatabaseUtils.sqlEscapeString(jObjAttributes.getString("attribute_name"))) +
                                                    ",'" + jObjAttributes.getString("attribute_type") +
                                                    "'," + jObjAttributes.getString("attribute_order") +
                                                    "," + jObjAttributes.getString("is_main") +
                                                    "," + jObjAttributes.getString("max_allowed") +
                                                    ",'" + jObjAttributes.getString("datetime") + "');");

                                            Object optionsObject = jObjAttributes.get("options");
                                            if (optionsObject instanceof JSONArray) {
                                                JSONArray optionsJsonArray = jObjAttributes.getJSONArray("options");
                                                if (optionsJsonArray.length() > 0) {
                                                    for (int j = 0; j < optionsJsonArray.length(); j++) {
                                                        JSONObject jObjOptions = optionsJsonArray.getJSONObject(j);
                                                        db.execSQL("INSERT INTO options VALUES(" + jObjOptions.getString("id") +
                                                                "," + jObjOptions.getString("item_id_fk") +
                                                                "," + jObjOptions.getString("res_id_fk") +
                                                                "," + jObjOptions.getString("main_attribute_id") +
                                                                "," + jObjOptions.getString("attribute_option_id") +
                                                                "," + jObjOptions.getString("attribute_price") +
                                                                "," + jObjOptions.getString("is_main") +
                                                                ",'" + jObjOptions.getString("parent_id") +
                                                                "'," + jObjOptions.getString("configitemid") +
                                                                "," + jObjOptions.getString("category_id_fk") +
                                                                "," + jObjOptions.getString("attribute_set_id_fk") +
                                                                "," + jObjOptions.getString("menu_attributes_id_fk") +
                                                                "," + jObjOptions.getString("option_id_fk") +
                                                                "," + jObjOptions.getString("option_id") +
                                                                "," + jObjOptions.getString("attribute_id_fk") +
                                                                "," + DatabaseUtils.sqlEscapeString(jObjOptions.getString("option_data"))  +
                                                                ",'" + jObjOptions.getString("datetime") + "');");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }//attributesObject instanceof
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("itemDetails", "exception=" + e.toString());
                            Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        } finally {
                            if (db.isOpen())
                                db.close();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("getItemDetailsvolleyError", "=" + volleyError.getMessage());
                Toast.makeText(getApplicationContext(), getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//getItemDetails


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private class updateSlider extends AsyncTask<String, Void, Integer> {
        int count = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(String... strings) {
            try {
                if (!db.isOpen())
                    db = openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);

                Cursor allOrderData = db.rawQuery("SELECT order_id,is_accepted from allOrders WHERE is_accepted=0", null);
                int count = allOrderData.getCount();

                allOrderData.close();
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            } finally {
                if (!db.isOpen())
                    db.close();
            }
            return count;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            Log.e("grp_update", "=" + integer);
            if (integer > 0) {
                Group.GROUPID.textNewordercount.setText(String.valueOf(integer));
            } else
                Group.GROUPID.textNewordercount.setText(String.valueOf(0));
        }
    }
}


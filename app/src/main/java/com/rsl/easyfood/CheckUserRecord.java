package com.rsl.easyfood;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CheckUserRecord extends Activity {
    TextView date, titlebar;
    //search/info
    TextView txt_info;
    Button btn_check_number;
    EditText edtmobilenumber;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_check_user_record);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        initview();

        edtmobilenumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String str = s.toString();
                if (str.length() > 0 && str.startsWith(" ")) {
                    edtmobilenumber.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
    }

    private void initview() {
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckUserRecord.this, Home.class);
                startActivity(intent);
            }
        });
        btn_check_number = (Button) findViewById(R.id.btn_check_number);
//        btn_check_number.setOnClickListener(btncheckListener);
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("PHONE ORDERS");
        txt_info = (TextView) findViewById(R.id.txt_info);
        txt_info.setText("Enter consumer mobile number");
        findViewById(R.id.lay_info).setVisibility(View.VISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);
        edtmobilenumber = (EditText) findViewById(R.id.edtmobilenumber);
//        edtmobilenumber.setTransformationMethod(new NumericKeyBoardTransformationMethod());
    }

    View.OnClickListener btncheckListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            db = openOrCreateDatabase("easyfood_db.db", Context.MODE_PRIVATE, null);
            String strMobile = edtmobilenumber.getText().toString();
            if (strMobile.equals("") || strMobile.length() <= 0) {
                edtmobilenumber.setError(getResources().getString(R.string.enter_mobile));
                edtmobilenumber.requestFocus();
            } else if (strMobile.length() != 10) {
                edtmobilenumber.setError(getResources().getString(R.string.enter_digits));
                edtmobilenumber.requestFocus();
            } else {
                Cursor consumerinfo = db.rawQuery("SELECT cid,cname,caddress,cmobileno FROM consumerinfo WHERE cmobileno='" + strMobile + "'", null);
                int consumercount = consumerinfo.getCount();
                Intent intent = new Intent(CheckUserRecord.this, GetUserInfo.class);
                if (consumerinfo.moveToFirst() && consumercount > 0) {
                    intent.putExtra("cid", consumerinfo.getString(0));
                    intent.putExtra("cname", consumerinfo.getString(1));
                    intent.putExtra("caddress", consumerinfo.getString(2));
                    intent.putExtra("cmobileno", consumerinfo.getString(3));
                } else {
                    intent.putExtra("cid", "0");
                    intent.putExtra("cname", "");
                    intent.putExtra("caddress", "");
                    intent.putExtra("cmobileno", strMobile);
                }
                consumerinfo.close();
                startActivity(intent);
            }
        }
    };



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

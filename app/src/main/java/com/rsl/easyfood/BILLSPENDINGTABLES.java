/**
 *
 */
package com.rsl.easyfood;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.instabug.library.InstabugTrackingDelegate;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author embdes
 */
@SuppressLint("ShowToast")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class BILLSPENDINGTABLES extends Activity {
    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    public static String[] prgmNameList = {"Continue Order", "Cart Check", "Change Table", "Change Location"};
    SQLiteDatabase db;
    ListView pendingList;
    Group group;
    MyDb myDb;
    TextView date, titlebar;
    ListView pending_menuorder_list;
    Dialog pending_Bill_option;
    Dialog tableChnage_popup, locationChnage_popup;
    PopupTables popupTables;
    String grand;
    public static final int Continue_order = 0;
    public static final int Change_table = 2;
    boolean selected_type = false;
    static BILLSPENDINGTABLES bILLSPENDINGTABLES;
    ArrayList<HashMap<String, String>> records = new ArrayList<>();
    ArrayList<HashMap<String, String>> sorted_records = new ArrayList<>();
    private GridViewAdapter mLeDeviceListAdapter;
    //search/info
    TextView txt_info;
    Spinner mSpinnerType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.pending_bill);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BILLSPENDINGTABLES.this, Home.class);
                startActivity(intent);
            }
        });
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        titlebar.setText("PENDING ORDERS");
        txt_info = (TextView) findViewById(R.id.txt_info);
        mSpinnerType = (Spinner) findViewById(R.id.spinner_type);
        txt_info.setText("View Pending Orders or manage delivery");
        findViewById(R.id.lay_info).setVisibility(View.INVISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);

        group = new Group();
        myDb = new MyDb(getApplicationContext());
        myDb.open();
        popupTables = new PopupTables();
        pendingList = (ListView) findViewById(R.id.pendinglist);
//        pendingList.setDivider(new ColorDrawable(Color.parseColor("#1681c4")));
        pendingList.setDividerHeight(1);
        pending_Bill_option = new Dialog(BILLSPENDINGTABLES.this);
        pending_Bill_option.setContentView(R.layout.pending_bill_option_menu_popup);
        pending_Bill_option.setCancelable(true);
        pending_menuorder_list = (ListView) pending_Bill_option.findViewById(R.id.pending_bill_option_list);
        tableChnage_popup = new Dialog(BILLSPENDINGTABLES.this);
        locationChnage_popup = new Dialog(BILLSPENDINGTABLES.this);
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);

        Cursor pendindDetails = db.rawQuery("SELECT cart_id,tableno,ordertype FROM cart WHERE billflag = 0 GROUP BY tableno;", null);// WHERE billflag=1 AND orderflag=1 GROUP BY tableno
        while (pendindDetails.moveToNext()) {
            Cursor pendindOrderNo = db.rawQuery("SELECT sno,orderno FROM ordernos WHERE tableno=" + pendindDetails.getString(pendindDetails.getColumnIndex("tableno")) + " AND billflag=0 GROUP BY orderno;", null);

            String orderType = pendindDetails.getString(pendindDetails.getColumnIndex("ordertype"));
            String tableno = pendindDetails.getString(pendindDetails.getColumnIndex("tableno"));

            while (pendindOrderNo.moveToNext()) {
                String cart_id = pendindOrderNo.getString(pendindOrderNo.getColumnIndex("sno"));
                String OrderNo = pendindOrderNo.getString(pendindOrderNo.getColumnIndex("orderno"));

                Log.e("cart_id", cart_id);
                if (Integer.parseInt(tableno) == 0) {
                    Cursor allOrdersData = db.rawQuery("SELECT is_accepted FROM allOrders WHERE order_id = '" + OrderNo + "' AND is_accepted=1;", null);// WHERE billflag=1 AND orderflag=1 GROUP BY tableno
                    if (allOrdersData.getCount() > 0) {
                        allOrdersData.moveToFirst();
                        String is_accepted = allOrdersData.getString(allOrdersData.getColumnIndex("is_accepted"));
                        Log.e("is_accepted", is_accepted);
                        HashMap<String, String> data = new HashMap<>();
                        data.put("cart_id", cart_id);
                        data.put("orderno", OrderNo);
                        data.put("ordertype", orderType);
                        data.put("tableid", tableno);
                        data.put("is_accepted", is_accepted);
                        records.add(data);
                        sorted_records.add(data);
                    }
                    allOrdersData.close();
                } else {
                    HashMap<String, String> data = new HashMap<>();
                    data.put("cart_id", "0");
                    data.put("orderno", OrderNo);
                    data.put("ordertype", orderType);
                    data.put("tableid", tableno);
                    data.put("is_accepted", "0");
                    records.add(data);
                    sorted_records.add(data);
                }
            }
            pendindOrderNo.close();
        }
        pendindDetails.close();
        db.close();
        mLeDeviceListAdapter = new GridViewAdapter(this);
        pendingList.setAdapter(mLeDeviceListAdapter);
        mLeDeviceListAdapter.notifyDataSetChanged();
        String types[] = {"All Types", "Counter Service", "Phone Order", "Dine In", "Web"};
        //Application of the Array to the Spinner
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(BILLSPENDINGTABLES.this, R.layout.pending_filter_spinner, types) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                //  v.setBackgroundResource(R.drawable.spinner_bg);

                ((TextView) v).setTextColor(
                        getResources().getColorStateList(R.color.black)
                );
                if (position == 0) {
                    // Set the hint text color gray
                    ((TextView) v).setTextColor(Color.GRAY);
                } else {
                    ((TextView) v).setTextColor(Color.BLACK);
                }
                return v;
            }

        };
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        mSpinnerType.setAdapter(spinnerArrayAdapter);

        mSpinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    selected_type = true;
                    // Toast.makeText(getApplicationContext(), "" + parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();

                    //  if(parent.getItemAtPosition(position).toString().equals("Dine In")){
                    //     if(Integer.parseInt(records.get(position).get("tableid"))>6){

                    filter(parent.getItemAtPosition(position).toString());
                    //  }
                    //   }
                } else if (position == 0) {
                    filter("");
                    selected_type = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    protected void onResume() {
        super.onResume();
//        if (MainActivity.getInstance() == null || UserLoginActivity.user_LoginID == null) {
//            Intent userLoginIntent = new Intent();
//            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
//            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(userLoginIntent);
//            this.finish();
//        }
    }

    public void filter(String searchText) {
        records.clear();
        if (searchText.length() > 0) {
            for (int i = 0; i < sorted_records.size(); i++) {
                HashMap<String, String> temp = new HashMap<String, String>();
                temp = sorted_records.get(i);
                if (temp.get("ordertype").toLowerCase().equals(searchText.toLowerCase())) {
                    records.add(temp);
                } else {
                    Toast.makeText(getApplicationContext(), "Record not found", Toast.LENGTH_SHORT);
                }


            }

        } else {
            records.addAll(sorted_records);
        }

        mLeDeviceListAdapter.notifyDataSetChanged();


    }

    public class GridViewAdapter extends BaseAdapter {

        // Declare variables
        private Activity activity;

        @SuppressWarnings("unused")
        private LayoutInflater inflater = null;

        public GridViewAdapter(Activity a) {
            activity = a;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        private void remove(int pos) {
            records.remove(pos);
            notifyDataSetChanged();
        }

        public int getCount() {

            return records.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            /**
             * getting the thumbnail for image and putting in image view
             *
             */
            View vi = convertView;
            ViewHolder viewh = null;
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                LayoutInflater inflater = (LayoutInflater) BILLSPENDINGTABLES.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.pendinglistforbill, parent, false);
                viewh = new ViewHolder();
                viewh.Table_no = (TextView) vi.findViewById(R.id.table_no);
                viewh.Order_no = (TextView) vi.findViewById(R.id.order_no);
                viewh.add_more = (Button) vi.findViewById(R.id.btnaddmore);
                viewh.Order_type = (TextView) vi.findViewById(R.id.order_type);

                viewh.printer = (Button) vi.findViewById(R.id.printer);
                viewh.payment = (Button) vi.findViewById(R.id.paybill);
                viewh.printer.setOnClickListener(printerConnect);
                viewh.payment.setOnClickListener(paymentConnect);
                viewh.Order_no.setOnLongClickListener(longPop);
                vi.setTag(viewh);
            } else {
                System.out.println("else" + position);
                viewh = (ViewHolder) vi.getTag();
            }
            if (Integer.parseInt(records.get(position).get("tableid")) > 1500 && Integer.parseInt(records.get(position).get("tableid")) < 1600) {
                viewh.add_more.setVisibility(View.VISIBLE);
                viewh.add_more.setOnClickListener(addToCart);
            } else
                viewh.add_more.setVisibility(View.INVISIBLE);
            //viewh.amount.setText();
//			viewh.Loc_id.setText(records.get(position).get("locid"));
            if (Integer.parseInt(records.get(position).get("tableid")) == 0) {
                viewh.Table_no.setText("-");

                viewh.payment.setText(getResources().getString(R.string.str_complete));
                viewh.payment.setOnClickListener(completeorderHandler);

            } else {
                viewh.payment.setText(getResources().getString(R.string.str_pay_bill));
                viewh.payment.setOnClickListener(paymentConnect);
                viewh.Table_no.setText(records.get(position).get("tableid"));
            }

            viewh.Order_no.setText(records.get(position).get("orderno"));
            viewh.Order_type.setText(records.get(position).get("ordertype"));

            return vi;

        }
    }

    private View.OnClickListener addToCart = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            final int position = pendingList.getPositionForView(v);
            if (records.get(position).get("tableid").length() > 0) {
                TablesFragment.COVERS = "1";
                Group.GROUPID.table = records.get(position).get("tableid");
                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);
                int count = c.getCount();
                c.moveToFirst();
                if (count > 0) {
                    int covers = c.getInt(c.getColumnIndex("noofchairs"));
                    if (covers >= Integer.parseInt(TablesFragment.COVERS)) {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
//                        if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) > 1) {
//                            editor.putInt("tableno", quickTable).apply();
//                        }
                        Home.variable = true;
                        CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                        CartStaticSragment.CARTStaticSragment.tableno.setText("#####");
                        CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                        CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                        CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                        CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                        Intent intent = new Intent(BILLSPENDINGTABLES.this, Group.class);
                        startActivity(intent);
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                        msg.setTitle("");
                        msg.setMessage("Covers Should Not Exceed " + covers);
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                } else {
                    AlertDialog.Builder msg = new AlertDialog.Builder(
                            BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Table is Not Available");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                }
            }
        }
    };

    private View.OnClickListener popupWindow = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            final int position = pendingList.getPositionForView(v);
            if (UserLoginActivity.user_LoginID.LOCATIONNO.equalsIgnoreCase(records.get(position).get("locid"))) {
                if (records.get(position).get("tableid").length() > 0) {
                    System.out.println("New Table :-" + records.get(position).get("tableid"));
                    pending_Bill_option.setTitle("Location:" + records.get(position).get("locid") + ", Table NO:" + records.get(position).get("tableid"));
                    pending_Bill_option.show();
                    pending_menuorder_list.setOnItemClickListener(new OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1,
                                                int arg2, long arg3) {
                            TablesFragment.TABLESID = TablesFragment.getInstance();
                            TablesFragment.COVERS = "1";
                            if (arg2 == Continue_order) {
                                if (UserLoginActivity.user_LoginID.userpermissionmenu >= UserLoginActivity.user_LoginID.TOrder) {
                                    if (records.get(position).get("tableid").length() != 0 &&
                                            Integer.parseInt(records.get(position).get("tableid")) > 0) {
                                        Group.GROUPID.table = records.get(position).get("tableid");
                                        CartStaticSragment.CARTStaticSragment.tableno.setText(Group.GROUPID.table);
                                        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                                        Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" +
                                                Group.GROUPID.table + " AND locationid =" + UserLoginActivity.user_LoginID.LOCATIONNO + ";", null);
                                        while (chairs.moveToNext()) {
                                            if (chairs.getString(0).length() != 0) {
                                                TablesFragment.COVERS = chairs.getString(0).toString();
                                            } else {
                                                TablesFragment.COVERS = "111";
                                            }
                                        }
                                        int chairsCount = chairs.getCount();
                                        chairs.moveToFirst();
                                        if (chairsCount > 0) {
                                            if (chairs.getInt(chairs.getColumnIndex("occupiedchairs")) < 1) {
                                                if (chairs.getString(0).length() != 0 &&
                                                        Integer.parseInt(chairs.getString(0).toString()) > 0) {
                                                    Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table +
                                                            " AND locationid =" + UserLoginActivity.user_LoginID.LOCATIONNO + ";", null);
                                                    int count = c.getCount();
                                                    c.moveToFirst();
                                                    if (count > 0) {
                                                        int covers = c.getInt(c.getColumnIndex("noofchairs"));
                                                        if (covers >= Integer.parseInt(TablesFragment.COVERS)) {
                                                            Intent intent = new Intent(BILLSPENDINGTABLES.this, Group.class);
                                                            startActivity(intent);
                                                            group.groupfragment();
                                                            group.alert();
                                                        } else {
                                                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                                                    BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                                                            msg.setTitle("");
                                                            msg.setMessage("Covers Should Not Exceed " + covers);
                                                            msg.setPositiveButton("Ok", null);
                                                            msg.show();
                                                        }

                                                    } else {
                                                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                                                BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                                                        msg.setTitle("");
                                                        msg.setMessage("Table is Not Available");
                                                        msg.setPositiveButton("Ok", null);
                                                        msg.show();
                                                    }
                                                }
                                            } else {
                                                Intent intent = new Intent(BILLSPENDINGTABLES.this, Group.class);
                                                startActivity(intent);
                                            }
                                        } else {
                                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                                    BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                                            msg.setTitle("");
                                            msg.setMessage("Table Is Not Available");
                                            msg.setPositiveButton("Ok", null);
                                            msg.show();

                                        }
                                        db.close();
                                    } else {
                                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                                BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                                        msg.setTitle("");
                                        msg.setMessage("Table Number Is Not Valid");
                                        msg.setPositiveButton("Ok", null);
                                        msg.show();
                                    }

                                } else {
                                    AlertDialog.Builder msg = new AlertDialog.Builder(
                                            BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                                    msg.setTitle("Opration Failed");
                                    msg.setMessage("User Doesn't have the permission for this opration");
                                    msg.setPositiveButton("Ok", null);
                                    msg.show();
                                }
                            } else if (arg2 == Change_table) {
                                PopupTables.popupTables.flag = true;
                                Group.GROUPID.table = records.get(position).get("tableid");
                                popupTables.show(getFragmentManager(), null);
                                CartStaticSragment.CARTStaticSragment.listView.setAdapter((BaseExpandableListAdapter) null);
                                CartStaticSragment.CARTStaticSragment.totalamountvalue.setText("");
                                CartStaticSragment.CARTStaticSragment.totaltax.setText("");
                                CartStaticSragment.CARTStaticSragment.itemno.setText("");
                            }
                            if (pending_Bill_option.isShowing()) {
                                pending_Bill_option.dismiss();
                            }

                        }
                    });
                }
            } else {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                msg.setTitle("");
                msg.setMessage("Location Does not match to Print " + "\n Current Loc:" + UserLoginActivity.user_LoginID.LOCATIONNO + "\n Table Loc:" + records.get(position).get("locid"));
                msg.setPositiveButton("Ok", null);
                msg.show();
            }

        }
    };
    private View.OnLongClickListener longPop = new View.OnLongClickListener() {

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public boolean onLongClick(View v) {
            final int position = pendingList.getPositionForView(v);
            Group.GROUPID.table = records.get(position).get("tableid");
            AlertDialog.Builder msg = new AlertDialog.Builder(
                    BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
            msg.setTitle("Do you want to close the table ?");
            msg.setMessage("It will delete all data belongs to table" + "\n Table :" + records.get(position).get("tableid"));

            msg.setPositiveButton("Yes", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
//                                    myDb = new MyDb(getApplicationContext());
//                                    myDb.open();
//                                    myDb.Insert(String.valueOf(MainActivity.MAINID.outletid), Group.GROUPID.table, "0", "NULL", "NULL");
//                                    Intent intent = new Intent(BILLSPENDINGTABLES.this, ServiceOccupiedChairs.class);
//                                    startService(intent);
                            db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                            db.execSQL("UPDATE tables SET occupiedchairs = 0 WHERE tableno = " + Group.GROUPID.table + ";");

                            db.execSQL("DELETE FROM ordernos WHERE tableno = " + Group.GROUPID.table + ";");// AND billflag = 0
                            db.execSQL("DELETE FROM cart WHERE tableno = " + Group.GROUPID.table + ";");// AND billflag = 0
                            db.close();
                            mLeDeviceListAdapter.remove(position);
//                            BILLSPENDINGTABLES.this.finish();
//                            Intent refresh = new Intent(BILLSPENDINGTABLES.this, BILLSPENDINGTABLES.class);
//                            startActivity(refresh);
                        }
                    });
            msg.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            myDb.close();
            msg.show();


            return false;
        }


    };

    private String editTable;
    float grandTotal = 0;


    private View.OnClickListener completeorderHandler = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            AlertDialog.Builder msg = new AlertDialog.Builder(
                    BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
            msg.setTitle("");
            msg.setMessage("Are you sure, you want to complete this order");
            msg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final int position = pendingList.getPositionForView(v);
                    competeOrder(position);
                    dialog.dismiss();
                }
            });
            msg.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            msg.show();

        }
    };
    private View.OnClickListener paymentConnect = new View.OnClickListener() {

        @SuppressLint("ShowToast")
        @Override
        public void onClick(View v) {
            final int position = pendingList.getPositionForView(v);

            if (records.get(position).get("tableid").length() > 0) {
                if (Integer.parseInt(records.get(position).get("tableid")) > 1500 && Integer.parseInt(records.get(position).get("tableid")) < 1600) {
                    TablesFragment.COVERS = "1";
                    Group.GROUPID.table = records.get(position).get("tableid");
                    db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                    Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);
                    int count = c.getCount();
                    c.moveToFirst();
                    if (count > 0) {
                        int covers = c.getInt(c.getColumnIndex("noofchairs"));
                        if (covers >= Integer.parseInt(TablesFragment.COVERS)) {

                            Home.variable = true;
                            CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away");
                            CartStaticSragment.CARTStaticSragment.tableno.setText("#####");
                            CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
                            CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
                            CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
                            CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
                            Intent intent = new Intent(BILLSPENDINGTABLES.this, Group.class);
                            intent.putExtra("print_bill", true);
                            startActivity(intent);
                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                            msg.setTitle("");
                            msg.setMessage("Covers Should Not Exceed " + covers);
                            msg.setPositiveButton("Ok", null);
                            msg.show();
                        }
                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                        msg.setTitle("");
                        msg.setMessage("Table is Not Available");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                } else {

                    TablesFragment.COVERS = "1";
                    Group.GROUPID.table = records.get(position).get("tableid");
                    db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                    Cursor c = db.rawQuery("SELECT noofchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);
                    int count = c.getCount();
                    c.moveToFirst();

                    Cursor chairs = db.rawQuery("SELECT occupiedchairs FROM tables WHERE tableno =" + Group.GROUPID.table + ";", null);
                    int cou = chairs.getCount();
                    chairs.moveToFirst();
                    if (cou > 0) {
                        String guests = chairs.getString(chairs.getColumnIndex("occupiedchairs"));
                        TablesFragment.COVERS = guests;
                    }
                    if (count > 0) {
                        Home.variable = true;
                        CartStaticSragment.CARTStaticSragment.tabletext.setText("Table");
                        CartStaticSragment.CARTStaticSragment.tableno.setText(Group.GROUPID.table);
                        CartStaticSragment.CARTStaticSragment.noofguests.setText(TablesFragment.COVERS);
                        Intent intent = new Intent(BILLSPENDINGTABLES.this, Group.class);
                        intent.putExtra("print_bill", true);
                        startActivity(intent);

                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                        msg.setTitle("");
                        msg.setMessage("Table is Not Available");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                }
            }

           /* final int position = pendingList.getPositionForView(v);
            if (records.get(position).get("tableid").length() > 0) {
                Group.GROUPID.table = records.get(position).get("tableid");
                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                String query = "SELECT * from cart WHERE tableno = " + records.get(position).get("tableid") + " AND billflag=0;";
                Log.e("query", "=" + query);
                Cursor cartData = db.rawQuery(query, null);
                int count = cartData.getCount();
                db.close();
                if (count > 0) {
                    Intent intent = new Intent(BILLSPENDINGTABLES.this, Printer.class);
                    intent.putExtra("TABLENO", Integer.parseInt(records.get(position).get("tableid")));
                    intent.putExtra("LOCATIONNO", 0);
                    intent.putExtra("FLAG", 1);
                    startActivity(intent);
                } else {
                    AlertDialog.Builder msg = new AlertDialog.Builder(BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Table Is Empty To Print ");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                }
            }*/

        }

    };

    private View.OnClickListener printerConnect = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            final int position = pendingList.getPositionForView(v);
            if (records.get(position).get("tableid").length() > 0) {
                Group.GROUPID.table = records.get(position).get("tableid");
                String ORDERID = records.get(position).get("orderno");
                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                String query = "SELECT * from cart WHERE tableno = " + records.get(position).get("tableid") + " AND billflag=0;";
                Log.e("query", "=" + query);
                Cursor cartData = db.rawQuery(query, null);
                int count = cartData.getCount();
                db.close();
                if (count > 0) {
                    Intent intent = new Intent(BILLSPENDINGTABLES.this, Printer.class);
                    intent.putExtra("TABLENO", Integer.parseInt(records.get(position).get("tableid")));
                    intent.putExtra("LOCATIONNO", 0);
                    intent.putExtra("ORDERID", ORDERID);
                    intent.putExtra("FLAG", 0);
                    startActivity(intent);
                } else {
                    AlertDialog.Builder msg = new AlertDialog.Builder(BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
                    msg.setTitle("");
                    msg.setMessage("Table Is Empty To Print ");
                    msg.setPositiveButton("Ok", null);
                    msg.show();
                }
            }
//            } else {
//                AlertDialog.Builder msg = new AlertDialog.Builder(
//                        BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
//                msg.setTitle("Opration Failed ");
//                msg.setMessage("User Doesn't have the permission for this opration");
//                msg.setPositiveButton("Ok", null);
//                msg.show();
//            }


        }
    };

    private class addRowsToDisplay extends AsyncTask<String, Void, Boolean> {
        String editLocation = UserLoginActivity.user_LoginID.LOCATIONNO;


        @Override
        protected Boolean doInBackground(String... params) {
            editTable = params[0];
            Log.e("editTable", "=" + editTable);
            db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
            db.execSQL("UPDATE cart SET  billqty = orderqty-billedqty, billtypeqty = ordertypeqty-billedtypeqty WHERE tableno = " +
                    editTable + " AND locationid = " +
                    UserLoginActivity.user_LoginID.LOCATIONNO + ";");

            Cursor c = db.rawQuery("SELECT * from cart WHERE locationid=" + UserLoginActivity.user_LoginID.LOCATIONNO + " AND tableno=" + editTable + " AND checkflag = 0 " +
                    " AND billflag = 0;", null);
            int count = c.getCount();
            c.moveToFirst();
            grandTotal = 0;
            for (Integer j = 0; j < count; j++) {
                int billtypeqty = c.getInt(c.getColumnIndex("ordertypeqty")) - c.getInt(c.getColumnIndex("billedtypeqty"));
                float total = (Float.parseFloat(c.getString(c.getColumnIndex("productprice"))) * Integer.parseInt(c.getString(c.getColumnIndex("billqty"))) +
                        Float.parseFloat(c.getString(c.getColumnIndex("typeprice"))) * billtypeqty);
                grandTotal = grandTotal + total;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                String cQuery = "SELECT productname,billflag,orderno,billtypeqty,checkflag,tbillflag,billtypeqty,billqty FROM cart WHERE tableno = " + editTable + " AND locationid= " + editLocation + ";";
                Cursor cart = db.rawQuery(cQuery, null);
                while (cart.moveToNext()) {
                    String data = cart.getString(0) + " , "//productname
                            + cart.getString(1) + " , "//billflag
                            + cart.getString(2) + " , "//orderno
                            + cart.getString(3) + " , "//billtypeqty
                            + cart.getString(4) + " , "//checkflag
                            + cart.getString(5) + " , "//tbillflag
                            + cart.getString(6) + " , "//billtypeqty
                            + cart.getString(7);//billqty
                    Log.e("data", "=" + data);
                }

                String query = "SELECT productname,productprice,productqty,billqty,discount,billedqty,typename,typeqty,typeprice," +
                        "orderno,billtypeqty FROM cart WHERE tableno = " + editTable + " AND locationid= " + editLocation + " AND billflag= 0 AND checkflag=0 AND " +
                        "tbillflag=0 AND (billtypeqty != 0 OR billqty != 0) ;";
                Log.e("query", "=" + query);
                Cursor productDetails = db.rawQuery(query, null);
                int count = productDetails.getCount();
                db.close();
                if (count > 0) {
                    Group.getInstance().totalAmount = grandTotal;
                    Group.getInstance().subTotalAmount = grandTotal;
                    cashpay();
                } else {
//                    AlertDialog.Builder adb = new AlertDialog.Builder(
//                            BILLSPENDINGTABLES.this, android.R.style.Theme_Dialog);
//                    adb.setTitle("");
//                    adb.setMessage("Products Not Found To Pay");
//                    adb.setCancelable(false);
//                    adb.setPositiveButton("Ok", new
//                            DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
                    db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                    db.execSQL("UPDATE cart SET checkflag = 0 WHERE locationid=" + editLocation + " AND tableno=" +
                            editTable + " AND checkflag = 1 AND billflag = 0;");
                    db.close();
                    Intent intent = new Intent(BILLSPENDINGTABLES.this, Home.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
//                                }
//                            });
//                    adb.show();
                }
            }
        }
    }

    static class ViewHolder {
        // TextView Loc_id;
        TextView Table_no;
        TextView Order_no;
        Button add_more;
        TextView Order_type;
        Button printer;
        Button payment;
        //TextView amount;
    }

    public void cashpay() {
        float cash = 0;
        {
            if (Group.GROUPID.finaltotal == null) {
                pay();
                cash = Float.parseFloat(Group.GROUPID.finaltotal);
            } else {
                pay();
                cash = Float.parseFloat(Group.GROUPID.finaltotal);
            }
            if (cash > 0) {
                db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
                String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                String datetime = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
                Cursor recieptno = db.rawQuery("SELECT * FROM receiptnos WHERE receipthead = '" + datetime + "' ;", null);
                if (recieptno.getCount() == 0) {
                    if (cash > 0) {
                        db.execSQL("INSERT INTO receiptnos VALUES('" + datetime + "',1);");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('R" + datetime + "1','CASH', '0'," + cash + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }

                    if (!Group.getInstance().voucherNumber.equals("0")) {
                        db.execSQL("UPDATE voucher SET usedflag = 1 WHERE voucherid = " + Group.getInstance().voucherNumber + ";");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('" + Group.getInstance().voucherNumber + "','VOUCHER', '" + Group.getInstance().voucherNumber + "'," +
                                Group.getInstance().voucherAmount + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }
                } else {
                    if (cash > 0) {
                        recieptno.moveToLast();
                        String newReceiptNo = "R" + recieptno.getString(0) + (recieptno.getInt(1) + (int) 1);
                        db.execSQL("UPDATE receiptnos SET receiptnumber = receiptnumber+1 WHERE receipthead= '" +
                                recieptno.getString(0) + "';");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('" + newReceiptNo + "','CASH', '0'," + cash + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");

                    }
                    if (!Group.getInstance().voucherNumber.equals("0")) {
                        db.execSQL("UPDATE voucher SET usedflag = 1 WHERE voucherid = " + Group.getInstance().voucherNumber + ";");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('" + Group.getInstance().voucherNumber + "','VOUCHER', '" + Group.getInstance().voucherNumber + "'," +
                                Group.getInstance().voucherAmount + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }
                }

                db.execSQL("UPDATE cart SET discount = " + Group.getInstance().discountAmount +
                        " WHERE locationid=" + UserLoginActivity.user_LoginID.LOCATIONNO + " AND tableno=" + editTable + " AND billflag = 0 AND orderflag = 1;");
                db.close();
                Intent intent = new Intent(BILLSPENDINGTABLES.this, Printer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("TABLENO", Integer.parseInt(editTable));
                intent.putExtra("LOCATIONNO", Integer.parseInt(UserLoginActivity.user_LoginID.LOCATIONNO));
                intent.putExtra("FLAG", 1);
                startActivity(intent);
            }
        }
    }

    public void pay() {
        double grandTotal = 0;
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid=" + UserLoginActivity.user_LoginID.LOCATIONNO + " AND tableno=" + Group.GROUPID.table +
                " AND billflag = 0;", null);
        int count = cartData.getCount();
        if (count > 0) {
            cartData.moveToLast();
            do {
                String productqty = cartData.getString(cartData.getColumnIndex("productqty"));
                String productprice = cartData.getString(cartData.getColumnIndex("productprice"));
                String typeprice = cartData.getString(cartData.getColumnIndex("typeprice"));
                String typeqty = cartData.getString(cartData.getColumnIndex("typeqty"));
                Double totalvalue = Double.valueOf((Float.parseFloat(productprice) *
                        Integer.parseInt(productqty))) + Double.valueOf(typeprice) * Double.valueOf(typeqty);
                grandTotal = grandTotal + totalvalue;
            } while (cartData.moveToPrevious());
        }
        db.close();
        db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
        Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);
        int taxDetailsCount = taxDetails.getCount();
        double tax1 = 0;
        if (taxDetailsCount > 0) {
            taxDetails.moveToFirst();            //Here we are inserting the tax information into the structure
            tax1 = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));

        } else {
        }
        db.close();
        grand = String.format("%.2f", grandTotal);
        double taxAmount = (tax1 * grandTotal) / 100;
        String grand_tax = String.format("%.2f", taxAmount);
        grandTotal += (tax1 * grandTotal) / 100;
        String grand_Amount = String.format("%.2f", grandTotal);
        Group.GROUPID.finaltotal = grand_Amount;
    }

    private void competeOrder(final int position) {
        final String cart_id = records.get(position).get("cart_id");
        final String order_id = records.get(position).get("orderno");
        final ProgressDialog dialog = new ProgressDialog(BILLSPENDINGTABLES.this);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.setCancelable(false);
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(BILLSPENDINGTABLES.this);
        String requestURL = getString(R.string.web_path) + "order_service/orderStatus/?orderId=111&status=Completed&orderId=" + order_id;
        Log.d("requestURL", "orderUpdateStatus:" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String result) {
                Log.d("orderUpdateStatus", "res:" + result);
                if (result != null) {
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.length() > 0) {
                            if (jObj.getString("type_id").equals("1")) {
                                if (updatePayInfo(cart_id, order_id)) {
                                    mLeDeviceListAdapter.remove(position);
                                }
                                Toast.makeText(BILLSPENDINGTABLES.this, jObj.getString("msg"), Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(BILLSPENDINGTABLES.this, jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        }
                        if (dialog.isShowing())
                            dialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(BILLSPENDINGTABLES.this, getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                        if (dialog.isShowing())
                            dialog.dismiss();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(BILLSPENDINGTABLES.this, getString(R.string.str_json_exception), Toast.LENGTH_SHORT).show();
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }//CompleteOrder

    public Boolean updatePayInfo(String cart_id, String orderNo) {
        try {
            db = openOrCreateDatabase("easyfood_db.db", MODE_PRIVATE, null);
            db.execSQL("UPDATE cart SET  billflag= 1 WHERE cart_id = " + cart_id + ";");

            db.execSQL("UPDATE receipt SET checkflag = 1 where orderid ='" + orderNo + "' AND checkflag = 0;");

            Cursor cartData = db.rawQuery("SELECT * from cart WHERE cart_id = " + cart_id + " AND orderflag = 1 AND billflag = 0 AND checkflag = 1;", null);
            int cartCount = cartData.getCount();
            if (cartCount < 1) {
                Cursor qtyData = db.rawQuery("SELECT * from cart WHERE cart_id = " + cart_id + " AND orderflag = 1 AND billflag = 0;", null);

                if (qtyData.getCount() == 0) {
                    Toast.makeText(BILLSPENDINGTABLES.this, "Order Successfully Closed ", Toast.LENGTH_SHORT)
                            .show();
                    db.execSQL("UPDATE ordernos SET  billflag= 1 WHERE orderno = " + orderNo + ";");
                } else {
                    qtyData.moveToFirst();
                }
                qtyData.close();
            } else {
                db.execSQL("UPDATE cart SET checkflag = 0 WHERE cart_id = " + cart_id + " AND billflag = 0 AND checkflag = 1;");
            }
            cartData.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static BILLSPENDINGTABLES getInstance() {
        return bILLSPENDINGTABLES;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

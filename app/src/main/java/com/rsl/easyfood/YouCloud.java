package com.rsl.easyfood;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Vinay on 19/08/2017.
 */

public class YouCloud extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AvenirHeavy.ttf")
               .setFontAttrId(R.attr.fontPath)
                .build()
        );
        new Instabug.Builder(this, "ff6eaa736a580cb18510f1c96c5c7ca8")
                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
                .build();
    }
}

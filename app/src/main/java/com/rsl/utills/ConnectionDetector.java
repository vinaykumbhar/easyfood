package com.rsl.utills;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionDetector {

    private Context _context;
    private static boolean is_showing = false;

    public ConnectionDetector(Context context) {
        this._context = context;
    }

    public boolean isConnectingToInternet() {

        ConnectivityManager cm = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return true;
            }
        } else {
            // not connected to the internet
        }
        return false;
    }

    public void showAlertDialog(Context context) {
        android.app.AlertDialog.Builder adb = new android.app.AlertDialog.Builder(context, android.R.style.Theme_Dialog);
        adb.setTitle("");
        adb.setMessage("No Internet connection");
        adb.setPositiveButton("Ok", null);
        adb.show();


    }
}

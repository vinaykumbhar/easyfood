package com.rsl.utills;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;
import com.onesignal.OneSignal;
import com.rsl.easyfood.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Vinay on 19/08/2017.
 */

public class MyApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/RobotoMedium.ttf")
               .setFontAttrId(R.attr.fontPath)
                .build()
        );
//        new Instabug.Builder(this, "ff6eaa736a580cb18510f1c96c5c7ca8")
//                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
//                .build();
        new Instabug.Builder(this, "3f6c1ee3dee0fa1afadf5270e9d4d86c")
                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
                .build();

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        // Call syncHashedEmail anywhere in your app if you have the user's email.
        // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        // OneSignal.syncHashedEmail(userEmail);
    }
}

package com.rsl.utills;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by vinay on 23-02-2016.
 */

public class TextViewRobotoRegular extends TextView {

    public TextViewRobotoRegular(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public TextViewRobotoRegular(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public TextViewRobotoRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/RobotoRegular.ttf", context);
        setTypeface(customFont);
    }
}


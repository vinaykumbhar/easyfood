package com.rsl;

import android.util.Log;

import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;
import com.rsl.utills.SessionManager;

/**
 * Created by Admin on 29/01/2018.
 */

public class NotificationExtenderExample extends NotificationExtenderService {

    public static SessionManager session;
    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
        // Read properties from result.
        session = new SessionManager(getApplicationContext());
        // Returning true tells the OneSignal SDK you have processed the notification and not to display it's own.
        if (session.isLoggedIn()) {
            Log.e("NotificationExtender","false");
            return false;

        }else {
            Log.e("NotificationExtender","true");
            return true;
        }

    }
}
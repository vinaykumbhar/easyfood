package com.rsl.model;

import java.util.ArrayList;

/**
 * Created by Administrator on 27/12/2017.
 */

public class OptionsData {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<OptionsData> getListOptions() {
        return listOptions;
    }

    public void setListOptions(ArrayList<OptionsData> listOptions) {
        this.listOptions = listOptions;
    }

    public String getSubitemid() {
        return subitemid;
    }

    public void setSubitemid(String subitemid) {
        this.subitemid = subitemid;
    }

    private String subitemid;

    public String getMenuidid() {
        return menuidid;
    }

    public void setMenuidid(String menuidid) {
        this.menuidid = menuidid;
    }

    private String menuidid;
    private String id;
    private String text;
    private String type;
    private double price;
    private int productType;

    public int getInclude_price() {
        return include_price;
    }

    public void setInclude_price(int include_price) {
        this.include_price = include_price;
    }

    private int include_price;

    public int getGroupPos() {
        return groupPos;
    }

    public void setGroupPos(int groupPos) {
        this.groupPos = groupPos;
    }

    private int groupPos;
    private ArrayList<OptionsData> listOptions;

}

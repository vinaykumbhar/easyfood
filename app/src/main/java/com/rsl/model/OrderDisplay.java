package com.rsl.model;

import java.util.ArrayList;

/**
 * Created by admin on 1/15/2018.
 */

public class OrderDisplay {

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_total() {
        return order_total;
    }

    public void setOrder_total(String order_total) {
        this.order_total = order_total;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getShipping_cost() {
        return shipping_cost;
    }

    public void setShipping_cost(String shipping_cost) {
        this.shipping_cost = shipping_cost;
    }

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public ArrayList<OrderDisplay> getCart_items() {
        return cart_items;
    }

    public void setCart_items(ArrayList<OrderDisplay> cart_items) {
        this.cart_items = cart_items;
    }

    private String order_id;
    private String order_total;
    private String order_time;
    private String shipping_cost;
    private String delivery_address;

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    private String customer_name;



    private String delivery_charges;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    private String name;
    private String qty;
    private String price;

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getIs_coupon() {
        return is_coupon;
    }

    public void setIs_coupon(String is_coupon) {
        this.is_coupon = is_coupon;
    }

    private String customername;
    private String options;
    private String subtotal;
    private String is_coupon;
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getOpt_qty() {
        return opt_qty;
    }

    public void setOpt_qty(String opt_qty) {
        this.opt_qty = opt_qty;
    }

    public String getOpt_price() {
        return opt_price;
    }

    public void setOpt_price(String opt_price) {
        this.opt_price = opt_price;
    }

    private String opt_qty;
    private String opt_price;


    public String getIs_subvalue() {
        return is_subvalue;
    }

    public void setIs_subvalue(String is_subvalue) {
        this.is_subvalue = is_subvalue;
    }

    private String is_subvalue;

    private ArrayList<OrderDisplay> cart_items;

    public ArrayList<OrderDisplay> getSub_items() {
        return sub_items;
    }

    public void setSub_items(ArrayList<OrderDisplay> sub_items) {
        this.sub_items = sub_items;
    }

    private ArrayList<OrderDisplay> sub_items;

}

package com.rsl.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 27/12/2017.
 */

public class MealOptions {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<MealOptions> getListOptions() {
        return listOptions;
    }

    public void setListOptions(ArrayList<MealOptions> listOptions) {
        this.listOptions = listOptions;
    }

    private String id;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private String code;

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    private String menuid;
    private String text;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;

    public String getSubItemId() {
        return subItemId;
    }

    public void setSubItemId(String subItemId) {
        this.subItemId = subItemId;
    }

    private String subItemId;

    public int getGroupPosition() {
        return groupPosition;
    }

    public void setGroupPosition(int groupPosition) {
        this.groupPosition = groupPosition;
    }

    private int groupPosition;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    private int quantity;
    private String type;
    private double price;
    private String productType;
    private String isSimple;

    public int getInclude_price() {
        return include_price;
    }

    public void setInclude_price(int include_price) {
        this.include_price = include_price;
    }

    private int include_price;
    private ArrayList<MealOptions> listOptions;
    private OptionsData optionsData;


    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getIsSimple() {
        return isSimple;
    }

    public void setIsSimple(String isSimple) {
        this.isSimple = isSimple;
    }

    public OptionsData getOptionsData() {
        return optionsData;
    }

    public void setOptionsData(OptionsData optionsData) {
        this.optionsData = optionsData;
    }

    public HashMap<String, HashMap<String, String>> getMealfinallist() {
        return mealfinallist;
    }

    public void setMealfinallist(HashMap<String, HashMap<String, String>> mealfinallist) {
        this.mealfinallist = mealfinallist;
    }

    HashMap<String, HashMap<String, String>> mealfinallist;
}

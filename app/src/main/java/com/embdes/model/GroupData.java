package com.embdes.model;

import java.util.ArrayList;

/**
 * Created by Develpomenteight on 19/09/2017.
 */

public class GroupData {
    String title="";
    String name;
    float value;

    public ArrayList<GroupData> getChilddata() {
        return Childdata;
    }

    public void setChilddata(ArrayList<GroupData> childdata) {
        Childdata = childdata;
    }

    ArrayList<GroupData> Childdata=new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}

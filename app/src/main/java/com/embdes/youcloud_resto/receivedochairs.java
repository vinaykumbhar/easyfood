package com.embdes.youcloud_resto;

/**
 * Created by EmbDes on 11-07-2017.
 */

public class receivedochairs {
    String mid,location,tableno,ochairs,orderid;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTableno() {
        return tableno;
    }

    public void setTableno(String tableno) {
        this.tableno = tableno;
    }

    public String getOchairs() {
        return ochairs;
    }

    public void setOchairs(String ochairs) {
        this.ochairs = ochairs;
    }
}

package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;

/**
 * @author EMBDES.
 * @version 1.0.
 *          This Activity doing all the reports printing operations.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Reports extends Activity {

    //// ���뷽ʽ
    SQLiteDatabase db;
    BluetoothDevice device;
    String btPrinterid;
    /**
     * printing text align left
     */
    public static final int AT_LEFT = 0;
    /**
     * printing text align center
     */
    public static final int AT_CENTER = 1;
    /**
     * printing text align right
     */
    public static final int AT_RIGHT = 2;
    boolean bConnect = true;
    String fdot1;

    class reportValueDetails {
        String header;
        String name;
        float value;

        public reportValueDetails(String newHeader, String newName, float newValue) {
            header = newHeader;
            name = newName;
            value = newValue;
        }
    }

    ;

    class itemDetails {
        String name;
        int qty;
        float price;

        public itemDetails(String newName, int newQty, float newPrice) {
            name = newName;
            price = newPrice;
            qty = newQty;
        }

    }

    ;

    class headerDetails {
        String title;
        String firstDate;
        String lastDate;
    }

    BluetoothAdapter mBluetoothAdapter;
    private static final int REQUEST_ENABLE_BT = 2;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    InputStream mmInStream;
    static OutputStream mmOutStream;
    BluetoothSocket mmSocket;
    public static Socket mysocket = null;
    private static int SERVERPORT = 9100;
    private static String SERVER_IP = "192.168.1.87";
    Dialog settingDialog;
    ListView lv;
    TextView titlebar, date;
    String printdata;

    ArrayAdapter<BluetoothDevice> adapter;
    ArrayList<BluetoothDevice> connections = new ArrayList<BluetoothDevice>();
    ArrayAdapter<String> adaptername;
    ArrayList<String> connectionsname = new ArrayList<String>();
    String globledevice;
    int mReportsFlag;
    String mstartDate, mendDate;
    boolean printerDeviceCheck = false;
    private static Reports REPORTSID;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.reports_display);
        REPORTSID = this;
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        date = (TextView) findViewById(R.id.date);
        titlebar = (TextView) findViewById(R.id.titelabar);
        Button menu = (Button) findViewById(R.id.Imageview);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Reports.this, Home.class);
                startActivity(intent);
            }
        });
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);

        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String startDate = bundle.getString("FROMDATE");
        String endDate = bundle.getString("TODATE");
        int ReportsFlag = bundle.getInt("REPORTSFLAG");

        if (ReportsFlag == 4) {
            titlebar.setText("FLASH SALE REPORTS");
        } else if (ReportsFlag == 5) {
            titlebar.setText("PRODUCT REPORTS");
        } else
            titlebar.setText("Main Reports");
        mstartDate = startDate;
        mendDate = endDate;
        mReportsFlag = ReportsFlag;
        fdot1 = String.format("-------------------------------\n");
        if (true == MainActivity.getInstance().isWifiPrinterEnable) {

            String strName = "192.168.1.92:5500";

            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor printerDetails = db.rawQuery("SELECT ipaddress, portno FROM " +
                    "locationprinter" + ";", null);/*WHERE locationorderid = "+user_Login.user_LoginID.LOCATIONNO
*/
            int printerDetailsCount = printerDetails.getCount();
            if (printerDetailsCount > 0) {
                printerDetails.moveToFirst();
                strName = printerDetails.getString(0) + ":" + printerDetails.getString(1);
                SERVER_IP = printerDetails.getString(0);
                SERVERPORT = Integer.parseInt(printerDetails.getString(1));
            }
            db.close();
            if (strName.length() == 0) {
                Toast.makeText(Reports.this, "Error:port name empty", Toast.LENGTH_SHORT).show();
                return;
            }

            Toast.makeText(Reports.this, strName, Toast.LENGTH_SHORT).show();

            if (bConnect) {
                try {
                    if (mysocket == null) {
                        new ClientThread().execute("ok");
                    }
                } finally {
                    if (ReportsFlag == 4) {
                        groupReports(startDate, endDate);
                        Pcut();
                    } else if (ReportsFlag == 5) {
                        setTitle("				PRODUCT REPORTS		");
                        productReports(startDate, endDate);
                    }
                    bConnect = false;
                }
            } else {
                bConnect = true;
            }
        } else {

            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                // Device does not support Bluetooth
            }
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
            final SharedPreferences mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

            String restoredText = mSharedPreference.getString("btPrinterid", null);
            if (restoredText != null) {
                btPrinterid = mSharedPreference.getString("btPrinterid", null);
                device = mBluetoothAdapter
                        .getRemoteDevice(btPrinterid);
                if (ReportsFlag == 4) {
                    if (false == MainActivity.getInstance().isWifiPrinterEnable) {
                        deviceconnection();
                    } else {
                        groupReports(startDate, endDate);
                        Pcut();
                    }
                } else if (ReportsFlag == 5) {
                    setTitle("				PRODUCT REPORTS		");
                    productReports(startDate, endDate);
                }

            } else {
                ListPairedDevices();
            }
        }
        final ImageView home = (ImageView) findViewById(R.id.imageView11);
        final ImageView opentabs = (ImageView) findViewById(R.id.imageView3);
        final ImageView printmenu = (ImageView) findViewById(R.id.imageView2);
        printmenu.setVisibility(View.INVISIBLE);
        opentabs.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            public void onClick(View v) {
                onBackPressed();
                Intent intent = new Intent(Reports.this, BILLSPENDINGTABLES.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

        home.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("static-access")
            public void onClick(View v) {

                finish();
                Intent intent = new Intent(Reports.this, Home.class);
                intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });
    }

    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null || user_Login.user_LoginID == null) {
            Intent userLoginIntent = new Intent(); //Created new Intent to
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(userLoginIntent);
            this.finish();
        }

    }

    ;

    public class ClientThread extends AsyncTask<String, Void, Boolean> {

		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isconnected = false;
        ProgressDialog pDialog;

        public void onPreExecute() {
            isconnected = false;
            pDialog = new ProgressDialog(Reports.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            isconnected = false;
            pDialog.show();
        }

        public Boolean doInBackground(final String... args) {
            try {
                mysocket = new Socket();
                mysocket.connect(new InetSocketAddress(SERVER_IP, SERVERPORT), 10000);
                mmOutStream = mysocket.getOutputStream();
                mmInStream = mysocket.getInputStream();
                isconnected = true;
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
                isconnected = false;
            } catch (IOException e1) {
                e1.printStackTrace();
                isconnected = false;
            }

            return false;
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }
            if (isconnected == true) {

            } else {
                if (mysocket != null) {
                    try {
                        mysocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mysocket = null;
                }
                Toast.makeText(getApplicationContext(), "Selected WIFi Printer is not online", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (settingDialog != null) {
            if (settingDialog.isShowing()) {
                settingDialog.dismiss();
            }
            settingDialog = null;

        }
        if (mysocket != null) {
            try {
                mysocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mysocket = null;
        }
    }

    /**
     * @return void
     * @brief Reports:: grouptReports here we are passing staring data and ending date to
     * get the reports according to the group between that dates.
     */

    public void groupReports(String astartDate, String aendDate) {
        headerDetails header = new headerDetails();
        reportValueDetails[] valueDetails = new reportValueDetails[1000];
        String startDate = reverseDateFormat(astartDate);
        String endDate = reverseDateFormat(aendDate);
        int valueCount = 0;
        float totalValue = 0;
        String newHeader = "EmbDes";
        String newName;
        Float newValue;
        SQLiteDatabase db;
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT terminalname FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {
            shopDetails.moveToFirst();
            header.title = shopDetails.getString(shopDetails.getColumnIndex("terminalname"));//copying shop name into the struct
            header.firstDate = startDate;//copying Starting date into the strut
            header.lastDate = endDate;    //copying Ending date into the struct
        } else {
            db.close();
            Toast.makeText(Reports.this, "No Data in master_terminal", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Reports.this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return;
        }

        Cursor productDetails = db.rawQuery("SELECT pgroup, productqty, productprice FROM cart where datetime >= '" +
                startDate + "' AND datetime<= '" + endDate + "';", null);
        int productCount = productDetails.getCount();
        if (productCount > 0) {
            productDetails.moveToPrevious();
            while (productDetails.moveToNext()) {
                newName = productDetails.getString(productDetails.getColumnIndex("pgroup"));//copying all group data into struct
                newValue = productDetails.getInt(productDetails.getColumnIndex("productqty")) *
                        productDetails.getFloat(productDetails.getColumnIndex("productprice"));
                totalValue += newValue;
                valueDetails[valueCount] = new reportValueDetails(newHeader, newName, newValue);
                valueCount++;

            }
        } else {
            db.close();
            Toast.makeText(Reports.this, "No Group Data In Cart", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Reports.this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return;
        }

        db.close();
        printFormat(header, valueDetails, valueCount, totalValue, 1);

        categoryReports(startDate, endDate);
        userNameReports(startDate, endDate);
        receiptReports(startDate, endDate);
        return;
    }

    /**
     * @param startDate
     * @param endDate
     * @return void
     * @brief Reports:: categeroryReports here we are passing staring data and ending date to
     * get the reports according to the categerory beteween that dates.
     */

    public void categoryReports(String startDate, String endDate) {
        reportValueDetails valueDetails[] = new reportValueDetails[200];
        headerDetails header = new headerDetails();
        int valueCount = 0;
        float totalValue = 0;
        SQLiteDatabase db;

        String newHeader = "EmbDes";
        String newName;
        Float newValue;
        header.title = "Category:";
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor productDetails = db.rawQuery("SELECT pcategory, productqty, productprice FROM cart where datetime >= '" +
                startDate + "' AND datetime <= '" + endDate + "';", null);
        int productCount = productDetails.getCount();
        if (productCount > 0) {
            productDetails.moveToPrevious();
            while (productDetails.moveToNext()) {

                newName = productDetails.getString(0);//Copying all Details into struct
                newValue = productDetails.getInt(1) * productDetails.getFloat(2);
                totalValue += newValue;
                valueDetails[valueCount] = new reportValueDetails(newHeader, newName, newValue);
                valueCount++;
            }
        } else {
            db.close();
            Toast.makeText(Reports.this, "No  data available between given dates", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Reports.this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        db.close();
        printFormat(header, valueDetails, valueCount, totalValue, 2);
        return;

    }

    /**
     * @param startDate
     * @param endDate
     * @return void
     * @brief Reports:: categeroryReports here we are passing staring data and ending date to
     * get the reports according to the categerory beteween that dates.
     */

    public void userNameReports(String startDate, String endDate) {
        reportValueDetails valueDetails[] = new reportValueDetails[200];
        int valueCount = 0;
        float totalValue = 0;
        boolean validDataFlag = false;
        headerDetails header = new headerDetails();

        String newHeader = "EmbDes";
        String newName;
        Float newValue;

        SQLiteDatabase db;
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor userDetails = db.rawQuery("SELECT username FROM userterminal;", null);
        userDetails.moveToPrevious();
        while (userDetails.moveToNext()) {
            header.title = userDetails.getString(0);
            Cursor productDetails = db.rawQuery("SELECT pgroup, productqty, productprice FROM cart where datetime >= '" +
                    startDate + "' AND datetime <= '" + endDate + "' AND username='" + userDetails.getString(0) + "';", null);
            int productCount = productDetails.getCount();
            if (productCount > 0) {
                productDetails.moveToPrevious();
                while (productDetails.moveToNext()) {
                    newName = productDetails.getString(0);
                    newValue = productDetails.getInt(1) * productDetails.getFloat(2);
                    totalValue += newValue;
                    valueDetails[valueCount] = new reportValueDetails(newHeader, newName, newValue);
                    valueCount++;
                    validDataFlag = true;
                }
                if (validDataFlag) {
                    printFormat(header, valueDetails, valueCount, totalValue, 3);
                    totalValue = 0;
                    valueCount = 0;
                    validDataFlag = false;
                }
            }
        }
        db.close();
    }


    /**
     * @param startDate
     * @param endDate
     * @return void
     * @brief receiptReports here we are passing staring data and ending date to
     * get the reports according to the receipts beteween that dates.
     */

    public void receiptReports(String startDate, String endDate) {

        itemDetails receiptDetails[] = new itemDetails[200];
        headerDetails header = new headerDetails();
        int receiptCount = 0;
        String newName;
        float newPrice;
        int newQty;

        SQLiteDatabase db;

        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor productDetails = db.rawQuery("SELECT docname,amount FROM receipt where datetime >= '" + startDate + "' AND datetime <= '" + endDate + "';", null);
        int productCount = productDetails.getCount();
        if (productCount > 0) {
            productDetails.moveToPrevious();
            while (productDetails.moveToNext()) {

                newName = productDetails.getString(0);
                newPrice = productDetails.getFloat(1);
                newQty = 1;
                receiptDetails[receiptCount] = new itemDetails(newName, newQty, newPrice);
                receiptCount++;
            }
        } else {
            if (true == MainActivity.getInstance().isWifiPrinterEnable) {
                if (mysocket != null) {
                    try {
                        mysocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mysocket = null;
                }
            } else {
                try {
                    mmSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Toast.makeText(Reports.this, "No Receipts between given dates", Toast.LENGTH_SHORT).show();

        }

        header.title = "Cash:";
        db.close();
        productPrintFormat(header, receiptDetails, receiptCount, 4);
        return;
    }

    /**
     * @param startDate
     * @param endDate
     * @return void
     * @brief Reports::productReports here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */
    public void productReports(String startDate, String endDate) {
        itemDetails productDetails[] = new itemDetails[1000];

        int productCount = 0;
        headerDetails header = new headerDetails();
        endDate = reverseDateFormat(endDate);
        startDate = reverseDateFormat(startDate);

        String newName;
        int newQty;
        Float newPrice;

        SQLiteDatabase db;
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT terminalname FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {
            shopDetails.moveToFirst();
            header.title = shopDetails.getString(0);//copying shop name into the struct
            header.firstDate = startDate;//copying Starting date into the struct
            header.lastDate = endDate;    //copying Ending date into the struct
        } else {
            Toast.makeText(Reports.this, "No Data in master_terminal", Toast.LENGTH_SHORT).show();
            return;
        }

        Cursor product = db.rawQuery("SELECT productname, productqty, productprice  FROM cart where datetime >= '" +
                startDate + "' AND datetime<= '" + endDate + "';", null);
        int Count = product.getCount();
        if (Count > 0) {
            product.moveToPrevious();
            while (product.moveToNext()) {
                newName = product.getString(0);
                newQty = product.getInt(1);
                newPrice = product.getFloat(2);
                productDetails[productCount] = new itemDetails(newName, newQty, newPrice);
                productCount++;
            }
        } else {
            db.close();
            Toast.makeText(Reports.this, "No Products available between that dates", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Reports.this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return;
        }
        db.close();
        productPrintFormat(header, productDetails, productCount, 5);
    }


    /**
     * @param header
     * @param totalSum
     * @param FormatFlag
     * @return void
     * @brief Reports::printFormat here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */

    public void printFormat(headerDetails header, reportValueDetails printableData[], int printableCount, float totalSum, int FormatFlag) {
        String tempBuffer = null;
        float sumOfValue = 0;
        float sumOfPercent = 0;
        String cut;
        for (int i = 1; i < printableCount; i++) {
            for (int j = 0; j < i; j++) {
                if (printableData[i].name != null && printableData[j].name != null) {

                    if (printableData[i].name.equals(printableData[j].name))    //Here we are adding the quantity of the same products
                    {
                        printableData[j].value = printableData[j].value + printableData[i].value;
                        printableData[i].value = 0;

                    }
                }
            }
        }

        if (FormatFlag == 1) {
            tempBuffer = String.format("%s\n", header.title);

            tempBuffer += String.format("Full Report\n");

            tempBuffer += String.format("From: %s\nTo: %s\n", header.firstDate.substring(8, 10) +
                    "-" + header.firstDate.substring(5, 7) + "-" + header.firstDate.substring(0, 4) +
                    header.firstDate.substring(10, 16), header.lastDate.substring(8, 10) +
                    "-" + header.lastDate.substring(5, 7) + "-" + header.lastDate.substring(0, 4) +
                    header.lastDate.substring(10, 16));
            //Copying Start & End dates into Buffer

            tempBuffer += String.format("Groups          Value   Percent\n");


        } else if (FormatFlag == 2) {
            tempBuffer = String.format("%-30s\n", header.title);
            tempBuffer += String.format("Categorie       Value  Percent\n");
        } else if (FormatFlag == 3) {
            tempBuffer = String.format("User:%-20s     \n", header.title);
            tempBuffer += String.format("Group          Value   Percent\n");
        }
        int k = 0;
        for (k = 0; k < printableCount; k++) {
            if (printableData[k].value != 0) {
                if (printableData[k].name.length() > 13) {
                    cut = printableData[k].name.substring(0, 13);
                } else {
                    cut = printableData[k].name;
                }

                tempBuffer += String.format("%-13s%10.2f%7.2f\n", cut, printableData[k].value,
                        (printableData[k].value * 100) / totalSum);
                sumOfValue = sumOfValue + printableData[k].value;
                sumOfPercent = sumOfPercent + ((printableData[k].value * 100) / totalSum);

            }
        }
        if (k > 0) {
            tempBuffer += String.format("%s Total %17.2f%7.2f\n%s", fdot1, sumOfValue, sumOfPercent, fdot1);
            TextView groupReport = (TextView) findViewById(R.id.groupProductReports);
            TextView categoryReport = (TextView) findViewById(R.id.categoryReport);
            TextView userReport = (TextView) findViewById(R.id.userReport);

            if (1 == FormatFlag) {
                groupReport.setText(tempBuffer);
            } else if (2 == FormatFlag) {
                categoryReport.setVisibility(View.VISIBLE);
                categoryReport.setText(tempBuffer);
            } else if (3 == FormatFlag) {
                userReport.setVisibility(View.VISIBLE);
                userReport.append(tempBuffer);
            }

            if (true == MainActivity.getInstance().isWifiPrinterEnable) {
                Begin();
                write((tempBuffer).getBytes());
                write(("\r\n").getBytes());
            } else {
                printdata = tempBuffer;
                Begin();
                write((tempBuffer + "\n").getBytes());
            }
        }
        return;
    }


    /**
     * @param header
     * @param printableData
     * @param printableCount
     * @param FormatFlag
     * @return void
     * @brief Reports::productPrintFormat here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */

    public void productPrintFormat(headerDetails header, itemDetails printableData[], int printableCount, int FormatFlag) {
        String tempBuffer = null;
        int sumOfQty = 0;
        float sumOfAmount = 0;
        String cut = null;

        for (int i = 1; i < printableCount; i++)                //Checking for Same Name Items
        {
            for (int j = 0; j < i; j++) {
                if (printableData[i].name != null && printableData[j].name != null)     //Checking for NULL values
                {
                    if (printableData[i].name.equals(printableData[j].name))//Checking for Items with same Name
                    {
                        printableData[j].qty = printableData[j].qty + printableData[i].qty;//Adding qty of same products
                        printableData[j].price = printableData[j].price + printableData[i].price;//Adding price of same products
                        printableData[i].qty = 0;        //making qty of new Item as 0 to avoid at printing time
                        printableData[i].price = 0;        //making qty of new Item as 0 to avoid at printing time

                    }
                }
            }
        }

        if (FormatFlag == 4) {
            tempBuffer = String.format("%-30s\n", header.title);        //Copying title to disply on top of the print into Buffer

            tempBuffer += String.format("Details       Receipts   Value \n"); //Copying column headings to Buffer

        } else if (FormatFlag == 5) {
            tempBuffer = String.format("%s\n", header.title);            //Copying shop name into Buffer


            tempBuffer += String.format("Product Report\n");            //Copying subtitle into Buffer


            tempBuffer += String.format("From: %s\nTo: %s\n", header.firstDate.substring(8, 10) +
                            "-" + header.firstDate.substring(5, 7) + "-" + header.firstDate.substring(0, 4) +
                            header.firstDate.substring(10, 16),
                    header.lastDate.substring(8, 10) + "-" + header.lastDate.substring(5, 7) +
                            "-" + header.lastDate.substring(0, 4) + header.lastDate.substring(10, 16));
            //Copying Start & End dates into Buffer


            tempBuffer += String.format("Products Sold    Total   Value \n");    //Copying column headings into Buffer


        }
        int k;
        for (k = 0; k < printableCount; k++) {
            if (printableData[k].qty != 0)                    //Checing for Items with qty=0 to avoid in print
            {

                //Decreasing the size of the product name to 14 charecters
                if (printableData[k].name.length() > 14) {
                    cut = printableData[k].name.substring(0, 14);
                } else {
                    cut = printableData[k].name;
                }
                tempBuffer += String.format("%-14s%5d%12.2f\n", cut, printableData[k].qty, printableData[k].price);
                sumOfQty = sumOfQty + printableData[k].qty;        //Adding all Items qty to dispalay at the bottom of the Print
                sumOfAmount = sumOfAmount + printableData[k].price;    //Adding all Items price to dispalay at the bottom of the Print
            }
        }
        if (k > 0) {
            tempBuffer += String.format("%s Total:       %6d%12.2f\n%s", fdot1, sumOfQty, sumOfAmount, fdot1);
            //Copying footer information into Buffer

            if (4 == FormatFlag) {
                TextView receiptReport = (TextView) findViewById(R.id.receiptReport);
                receiptReport.setVisibility(View.VISIBLE);
                receiptReport.setText(tempBuffer);
            } else if (5 == FormatFlag) {
                TextView report = (TextView) findViewById(R.id.groupProductReports);
                report.setTypeface(Typeface.MONOSPACE);
                report.setText(tempBuffer);
            }


            if (true == MainActivity.getInstance().isWifiPrinterEnable) {
                Begin();
                write((tempBuffer).getBytes());
                write(("\r\n").getBytes());
            } else if (5 == FormatFlag) {
                printdata = tempBuffer;
                deviceconnection();
            } else {
                printdata = tempBuffer;
                Begin();
                write((tempBuffer + "\n").getBytes());
                try {
                    mmSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Intent intent = new Intent(Reports.this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        return;
    }

    /**
     * @return void
     * @brief reverseDateFormat here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */

    public String reverseDateFormat(String date) {
        //return date;
        return date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2) + date.substring(10, 16);
    }


    /**
     * Here we are creating instance for class
     *
     * @return
     */

    public static Reports getInstance() {
        return REPORTSID;
    }

    @SuppressLint("NewApi")
    public synchronized void connect(BluetoothDevice device) throws IOException {

        BluetoothSocket tmp = null;
        // Get a BluetoothSocket to connect with the given BluetoothDevice
        try {
            Method m;
            m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
            tmp = (BluetoothSocket) m.invoke(device, 1);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(Reports.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(Reports.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(Reports.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(Reports.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        }
        mmSocket = tmp;
        mBluetoothAdapter.cancelDiscovery();
        new Connection().execute("ok");
    }

    public class Connection extends AsyncTask<String, Void, Boolean> {

		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isconnected = false;
        ProgressDialog pDialog;

        public void onPreExecute() {
            pDialog = new ProgressDialog(Reports.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            isconnected = false;
            pDialog.show();
        }

        public Boolean doInBackground(final String... args) {
            try {
                mmSocket.connect();
                mmInStream = mmSocket.getInputStream();
                mmOutStream = mmSocket.getOutputStream();
                isconnected = true;
            } catch (IOException e) {
                e.printStackTrace();
                isconnected = false;
            }

            return false;
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }
            if (mReportsFlag == 4) {
                groupReports(mstartDate, mendDate);
                Pcut();
            }
            if (isconnected == true) {
                if (mReportsFlag == 4) {
                } else {
                    Begin();
                    write((printdata + "\n").getBytes());
                    Pcut();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Selected Printer is not Online", Toast.LENGTH_LONG).show();

            }
            try {
                mmSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void ListPairedDevices() {

        Set<BluetoothDevice> bondedDevices = BluetoothAdapter
                .getDefaultAdapter().getBondedDevices();
        for (int k = 0; k < bondedDevices.size(); k++) {
        }
        if (bondedDevices.size() > 0) {
            for (BluetoothDevice bondDeice : bondedDevices) {
                connections.add(bondDeice);
                connectionsname.add(bondDeice.getName());
                printerDeviceCheck = true;

            }
        }
        if (printerDeviceCheck == true) {
            printerDeviceCheck = false;
            Device_Select();
        } else {
            Toast.makeText(getApplicationContext(), "Printer not found", Toast.LENGTH_SHORT).show();
        }

    }

    private void Device_Select() {
        settingDialog = new Dialog(Reports.this);
        settingDialog.setContentView(R.layout.fragment_scanner_device_selection);
        settingDialog.setTitle("Paired Device");
        settingDialog.setCancelable(true);
        lv = (ListView) settingDialog.findViewById(R.id.list);
        settingDialog.show();
        adapter = new ArrayAdapter<BluetoothDevice>(this, android.R.layout.simple_list_item_1, connections);
        adaptername = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, connectionsname);
        lv.setAdapter(adaptername);


        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                try {
                    globledevice = adapter.getItem(arg2).getAddress();
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("btPrinterid", globledevice);
                    editor.commit();
                    device = mBluetoothAdapter
                            .getRemoteDevice(globledevice);
                    if (mReportsFlag == 4) {
                        if (false == MainActivity.getInstance().isWifiPrinterEnable) {
                            deviceconnection();

                        } else {
                            groupReports(mstartDate, mendDate);
                            Pcut();
                        }
                    } else if (mReportsFlag == 5) {
                        setTitle("				PRODUCT REPORTS		");
                        productReports(mstartDate, mendDate);
                    }
//					
                } catch (RuntimeException e) {
                    Intent mIntent = new Intent();
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.setClass(getApplicationContext(), Home.class);
                    startActivity(mIntent);
                    Toast.makeText(Reports.this, " insert run time exception" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                settingDialog.dismiss();

            }
        });
    }

    private void deviceconnection() {

        try {
            connect(device);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void Begin() {
        WakeUpPritner();
        InitPrinter();
    }

    public static void InitPrinter() {
        byte[] combyte = new byte[]{(byte) 27, (byte) 64};

        // BT_Write(combyte);
        try {
            if (mmOutStream != null) {
                mmOutStream.write(combyte);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void WakeUpPritner() {
        byte[] b = new byte[3];

        try {
            if (mmOutStream != null) {
                mmOutStream.write(b);
            }
            Thread.sleep(100L);
        } catch (Exception var2) {
            var2.printStackTrace();
        }
    }

    public static void Pcut() {
        byte[] pcut = new byte[]{(byte) 0x1D, (byte) 0x56, (byte) 0x42, (byte) 0x00};

        // BT_Write(combyte);
        try {
            if (mmOutStream != null) {
                mmOutStream.write(pcut);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* Call this from the main activity to send data to the remote device */
    public void write(byte[] bytes) {
        try {
            byte[] cAlign = {(byte) 0x1b, (byte) 0x61, (byte) 0x01};
            if (mmOutStream != null) {
                mmOutStream.write(cAlign);
                mmOutStream.write(bytes);
            }
        } catch (IOException e) {
            Toast.makeText(this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }
}

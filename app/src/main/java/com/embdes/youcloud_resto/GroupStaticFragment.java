package com.embdes.youcloud_resto;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.lucasr.twowayview.TwoWayView;

import java.io.File;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 * From This fragment it will go to Groupactivity and catgory fragment is displayed
 */
public class GroupStaticFragment extends Fragment {
    SQLiteDatabase db;
    TextView group;
    ArrayList<Imagepojo> imageview;
    String groupId;
    String groupName;
    Bitmap mBitmapgroup;
    static boolean group_clicked = false;
    Group group1;
    static GroupStaticFragment GROUPID;
    File file;
    File[] allFiles;
    TwoWayView adapter;
    Myadapter myadapter;

    private static class ViewHolder {
        TextView itemName;
        ImageView itemImage;
    }

    public class Myadapter extends BaseAdapter {

        // Declare variables
        private Activity activity;
        private LayoutInflater layoutInflater = null;

        public Myadapter(Activity a) {
            this.activity = a;
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return imageview.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            ViewHolder holder = null;
            if (vi == null) {
                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.grid_viewitem1, parent, false);
                holder = new ViewHolder();
                holder.itemName = (TextView) vi.findViewById(R.id.text);
                holder.itemImage = (ImageView) vi.findViewById(R.id.image);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }
            holder.itemName.setText(imageview.get(position).getPaymnet());
            File imgFile = new File(Environment.getExternalStorageDirectory().getPath() + "/pos/img/" + imageview.get(position).getImageur());
            if (imgFile.exists()) {
                mBitmapgroup = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.itemImage.setImageBitmap(mBitmapgroup);

            } else {
                holder.itemImage.setImageResource(R.drawable.you_cloud);
            }
//            if (position == 0 && !group_clicked) {
//                db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
//                groupName = String.valueOf(imageview.get(position).getPaymnet());
//                Cursor groupData = db.rawQuery("SELECT groupid from grouptable " +
//                        "WHERE groupname ='" + String.valueOf(imageview.get(position).getPaymnet()) + "';", null);
//                groupData.moveToFirst();
//                if (groupData.getCount() > 0) {
//                    groupId = groupData.getString(groupData.getColumnIndex("groupid"));
//                }
//                group1.catgoryfragment(groupId);
//                db.close();
//            }

            return vi;
        }
    }

    public GroupStaticFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group_static, container, false);
        group = (TextView) view.findViewById(R.id.group);
        imageview = new ArrayList<Imagepojo>();
        adapter = (TwoWayView) view.findViewById(R.id.gridview3);
        adapter.setSelector(getResources().getDrawable(R.drawable.item_selector));
        adapter.setItemMargin(10);
        GROUPID = this;
        group_clicked = false;
        group1 = (Group) getActivity();
        groupName = "group";
        Group.GROUPID.titlebar.setText("ORDERS");
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_LONG).show();
        } else {
            file = new File(Environment.getExternalStorageDirectory()+ File.separator + "/pos/img");
            allFiles = file.listFiles();
            if (allFiles.length == 0) {
                Toast.makeText(getActivity(), "FOLDER IS EMPTY", Toast.LENGTH_LONG).show();
            }
        }
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * from grouptable ;", null);
        int count = c.getCount();
        System.out.println("Count :- " + count);
        c.moveToFirst();
        for (Integer rowCount = 0; rowCount < count; rowCount++) {
            Imagepojo imagepojo = new Imagepojo();
            String product = "";
            String image = "";
            product = c.getString(c.getColumnIndex("groupname"));
            image = c.getString(c.getColumnIndex("imageurl"));
            System.out.println("anything");
            String[] path = image.split("\\/");
            String val = path[path.length - 1];
            imagepojo.setPaymnet(product);
            imagepojo.setImageur(val);
            imageview.add(imagepojo);
            c.moveToNext();
        }

        db.close();
        myadapter = new Myadapter(getActivity());
        adapter.setAdapter(myadapter);
        myadapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                group_clicked = true;
                db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                groupName = String.valueOf(imageview.get(arg2).getPaymnet())/*Menu[arg2]*/;//groupList.getAdapter().getItem(arg2).toString() ;
                Cursor groupData = db.rawQuery("SELECT groupid from grouptable " +
                        "WHERE groupname ='" +/*Menu[arg2]*/String.valueOf(imageview.get(arg2).getPaymnet())/*groupList.getAdapter().getItem(arg2).toString()*/ + "';", null);
                groupData.moveToFirst();
                if (groupData.getCount() > 0) {
                    groupId = groupData.getString(groupData.getColumnIndex("groupid"));
                } else {
                }
                if (arg2 > -1) {
                    group1.catgoryfragment(groupId);
                } else {
                }
                db.close();
            }
        });

        return view;
    }

    public static GroupStaticFragment getInstance() {
        return GROUPID;
    }
}




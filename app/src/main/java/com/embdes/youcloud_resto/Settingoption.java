/**
 * 
 */
package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * @author embdes
 *
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Settingoption extends Activity{
	SQLiteDatabase db;
	public float discountAmount;
	public float voucherAmount =0;
	Dialog dialogMarketList;
	View viewList;
	int occupiedchairs;
	Dialog settingDialog,PaymentLoginpopup;
	ListView lv;
	TextView date,titlebar;
	ArrayAdapter<BluetoothDevice> adapter;
	ArrayList<BluetoothDevice> connections = new ArrayList<BluetoothDevice>();
	ArrayAdapter<String> adaptername;
	ArrayList<String> connectionsname = new ArrayList<String>();
	String globledevice;
	boolean printerDeviceCheck=false;
	boolean kotcheck=false;
    //search/info
    TextView txt_info;
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.settings);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.titlemenu);
		date = (TextView) findViewById(R.id.date);
		titlebar = (TextView) findViewById(R.id.titelabar);
		Button menu = (Button) findViewById(R.id.Imageview);
		menu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent  =new Intent(Settingoption.this,Home.class);
				startActivity(intent);
			}
		});
		final Calendar now = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
		String nowDate = formatter.format(now.getTime());
		date.setText(nowDate);
		titlebar.setText("Settings");

        txt_info = (TextView) findViewById(R.id.txt_info);
        txt_info.setText("Customize printer settings");
        findViewById(R.id.lay_info).setVisibility(View.VISIBLE);
        findViewById(R.id.lay_search).setVisibility(View.GONE);
        findViewById(R.id.titlebarSearchview).setVisibility(View.INVISIBLE);

		Button serverSettings = (Button)findViewById(R.id.severIpButton);
		Button kotsetting =(Button)findViewById(R.id.Button01);
		Button paymentloginButton =(Button)findViewById(R.id.Button02);
		kotsetting.setOnClickListener(kotbtPrinterHandler);
		serverSettings.setOnClickListener(serverIpHandler);
		Button btPrinterSettings = (Button)findViewById(R.id.printerBluetoothButton);
		Button wifiPrinterSettings = (Button)findViewById(R.id.PrinterWifiButton1);
		btPrinterSettings.setOnClickListener(btPrinterHandler);
		wifiPrinterSettings.setOnClickListener(wifiPrinterHandler);
		final CheckBox  btPrinterName = (CheckBox) findViewById(R.id.RadioBtPrinter);
		final CheckBox wifiPrinterName = (CheckBox) findViewById(R.id.RadioWifiPrinter);
		final CheckBox kotbtPrinterName= (CheckBox)findViewById(R.id.CheckBox01);
		kotbtPrinterName.setChecked(MainActivity.getInstance().kotPrinterEnable);
		btPrinterName.setChecked(!MainActivity.getInstance().isWifiPrinterEnable);
		wifiPrinterName.setChecked(MainActivity.getInstance().isWifiPrinterEnable);
		final ImageView home = (ImageView)findViewById(R.id.imageView11);
		final ImageView opentabs = (ImageView)findViewById(R.id.imageView3);
		opentabs.setOnClickListener(new View.OnClickListener() {
			@SuppressLint("NewApi")
			public void onClick(View v) {
				Intent intent=new Intent(Settingoption.this, BILLSPENDINGTABLES.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				   
			}
		});

		home.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				finish();
				Intent intent=new Intent(Settingoption.this, Home.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				
			}
		});

		btPrinterName.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(true == btPrinterName.isChecked())
				{
					wifiPrinterName.setChecked(false);
					MainActivity.getInstance().isWifiPrinterEnable = false;
					
				}
				else
				{
					wifiPrinterName.setChecked(true);
					MainActivity.getInstance().isWifiPrinterEnable = true;
				}
			}
		});
		kotbtPrinterName.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(true == kotbtPrinterName.isChecked())
				{
					MainActivity.getInstance().kotPrinterEnable = true;
					kotcheck=true;
					ListPairedDevices();
					
				}
				else
				{
					MainActivity.getInstance().kotPrinterEnable = false;
				}
			}
		});

		wifiPrinterName.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(true == wifiPrinterName.isChecked())
				{
					btPrinterName.setChecked(false);
					MainActivity.getInstance().isWifiPrinterEnable = true;
				}
				else
				{
					btPrinterName.setChecked(true);
					MainActivity.getInstance().isWifiPrinterEnable = false;
					
				}
			}
		});

		db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
		Cursor serverIpDetails = db.rawQuery("SELECT ipaddress, portno " +
				" FROM masterterminal WHERE terminalid ="+
				MainActivity.getInstance().outletid+";", null);
		if(serverIpDetails.getCount() > 0)
		{
			serverIpDetails.moveToFirst();
			TextView serverDetails = (TextView) findViewById(R.id.severIpView);
			serverDetails.setText("Server IpAddress\nIpAddress:"+serverIpDetails.getString(0)+
					"\nPortNo:"+serverIpDetails.getString(1));

		}
		Cursor printerDetails = db.rawQuery("SELECT ipaddress, portno, btprintername " +
				" FROM locationprinter WHERE locationorderid ="+
				user_Login.user_LoginID.LOCATIONNO+";", null);
		if(printerDetails.getCount() > 0)
		{
			printerDetails.moveToFirst();
			wifiPrinterName.setText("WiFi Printer\nIP Address: "+printerDetails.getString(0)+
					"\nPort No: "+printerDetails.getString(1));

			btPrinterName.setText("Bluetooth Printer\n"+printerDetails.getString(2));
		}
		db.close();
	}
	protected void onResume() {
		super.onResume();
		if(MainActivity.getInstance() ==null || user_Login.user_LoginID==null){
			Intent userLoginIntent = new Intent(); //Created new Intent to 
			userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
			userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(userLoginIntent);
			this.finish();
		}
		
	};
	/**
	 * Creating pop-up window for web server configuration.
	 * Here user can enter IP address or Host name and Port number of Server. 
	 * 
	 */
	View.OnClickListener serverIpHandler = new View.OnClickListener() {
		
		public void onClick(View v) {  
			if(user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.MTUpdate ){
				viewList = Settingoption.this.getLayoutInflater().inflate(R.layout.settings_popup,
						null);
				dialogMarketList = new Dialog(Settingoption.this);
	
				dialogMarketList.getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
				dialogMarketList.setTitle("Server Configuration"	);
				dialogMarketList.setContentView(viewList);
				dialogMarketList.setCancelable(false);
				dialogMarketList.show();
				ImageButton popUpOk = (ImageButton) viewList.findViewById(R.id.settingsPopUpOk);
				ImageButton  PopUpCancel= (ImageButton) viewList.findViewById(R.id.settingsPopUpCancel);
				final CheckBox ipAddressCheck = (CheckBox) viewList.findViewById(R.id.checkBoxIpAddress);
				final CheckBox hostNameCheck = (CheckBox) viewList.findViewById(R.id.checkBoxHostName);
				ipAddressCheck.setChecked(true);
	
				PopUpCancel.setOnClickListener(new View.OnClickListener() {
	
					@Override
					public void onClick(View arg0) {
						dialogMarketList.dismiss();	
					}
				});
	
				final EditText  editIpAddress = (EditText) viewList.findViewById(R.id.editPopUpIpNumber);
				final EditText  editPortNumber = (EditText) viewList.findViewById(R.id.editPopUpPortNumber);
	
				if(true == ipAddressCheck.isChecked())
				{
					editIpAddress.setText("");
					editPortNumber.setText("");
					TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
					hostNameView.setText("Ip Address");
					editIpAddress.setHint("192.168.001.102");
					InputFilter[] filters = new InputFilter[1];
					filters[0] = new InputFilter() {
						@Override
						public CharSequence filter(CharSequence source, int start, int end,
								android.text.Spanned dest, int dstart, int dend) {
							if (end > start) {
								String destTxt = dest.toString();
								String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
								if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) { 
									return "";
								} else {
									String[] splits = resultingTxt.split("\\.");
									for (int i=0; i<splits.length; i++) {
										if (Integer.valueOf(splits[i]) > 255) {
											return "";
										}
									}
								}
							}
							return null;
						}
	
					};
					editIpAddress.setFilters(filters);
				}
				else
				{
					editIpAddress.setText("");
					editPortNumber.setText("");
					TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
					hostNameView.setText("Host Name");
					editIpAddress.setHint("www.google.com");
					editIpAddress.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
					InputFilter[] filters = new InputFilter[1];
					filters[0] = new InputFilter.LengthFilter(50);
					editIpAddress.setFilters(filters);
				}
	
				ipAddressCheck.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View arg0) {
						if(true == ipAddressCheck.isChecked())
						{
							editIpAddress.setText("");
							editPortNumber.setText("");
							editIpAddress.setInputType(InputType.TYPE_CLASS_PHONE);
							hostNameCheck.setChecked(false);
							TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
							hostNameView.setText("Ip Address");
							editIpAddress.setHint("192.168.001.102");
							InputFilter[] filters = new InputFilter[1];
							filters[0] = new InputFilter() {
								@Override
								public CharSequence filter(CharSequence source, int start, int end,
										android.text.Spanned dest, int dstart, int dend) {
									if (end > start) {
										String destTxt = dest.toString();
										String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
										if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) { 
											return "";
										} else {
											String[] splits = resultingTxt.split("\\.");
											for (int i=0; i<splits.length; i++) {
												if (Integer.valueOf(splits[i]) > 255) {
													return "";
												}
											}
										}
									}
									return null;
								}
							};
							editIpAddress.setFilters(filters);
						}
						else
						{
							hostNameCheck.setChecked(true);
							TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
							hostNameView.setText("Host Name");
							editIpAddress.setHint("www.google.com");
							editIpAddress.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
							editIpAddress.setText("");
							editPortNumber.setText("");
							InputFilter[] filters = new InputFilter[1];
							filters[0] = new InputFilter.LengthFilter(50);
							editIpAddress.setFilters(filters);
	
						}
	
					}
				});
	
				hostNameCheck.setOnClickListener(new View.OnClickListener() {
	
					@Override
					public void onClick(View v) {
						if(false == hostNameCheck.isChecked())
						{
							editIpAddress.setText("");
							editPortNumber.setText("");
							editIpAddress.setInputType(InputType.TYPE_CLASS_PHONE);
							ipAddressCheck.setChecked(true);
							TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
							hostNameView.setText("Ip Address");
							editIpAddress.setHint("192.168.001.102");
							InputFilter[] filters = new InputFilter[1];
							filters[0] = new InputFilter() {
								@Override
								public CharSequence filter(CharSequence source, int start, int end,
										android.text.Spanned dest, int dstart, int dend) {
									if (end > start) {
										String destTxt = dest.toString();
										String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
										if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) { 
											return "";
										} else {
											String[] splits = resultingTxt.split("\\.");
											for (int i=0; i<splits.length; i++) {
												if (Integer.valueOf(splits[i]) > 255) {
													return "";
												}
											}
										}
									}
									return null;
								}
	
							};
							editIpAddress.setFilters(filters);
						}
						else
						{
							TextView hostNameView = (TextView) viewList.findViewById(R.id.popUpIpView);
							hostNameView.setText("Host Name");
							editIpAddress.setHint("www.google.com");
							editIpAddress.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
							ipAddressCheck.setChecked(false);
							editIpAddress.setText("");
							editPortNumber.setText("");
							InputFilter[] filters = new InputFilter[1];
							filters[0] = new InputFilter.LengthFilter(50);
							editIpAddress.setFilters(filters);
	
	
						}
					}
				});
	
				popUpOk.setOnClickListener(new View.OnClickListener() {
	
					@Override
					public void onClick(View arg0) {
						if( editIpAddress.getText().length() > 0 && 
								editPortNumber.getText().length() > 0)
						{
							TextView serverDetails = (TextView) findViewById(R.id.severIpView);
	
	
							if(true == ipAddressCheck.isChecked())
							{
								serverDetails.setText("Server Ip Address\nIP Address: "+
										editIpAddress.getText()+"\nPort No: "+ editPortNumber.getText());
	
								db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
								db.execSQL("UPDATE masterterminal SET ipAddress ='"+ 
										editIpAddress.getText()+"', portno ="+ editPortNumber.getText()+
										" WHERE terminalid = "+MainActivity.getInstance().outletid+";");
								db.close();
								dialogMarketList.dismiss();
							}
							else
							{
	
								try {
									InetAddress address = InetAddress.getByName(""+editIpAddress.getText());
									serverDetails.setText("Server Ip Address\nIP Address: "+
											address.getHostAddress()+"\nPort No: "+ editPortNumber.getText());
									db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
									db.execSQL("UPDATE masterterminal SET ipAddress ='"+ 
											address.getHostAddress()+"', portno ="+ editPortNumber.getText()+
											" WHERE terminalid = "+MainActivity.getInstance().outletid+";");
									db.close();
									dialogMarketList.dismiss();
								} catch (UnknownHostException e) {
									e.printStackTrace();
									Toast.makeText(Settingoption.this, ""+e.getMessage(),
											Toast.LENGTH_LONG).show();
									dialogMarketList.dismiss();
									return;
								}
							}
						}
					}
				});
			}else{
				AlertDialog.Builder msg = new AlertDialog.Builder(
						Settingoption.this, android.R.style.Theme_Dialog);
				msg.setTitle("Opration Failed");
				msg.setMessage("User Doesn't have the permission for this opration");
				msg.setPositiveButton("Ok", null);
				msg.show();
			}
		}
		
	};

	View.OnClickListener kotbtPrinterHandler = new View.OnClickListener() {
		public void onClick(View v) {
			if(user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.BPrint ){
				kotcheck=true;
				ListPairedDevices();
			}else{
				AlertDialog.Builder msg = new AlertDialog.Builder(
						Settingoption.this, android.R.style.Theme_Dialog);
				msg.setTitle("Opration Failed");
				msg.setMessage("User Doesn't have the permission for this opration");
				msg.setPositiveButton("Ok", null);
				msg.show();
			}
//			
		}
	};
	/**
	 * Creating pop-up window for WiFi printer configuration.
	 * Here user can select printer from the listview items.
	 * 
	 */
	
	View.OnClickListener btPrinterHandler = new View.OnClickListener() {
		public void onClick(View v) {
			if(user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.BPrint ){

				ListPairedDevices();
			}else{
				AlertDialog.Builder msg = new AlertDialog.Builder(
						Settingoption.this, android.R.style.Theme_Dialog);
				msg.setTitle("Opration Failed");
				msg.setMessage("User Doesn't have the permission for this opration");
				msg.setPositiveButton("Ok", null);
				msg.show();
			}
		}
	};

	private void ListPairedDevices() {

		Set<BluetoothDevice> bondedDevices = BluetoothAdapter
	            .getDefaultAdapter().getBondedDevices();
		for(int k=0;k<bondedDevices.size();k++){
		}
		if (bondedDevices.size() > 0) 
        {
            for ( BluetoothDevice bondDeice : bondedDevices) 
            {
		              connections.add(bondDeice);
		              connectionsname.add(bondDeice.getName());
		              printerDeviceCheck=true;
            }
        }
		if(printerDeviceCheck==true){
			printerDeviceCheck=false;
			Device_Select();
		}else{
    		Toast.makeText(getApplicationContext(), "Printer not found", Toast.LENGTH_SHORT).show();
    	}
		
}
 private void Device_Select(){
	 	settingDialog=new Dialog(Settingoption.this);
		settingDialog.setContentView(R.layout.fragment_scanner_device_selection);
		settingDialog.setTitle("Paired Device");
		settingDialog.setCancelable(true);
		lv = (ListView)settingDialog.findViewById(R.id.list);
	 	settingDialog.show();
		adapter = new ArrayAdapter<BluetoothDevice>(this,android.R.layout.simple_list_item_1, connections); 
		adaptername = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, connectionsname);
		lv.setAdapter(adaptername);
		
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				globledevice=adapter.getItem(arg2).getAddress();
				if(kotcheck==true){
					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		            SharedPreferences.Editor editor = prefs.edit();
		            editor.putString("btkPrinterid", globledevice);
		            editor.commit();
		            kotcheck=false;
				}else{
					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		            SharedPreferences.Editor editor = prefs.edit();
		            editor.putString("btPrinterid", globledevice);
		            editor.commit();
				}
				settingDialog.dismiss();
				
			}
		});
 }
 /**
	 * @param data
	 * @param printformate
	 */

	/**
	 * Creating pop-up window for blue-tooth printer configuration.
	 * Here user can enter IP address and Port number of WiFi printer.  
	 * 
	 */
	View.OnClickListener wifiPrinterHandler = new View.OnClickListener() {
			public void onClick(View v) {  
				if(user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.BPrint ){
					@SuppressWarnings("unused")
					LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					viewList = Settingoption.this.getLayoutInflater().inflate(R.layout.settings_popup,
							null);
					dialogMarketList = new Dialog(Settingoption.this);
		
					dialogMarketList.getWindow().setSoftInputMode(
							WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
					dialogMarketList.setTitle("WIFI Printer Configuration"	);
					dialogMarketList.setContentView(viewList);
					dialogMarketList.setCancelable(false);
					dialogMarketList.show();
		
					final CheckBox ipAddressCheck = (CheckBox) viewList.findViewById(R.id.checkBoxIpAddress);
					ipAddressCheck.setVisibility(View.INVISIBLE);
					final CheckBox hostNameCheck = (CheckBox) viewList.findViewById(R.id.checkBoxHostName);
					hostNameCheck.setVisibility(View.INVISIBLE);
		
					ImageButton popUpOk = (ImageButton) viewList.findViewById(R.id.settingsPopUpOk);
					ImageButton  PopUpCancel= (ImageButton) viewList.findViewById(R.id.settingsPopUpCancel);
					PopUpCancel.setOnClickListener(new View.OnClickListener() {
		
						@Override
						public void onClick(View arg0) {
							dialogMarketList.dismiss();	
						}
					});
		
					final EditText  editIpAddress = (EditText) viewList.findViewById(R.id.editPopUpIpNumber);
					final EditText editPortNumber = (EditText) viewList.findViewById(R.id.editPopUpPortNumber);
		
					InputFilter[] filters = new InputFilter[1];
					filters[0] = new InputFilter() {
						@Override
						public CharSequence filter(CharSequence source, int start, int end,
								android.text.Spanned dest, int dstart, int dend) {
							if (end > start) {
								String destTxt = dest.toString();
								String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
								if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) { 
									return "";
								} else {
									String[] splits = resultingTxt.split("\\.");
									for (int i=0; i<splits.length; i++) {
										if (Integer.valueOf(splits[i]) > 255) {
											return "";
										}
									}
								}
							}
							return null;
						}
		
					};
					editIpAddress.setFilters(filters);
		
		
					popUpOk.setOnClickListener(new View.OnClickListener() {
		
						@Override
						public void onClick(View arg0) {
							if( editIpAddress.getText().length() > 0 && 
									editPortNumber.getText().length() > 0)
							{
								CheckBox  printerName = (CheckBox) findViewById(R.id.RadioWifiPrinter);
								printerName.setText("WiFi Printer\nIP Address: "+editIpAddress.getText()+"\nPort No: "+editPortNumber.getText());
								db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
								db.execSQL("UPDATE locationprinter SET ipAddress ='"+ 
										editIpAddress.getText()+"', portno ="+ editPortNumber.getText()+
										" WHERE locationorderid = "+user_Login.user_LoginID.LOCATIONNO+";");
								db.close();
								dialogMarketList.dismiss();	
							}
						}
					});
			}else{
				AlertDialog.Builder msg = new AlertDialog.Builder(
						Settingoption.this, android.R.style.Theme_Dialog);
				msg.setTitle("Opration Failed");
				msg.setMessage("User Doesn't have the permission for this opration");
				msg.setPositiveButton("Ok", null);
				msg.show();
			}
		}
	};
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
		return super.dispatchTouchEvent(ev);
	}
	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

}

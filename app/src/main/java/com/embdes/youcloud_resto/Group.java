package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author EMBDES.
 * @version 1.0.
 *          This Activity display all available groups.
 *          This is the main Activity(Contains All Fragments )
 *          (Group Fragmnet,Cart-Static Fragment)
 */
@SuppressLint("NewApi")
public class Group extends Activity {
    public float discountAmount;
    float totalAmount;
    float subTotalAmount;
    public float voucherAmount = 0;
    float taxAmount;
    SQLiteDatabase db;
    String finaltotal;
    static String table;
    String groupId;
    TextView group, product;
    Button menu;
    //search/info
    TextView txt_info;
    ImageView searchView;
    LinearLayout lay_info;
    RelativeLayout lay_search;
    //
    EditText search;
    String groupName;
    String categoryName;
    public String voucherNumber = "0";
    public String ORDERNO = "0";
    public static String CONSUMERID = "0";
    TextView date, titlebar;
    static Group GROUPID;
    Bitmap bmThumbnail;
    int imagecount = 1;
    int i = 1;
    TablesFragment tablesFragment;
    ArrayList<tablespojo> tableno1;
    TextView tableno;
    TextView username;
    CartStaticSragment cartstaticfragment;
    GroupFragment groupfragment;
    ImageView delete;
    CategoryFragment categoryFragment, cat1frag;
    android.app.FragmentManager fragmentManager;
    ProductFragment productFragment, pfrag;
    android.app.FragmentTransaction fragmentTransaction;
    ArrayList<String> paymentTitle = new ArrayList<String>();
    ArrayList<String> imageurl = new ArrayList<String>();
    Boolean productflag = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.group);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlemenu);
        GROUPID = this;
        menu = (Button) findViewById(R.id.Imageview);
        date = (TextView) findViewById(R.id.date);
        delete = (ImageView) findViewById(R.id.delete1);
        searchView = (ImageView) findViewById(R.id.titlebarSearchview);
        search = (EditText) findViewById(R.id.titleditext);
        txt_info = (TextView) findViewById(R.id.txt_info);
        titlebar = (TextView) findViewById(R.id.titelabar);
        lay_info = (LinearLayout) findViewById(R.id.lay_info);
        lay_search = (RelativeLayout) findViewById(R.id.lay_search);

//        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
//        String query = "SELECT locationid,billflag,orderflag from cart WHERE locationid = " +
//                user_Login.user_LoginID.LOCATIONNO + " AND tableno = 1501;";
//        Log.e("query", "=" + query);
//        Cursor cartData = db.rawQuery(query, null);
//        int cartcount = cartData.getCount();
//        Log.e("count", "=" + cartcount);
//        while (cartData.moveToNext()) {
//            Log.e("locationid", "=" + cartData.getString(0));
//            Log.e("billflag", "=" + cartData.getString(1));
//            Log.e("orderflag", "=" + cartData.getString(2));
//        }
//
//        db.close();

        search.setFocusable(true);
        final Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM");
        String nowDate = formatter.format(now.getTime());
        date.setText(nowDate);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Group.this, Home.class);
                startActivity(intent);
            }
        });

        groupName = "group";
        categoryName = "Category";
        discountAmount = 0;
        totalAmount = 0;
        voucherAmount = 0;
        taxAmount = 0;
        subTotalAmount = 0;
        categoryFragment = new CategoryFragment();
        cat1frag = new CategoryFragment();
        productFragment = new ProductFragment();
        groupfragment = new GroupFragment();
        tablesFragment = new TablesFragment();
        pfrag = new ProductFragment();
        fragmentManager = getFragmentManager();
        cartstaticfragment = (CartStaticSragment) fragmentManager.findFragmentById(R.id.staticfragmet2);

        if (!tablesFragment.isResumed()) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.container2, tablesFragment);
            fragmentTransaction.commit();
        }
        if (Home.HOMEID.variable || GetUserInfo.User.deliveryvariable) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container2, groupfragment);
            fragmentTransaction.commit();
            CartStaticSragment.CARTStaticSragment.tableno.setText(Group.GROUPID.table);
            Home.HOMEID.variable = false;
            GetUserInfo.User.deliveryvariable = false;
            CartStaticSragment.CARTStaticSragment.tabletext.setText("Take Away:-");
            CartStaticSragment.CARTStaticSragment.nofgueststext.setVisibility(View.INVISIBLE);
            CartStaticSragment.CARTStaticSragment.plus.setVisibility(View.INVISIBLE);
            CartStaticSragment.CARTStaticSragment.minus.setVisibility(View.INVISIBLE);
            CartStaticSragment.CARTStaticSragment.noofguests.setVisibility(View.INVISIBLE);
        } else
            CONSUMERID = "0";
        tableno1 = new ArrayList<tablespojo>();
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * from grouptable ;", null);
        int count = c.getCount();
        c.moveToFirst();
        for (Integer rowCount = 0; rowCount < count; rowCount++) {
            String product = "";
            String image = "";
            product = c.getString(c.getColumnIndex("groupname"));
            image = c.getString(c.getColumnIndex("imageurl"));
            paymentTitle.add(product);
            imageurl.add(image);
            c.moveToNext();
        }
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        CartStaticSragment.CARTStaticSragment.noofguests.setText("0");
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setText("");
                lay_info.setVisibility(View.VISIBLE);
                lay_search.setVisibility(View.GONE);
                if (GROUPID != null) {
                    View view = GROUPID.getCurrentFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productflag = true;

                Group.GROUPID.lay_info.setVisibility(View.GONE);
                Group.GROUPID.lay_search.setVisibility(View.VISIBLE);

                search.setVisibility(View.VISIBLE);
                delete.setVisibility(View.VISIBLE);
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                searchView.setVisibility(View.INVISIBLE);
                if (!productFragment.isResumed()) {
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container2, productFragment);
                    fragmentTransaction.commit();
                    Bundle bundle = new Bundle();
                    bundle.putString("searchFlag", "0");
                    bundle.putString("SearchText", groupId);
                    productFragment.setArguments(bundle);
                }
            }
        });

        db.close();
    }

    //
    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null || user_Login.user_LoginID == null) {
            Intent userLoginIntent = new Intent(); //Created new Intent to
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(userLoginIntent);
            this.finish();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder adb = new AlertDialog.Builder(
                Group.this, android.R.style.Theme_Dialog);
        adb.setTitle("");
        adb.setMessage("Do You Want To Return Home ?");
        adb.setPositiveButton("Yes", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setClass(Group.this, Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                Group.getInstance().finish();
            }
        });
        adb.setNegativeButton("No", null);
        adb.show();
    }

    public void groupfragment() {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container2, groupfragment);
        fragmentTransaction.commit();

    }

    public void alert() {
        cartstaticfragment.alertadapter(GROUPID);
    }

    public void catgoryfragment(String groupId) {
        if (!categoryFragment.isResumed()) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container1, categoryFragment);
            fragmentTransaction.commit();
            Bundle bundle = new Bundle();
            bundle.putString("SearchText", groupId);
            categoryFragment.setArguments(bundle);
        } else {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container1, cat1frag);
            fragmentTransaction.commit();
            Bundle bundle = new Bundle();
            bundle.putString("SearchText", groupId);
            cat1frag.setArguments(bundle);
        }
    }

    public void logout() {
        String timeStamp = new SimpleDateFormat("yy/MM/dd HH:mm").format(Calendar.getInstance().getTime());
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor userId = db.rawQuery("SELECT userid from userterminal WHERE username =" +
                "'" + MainActivity.getInstance().USERNAME + "';", null);
        userId.moveToFirst();
        String userid = userId.getString(0);
        db.close();
        MainActivity.getInstance().messageid = "M61";
        MainActivity.getInstance().checkid = 1;
        MainActivity.getInstance().messagecontentarray = "1111";
        MainActivity.getInstance().noofpackets = 1;
        MainActivity.getInstance().packetno = 1;
        MainActivity.getInstance().message = userid + "|" + timeStamp + "|" + "0" + "|" + user_Login.user_LoginID.LOCATIONNO;
        MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
        MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
        Socket_Connection socket = new Socket_Connection();
        int status = socket.connectionToServer();
        if (status != 0) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("INSERT INTO queuetable(message) VALUES('" + MainActivity.getInstance().finnalMesgToSend + "');");
            MainActivity.getInstance().queueMessage = "0";
            db.close();
        }
        this.finish();
        Intent intent = new Intent(this, user_Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void cartUpdate() {
        String consumer_name = "";
        String consumer_mobile = "";
        String consumer_address = "";

        Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
        String editLocation = user_Login.user_LoginID.LOCATIONNO;
        String editTable = Group.GROUPID.table;
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);

        Cursor CustomerQuery = db.rawQuery("SELECT cname,cmobileno,caddress from consumerinfo WHERE cid=" + CONSUMERID, null);
        int consumerCount = CustomerQuery.getCount();
        if (consumerCount > 0) {
            CustomerQuery.moveToFirst();

            consumer_name = CustomerQuery.getString(0);
            consumer_mobile = CustomerQuery.getString(1);
            consumer_address = CustomerQuery.getString(2);
        }
        CustomerQuery.close();
        Cursor updateCount = db.rawQuery("SELECT * from cart WHERE locationid=" + editLocation + " AND tableno=" +
                editTable + " AND billflag = 0 AND (orderqty < productqty OR ordertypeqty < typeqty) AND foodid=" + Printer.kitchenid + ";", null);


        int count = updateCount.getCount();
        updateCount.moveToPrevious();
        String newMessage = null;
        String message = null;
        while (updateCount.moveToNext()) {
            newMessage = updateCount.getString(updateCount.getColumnIndex("sno")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("productname")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("typename")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("productprice")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("productqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("billqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("typeprice")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("typeqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("billtypeqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("totalprice")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("tableno")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("locationid")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("orderflag")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("billflag")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("orderqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("billedqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("checkflag")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("orderno")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("covers")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("tdiscount")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("discount")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("totaltax")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("ordertypeqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("billedtypeqty")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("datetime")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("tbillflag")) +
                    "|" + updateCount.getString(updateCount.getColumnIndex("username")) +
                    "|" + consumer_name +//27
                    "|" + consumer_mobile +//28
                    "|" + consumer_address +//29
                    "?";

            if (message == null) {
                message = newMessage;
            } else {
                message += "|?" + newMessage;
            }
        }
        db.close();
        if (count > 0) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("UPDATE cart SET orderflag = 1 , orderqty = productqty" +
                    " , ordertypeqty = typeqty WHERE locationid=" + editLocation +
                    " AND tableno=" + editTable + "  AND billflag = 0 AND foodid=" + Printer.kitchenid + ";");
            db.execSQL("UPDATE ordernos SET orderflag = 1 WHERE locationid=" + editLocation +
                    " AND tableno=" + editTable + "  AND billflag = 0;");
            db.close();
            MainActivity.getInstance().messageid = "M03";
            MainActivity.getInstance().checkid = 1;
            MainActivity.getInstance().messagecontentarray = "111111111111111111111111111111";//(30)
            MainActivity.getInstance().noofpackets = 1;
            MainActivity.getInstance().packetno = 1;
            MainActivity.getInstance().message = message;
            MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length();
            MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
            Socket_Connection socket = new Socket_Connection();
            int status = socket.connectionToServer();
            if (status != 0) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                db.execSQL("INSERT INTO queuetable(message) VALUES('" + MainActivity.getInstance().finnalMesgToSend + "');");
                MainActivity.getInstance().queueMessage = "0";
                db.close();
            }
        }
    }

   /* private void getDelboys() {
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor order = db.rawQuery("SELECT * FROM deliveryboy;", null);
        int count = order.getCount();
        String delName = null;
        if (count > 0) {
            while (order.moveToNext()) {
                delName = order.getString(order.getColumnIndex("dname"));
                Log.e("Name",""+delName);
                Toast.makeText(getApplicationContext(), "Name : " + delName, Toast.LENGTH_SHORT).show();
            }
        }
        order.close();
    }*/


    @Override
    protected void onDestroy() {
        if (bmThumbnail != null) {
            bmThumbnail.recycle();
        }
        super.onDestroy();
    }

    public static Group getInstance() {
        return GROUPID;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}


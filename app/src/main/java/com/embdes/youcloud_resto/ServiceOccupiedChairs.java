package com.embdes.youcloud_resto;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ServiceOccupiedChairs extends Service {
    MyTask myTask;
    MyDb myDb;
    long sno;
    String order ;
    @Override
    public void onCreate() {
        myDb = new MyDb(getApplicationContext());
        myDb.open();
    }

    public ServiceOccupiedChairs() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        myTask = new MyTask();
        myTask.execute("http://52.202.56.230:8080/OrdaposServer/servlet/ReceivePendingTableDetails/");
        return super.onStartCommand(intent, flags, startId);

    }
    public class MyTask extends AsyncTask<String,Void,String >{
        URL myurl;
        HttpURLConnection connection;
        OutputStream outputStream;
        OutputStreamWriter outputStreamWriter;
        @Override
        protected String doInBackground(String... params) {
            String uname,tableno,occupiedchairs,order;
            int mid;
            Cursor cursor;
            cursor  =myDb.query();
            while (cursor.moveToNext()) {
                try {
                    sno = cursor.getLong(0);
                    mid = cursor.getInt(1);
                    tableno = cursor.getString(2);
                    occupiedchairs = cursor.getString(3);
                    uname = cursor.getString(4);
                    order = cursor.getString(5);
                    myurl = new URL(params[0]);
                    connection = (HttpURLConnection) myurl.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.accumulate("merchantid", mid);
                    jsonObject.accumulate("occupiedchairs", occupiedchairs);
                    jsonObject.accumulate("tableno", tableno);
                    jsonObject.accumulate("orderid",order);
                    jsonObject.accumulate("user", uname);
                    outputStream = connection.getOutputStream();
                    outputStreamWriter = new OutputStreamWriter(outputStream);
                    outputStreamWriter.write(jsonObject.toString());
                    outputStreamWriter.flush();
                    int responsecode = connection.getResponseCode();
                    if (responsecode == HttpURLConnection.HTTP_OK) {
                        if (cursor.isLast()){
                        }
                        else {
                            myDb.delete(sno);
                        }
                    } else {
                        return "failure";
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                        if (outputStream != null) {
                            try {
                                outputStream.close();
                                if (outputStreamWriter != null) {
                                    outputStreamWriter.close();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

}

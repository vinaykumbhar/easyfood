package com.embdes.youcloud_resto;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by Admin on 29/11/2017.
 */

public class WebserviceController {

    private static final String TAG = "WebserviceController";
    private static final String HOST_URL = "http://52.202.56.230";
    private static final String WS_CHECK_RECORDS = "/ordapos/ws/add_consumer_info.php";

    private static boolean isRunning = false;
    private static WebserviceController webServiceInstance = new WebserviceController();
    private static RequestQueue requestQueue;

    private WebserviceController() {


    }

    public static void checkRecord(Context context) {
        if (requestQueue != null) {
            requestQueue = Volley.newRequestQueue(context);
        }
//        final String url=HOST_URL + WS_CHECK_RECORDS;
        final String url = HOST_URL + WS_CHECK_RECORDS;
        StringRequest request = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "response :" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);

                } catch (Exception exception) {
                    exception.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Log.d(TAG, "Error");
            }
        });
        requestQueue.add(request);
    }
}


package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author EMBDES.
 * @version 1.0.
 * This Activity contains locations available, user can choose any one of the location. 
 * 
 */

public class Location extends Activity {

	static SQLiteDatabase db;
	Spinner location;
	static  Location LOCATIONID;
	String[] locationidname;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LOCATIONID = this;
		setContentView(R.layout.location);
		setTitle("Location");
		if(Home.flag==1){
			final ImageView home = (ImageView)findViewById(R.id.imageView11);
			final ImageView opentabs = (ImageView)findViewById(R.id.imageView3);
			home.setVisibility(View.VISIBLE);
			opentabs.setVisibility(View.VISIBLE);
			opentabs.setOnClickListener(new View.OnClickListener() {
				@SuppressLint("NewApi")
				public void onClick(View v) {
					Home.flag=0;
					finish();
					home.setVisibility(View.INVISIBLE);
					opentabs.setVisibility(View.INVISIBLE);
					Intent intent=new Intent(Location.this, BILLSPENDINGTABLES.class);
					startActivity(intent);
					   
				}
			});
			home.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					Home.flag=0;
					finish();
					home.setVisibility(View.INVISIBLE);
					opentabs.setVisibility(View.INVISIBLE);
					Intent intent=new Intent(Location.this, Home.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}
			});
		}
		insertLocation();
	}
	protected void onResume() {
		super.onResume();
		if(MainActivity.getInstance() ==null || user_Login.user_LoginID==null){
			Intent userLoginIntent = new Intent(); //Created new Intent to 
			userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
			userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(userLoginIntent);
			this.finish();
		}
		
	};
	public void insertLocation() {
		ArrayList<String> locName = new ArrayList<String>();
		location = (Spinner)findViewById(R.id.spinner1);
		TextView  locationId = (TextView)findViewById(R.id.textView1);
		locationId.setText("Location ID ");
		location.clearAnimation();
		db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
		Cursor data = db.rawQuery("SELECT * FROM location;" , null);
		int loccount = data.getCount();
		data.moveToFirst();
		for(Integer j=0; j< loccount; j++)
		{
			String name ="";
			String locationname="";
			name = data.getString(data.getColumnIndex("locationid"));
			locationname = data.getString(data.getColumnIndex("locationname"));
			locName.add(name+": "+locationname);
			data.moveToNext();
		}
		ArrayAdapter<String> locadapter =
				new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, locName);
		location.setAdapter(locadapter);
		ImageButton okButton = (ImageButton)findViewById(R.id.submit);
		okButton.setOnClickListener(okhandler);
		db.close();
	}
	View.OnClickListener okhandler = new View.OnClickListener() {
		public void onClick(View v) {  
			locationidname=(location.getItemAtPosition(location.getSelectedItemPosition()).toString()).split(":");
			String timeStamp = new SimpleDateFormat("yy/MM/dd HH:mm").format(Calendar.getInstance().getTime());
			db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
			db.beginTransaction();
			SQLiteStatement statement = db.compileStatement("INSERT INTO loginrecord VALUES((select userid from userterminal where username =" +
					" '"+ MainActivity.getInstance().USERNAME+"'),'"+timeStamp +"', 0, "+ 
					locationidname[0]+", 0);");
			statement.executeInsert();
			db.execSQL("UPDATE masterterminal SET lastloginusername='"+MainActivity.getInstance().USERNAME+"';");
			Cursor userId = db.rawQuery("SELECT userid from userterminal WHERE username =" +
					"'"+ MainActivity.getInstance().USERNAME+"';" , null);
			userId.moveToFirst();
			String userid = userId.getString(0);
			db.setTransactionSuccessful();
			db.endTransaction();
			db.close();
			user_Login.user_LoginID.LOCATIONNO = locationidname[0].toString();
			MainActivity.getInstance().messageid = "M27";
			MainActivity.getInstance().checkid = 1;
			MainActivity.getInstance().messagecontentarray = "1111";
			MainActivity.getInstance().noofpackets = 1;
			MainActivity.getInstance().packetno = 1;
			MainActivity.getInstance().message = userid+"|"+ timeStamp+"|"+"0"+"|"+
					locationidname[0];
			MainActivity.getInstance().currentmessagelength = MainActivity.getInstance().message.length() ;
			MainActivity.getInstance().totalmessagelength = MainActivity.getInstance().message.length();
			Toast.makeText(Location.this, "Send Login Data To Server ... ", Toast.LENGTH_SHORT)
			.show();
			Intent homeIntent = new Intent();
			homeIntent.setClass(getApplicationContext(), Home.class);//------------
			startActivity(homeIntent);
			Location.getInstance().finish();
		}
	};


	public void onBackPressed() {
		if(Home.flag==1){
			Home.flag=0;
			Intent intent = new Intent(this, Home.class); 
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		else {
			Intent intent = new Intent(this, user_Login.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
	}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
		return super.dispatchTouchEvent(ev);
	}
	public static Location getInstance(){
		return LOCATIONID;
	}
	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}
}


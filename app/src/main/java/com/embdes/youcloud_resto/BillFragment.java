package com.embdes.youcloud_resto;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.embdes.utills.NonScrollListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class BillFragment extends Fragment {
    SQLiteDatabase db;
    LinearLayout print;
    Button pay;
    EditText cash_amount;
    TextView name, terminal, time, orderid, table, guests, change_amount;
    NonScrollListView listView;
    TextView dine;
    ArrayList<Cartpojo> cart;
    String productname;
    String producttype;
    String totalprice;
    String qty;
    String sno;
    int i;
    TextView billouttax, billoutsubtotal, billouttotal;
    BilloutAdapater billoutAdapater;


    public BillFragment() {
        // Required empty public constructor
    }

    double total = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bill, container, false);
        name = (TextView) view.findViewById(R.id.billoutname);
        terminal = (TextView) view.findViewById(R.id.billoutterminal);
        time = (TextView) view.findViewById(R.id.billouttime);
        orderid = (TextView) view.findViewById(R.id.billoutorderid);
        table = (TextView) view.findViewById(R.id.billouttable);
        guests = (TextView) view.findViewById(R.id.billoutguests);
        listView = (NonScrollListView) view.findViewById(R.id.billoutlistview);
        print = (LinearLayout) view.findViewById(R.id.print);
        Group.GROUPID.titlebar.setText("Bill Out");
        pay = (Button) view.findViewById(R.id.pay);
        dine = (TextView) view.findViewById(R.id.dine);
        billouttax = (TextView) view.findViewById(R.id.billouttax);
        billoutsubtotal = (TextView) view.findViewById(R.id.billoutsubtotal);
        billouttotal = (TextView) view.findViewById(R.id.billouttotal);
        change_amount = (TextView) view.findViewById(R.id.txt_change_amount);
        cash_amount = (EditText) view.findViewById(R.id.edt_cash_amount);

        try {
            total = Double.parseDouble(CartStaticSragment.CARTStaticSragment.totalamountvalue.getText().toString());
        } catch (Exception e) {
            total = 0;
        }
        cash_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (s.equals("") || s.toString().trim().length() <= 0) {
                        change_amount.setText("0.00");
                    } else if (total > 0 && Double.parseDouble(s.toString()) > total) {
                        double change = Double.parseDouble(s.toString()) - total;
                        String grand_Amount = String.format("%.2f", change);
                        change_amount.setText(String.valueOf(grand_Amount));
                    } else
                        change_amount.setText("0.00");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        billouttax.setText(CartStaticSragment.CARTStaticSragment.totaltax.getText().

                toString());
        billouttotal.setText(CartStaticSragment.CARTStaticSragment.totalamountvalue.getText().

                toString());
        billoutsubtotal.setText(CartStaticSragment.CARTStaticSragment.grand);
        cart = new ArrayList<Cartpojo>();
        if (cart.size() != 0)

        {
            cart.clear();
        }

        Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:MM:SS");
        String nowDate = formatter.format(now.getTime());
        name.setText(user_Login.user_LoginID.nameid);
        terminal.setText(user_Login.user_LoginID.terminal);
        time.setText(nowDate.toString());
        Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().

                toString();
        if (Integer.parseInt(Group.GROUPID.table) < 1500)

        {
            dine.setText("DINE IN");
        } else

        {
            dine.setText("T/A");
        }

        db = getActivity().

                openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        if (CartStaticSragment.CARTStaticSragment.tableno.length() != 0)

        {
            Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND tableno=" + Group.GROUPID.table +
                    " AND billflag = 0;", null);
            int count = cartData.getCount();
            if (count > 0) {
                while (cartData.moveToNext()) {
                    i = i + 1;
                    Cartpojo cartpojo = new Cartpojo();
                    sno = cartData.getString(cartData.getColumnIndex("sno")).toString();
                    productname = cartData.getString(cartData.getColumnIndex("productname"));
                    producttype = cartData.getString(cartData.getColumnIndex("typename"));
                    totalprice = cartData.getString(cartData.getColumnIndex("totalprice"));
                    qty = cartData.getString(cartData.getColumnIndex("productqty"));
                    cartpojo.setPqty(qty);
                    cartpojo.setName(productname);
                    cartpojo.setType(producttype);
                    cartpojo.setTotal(totalprice);
                    cart.add(cartpojo);

                }
            }
        } else

        {
            AlertDialog.Builder msg = new AlertDialog.Builder(
                    getActivity(), android.R.style.Theme_Dialog);
            msg.setTitle("");
            msg.setMessage("Table Is Empty To Print ");
            msg.setPositiveButton("Ok", null);
            msg.show();
            guests.setText("");
            name.setText("");
            terminal.setText("");
        }
        db.close();
        orderid.setText("");
        table.setText(CartStaticSragment.CARTStaticSragment.tableno.getText().

                toString());
        guests.setText(CartStaticSragment.CARTStaticSragment.noofguests.getText().

                toString());
        billoutAdapater = new

                BilloutAdapater(getActivity());
        listView.setAdapter(billoutAdapater);
        billoutAdapater.notifyDataSetChanged();
        pay.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        getActivity(), android.R.style.Theme_Dialog);
                msg.setTitle("Amount Payable " + Group.GROUPID.finaltotal);
                msg.setMessage("Proceed to Pay The bill ?");
                msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.PBill) {
                            if (CartStaticSragment.CARTStaticSragment.tableno.getText().toString().length() > 0) {

                                addRowsToDisplay pay = new addRowsToDisplay();
                                pay.execute(Group.GROUPID.table.toString());
//                                send();
//
//                                Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
//                                db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
//                                Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid = " +
//                                        user_Login.user_LoginID.LOCATIONNO + " AND tableno = " + Group.GROUPID.table +
//                                        " AND billflag = 0 AND orderflag = 1;", null);
//                                int count = cartData.getCount();
//                                db.close();
//                                if (count > 0) {
//                                    Intent intent = new Intent(getActivity(), Payment.class);
//                                    intent.putExtra("TABLE", Group.GROUPID.table);
//                                    startActivity(intent);
//                                } else {
//                                    AlertDialog.Builder msg = new AlertDialog.Builder(
//                                            getActivity(), android.R.style.Theme_Dialog);
//                                    msg.setTitle("");
//                                    msg.setMessage("Table Is Empty To Pay ");
//                                    msg.setPositiveButton("Ok", null);
//                                    msg.show();
//                                }
                            }
                        } else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(
                                    getActivity(), android.R.style.Theme_Dialog);
                            msg.setTitle("Opration Failed");
                            msg.setMessage("User Doesn't have the permission for this opration");
                            msg.setPositiveButton("Ok", null);
                            msg.show();
                        }
                    }

                });
                msg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                msg.show();
            }
        });
        print.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                setPrint();
            }
        });
        return view;
    }


    private String editTable;
    float grandTotal = 0;
    String grand;

    private class addRowsToDisplay extends AsyncTask<String, Void, Boolean> {
        String editLocation = user_Login.user_LoginID.LOCATIONNO;


        @Override
        protected Boolean doInBackground(String... params) {
            editTable = params[0];
            Log.e("editTable", "=" + editTable);
            db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("UPDATE cart SET  billqty = orderqty-billedqty, billtypeqty = ordertypeqty-billedtypeqty WHERE tableno = " +
                    editTable + " AND locationid = " + editLocation + ";");

            Cursor c = db.rawQuery("SELECT * from cart WHERE locationid=" + editLocation + " AND tableno=" + editTable + " AND checkflag = 0 " +
                    " AND billflag = 0;", null);
            int count = c.getCount();
            c.moveToFirst();
            grandTotal = 0;
            for (Integer j = 0; j < count; j++) {
                int billtypeqty = c.getInt(c.getColumnIndex("ordertypeqty")) - c.getInt(c.getColumnIndex("billedtypeqty"));
                float total = (Float.parseFloat(c.getString(c.getColumnIndex("productprice"))) * Integer.parseInt(c.getString(c.getColumnIndex("billqty"))) +
                        Float.parseFloat(c.getString(c.getColumnIndex("typeprice"))) * billtypeqty);
                grandTotal = grandTotal + total;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                String Query = "SELECT productname,productprice,productqty,billqty,discount,billedqty,typename,typeqty,typeprice," +
                        "orderno,billtypeqty FROM cart WHERE tableno = " + editTable + " AND locationid= " + editLocation + " AND billflag= 0 AND checkflag=0 AND " +
                        "tbillflag=0 AND (billtypeqty != 0 OR billqty != 0) ;";
                Log.e("Query", "=" + Query);
                Cursor productDetails = db.rawQuery(Query, null);
                int count = productDetails.getCount();
                db.close();
                if (count > 0) {
                    Group.getInstance().totalAmount = grandTotal;
                    Group.getInstance().subTotalAmount = grandTotal;
                    cashpay();
                } else {
//                    AlertDialog.Builder adb = new AlertDialog.Builder(
//                            getActivity(), android.R.style.Theme_Dialog);
//                    adb.setTitle("");
//                    adb.setMessage("Products Not Found To Pay");
//                    adb.setCancelable(false);
//                    adb.setPositiveButton("Ok", new
//                            DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
                    db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                    db.execSQL("UPDATE cart SET checkflag = 0 WHERE locationid=" + editLocation + " AND tableno=" +
                            editTable + " AND checkflag = 1 AND billflag = 0;");
                    db.close();
                    Intent intent = new Intent(getActivity(), Home.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
//                                }
//                            });
//                    adb.show();
                }
            }
        }
    }

    public void cashpay() {
        float cash = 0;
        {
            if (Group.GROUPID.finaltotal == null) {
                payBill();
                cash = Float.parseFloat(Group.GROUPID.finaltotal);
            } else {
                payBill();
                cash = Float.parseFloat(Group.GROUPID.finaltotal);
            }
            if (cash > 0) {
                db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
                String datetime = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
                Cursor recieptno = db.rawQuery("SELECT * FROM receiptnos WHERE receipthead = '" + datetime + "' ;", null);
                if (recieptno.getCount() == 0) {
                    if (cash > 0) {
                        db.execSQL("INSERT INTO receiptnos VALUES('" + datetime + "',1);");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('R" + datetime + "1','CASH', '0'," + cash + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }

                    if (!Group.getInstance().voucherNumber.equals("0")) {
                        db.execSQL("UPDATE voucher SET usedflag = 1 WHERE voucherid = " + Group.getInstance().voucherNumber + ";");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('" + Group.getInstance().voucherNumber + "','VOUCHER', '" + Group.getInstance().voucherNumber + "'," +
                                Group.getInstance().voucherAmount + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }
                } else {
                    if (cash > 0) {
                        recieptno.moveToLast();
                        String newReceiptNo = "R" + recieptno.getString(0) + (recieptno.getInt(1) + (int) 1);
                        db.execSQL("UPDATE receiptnos SET receiptnumber = receiptnumber+1 WHERE receipthead= '" +
                                recieptno.getString(0) + "';");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('" + newReceiptNo + "','CASH', '0'," + cash + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");

                    }
                    if (!Group.getInstance().voucherNumber.equals("0")) {
                        db.execSQL("UPDATE voucher SET usedflag = 1 WHERE voucherid = " + Group.getInstance().voucherNumber + ";");
                        db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
                                "username,orderid,checkflag) VALUES('" + Group.getInstance().voucherNumber + "','VOUCHER', '" + Group.getInstance().voucherNumber + "'," +
                                Group.getInstance().voucherAmount + " ,'" + timeStamp + "','" + MainActivity.getInstance().USERNAME + "','" + Group.getInstance().ORDERNO + "',0 );");
                    }
                }

                db.execSQL("UPDATE cart SET discount = " + Group.getInstance().discountAmount +
                        " WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND tableno=" + editTable + " AND billflag = 0 AND orderflag = 1;");
                db.close();
                Intent intent = new Intent(getActivity(), Printer.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("TABLENO", Integer.parseInt(editTable));
                intent.putExtra("LOCATIONNO", Integer.parseInt(user_Login.user_LoginID.LOCATIONNO));
                intent.putExtra("FLAG", 1);
                startActivity(intent);
            }
        }
    }

    public void payBill() {
        double grandTotal = 0;
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid=" + user_Login.user_LoginID.LOCATIONNO + " AND tableno=" + Group.GROUPID.table +
                " AND billflag = 0;", null);
        int count = cartData.getCount();
        if (count > 0) {
            cartData.moveToLast();
            do {
                String productqty = cartData.getString(cartData.getColumnIndex("productqty"));
                String productprice = cartData.getString(cartData.getColumnIndex("productprice"));
                String typeprice = cartData.getString(cartData.getColumnIndex("typeprice"));
                String typeqty = cartData.getString(cartData.getColumnIndex("typeqty"));
                Double totalvalue = Double.valueOf((Float.parseFloat(productprice) *
                        Integer.parseInt(productqty))) + Double.valueOf(typeprice) * Double.valueOf(typeqty);
                grandTotal = grandTotal + totalvalue;
            } while (cartData.moveToPrevious());
        }
        db.close();
        db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        Cursor taxDetails = db.rawQuery("SELECT servicetaxper,taxper1,taxper2,taxper3 FROM tax;", null);
        int taxDetailsCount = taxDetails.getCount();
        double tax1 = 0;
        if (taxDetailsCount > 0) {
            taxDetails.moveToFirst();            //Here we are inserting the tax information into the structure
            tax1 = taxDetails.getFloat(taxDetails.getColumnIndex("servicetaxper"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper1"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper2"));
            tax1 += taxDetails.getFloat(taxDetails.getColumnIndex("taxper3"));

        } else {
        }
        db.close();
        grand = String.format("%.2f", grandTotal);
        double taxAmount = (tax1 * grandTotal) / 100;
        String grand_tax = String.format("%.2f", taxAmount);
        grandTotal += (tax1 * grandTotal) / 100;
        String grand_Amount = String.format("%.2f", grandTotal);
        Group.GROUPID.finaltotal = grand_Amount;
    }

    public void setPrint() {
        if (user_Login.user_LoginID.LOCATIONNO.equalsIgnoreCase(user_Login.user_LoginID.LOCATIONNO)) {
            if (user_Login.user_LoginID.userpermissionmenu >= user_Login.user_LoginID.BPrint) {


                if (CartStaticSragment.CARTStaticSragment.tableno.getText().toString().length() > 0) {
                    Group.GROUPID.table = CartStaticSragment.CARTStaticSragment.tableno.getText().toString();
                    db = getActivity().openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);

                    Cursor cartData = db.rawQuery("SELECT * from cart WHERE locationid = " +
                            user_Login.user_LoginID.LOCATIONNO + " AND tableno = " + Group.GROUPID.table +";", null);
//                            " AND billflag = 0 AND orderflag = 1;", null);
                    int count = cartData.getCount();
                    db.close();
                    if (count > 0) {
                        Intent intent = new Intent(getActivity(), Printer.class);
                        intent.putExtra("TABLENO", Integer.parseInt(CartStaticSragment.CARTStaticSragment.tableno.getText().toString()));
                        intent.putExtra("LOCATIONNO", Integer.parseInt(user_Login.user_LoginID.LOCATIONNO));
                        intent.putExtra("FLAG", 0);
                        startActivity(intent);

                    } else {
                        AlertDialog.Builder msg = new AlertDialog.Builder(
                                getActivity(), android.R.style.Theme_Dialog);
                        msg.setTitle("");
                        msg.setMessage("Table Is Empty To Print ");
                        msg.setPositiveButton("Ok", null);
                        msg.show();
                    }
                }
            } else {
                AlertDialog.Builder msg = new AlertDialog.Builder(
                        getActivity(), android.R.style.Theme_Dialog);
                msg.setTitle("Opration Failed ");
                msg.setMessage("User Doesn't have the permission for this opration");
                msg.setPositiveButton("Ok", null);
                msg.show();
            }
        } else {
            AlertDialog.Builder msg = new AlertDialog.Builder(
                    getActivity(), android.R.style.Theme_Dialog);
            msg.setTitle("");
            msg.setMessage("Location Does not match to Print " + "\n Current Loc:" + user_Login.user_LoginID.LOCATIONNO + "\n Table Loc:" + user_Login.user_LoginID.LOCATIONNO);
            msg.setPositiveButton("Ok", null);
            msg.show();
        }
    }

    static class ViewHolder {
        TextView billoutname, bilouttype, billoutamount;
        TextView sno;
    }

    public class BilloutAdapater extends BaseAdapter {
        private Activity activity;
        private LayoutInflater inflater = null;

        public BilloutAdapater(Activity a) {
            activity = a;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return cart.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            ViewHolder holder = null;
            if (vi == null) {
                // if it's not recycled, initialize some attributes
                final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                vi = inflater.inflate(R.layout.billoutadapter, parent, false);
                holder = new ViewHolder();
                holder.billoutname = (TextView) vi.findViewById(R.id.billoutitemname);
                holder.billoutamount = (TextView) vi.findViewById(R.id.billoutamount);
                holder.bilouttype = (TextView) vi.findViewById(R.id.billouttype);
                holder.sno = (TextView) vi.findViewById(R.id.sno);
                vi.setTag(holder);

            } else {
                holder = (ViewHolder) vi.getTag();
            }
            holder.sno.setText(cart.get(position).getPqty());
            holder.billoutname.setText(cart.get(position).getName());
            if (cart.get(position).getType().equals("NO TYPE")) {
                holder.bilouttype.setVisibility(View.GONE);
            } else {
                holder.bilouttype.setVisibility(View.VISIBLE);
                holder.bilouttype.setText(cart.get(position).getType());
            }
            holder.billoutamount.setText(cart.get(position).getTotal());

            return vi;
        }
    }
}



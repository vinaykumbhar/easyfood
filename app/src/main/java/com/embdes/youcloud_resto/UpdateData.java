package com.embdes.youcloud_resto;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.MotionEvent;

import com.instabug.library.InstabugTrackingDelegate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class UpdateData extends Activity {
    public static final int ptype = 0;
    private ProgressDialog pDialog;
    public static int max, max2, max3, max4, max5, max6, max7;
    public static int count = 0;
    String message;
    String receivedMessage;
    public static SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new updatedata().execute("Sucess");
    }

    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null || user_Login.user_LoginID == null) {
            Intent userLoginIntent = new Intent(); //Created new Intent to
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(userLoginIntent);
            this.finish();
        }

    }

    ;

    /**
     * This will scan all the files and set the maximum value for progress bar.
     * It will pop up respective dialog based on id.
     */
    @SuppressWarnings({"resource", "unused"})
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case ptype:
                String cardName = Environment.getExternalStorageDirectory().getPath();
                int count = 0;
                pDialog = new ProgressDialog(UpdateData.this, android.R.style.Theme_Dialog);
                pDialog.setMessage("Uploading Data...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                try {
                    Scanner scanner1 = new Scanner(new File(cardName + "/pos",
                            "tables.csv"));
                    while (scanner1.hasNextLine()) {
                        String line = scanner1.nextLine();
                        max = count++;
                    }
                    Scanner scanner2 = new Scanner(new File(cardName + "/pos", "product.csv"));
                    while (scanner2.hasNextLine()) {
                        String line = scanner2.nextLine();
                        max2 = count++;
                    }
                    Scanner scanner3 = new Scanner(new File(cardName + "/pos", "userterminal.csv"));
                    while (scanner3.hasNextLine()) {
                        String line = scanner3.nextLine();
                        max3 = count++;
                    }
                    Scanner scanner4 = new Scanner(new File(cardName + "/pos", "location.csv"));
                    while (scanner4.hasNextLine()) {
                        String line = scanner4.nextLine();
                        max4 = count++;
                    }
                    Scanner scanner5 = new Scanner(new File(cardName + "/pos", "group.csv"));
                    while (scanner5.hasNextLine()) {
                        String line = scanner5.nextLine();
                        max5 = count++;
                    }
                    Scanner scanner6 = new Scanner(new File(cardName + "/pos", "category.csv"));
                    while (scanner6.hasNextLine()) {
                        String line = scanner6.nextLine();
                        count++;
                    }

                    Scanner scanner7 = new Scanner(new File(cardName + "/pos", "type.csv"));
                    while (scanner7.hasNextLine()) {
                        String line = scanner7.nextLine();
                        count++;
                    }

                    Scanner scanner8 = new Scanner(new File(cardName + "/pos", "locationprinter.csv"));
                    while (scanner8.hasNextLine()) {
                        String line = scanner8.nextLine();
                        count++;
                    }

                    Scanner scanner9 = new Scanner(new File(cardName + "/pos", "locationprice.csv"));
                    while (scanner9.hasNextLine()) {
                        String line = scanner9.nextLine();
                        count++;

                    }
                } catch (Exception e) {
                }
                pDialog.setMax(count);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    class updatedata extends AsyncTask<String, String, String>

    {
        @SuppressWarnings("deprecation")
        @Override
        protected void onPreExecute() {
            showDialog(ptype);
        }

        @Override
        protected String doInBackground(String... params) {

            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor login = db.rawQuery("SELECT * from masterterminal;", null);
            int registrationCount = login.getCount();
            if (registrationCount > 0) {
                login.moveToNext();
                MainActivity.getInstance().outletid = login.getInt(login.getColumnIndex("terminalid"));
                MainActivity.getInstance().ipAddress = login.getString(login.getColumnIndex("ipaddress"));
                MainActivity.getInstance().portNumber = login.getInt(login.getColumnIndex("portno"));
            }

            try {
                count = 0;
                db.execSQL("DROP TABLE IF EXISTS product;");
                db.execSQL("DROP TABLE IF EXISTS userterminal;");
                db.execSQL("DROP TABLE IF EXISTS loginrecord;");
                db.execSQL("Drop Table IF EXISTS location;");
                db.execSQL("DROP TABLE IF EXISTS masterterminal;");
                db.execSQL("Drop Table IF EXISTS grouptable;");
                db.execSQL("Drop Table IF EXISTS category;");
                db.execSQL("DROP TABLE IF EXISTS type;");
                db.execSQL("DROP TABLE IF EXISTS tables;");
                db.execSQL("DROP TABLE IF EXISTS receipt;");
                db.execSQL("DROP TABLE IF EXISTS ordernos;");
                db.execSQL("DROP TABLE IF EXISTS cart;");
                db.execSQL("DROP TABLE IF EXISTS receiptnos;");
                db.execSQL("DROP TABLE IF EXISTS locationprinter;");
                db.execSQL("DROP TABLE IF EXISTS locationprice;");
                db.execSQL("DROP TABLE IF EXISTS queuetable;");
                db.execSQL("DROP TABLE IF EXISTS color;");
                db.execSQL("DROP TABLE IF EXISTS discount;");
                db.execSQL("DROP TABLE IF EXISTS consumerinfo;");
                db.execSQL("DROP TABLE IF EXISTS deliveryboy;");
                db.execSQL("CREATE TABLE if not exists queuetable(sno Integer PRIMARY KEY AUTOINCREMENT, message VARCHAR[5000]);");

                db.execSQL("CREATE TABLE if not exists ordernos(sno INTEGER NOT NULL ,locationid INTEGER NOT NULL, tableno INTEGER NOT NULL," +
                        " orderno VARCHAR[15], billflag INTEGER, orderflag INTEGER);");

                db.execSQL("CREATE TABLE if not exists receiptnos(receipthead VARCHAR[20] NOT NULL, receiptnumber INTEGER NOT NULL);");

                db.execSQL("CREATE TABLE if not exists loginrecord(userid INTEGER NOT NULL,datetime VARCHAR[35] NOT NULL" +
                        " ,openingcash DECIMAL(15.2) NOT NULL,locationid INTEGER NOT NULL,updateflag INTEGER NOT NULL);");

                db.execSQL("CREATE TABLE if not exists receipt(receiptno VARCHAR[20] NOT NULL,docname VARCHAR[35] NOT NULL" +
                        ",docid VARCHAR[35] NOT NULL,amount DECIMAL(9.2) NOT NULL,datetime VARCHAR[35] NOT NULL,username VARCHAR[35] NOT NULL," +
                        "orderid VARCHAR[35] NOT NULL,checkflag INTEGER NOT NULL);");

                db.execSQL("CREATE TABLE if not exists tax(tinno VARCHAR[35] NOT NULL,ctsno VARCHAR[35] NOT NULL,servicetaxno VARCHAR[35] NOT NULL" +
                        ",servicetaxper FLOAT NOT NULL,taxper1 FLOAT NOT NULL,taxper2 FLOAT NOT NULL,taxper3 FLOAT NOT NULL);");

                db.execSQL("CREATE TABLE if not exists voucher(voucherid INTEGER NOT NULL,vouchervalues DECIMAL(9.2) NOT NULL," +
                        "expdate VARCHAR[35] NOT NULL,vouchertype VARCHAR[35] NOT NULL,usedflag INTEGER NOT NULL);");

                db.execSQL("CREATE TABLE if not exists tables(locationid INTEGER NOT NULL,tableno INTEGER NOT NULL,noofchairs INTEGER NOT NULL,"+
                        "occupiedchairs INTEGER NOT NULL,freechairs INTEGER NOT NULL," +
                        "fullybooked INTEGER NOT NULL,colorid INTEGER NOT NULL,orderid VARCHAR[15]);");

                db.execSQL("CREATE TABLE if not exists type(typeid INTEGER NOT NULL,typename VARCHAR[35] NOT NULL,price DECIMAL(9.2) NOT NULL);");

                db.execSQL("CREATE TABLE if not exists product(productid INTEGER NOT NULL,categoryid INTEGER NOT NULL," +
                        "groupid INTEGER NOT NULL,productdescriptionterminal VARCHAR[35] NOT NULL,productdescriptionkitchen VARCHAR[35] NOT NULL," +
                        "productdescriptionbill VARCHAR[35] NOT NULL,active INTEGER NOT NULL,type VARCHAR[35],price1 DECIMAL(9.2) NOT NULL,price2 DECIMAL(9.2) NOT NULL," +
                        "price3 DECIMAL(9.2) NOT NULL,taxper DECIMAL(9.2) NOT NULL,available INTEGER NOT NULL,foodid INTEGER NOT NULL,discountper DECIMAL(9.2) NOT NULL,productdescription VARCHAR[1000] NOT NULL,foodimages VARCHAR[10] NOT NULL);");

                db.execSQL("CREATE TABLE if not exists userterminal(userid INTEGER NOT NULL,username VARCHAR[35] NOT NULL,password VARCHAR[35] NOT NULL,permissionsmenu " +
                        "INTEGER NOT NULL,permissionterminal INTEGER NOT NULL,permissionslocation INTEGER NOT NULL);");

                db.execSQL("CREATE TABLE if not exists loginrecord(userid INTEGER NOT NULL,datetime VARCHAR[35] NOT NULL,openingcash DECIMAL(9.2) NOT NULL," +
                        "locationid INTEGER NOT NULL,updateflag INTEGER NOT NULL);");

                db.execSQL("CREATE TABLE if not exists location(locationid INTEGER NOT NULL,locationname VARCHAR[35] NOT NULL," +
                        "locationprinterid VARCHAR[35] NOT NULL,permissionslocation INTEGER NOT NULL);");

                db.execSQL("CREATE TABLE if not exists grouptable(groupname VARCHAR[35] NOT NULL,groupid INTEGER NOT NULL," +
                        "imageurl VARCHAR[25] NOT NULL,discountper DECIMAL(9.2));");

                db.execSQL("CREATE TABLE if not exists category(categoryname VARCHAR[35] NOT NULL," +
                        "categoryid INTEGER NOT NULL,groupid INTEGER NOT NULL,imageurl VARCHAR[25] NOT NULL);");

                db.execSQL("CREATE TABLE if not exists consumerinfo(cid Integer PRIMARY KEY AUTOINCREMENT,consumerid VARCHAR[10]," +
                        "cname VARCHAR[20] NOT NULL,cmobileno VARCHAR[10] NOT NULL,caddress VARCHAR[35] NOT NULL);");

                db.execSQL("CREATE TABLE if not exists deliveryboy(deliveryboyid INTEGER PRIMARY KEY," +
                        "dname VARCHAR[20] NOT NULL,terminalid INTEGER NOT NULL,mobileno VARCHAR[10] NOT NULL);");

                db.execSQL("CREATE TABLE if not exists cart(sno Integer PRIMARY KEY AUTOINCREMENT,productname VARCHAR[35] NOT NULL," +
                        "typename VARCHAR[35] NOT NULL,productprice DECIMAL(9.2) NOT NULL,typeprice DECIMAL(9.2) NOT NULL," +
                        "productqty INTEGER NOT NULL,typeqty INTEGER NOT NULL,totalprice DECIMAL(15.2) NOT NULL,orderqty INTEGER NOT NULL,ordertypeqty INTEGER NOT NULL," +
                        "orderflag INTEGER NOT NULL,billqty INTEGER NOT NULL,billtypeqty INTEGER NOT NULL,billedqty INTEGER NOT NULL,billedtypeqty INTEGER NOT NULL," +
                        "billflag INTEGER NOT NULL,locationid INTEGER NOT NULL,tableno INTEGER NOT NULL,covers INTEGER NOT NULL," +
                        "checkflag INTEGER NOT NULL,orderno VARCHAR[15] NOT NULL,tdiscount DECIMAL(9.2) NOT NULL," +
                        "discount DECIMAL(9.2) NOT NULL,totaltax DECIMAL(9.2) NOT NULL,datetime VARCHAR[25] NOT NULL," +
                        "tbillflag INTEGER NOT NULL,pgroup VARCHAR[35],pcategory VARCHAR[35],username VARCHAR[35],foodid INTEGER NOT NULL,consumerid INTEGER NOT NULL);");

                db.execSQL("CREATE TABLE if not exists masterterminal(terminalid INTEGER PRIMARY KEY NOT NULL," +
                        "terminalname VARCHAR[35] NOT NULL,shortname VARCHAR[10] NOT NULL,checkid INTEGER NOT NULL," +
                        "ipaddress VARCHAR[25] NOT NULL,portno INTEGER NOT NULL,imeino VARCHAR[20] NOT NULL,openingcash DECIMAL(15.2)," +
                        "currentcash DECIMAL(15.2),lastloginusername VARCHAR[20],logindate VARCHAR[20],cashdate VARCHAR[10] NOT NULL," +
                        "terminalpermisiions INTEGER NOT NULL,webadd VARCHAR[35] NOT NULL,facebookid  VARCHAR[35] NOT NULL," +
                        "twitterid  VARCHAR[35] NOT NULL,email  VARCHAR[35] NOT NULL,add1  VARCHAR[35] NOT NULL, " +
                        "add2  VARCHAR[35] NOT NULL, add3  VARCHAR[35] NOT NULL, city  VARCHAR[20] NOT NULL, pinno  VARCHAR[10] NOT NULL," +
                        " mobileno VARCHAR[10] NOT NULL,message1  VARCHAR[20] NOT NULL, message2 VARCHAR[20] NOT NULL);");

                db.execSQL("CREATE TABLE if not exists locationprinter(foodid INTEGER NOT NULL,locationorderid INTEGER NOT NULL," +
                        "ipaddress VARCHAR[35] NOT NULL,portno INTEGER NOT NULL, btprintername VARCHAR[150] );");

                db.execSQL("CREATE TABLE if not exists locationprice(locationid INTEGER NOT NULL,price INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE if not exists locationprinter(foodid INTEGER NOT NULL,locationorderid INTEGER NOT NULL," +
                        "ipaddress VARCHAR[35] NOT NULL,portno INTEGER NOT NULL);");

                db.execSQL("CREATE TABLE if not exists locationprice(locationid INTEGER NOT NULL,price INTEGER NOT NULL);");
                db.execSQL("CREATE TABLE if not exists color(colorid INTEGER NOT NULL,colorname VARCHAR[20] NOT NULL,status VARCHAR[20] NOT NULL);");
                db.execSQL("CREATE TABLE if not exists discount(discountid INTEGER NOT NULL,maxamount  FLOAT NOT NULL,"
                        + "minamount FLOAT NOT NULL,discountper FLOAT NOT NULL,discountvalue FLOAT NOT NULL,"
                        + "val_per FLOAT NOT NULL,working FLOAT NOT NULL);");


                String cardName = Environment.getExternalStorageDirectory().getPath();
                BufferedReader tables_bf = new BufferedReader(new FileReader(cardName + "/pos/tables.csv"));
                String line;
                while ((line = tables_bf.readLine()) != null) {
                    count++;
                    String[] value = line.split(",");    // seperator
                    db.execSQL("INSERT INTO tables VALUES(" + value[0] + "," + value[1] + "," +
                            value[2] + "," + value[3] + "," + value[4] + "," + value[5] + "," + value[6] + "," + value[7] + ");");
                    publishProgress("" + count);

                }

                tables_bf.close();
                BufferedReader product_bf = new BufferedReader(new FileReader(cardName + "/pos/product.csv"));
                String productline;
                while ((productline = product_bf.readLine()) != null) {
                    count++;
                    String[] value = productline.split(",");    // seperator
                    db.execSQL("INSERT INTO product VALUES(" + value[0] + "," + value[1] + "," + value[2] + ",'" + value[3] + "','"
                            + value[4] + "','" + value[5] + "'," + value[6] + ",'" + value[7] + "'," + value[8] + "," + value[9]
                            + "," + value[10] + "," + value[11] + "," + value[12] + "," + value[13] + "," + value[14] + ",'" + value[15] + "','" + value[16] + "');");

                    publishProgress("" + count);
                }

                product_bf.close();
                BufferedReader user_bf = new BufferedReader(new FileReader(cardName + "/pos/userterminal.csv"));
                {

                    String user_count;
                    while ((user_count = user_bf.readLine()) != null) {
                        count++;
                        String[] value = user_count.split(",");    // seperator
                        db.execSQL("INSERT INTO userterminal VALUES(" + value[0] + ",'" + value[1] + "','" + value[2] + "'," + value[3] + ","
                                + value[4] + "," + value[5] + ");");

                        publishProgress("" + count);
                    }

                    user_bf.close();
                }

                BufferedReader location_bf = new BufferedReader(new FileReader(cardName + "/pos/location.csv"));
                {

                    String location_count;
                    while ((location_count = location_bf.readLine()) != null) {
                        count++;
                        String[] value = location_count.split(",");    // seperator
                        db.execSQL("INSERT INTO location VALUES(" + value[0] + ",'" + value[1] + "','" + value[2] + "'," + value[3] + ");");
                        publishProgress("" + count);
                    }
                    location_bf.close();
                }
                BufferedReader group_bf = new BufferedReader(new FileReader(cardName + "/pos/group.csv"));
                {

                    String group_count;
                    while ((group_count = group_bf.readLine()) != null) {
                        count++;
                        String[] value = group_count.split(",");    // seperator
                        db.execSQL("INSERT INTO grouptable VALUES('" + value[0] + "'," + value[1] + ",'" + value[2] + "'," + value[3] + ");");
                        publishProgress("" + count);
                    }
                    group_bf.close();
                }

                BufferedReader category_bf = new BufferedReader(new FileReader(cardName + "/pos/category.csv"));
                {

                    String category_count;
                    while ((category_count = category_bf.readLine()) != null) {
                        count++;
                        String[] value = category_count.split(",");    // seperator
                        db.execSQL("INSERT INTO category VALUES('"+value[0]+"',"+value[1]+","+value[2]+",'"+value[3]+"');");
                        publishProgress("" + count);
                    }
                    category_bf.close();
                }

                BufferedReader type_bf = new BufferedReader(new FileReader(cardName + "/pos/type.csv"));
                {
                    String type_count;
                    while ((type_count = type_bf.readLine()) != null) {
                        count++;
                        String[] value = type_count.split(",");    // seperator
                        db.execSQL("INSERT INTO type VALUES(" + value[0] + ",'" + value[1] + "'," + value[2] + ");");

                        publishProgress("" + count);
                    }
                    type_bf.close();
                }

                BufferedReader tax_bf = new BufferedReader(new FileReader(cardName + "/pos/tax.csv"));
                {
                    String tax_count;
                    while ((tax_count = tax_bf.readLine()) != null) {
                        count++;
                        String[] value = tax_count.split(",");    // seperator
                        db.execSQL("INSERT INTO tax VALUES('" + value[0] + "','" + value[1] + "','" + value[2] +
                                "'," + value[3] + "," + value[4] + "," + value[5] + "," + value[6] + ");");
                        publishProgress("" + count);
                    }
                }
                tax_bf.close();

                BufferedReader voucher_bf = new BufferedReader(new FileReader(cardName + "/pos/voucher.csv"));
                {
                    String voucher_count;
                    while ((voucher_count = voucher_bf.readLine()) != null) {
                        count++;
                        String[] value = voucher_count.split(",");    // seperator
                        db.execSQL("INSERT INTO voucher VALUES(" + value[0] + "," + value[1] + ",'" +
                                value[2] + "','" + value[3] + "'," + value[4] + ");");
                        publishProgress("" + count);
                    }
                    voucher_bf.close();
                }

                BufferedReader locationprinter_bf = new BufferedReader(new FileReader(cardName + "/pos/locationprinter.csv"));
                {
                    String locationprinter_count;
                    while ((locationprinter_count = locationprinter_bf.readLine()) != null) {
                        count++;
                        String[] value = locationprinter_count.split(",");
                        db.execSQL("INSERT INTO locationprinter VALUES(" + value[0] + "," + value[1] +
                                ",'" + value[2] + "'," + value[3] + ",'Empty\n00:00:00:00:00');");
                        publishProgress("" + count);
                    }
                    locationprinter_bf.close();
                }

                BufferedReader locationprice_bf = new BufferedReader(new FileReader(cardName + "/pos/locationprice.csv"));
                {
                    String locationprice_count;
                    while ((locationprice_count = locationprice_bf.readLine()) != null) {
                        count++;
                        String[] value = locationprice_count.split(",");    // seperator
                        db.execSQL("INSERT INTO locationprice VALUES(" + value[0] + "," + value[1] + ");");
                        publishProgress("" + count);
                    }
                    locationprice_bf.close();
                }
                BufferedReader discount_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                        "/pos/discount.csv"));
                {
                    String discount_count;

                    while ((discount_count = discount_bf.readLine()) != null) {
                        count++;
                        String[] value = discount_count.split(",");    // seperator
                        db.execSQL("INSERT INTO discount VALUES(" + value[0] + "," + value[1] + "," + value[2] + "," +
                                value[3] + "," + value[4]
                                + "," + value[5] + "," + value[6] + ");");
                        publishProgress("" + count);
                    }
                    discount_bf.close();
                }
                BufferedReader color_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                        "/pos/color.csv"));
                {
                    String color_count;
                    while ((color_count = color_bf.readLine()) != null) {
                        count++;
                        String[] value = color_count.split(",");    // seperator
                        db.execSQL("INSERT INTO color VALUES(" + value[0] + ",'" + value[1] + "','" + value[2] + "');");
                        publishProgress("" + count);
                    }
                    color_bf.close();
                }
                db.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String file_url) {
            dismissDialog(ptype);
            Intent mainIntent = new Intent();
            mainIntent.setClass(UpdateData.this, MainActivity.class);
            startActivity(mainIntent);
        }
    }

    public void insertMasterTerminalData() {
        String[] receiveMessage = receivedMessage.split("\\|");
        if (receiveMessage.length == 25) {
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("INSERT INTO masterterminal values(" + receiveMessage[0] + ", '" + receiveMessage[1] + "' , '" +
                    receiveMessage[2] + "' , " + receiveMessage[3] + " , '" + receiveMessage[4] + "' ," + receiveMessage[5] + ", '" +
                    receiveMessage[6] + "'," + receiveMessage[7] + "," + receiveMessage[8] + " ,'" + receiveMessage[9] +
                    "','" + receiveMessage[10] + "','" + receiveMessage[11] + "'," + receiveMessage[12] + ",'" +
                    receiveMessage[13] + "','" + receiveMessage[14] + "','" + receiveMessage[15] + "'" +
                    ",'" + receiveMessage[16] + "','" + receiveMessage[17] + "','" + receiveMessage[18] + "','" +
                    receiveMessage[19] + "','" + receiveMessage[20] + "','" + receiveMessage[21] + "','" +
                    receiveMessage[22] + "','" + receiveMessage[23] + "','" + receiveMessage[24] + "');");
            db.close();
        } else {
        }
    }

    public void insertLocationPrice() {
        BufferedReader user_bf;
        try {

            user_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/locationprice.csv"));
            String user_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM locationprice");
            db.close();
            while ((user_count = user_bf.readLine()) != null) {
                String[] value = user_count.split(",");    // seperator
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                db.execSQL("INSERT INTO locationprice VALUES(" + value[0] + "," + value[1] + ");");
                db.close();
            }
            user_bf.close();
            MainActivity.getInstance().message = "OK";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        }

    }

    public void insertTaxData() {
        BufferedReader tax_bf;
        try {


            tax_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/tax.csv"));
            String tax_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM tax");

            while ((tax_count = tax_bf.readLine()) != null) {

                String[] value = tax_count.split(",");    // seperator
                db.execSQL("INSERT INTO tax VALUES('" + value[0] + "','" + value[1] + "','" + value[2] + "'," +
                        value[3] + "," + value[4]
                        + "," + value[5] + "," + value[6] + ");");
            }
            tax_bf.close();
            db.close();

            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertDiscountData() {
        BufferedReader discount_bf;
        try {


            discount_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/discount.csv"));
            String discount_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM discount");

            while ((discount_count = discount_bf.readLine()) != null) {

                String[] value = discount_count.split(",");    // seperator
                db.execSQL("INSERT INTO discount VALUES(" + value[0] + "," + value[1] + "," + value[2] + "," +
                        value[3] + "," + value[4]
                        + "," + value[5] + "," + value[6] + ");");
            }
            discount_bf.close();
            db.close();

            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }

    }

    @SuppressWarnings("unused")
    public void insertProductData() {
        BufferedReader product_bf = null;
        db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
        try {
            int count = 0;
            product_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/product.csv"));
            String productline;
            db.execSQL("DELETE FROM product");

            while ((productline = product_bf.readLine()) != null) {
                String[] value = productline.split(",");    // seperator
                db.execSQL("INSERT INTO product VALUES(" + value[0] + "," + value[1] + "," + value[2] + ",'" + value[3] + "','"
                        + value[4] + "','" + value[5] + "'," + value[6] + ",'" + value[7] + "'," + value[8] + "," + value[9]
                        + "," + value[10] + "," + value[11] + "," + value[12] + "," + value[13] + "," + value[14] + ",'" + value[15] + "','" + value[16] + "');");

            }


            product_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }

    }


    public void insertTablesData() {
        BufferedReader tables_bf;
        try {
            tables_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/tables.csv"));
            String line;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM tables");
            db.close();
            while ((line = tables_bf.readLine()) != null) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                String[] value = line.split(",");    // seperator
                db.execSQL("INSERT INTO tables VALUES(" + value[0] + "," + value[1] + "," + value[2] + "," + value[3] + ","
                        + value[4] + "," + value[5] + "," + value[6] + ");");

                db.close();
            }
            tables_bf.close();
            db.close();
            message = "OK";

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }


    }

    public void insertUserterminalData() {
        BufferedReader user_bf;
        try {
            user_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/userterminal.csv"));

            String user_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM userterminal");
            db.close();
            while ((user_count = user_bf.readLine()) != null) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);

                String[] value = user_count.split(",");    // seperator
                db.execSQL("INSERT INTO userterminal VALUES(" + value[0] + ",'" + value[1] + "','" + value[2] + "'," + value[3] + ","
                        + value[4] + "," + value[5] + ");");
                db.close();

            }

            user_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertLocationData() {

        BufferedReader location_bf;
        try {
            location_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/location.csv"));
            String location_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM location");
            db.close();
            while ((location_count = location_bf.readLine()) != null) {
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);

                String[] value = location_count.split(",");    // seperator
                db.execSQL("INSERT INTO location VALUES(" + value[0] + ",'" + value[1] + "','" + value[2] + "'," + value[3] + ");");


                db.close();
            }
            location_bf.close();

            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertColorData() {
        BufferedReader color_bf;
        try {
            color_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/color.csv"));
            String color_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM color");
            while ((color_count = color_bf.readLine()) != null) {
                String[] value = color_count.split(",");    // seperator
                db.execSQL("INSERT INTO color VALUES(" + value[0] + ",'" + value[1] + "','" + value[2] + "');");
            }
            color_bf.close();
            db.close();
            message = "OK";

            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }

    }

    public void insertGroupData() {

        BufferedReader group_bf;
        try {
            group_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/group.csv"));
            String group_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM grouptable");
            while ((group_count = group_bf.readLine()) != null) {
                String[] value = group_count.split(",");    // seperator
                db.execSQL("INSERT INTO grouptable VALUES('" + value[0] + "'," + value[1] + ",'" + value[2] + "'," + value[3] + ");");
            }
            group_bf.close();
            db.close();
            message = "OK";

            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertCategoryData() {
        @SuppressWarnings("unused")
        int count = 0;
        BufferedReader category_bf;
        try {
            category_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/category.csv"));
            String category_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM category");
            while ((category_count = category_bf.readLine()) != null) {

                String[] value = category_count.split(",");    // seperator
                db.execSQL("INSERT INTO category VALUES('" + value[0] + "'," + value[1] + "," + value[2] + "," + value[3] + ");");

            }
            category_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertTypeData() {
        BufferedReader type_bf;
        try {

            type_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/type.csv"));
            String type_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM type");
            while ((type_count = type_bf.readLine()) != null) {

                String[] value = type_count.split(",");    // seperator
                db.execSQL("INSERT INTO type VALUES(" + value[0] + ",'" + value[1] + "'," + value[1] + ");");
            }
            type_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    public void insertLocationPrinter() {
        BufferedReader user_bf;
        try {
            user_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/locationprinter.csv"));
            String user_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM locationprinter");
            db.close();
            while ((user_count = user_bf.readLine()) != null) {
                String[] value = user_count.split(",");    // seperator
                db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
                db.execSQL("INSERT INTO locationprinter VALUES(" + value[0] + "," + value[1] + ",'" +
                        value[2] + "'," + value[3] + ", 'Empty\n00:00:00:00:00:00');");
                db.close();
            }
            user_bf.close();
            MainActivity.getInstance().message = "OK";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            MainActivity.getInstance().message = "ERROR";
        }


    }

    public void insertVoucherData() {
        BufferedReader voucher_bf;
        try {
            voucher_bf = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath() +
                    "/pos/voucher.csv"));
            String voucher_count;
            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            db.execSQL("DELETE FROM voucher");
            while ((voucher_count = voucher_bf.readLine()) != null) {

                String[] value = voucher_count.split(",");    // seperator
                db.execSQL("INSERT INTO voucher VALUES(" + value[0] + "," + value[1] + ",'" + value[2] + "','" + value[3] +
                        "'," + value[4] + ");");
            }
            voucher_bf.close();
            db.close();
            message = "OK";
            return;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (SQLException e) {
            e.printStackTrace();
            message = "ERROR";
        } catch (IOException e) {
            e.printStackTrace();
            message = "ERROR";
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.embdes.model.GroupData;
import com.instabug.library.InstabugTrackingDelegate;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PrintReportsActivity extends Activity {


    //// ���뷽ʽ
    SQLiteDatabase db;
    BluetoothDevice device;
    String btPrinterid;
    /**
     * printing text align left
     */
    public static final int AT_LEFT = 0;
    /**
     * printing text align center
     */
    public static final int AT_CENTER = 1;
    /**
     * printing text align right
     */
    public static final int AT_RIGHT = 2;
    boolean bConnect = true;
    String fdot1;

    class headerDetails {
        String title;
        String firstDate;
        String lastDate;
    }

    BluetoothAdapter mBluetoothAdapter;
    private static final int REQUEST_ENABLE_BT = 2;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    InputStream mmInStream;
    static OutputStream mmOutStream;
    BluetoothSocket mmSocket;
    public static Socket mysocket = null;
    private static int SERVERPORT = 9100;
    private static String SERVER_IP = "192.168.1.87";
    Dialog settingDialog;
    ListView lv;

    String printdata;

    ArrayAdapter<BluetoothDevice> adapter;
    ArrayList<BluetoothDevice> connections = new ArrayList<BluetoothDevice>();
    ArrayAdapter<String> adaptername;
    ArrayList<String> connectionsname = new ArrayList<String>();
    String globledevice;
    int mReportsFlag;
    String mstartDate, mendDate, foodtype;
    boolean date_flag = false;
    boolean printerDeviceCheck = false;
    private static Reports REPORTSID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_reports);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String startDate = bundle.getString("FROMDATE");
        String endDate = bundle.getString("TODATE");
        //int ReportsFlag = bundle.getInt("REPORTSFLAG");
        date_flag = bundle.getBoolean("DATEFLAG");
        foodtype = bundle.getString("FOODTYPE");
        mstartDate = startDate;
        mendDate = endDate;
        //mReportsFlag = ReportsFlag;
        fdot1 = String.format("-------------------------------\n");

        if (true == MainActivity.getInstance().isWifiPrinterEnable) {

            String strName = "192.168.1.92:5500";

            db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
            Cursor printerDetails = db.rawQuery("SELECT ipaddress, portno FROM " +
                    "locationprinter" + ";", null);//WHERE locationorderid = "+user_Login.user_LoginID.LOCATIONNO

            int printerDetailsCount = printerDetails.getCount();
            if (printerDetailsCount > 0) {
                printerDetails.moveToFirst();
                strName = printerDetails.getString(0) + ":" + printerDetails.getString(1);
                SERVER_IP = printerDetails.getString(0);
                SERVERPORT = Integer.parseInt(printerDetails.getString(1));
            }
            db.close();
            if (strName.length() == 0) {
                Toast.makeText(PrintReportsActivity.this, "Error:port name empty", Toast.LENGTH_SHORT).show();
                return;
            }

            Toast.makeText(PrintReportsActivity.this, strName, Toast.LENGTH_SHORT).show();

            if (bConnect) {
                try {
                    if (mysocket == null) {
                        new ClientThread().execute("ok");
                    }
                } finally {
                    groupReportsByDate(mstartDate, mendDate, foodtype, date_flag);
                    Pcut();
                    bConnect = false;
                }
            } else {
                bConnect = true;
            }
        } else {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                // Device does not support Bluetooth
            }
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
            final SharedPreferences mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

            String restoredText = mSharedPreference.getString("btPrinterid", null);
            if (restoredText != null) {
                btPrinterid = mSharedPreference.getString("btPrinterid", null);
                device = mBluetoothAdapter
                        .getRemoteDevice(btPrinterid);
                if (false == MainActivity.getInstance().isWifiPrinterEnable) {
                    deviceconnection();
                } else {
                    groupReportsByDate(mstartDate, mendDate, foodtype, date_flag);
                    Pcut();
                }
            } else {
                ListPairedDevices();
            }
        }

    }

    protected void onResume() {
        super.onResume();
        if (MainActivity.getInstance() == null || user_Login.user_LoginID == null) {
            Intent userLoginIntent = new Intent(); //Created new Intent to
            userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
            userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(userLoginIntent);
            this.finish();
        }
    }

    public void groupReportsByDate(String astartDate, String aendDate, String foodtype, boolean dateFlag) {
        String startDate = reverseDateFormat(astartDate);
        String endDate = reverseDateFormat(aendDate);
        headerDetails header = new headerDetails();
        String newHeader = "EmbDes";
        String newName;
        ArrayList<GroupData> groupdetails = new ArrayList<>();
        SQLiteDatabase db;
        db = openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
        Cursor shopDetails = db.rawQuery("SELECT terminalname FROM masterterminal;", null);
        int shopCount = shopDetails.getCount();
        if (shopCount > 0) {
            shopDetails.moveToFirst();
            header.title = shopDetails.getString(shopDetails.getColumnIndex("terminalname"));//copying shop name into the struct
            header.firstDate = startDate;//copying Starting date into the strut
            header.lastDate = endDate;    //copying Ending date into the struct
        } else {
            db.close();
            Toast.makeText(PrintReportsActivity.this, "No Data in master_terminal", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(PrintReportsActivity.this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return;
        }
        Cursor groupDetails;
        if (foodtype.equalsIgnoreCase("all")) {
            groupDetails = db.rawQuery("SELECT DISTINCT groupname,groupid from grouptable;", null);
        } else
            groupDetails = db.rawQuery("SELECT DISTINCT groupname,groupid from grouptable WHERE groupname='" + foodtype + "';", null);
        int productCount = groupDetails.getCount();
        if (productCount > 0) {
            int i = 0;
            groupDetails.moveToPrevious();
            while (groupDetails.moveToNext()) {

                newName = groupDetails.getString(0);//group name
                Cursor cartGroupDetails;
                if (!dateFlag) {
                    cartGroupDetails = db.rawQuery("SELECT pgroup, productqty, productprice,Sum(productqty) FROM cart WHERE pgroup='" + newName + "' GROUP BY pgroup;", null);
                } else
                    cartGroupDetails = db.rawQuery("SELECT pgroup, productqty, productprice,Sum(productqty) FROM cart WHERE pgroup='" + newName + "' AND datetime >='" + startDate + "' AND datetime <= '" + endDate + "' GROUP BY pgroup;", null);
                int cartGroupDetailsCount = cartGroupDetails.getCount();

                if (cartGroupDetailsCount > 0) {
                    cartGroupDetails.moveToPrevious();
                    while (cartGroupDetails.moveToNext()) {
                        GroupData group = new GroupData();
                        newName = groupDetails.getString(groupDetails.getColumnIndex("groupname"));//copying all group data into struct
                        group.setName(newName);

                        Cursor categoryDetails = db.rawQuery("SELECT DISTINCT categoryname,categoryid from category WHERE groupid='" + groupDetails.getString(1) + "';", null);
                        int categoryCount = categoryDetails.getCount();

                        float totalCatValue = 0;
                        if (categoryCount > 0) {
                            categoryDetails.moveToPrevious();
                            String catname = "";
                            ArrayList<GroupData> categorydetails = new ArrayList<>();
                            while (categoryDetails.moveToNext()) {
                                GroupData category = new GroupData();

                                catname = categoryDetails.getString(0);//copying all group data into struct//item name
                                category.setName(catname);

                                String cat_id = categoryDetails.getString(1);
                                Cursor cartProductDetails;
                                if (!dateFlag) {
                                    cartProductDetails = db.rawQuery("SELECT pcategory, productqty, productprice,Sum(productqty),productname FROM cart WHERE pcategory='" + catname + "' GROUP BY pcategory;", null);
                                } else
                                    cartProductDetails = db.rawQuery("SELECT pcategory, productqty, productprice,Sum(productqty),productname FROM cart WHERE pcategory='" + catname + "' AND datetime >='" + startDate + "' AND datetime <= '" + endDate + "' GROUP BY pcategory;", null);
                                ArrayList<GroupData> productdetails = new ArrayList<>();
                                float totalprodValue = 0;
                                int cartProductCount = cartProductDetails.getCount();
                                if (cartProductCount > 0) {
                                    cartProductDetails.moveToPrevious();
                                    while (cartProductDetails.moveToNext()) {
                                        GroupData product = new GroupData();
                                        Float newValue = cartProductDetails.getInt(3) * cartProductDetails.getFloat(2);//total qty * price

                                        product.setValue(newValue);
                                        product.setName(cartProductDetails.getString(4));

                                        productdetails.add(product);
                                        totalprodValue += newValue;
                                    }
                                }
                                category.setChilddata(productdetails);
                                totalCatValue += totalprodValue;
                                category.setValue(totalprodValue);
                                categorydetails.add(category);
                            }
                            group.setChilddata(categorydetails);
                            group.setValue(totalCatValue);
                            groupdetails.add(group);
                            Log.e("category_size", "=" + categorydetails.size());
                        }
                    }
                }
            }
            Log.e("group_size", "=" + groupdetails.size());
        } else {
            db.close();
            Toast.makeText(PrintReportsActivity.this, "No Group Data In Cart", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(PrintReportsActivity.this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return;
        }
        db.close();
        printFormat(header, groupdetails, 1);
    }

    /**
     * @param header
     * @param FormatFlag
     * @return void
     * @brief Reports::printFormat here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     */

    public void printFormat(headerDetails header, ArrayList<GroupData> GroupDetails, int FormatFlag) {
        String tempBuffer = null;
        float sumOfValue = 0;
        float sumOfPercent = 0;

        String cut;

        tempBuffer = String.format("%s\n", header.title);
        tempBuffer += String.format("Full Report\n");
        tempBuffer += String.format("From: %s\nTo: %s\n", header.firstDate.substring(8, 10) +
                "-" + header.firstDate.substring(5, 7) + "-" + header.firstDate.substring(0, 4) +
                header.firstDate.substring(10, 16), header.lastDate.substring(8, 10) +
                "-" + header.lastDate.substring(5, 7) + "-" + header.lastDate.substring(0, 4) +
                header.lastDate.substring(10, 16));

        for (int i = 0; i < GroupDetails.size(); i++) {//TODO group
            GroupData group = GroupDetails.get(i);
            //create group buffer
            //Copying Start & End dates into Buffer

            tempBuffer += String.format("Group: %s\n", group.getName());
            tempBuffer += String.format("Categorie       Value  Percent\n");

            //create category data buffer
            LinearLayout mLayout = (LinearLayout) findViewById(R.id.linearcategory);
            LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View mTableHeader = inflater.inflate(R.layout.loginspinner, null);
            TextView textcategory1 = (TextView) mTableHeader.findViewById(R.id.login_spinner);
            textcategory1.setTextSize(16);
            textcategory1.setText(tempBuffer);
            mLayout.addView(mTableHeader);
            for (int j = 0; j < group.getChilddata().size(); j++) {//TODO category

                View mTableHeader1 = inflater.inflate(R.layout.loginspinner, null);
                TextView textcategory = (TextView) mTableHeader1.findViewById(R.id.login_spinner);
                textcategory.setTextSize(14);
                GroupData category = group.getChilddata().get(j);

                if (category.getName().length() > 13) {
                    cut = category.getName().substring(0, 13);
                } else {
                    cut = category.getName();
                }
                Float totalSum = 0f;
                for (int k = 0; k < category.getChilddata().size(); k++) {//TODO product
                    totalSum += category.getChilddata().get(k).getValue();
                }

                tempBuffer += String.format("%-13s%10.2f%7.2f\n", cut, totalSum,
                        (category.getValue() * 100) / totalSum);
                sumOfValue = sumOfValue + category.getValue();
                sumOfPercent = sumOfPercent + ((category.getValue() * 100) / totalSum);


                textcategory.setText(String.format("%-13s%10.2f%7.2f", cut, totalSum, (category.getValue() * 100) / totalSum));
                mLayout.addView(mTableHeader1);

            }
            tempBuffer += "\n";
        }

        Log.e("tempBuffer", "" + tempBuffer);

        //print Category data
        if (true == MainActivity.getInstance().isWifiPrinterEnable) {
            Begin();
            write((tempBuffer).getBytes());
            write(("\r\n").getBytes());
        } else {
            printdata = tempBuffer;
            Begin();
            write((tempBuffer + "\n").getBytes());
        }

        return;
    }


    /**
     * @brief Reports::productPrintFormat here we are passing staring data and ending date to
     * get the reports according to the product sold beteween that dates.
     * @return void
     */


    public class ClientThread extends AsyncTask<String, Void, Boolean> {

		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isconnected = false;
        ProgressDialog pDialog;

        public void onPreExecute() {
            isconnected = false;
            pDialog = new ProgressDialog(PrintReportsActivity.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            isconnected = false;
            pDialog.show();
        }

        public Boolean doInBackground(final String... args) {
            try {
                mysocket = new Socket();
                mysocket.connect(new InetSocketAddress(SERVER_IP, SERVERPORT), 10000);
                mmOutStream = mysocket.getOutputStream();
                mmInStream = mysocket.getInputStream();
                isconnected = true;
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
                isconnected = false;
            } catch (IOException e1) {
                e1.printStackTrace();
                isconnected = false;
            }

            return false;
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }
            if (isconnected == true) {

            } else {
                if (mysocket != null) {
                    try {
                        mysocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mysocket = null;
                }
                Toast.makeText(getApplicationContext(), "Selected WIFi Printer is not online", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Here we are creating instance for class
     *
     * @return
     */

    public static Reports getInstance() {
        return REPORTSID;
    }

    @SuppressLint("NewApi")
    public synchronized void connect(BluetoothDevice device) throws IOException {

        BluetoothSocket tmp = null;
        // Get a BluetoothSocket to connect with the given BluetoothDevice
        try {
            Method m;
            m = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});
            tmp = (BluetoothSocket) m.invoke(device, 1);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(PrintReportsActivity.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(PrintReportsActivity.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(PrintReportsActivity.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            e.printStackTrace();
            Toast.makeText(PrintReportsActivity.this, "connect method IllegalArgumentException" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
            startActivity(mIntent);
        }
        mmSocket = tmp;
        mBluetoothAdapter.cancelDiscovery();
        new Connection().execute("ok");
    }

    public class Connection extends AsyncTask<String, Void, Boolean> {

		/* progress dialog to show user that the backup is processing. */
        /**
         * application context.
         */
        boolean isconnected = false;
        ProgressDialog pDialog;

        public void onPreExecute() {
            pDialog = new ProgressDialog(PrintReportsActivity.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            isconnected = false;
            pDialog.show();
        }

        public Boolean doInBackground(final String... args) {
            try {
                mmSocket.connect();
                mmInStream = mmSocket.getInputStream();
                mmOutStream = mmSocket.getOutputStream();
                isconnected = true;
            } catch (IOException e) {
                e.printStackTrace();
                isconnected = false;
            }

            return false;
        }

        @SuppressLint("NewApi")
        @Override
        public void onPostExecute(final Boolean success) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }
            if (mReportsFlag == 4) {
                groupReportsByDate(mstartDate, mendDate, foodtype, date_flag);
                Pcut();
            }
            if (isconnected == true) {
                if (mReportsFlag == 4) {
                } else {
                    Begin();
                    write((printdata + "\n").getBytes());
                    Pcut();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Selected Printer is not Online", Toast.LENGTH_LONG).show();
            }
            try {
                mmSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void ListPairedDevices() {

        Set<BluetoothDevice> bondedDevices = BluetoothAdapter
                .getDefaultAdapter().getBondedDevices();
        for (int k = 0; k < bondedDevices.size(); k++) {
        }
        if (bondedDevices.size() > 0) {
            for (BluetoothDevice bondDeice : bondedDevices) {
                connections.add(bondDeice);
                connectionsname.add(bondDeice.getName());
                printerDeviceCheck = true;

            }
        }
        if (printerDeviceCheck == true) {
            printerDeviceCheck = false;
            Device_Select();
        } else {
            Toast.makeText(getApplicationContext(), "Printer not found", Toast.LENGTH_SHORT).show();
        }

    }

    private void Device_Select() {
        settingDialog = new Dialog(PrintReportsActivity.this);
        settingDialog.setContentView(R.layout.fragment_scanner_device_selection);
        settingDialog.setTitle("Paired Device");
        settingDialog.setCancelable(true);
        lv = (ListView) settingDialog.findViewById(R.id.list);
        settingDialog.show();
        adapter = new ArrayAdapter<BluetoothDevice>(this, android.R.layout.simple_list_item_1, connections);
        adaptername = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, connectionsname);
        lv.setAdapter(adaptername);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                try {
                    globledevice = adapter.getItem(arg2).getAddress();
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("btPrinterid", globledevice);
                    editor.commit();
                    device = mBluetoothAdapter
                            .getRemoteDevice(globledevice);
                    if (mReportsFlag == 4) {
                        if (false == MainActivity.getInstance().isWifiPrinterEnable) {
                            deviceconnection();

                        } else {
                            groupReportsByDate(mstartDate, mendDate, foodtype, date_flag);
                            Pcut();
                        }
                    } else if (mReportsFlag == 5) {
                        setTitle("				PRODUCT REPORTS		");
                        groupReportsByDate(mstartDate, mendDate, foodtype, date_flag);
                    }
//
                } catch (RuntimeException e) {
                    Intent mIntent = new Intent();
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.setClass(getApplicationContext(), Home.class);
                    startActivity(mIntent);
                    Toast.makeText(PrintReportsActivity.this, " insert run time exception" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                settingDialog.dismiss();

            }
        });
    }

    private void deviceconnection() {

        try {
            connect(device);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void Begin() {
        WakeUpPritner();
        InitPrinter();
    }

    public static void InitPrinter() {
        byte[] combyte = new byte[]{(byte) 27, (byte) 64};

        // BT_Write(combyte);
        try {
            if (mmOutStream != null) {
                mmOutStream.write(combyte);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void WakeUpPritner() {
        byte[] b = new byte[3];

        try {
            if (mmOutStream != null) {
                mmOutStream.write(b);
            }
            Thread.sleep(100L);
        } catch (Exception var2) {
            var2.printStackTrace();
        }
    }

    public static void Pcut() {
        byte[] pcut = new byte[]{(byte) 0x1D, (byte) 0x56, (byte) 0x42, (byte) 0x00};

        // BT_Write(combyte);
        try {
            if (mmOutStream != null) {
                mmOutStream.write(pcut);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* Call this from the main activity to send data to the remote device */
    public void write(byte[] bytes) {
        try {
            byte[] cAlign = {(byte) 0x1b, (byte) 0x61, (byte) 0x01};
            if (mmOutStream != null) {
                mmOutStream.write(cAlign);
                mmOutStream.write(bytes);
            }
        } catch (IOException e) {
            Toast.makeText(this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
            Intent mIntent = new Intent();
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.setClass(getApplicationContext(), Home.class);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
        return super.dispatchTouchEvent(ev);
    }

    public String reverseDateFormat(String date) {
        //return date;
        return date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2) + date.substring(10, 16);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (settingDialog != null) {
            if (settingDialog.isShowing()) {
                settingDialog.dismiss();
            }
            settingDialog = null;

        }
        if (mysocket != null) {
            try {
                mysocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mysocket = null;
        }
    }
}

package com.embdes.youcloud_resto;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.lucasr.twowayview.TwoWayView;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import static com.embdes.youcloud_resto.GroupStaticFragment.group_clicked;


/**
 * A simple {@link Fragment} subclass.
 * From this fragment it will go to group ACTIVITY it will display product fragment
 */
public class CategoryFragment extends Fragment {
    SQLiteDatabase db;
    Bitmap mBitmapcat;
    String groupId;
    ArrayList<Imagepojo> cartarray;
    static CategoryFragment CATEGORYID;
    MyAdapter adapter;
    ProductFragment productFragment, cat1Frag;
    android.app.FragmentManager fragmentManager;
    android.app.FragmentTransaction fragmentTransaction;
    File file;
    File[] allFiles;
    Random r;

    static class ViewHolder {
        TextView itemName;
        ImageView itemImage;
    }

    public class MyAdapter extends BaseAdapter {

        // Declare variables
        private Activity activity;

        @SuppressWarnings("unused")
        private LayoutInflater inflater = null;

        public MyAdapter(Activity a) {
            activity = a;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        public int getCount() {
            return cartarray.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            ViewHolder holder = null;

            if (vi == null) {
                vi = inflater.inflate(R.layout.grid_viewitem1, parent, false);
                holder = new ViewHolder();
                holder.itemName = (TextView) vi.findViewById(R.id.text);
                holder.itemImage = (ImageView) vi.findViewById(R.id.image);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }
            holder.itemName.setText(cartarray.get(position).getPaymnet());
            File imgFile = new File(Environment.getExternalStorageDirectory().getPath() + "/pos/img/" + cartarray.get(position).getImageur());
            if (imgFile.exists()) {
                mBitmapcat = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.itemImage.setImageBitmap(mBitmapcat);
            } else {
                holder.itemImage.setImageResource(R.drawable.you_cloud);
            }
//            if (position == 0) {
//                String categoryId = "-1";
//                Log.e("CategoryFragment", "=" + group_clicked);
//                if (!group_clicked) {
//                    db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
//                    Cursor c = db.rawQuery("SELECT categoryid from category WHERE categoryname ='"
//                            + cartarray.get(position).getPaymnet() + "';", null);
//                    c.moveToFirst();
//                    categoryId = c.getString(c.getColumnIndex("categoryid"));
//                    db.close();
//                }
//                if (!productFragment.isResumed()) {
//                    fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.container3, productFragment);
//                    fragmentTransaction.commit();
//                    Bundle bundle1 = new Bundle();
//                    bundle1.putString("searchFlag", "1");
//                    bundle1.putString("SearchText", categoryId);
//                    productFragment.setArguments(bundle1);
//                } else {
//                    fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.container3, cat1Frag);
//                    fragmentTransaction.commit();
//                    Bundle bundle1 = new Bundle();
//                    bundle1.putString("searchFlag", "1");
//                    bundle1.putString("SearchText", categoryId);
//                    cat1Frag.setArguments(bundle1);
//                }
//            }

            return vi;
        }
    }

    public CategoryFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        CATEGORYID = this;
        productFragment = new ProductFragment();
        cat1Frag = new ProductFragment();
        cartarray = new ArrayList<Imagepojo>();
        fragmentManager = getFragmentManager();
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_LONG)
                    .show();
        } else {
            file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "/pos/img");
            allFiles = file.listFiles();
            if (allFiles.length == 0) {
                Toast.makeText(getActivity(), "FOLDER IS EMPTY", Toast.LENGTH_LONG).show();
            }
        }

        final TwoWayView categoryList = (TwoWayView) view.findViewById(R.id.gridview3);
        Bundle bundle = getArguments();
        groupId = bundle.getString("SearchText");
        db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * from category WHERE groupid =" + groupId + ";", null);
        int count = c.getCount();
        if (count > 0) {
            while (c.moveToNext()) {
                Imagepojo imagepojo = new Imagepojo();
                String product = "";
                String image = "";
                product = c.getString(c.getColumnIndex("categoryname"));
                image = c.getString(c.getColumnIndex("imageurl"));
                String[] path = image.split("\\/");
                String val = path[path.length - 1];
                imagepojo.setPaymnet(product);
                imagepojo.setImageur(val);
                cartarray.add(imagepojo);
            }
        }
        db.close();
        adapter = new MyAdapter(getActivity());
        categoryList.setAdapter(adapter);
        categoryList.setItemMargin(10);
        categoryList.setSelector(getResources().getDrawable(R.drawable.item_selector));
        adapter.notifyDataSetChanged();
        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                db = getActivity().openOrCreateDatabase("rebelPOS.db", Context.MODE_PRIVATE, null);
                Cursor c = db.rawQuery("SELECT categoryid from category WHERE categoryname ='"
                        + cartarray.get(arg2).getPaymnet()/*categoryList.getAdapter().getItem(arg2).toString()*/ + "';", null);
                c.moveToFirst();
                String categoryId = c.getString(c.getColumnIndex("categoryid"));
                db.close();
                if (arg2 > -1) {
                    if (!productFragment.isResumed()) {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container3, productFragment);
                        fragmentTransaction.commit();
                        Bundle bundle1 = new Bundle();
                        bundle1.putString("searchFlag", "1");
                        bundle1.putString("SearchText", categoryId);
                        productFragment.setArguments(bundle1);
                    } else {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container3, cat1Frag);
                        fragmentTransaction.commit();
                        Bundle bundle1 = new Bundle();
                        bundle1.putString("searchFlag", "1");
                        bundle1.putString("SearchText", categoryId);
                        cat1Frag.setArguments(bundle1);
                    }
                } else {
                }


            }
        });
        return view;
    }

}

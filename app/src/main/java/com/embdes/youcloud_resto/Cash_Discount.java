package com.embdes.youcloud_resto;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.instabug.library.InstabugTrackingDelegate;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author EMBDES.
 * @version 1.0.
 * This Activity contains voucher, discount and cash operations.
 * 
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Cash_Discount extends Activity {

	SQLiteDatabase db;
	String tableno;
	float grandTotal;
	public float cash;
	static Cash_Discount CASH_DISCOUNTID;
	EditText TableNo;
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		CASH_DISCOUNTID = this;
		cash = 0;
		setContentView(R.layout.custom_type);
		TextView tableName = (TextView) findViewById(R.id.coversView);
		TableNo =(EditText)findViewById(R.id.covers);
		LinearLayout tableLayout = (LinearLayout) findViewById(R.id.taglelinearLayout);
		ImageButton cancelButton = (ImageButton)findViewById(R.id.typecancel);
		TableNo.setInputType(InputType.TYPE_CLASS_NUMBER);
		int maxLength = 11; 
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(maxLength);
		TableNo.setFilters(FilterArray);
		cancelButton.setOnClickListener(cancelhandler);		//Creating onclick listener for home button.
		tableLayout.setVisibility(View.INVISIBLE);
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		int key = bundle.getInt("KEY");
		tableno = bundle.getString("TABLE");
		grandTotal = Group.getInstance().subTotalAmount;
		System.out.println("Intent argumentId :"+key + tableno + grandTotal);
		if(key == 1)
		{
		}
		else if(key == 2)
		{
			tableName.setText("Amount");
			cashPay();
		}
		else if(key == 3)
		{
			tableName.setText("Voucher No     ");
			ImageButton okButton = (ImageButton)findViewById(R.id.typeok);
			okButton.setOnClickListener(voucherhandler);	//Creating on click listener for home button.
		}
		else if(key == 4)
		{
			tableName.setText("Discount   ");
			discount();
		}
	}
	protected void onResume() {
		super.onResume();
		if(MainActivity.getInstance() ==null || user_Login.user_LoginID==null){
			Intent userLoginIntent = new Intent(); //Created new Intent to 
			userLoginIntent.setClass(getApplicationContext(), MainActivity.class);
			userLoginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(userLoginIntent);
			this.finish();
		}
		
	};

	/** 
	 * This cashPay get called when user selects cash option in payment screen
	 * @param
	 * @return void
	 * 
	 */
	public void cashPay() {
		ImageButton okButton = (ImageButton)findViewById(R.id.typeok);
		String grand_Amount =String.format("%.2f", SubMenu.getInstance().grandTotal);
		TableNo.setText(grand_Amount);
		okButton.setOnClickListener(cahpayhandler);
		return;
	}

	/**
	 * This Discount get called when user selects discount option in payment screen
	 * @param
	 * @return void
	 * 
	 */
	public void discount()
	{
		EditText discountEdit = (EditText) findViewById(R.id.covers);
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(2);
		discountEdit.setFilters(FilterArray);
		ImageButton okButton = (ImageButton)findViewById(R.id.typeok);
		okButton.setOnClickListener(discounthandler);		//Creating onclick listener for home button.
	}

	View.OnClickListener  voucherhandler = new View.OnClickListener() {
		@SuppressLint("NewApi")
		public void onClick(View v) {  
			EditText voucherId =(EditText)findViewById(R.id.covers);
			Group.getInstance().voucherNumber = voucherId.getText().toString();
			if(voucherId.getText().length() > 0) {
				if(voucherId.getText().toString().equals("0")) {
					Group.getInstance().voucherAmount = 0;
					Intent intent = new Intent(Cash_Discount.this, SubMenu.class);
					intent.putExtra("TABLE", tableno);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					
				}else {
			
					db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
					String timeStamp = new SimpleDateFormat("yy/MM/dd").format(Calendar.getInstance().getTime());
					Cursor voucher = db.rawQuery("SELECT vouchervalues FROM voucher WHERE voucherid = "+ voucherId.getText()+
							" AND usedflag = 0 AND  expdate >= "+timeStamp+";", null);
					int voucherCount = voucher.getCount();				
					if(voucherCount > 0)
					{
						voucher.moveToFirst();	
						Group.getInstance().voucherAmount = voucher.getFloat(0);
						if(Group.getInstance().voucherAmount > SubMenu.getInstance().grandTotal)
						{
							AlertDialog.Builder adb = new AlertDialog.Builder(
									Cash_Discount.this, android.R.style.Theme_Dialog);
							adb.setTitle("");
							adb.setMessage("Voucher Value Should Not Exceed Amount To Pay\n" +
									"Voucher Amount : "+ Group.getInstance().voucherAmount );
							adb.setPositiveButton("Ok",null);
							adb.show();
							voucherId.getText().clear();
							Group.getInstance().voucherAmount = 0;
						}
						else
						{
							grandTotal = (grandTotal - Group.getInstance().voucherAmount);
							db.close();
							Intent intent = new Intent(Cash_Discount.this, SubMenu.class);
							intent.putExtra("TABLE", tableno);
							intent.putExtra("TAXFLAG", 0);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
						}

					}
					else
					{
						db.close();
						AlertDialog.Builder adb = new AlertDialog.Builder(
								Cash_Discount.this,  android.R.style.Theme_Dialog);
						adb.setTitle("");
						adb.setMessage("Voucher is Not Available");
						adb.setPositiveButton("Ok", new 
								OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								Intent intent = new Intent(Cash_Discount.this, SubMenu.class);
								intent.putExtra("TABLE", tableno);
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(intent);
								Group.getInstance().voucherNumber= "0";
							}
						});
						adb.show();
					}
				}
			}
		}
	};

	View.OnClickListener  discounthandler = new View.OnClickListener() {
		@SuppressLint("NewApi")
		public void onClick(View v) {  
			EditText TableNo =(EditText)findViewById(R.id.covers);
			float oldDiscount = Group.getInstance().discountAmount;
			float discount = Float.parseFloat(TableNo.getText().toString());
			Group.getInstance().discountAmount = (discount* Group.getInstance().totalAmount/100);
			if(Group.getInstance().discountAmount > (SubMenu.getInstance().grandTotal+oldDiscount))
			{
				AlertDialog.Builder adb = new AlertDialog.Builder(
						Cash_Discount.this,  android.R.style.Theme_Dialog);
				adb.setTitle("");
				adb.setMessage("Discount Value Should Not Exceed Amount To Pay\n" +
						"Discount Amount : "+ Group.getInstance().discountAmount);
				adb.setPositiveButton("Ok",null);
				adb.show();
				TableNo.getText().clear();
			} else{
				Intent intent = new Intent(Cash_Discount.this, SubMenu.class);
				intent.putExtra("TAXFLAG", 0);
				intent.putExtra("TABLE", tableno);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		}
	};

	View.OnClickListener cahpayhandler = new View.OnClickListener() {
		@SuppressLint("NewApi")
		public void onClick(View v) {  
			TableNo =(EditText)findViewById(R.id.covers);
			TableNo.setInputType(InputType.TYPE_CLASS_NUMBER);
			if(TableNo.getText().length() > 0)
			{
				String grand_Amount =String.format("%.2f", SubMenu.getInstance().grandTotal);
				SubMenu.getInstance().grandTotal=Float.parseFloat(grand_Amount);
				cash = Float.parseFloat(TableNo.getText().toString());
				
				if(cash >= SubMenu.getInstance().grandTotal)
				{
					db = openOrCreateDatabase("rebelPOS.db", MODE_PRIVATE, null);
					String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
					String datetime = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
					Cursor recieptno =  db.rawQuery("SELECT * FROM receiptnos WHERE receipthead = '"+datetime+"' ;", null);
					if(recieptno.getCount() == 0)
					{
						if(cash > 0)
						{
							db.execSQL("INSERT INTO receiptnos VALUES('"+datetime+"',1);");
							db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
									"username,orderid,checkflag) VALUES('R"+datetime+ "1','CASH', '0',"+SubMenu.getInstance().grandTotal+" ,'"+timeStamp+"','"+ MainActivity.getInstance().USERNAME+"','"+ Group.getInstance().ORDERNO +"',0 );");
						}

						if(!Group.getInstance().voucherNumber.equals("0"))
						{
							db.execSQL("UPDATE voucher SET usedflag = 1 WHERE voucherid = "+ Group.getInstance().voucherNumber+";");
							db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
									"username,orderid,checkflag) VALUES('"+ Group.getInstance().voucherNumber+"','VOUCHER', '"+ Group.getInstance().voucherNumber+"',"+
									Group.getInstance().voucherAmount+" ,'"+timeStamp+"','"+ MainActivity.getInstance().USERNAME+"','"+ Group.getInstance().ORDERNO +"',0 );");
						}
					}
					else
					{	
						System.out.println("*** Voucher Number Else Cash*** "+ Group.getInstance().voucherNumber + cash+ TableNo.getText().toString());
						if(cash > 0)
						{
							recieptno.moveToLast();
							String newReceiptNo = "R" +recieptno.getString(0)+ (recieptno.getInt(1)+(int)1);
							db.execSQL("UPDATE receiptnos SET receiptnumber = receiptnumber+1 WHERE receipthead= '"+
									recieptno.getString(0)+"';");
							db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
									"username,orderid,checkflag) VALUES('"+newReceiptNo+"','CASH', '0',"+SubMenu.getInstance().grandTotal+" ,'"+timeStamp+"','"+ MainActivity.getInstance().USERNAME+"','"+ Group.getInstance().ORDERNO +"',0 );");

						}
						if(!Group.getInstance().voucherNumber.equals("0"))
						{
							db.execSQL("UPDATE voucher SET usedflag = 1 WHERE voucherid = "+ Group.getInstance().voucherNumber+";");
							db.execSQL("INSERT INTO receipt(receiptno ,docname ,docid ,amount,datetime, " +
									"username,orderid,checkflag) VALUES('"+ Group.getInstance().voucherNumber+"','VOUCHER', '"+ Group.getInstance().voucherNumber+"',"+
									Group.getInstance().voucherAmount+" ,'"+timeStamp+"','"+ MainActivity.getInstance().USERNAME+"','"+ Group.getInstance().ORDERNO +"',0 );");
						}
					}
					db.execSQL("UPDATE cart SET discount = "+ Group.getInstance().discountAmount+
							" WHERE locationid="+ user_Login.user_LoginID.LOCATIONNO+" AND tableno="+tableno+" AND billflag = 0 AND orderflag = 1;");
					db.close();
					Intent intent = new Intent(Cash_Discount.this, Printer.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("TABLENO", Integer.parseInt(tableno));
					intent.putExtra("LOCATIONNO", Integer.parseInt(user_Login.user_LoginID.LOCATIONNO));
					intent.putExtra("FLAG", 1);
					startActivity(intent);
				}
				else
				{
					AlertDialog.Builder adb = new AlertDialog.Builder(
							Cash_Discount.this,  android.R.style.Theme_Dialog);
					adb.setTitle("");
					adb.setMessage("Amount should not be less than "+SubMenu.getInstance().grandTotal );
					adb.setPositiveButton("Ok", null);
					adb.show();
				}
			}
		}
	};
	View.OnClickListener  cancelhandler = new View.OnClickListener() {
		@SuppressLint("NewApi")
		public void onClick(View v) {  
			System.out.print("*** onBackPressed() ***");
			onBackPressed();

		}
	};

	public static Cash_Discount getInstance(){
		return CASH_DISCOUNTID;
	}
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		InstabugTrackingDelegate.notifyActivityGotTouchEvent(ev, this);
		return super.dispatchTouchEvent(ev);
	}
}
